<@override name="view_body">

<div class="page-content" id="view_content" data-view="syslog">
    <div class="page-head">
        <div class="page-title">
            <h1>系统日志 <small>系统日志</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">系统日志</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">系统日志</span>
                        <span class="caption-helper">系统日志列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "" for="s_date">日期</label>
                            <input type="text" class="form-control input-medium" id="s_date" name="s_date" readonly>
                            <label class="" for="s_name">操作人:</label>
                            <input type="text" class="form-control" id="operator_name" name="operator_name" placeholder="请输入操作人姓名">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="syslog_dt"
                                   data-ajax="${ctx}/syslog/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width100">模块名称</th>
                                    <th>日志内容</th>
                                    <th class="width100">操作时间</th>
                                    <th class="width100">操作人</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
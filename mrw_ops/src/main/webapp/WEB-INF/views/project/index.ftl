<@override name="view_body">

<div class="page-content" id="view_content" data-view="project">
    <div class="page-head">
        <div class="page-title">
            <h1>理财项目 <small>理财项目管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">理财项目</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财项目</span>
                        <span class="caption-helper">理财项目列表</span>
                    </div>
                    <#--<div class="actions">-->
                        <#--<div class="btn-group">-->
                            <#--<a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">-->
                                <#--<i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>-->
                            <#--</a>-->
                            <#--<ul class="dropdown-menu pull-right">-->
                                <#--<li><a href="#"> 导出Excel </a>-->
                                <#--</li>-->
                            <#--</ul>-->
                        <#--</div>-->
                    <#--</div>-->
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class="" for="s_name">项目名称:</label>
                            <input type="text" class="form-control" id="s_name" placeholder="请输入项目名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-right"><a id="new" href="${ctx}/project/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="projects_dt"
                                   data-ajax="${ctx}/project/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width20"></th>
                                    <th class="width100">项目名称</th>
                                    <th class="width80">项目图标</th>
                                    <th class="width80">项目类型</th>
                                    <th class="width200">项目介绍</th>
                                    <th class="width60">产品数量</th>
                                    <th class="width45">状态</th>
                                    <th class="width200">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
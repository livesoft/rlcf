<@override name="view_body">

<div class="page-content" id="view_content" data-view="points">
    <div class="page-head">
        <div class="page-title">
            <h1>积分规则
                <small>积分规则设置</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">积分规则设置</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-purple-plum">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 签到赠送</span>
                        <span class="caption-helper">签到赠送积分设置</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="${ctx}/points/save" class="form-horizontal validate-form ajax-form" method="post">
                            <input type="hidden" name="points.code" value="sign">
                            <input type="hidden" name="points.name" value="签到积分">
                            <input type="hidden" name="points.id"   value="${(code_map.sign.id)!}">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">基础积分</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required" name="points.integral" value="${(code_map.sign.integral)!}"
                                                   id="sign_integral" placeholder="" >
                                            <span class="input-group-addon"><i class="fa">积分</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="code"></label>
                                    <div class="col-md-9 checkbox-list icheck">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cell_phone" name="points.status" value="1"
                                                   <#if code_map.sign.status?? && code_map.sign.status==1>checked="checked" </#if>>
                                            启用
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn green">保存</button>
                                <button type="reset"  class="btn default">重置</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 消费赠送</span>
                        <span class="caption-helper">消费赠送积分设置</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="${ctx}/points/save" class="form-horizontal validate-form ajax-form" method="post">
                            <input type="hidden" name="points.code" value="cost">
                            <input type="hidden" name="points.name" value="消费积分">
                            <input type="hidden" name="points.id" value="${(code_map.cost.id)!}">

                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                    <input type="number" min="1" class="form-control" name="points.ratio_val" value="${(code_map.cost.ratio_val)!1}"
                                                           id="cost.ratio_val" placeholder="" required>
                                                    <span class="input-group-addon"><i class="fa">元</i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="input-group">
                                                    =
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                    <input type="number" min="0" class="form-control" name="points.integral" value="${(code_map.cost.integral)!}"
                                                           id="cost_integral" placeholder="" required>
                                                    <span class="input-group-addon"><i class="fa">积分</i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5" for="code"></label>
                                    <div class="col-md-9 checkbox-list">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cell_phone" name="points.status" value="1"
                                                   <#if code_map.cost.status?? && code_map.cost.status==1>checked="checked" </#if>>
                                            启用
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn green">保存</button>
                                <button type="reset" class="btn default">重置</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-purple-plum">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 邀请赠送</span>
                        <span class="caption-helper">邀请赠送积分设置</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="${ctx}/points/save" class="form-horizontal validate-form ajax-form" method="post">
                            <input type="hidden" name="points.code" value="invite">
                            <input type="hidden" name="points.name" value="邀请积分">
                            <input type="hidden" name="points.id"   value="${(code_map.invite.id)!}">

                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-3 control-label">邀请赠送</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="number" min="0" class="form-control" name="points.integral" value="${(code_map.invite.integral)!}"
                                                       id="forward_integral" placeholder="" required>
                                                <span class="input-group-addon"><i class="fa">积分</i></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="code"></label>
                                    <div class="col-md-9 checkbox-list">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cell_phone" name="points.status" value="1"
                                                   <#if code_map.invite.status?? && code_map.invite.status==1>checked="checked" </#if>>
                                            启用
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn green">保存</button>
                                    <button type="reset" class="btn default">重置</button>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>

        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-purple-plum">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 转发赠送</span>
                        <span class="caption-helper">转发赠送积分设置</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="${ctx}/points/save" class="form-horizontal validate-form ajax-form" method="post">
                            <input type="hidden" name="points.code" value="frd">
                            <input type="hidden" name="points.name" value="转发积分">
                            <input type="hidden" name="points.id" value="${(code_map.frd.id)!}">

                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-3 control-label">转发赠送</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="number" min="0" class="form-control" name="points.integral" value="${(code_map.frd.integral)!}"
                                                       id="forward_integral" placeholder="" required>
                                                <span class="input-group-addon"><i class="fa">积分</i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="code"></label>
                                    <div class="col-md-9 checkbox-list">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cell_phone" name="points.status" value="1"
                                                   <#if code_map.frd.status?? && code_map.frd.status==1>checked="checked" </#if>>
                                            启用
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn green">保存</button>
                                    <button type="reset" class="btn default">重置</button>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
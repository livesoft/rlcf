<@override name="view_body">

<div class="page-content" id="view_content" data-view="question#item">
    <div class="page-head">
        <div class="page-title">
            <h1>理财产品管理
                <small>${(action=='create')?string('创建','编辑')}理财产品</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/question">理财产品</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}理财产品</a>
        </li>
    </ul>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">理财产品</span>
                    <span class="caption-helper">${(action=='create')?string('创建','编辑')}理财产品</span>
                </div>
            </div>

            <div class="portlet-body form">
                <form role="form" class="form-horizontal validate-form" action="${ctx}/question/save" method="post"
                      id="question_form">
                    <input type="hidden" name="question.id" value="${(question.id)!}">

                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3" for="project">
                                <span class="required"> * </span> 评估分类
                            </label>

                            <div class="col-md-4">
                                <select data-placeholder="请选择分类" id="project" name="question.category"
                                        class="select2me form-control required">
                                    <option value=""></option>
                                    <#list categories! as cg>
                                        <option value="${cg.id!}" <#if (question.category)?? && question.category == cg.id> selected="selected"</#if>>${cg.name!}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="name">
                                <span class="required"> * </span> 问题标题
                            </label>

                            <div class="col-md-4">
                                <input type="text" id="name" name="question.title" value="${(question.title)!}"
                                       class="form-control required" autocomplete="off">
                            </div>
                        </div>





                        <div class="form-group">
                            <label class="control-label col-md-3" for="sort">
                                <span class="required"> * </span> 显示排序
                            </label>

                            <div class="col-md-4">
                                <div class="spinnerme" data-value="${(question.sort)!'0'}" data-step="1"
                                     data-min="1" data-max="100">
                                    <div class="input-group" style="width:150px;">
                                        <div class="spinner-buttons input-group-btn">
                                            <button type="button" class="btn spinner-up blue">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <input type="number" id="sort" name="question.sort"
                                               class="spinner-input form-control" maxlength="3">

                                        <div class="spinner-buttons input-group-btn">
                                            <button type="button" class="btn spinner-down red">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary blue">保存</button>
                                <a href="${ctx}/question" class="btn btn-default default">取消</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
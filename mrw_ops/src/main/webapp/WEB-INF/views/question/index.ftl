<@override name="view_body">

<div class="page-content" id="view_content" data-view="question">
    <div class="page-head">
        <div class="page-title">
            <h1>风险评估问题 <small>风险评估问题管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">风险评估问题</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">风险评估问题</span>
                        <span class="caption-helper">风险评估问题列表</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="" class="form-horizontal validate-form">
                        <div class="form-body"></div>
                    </form>
                    <!-- END FORM-->
                </div>
                    <a id="new" href="${ctx}/question/create" class="btn blue">
                        <i class="icon-plus mr5"></i>创建</a>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="question_dt"
                                   data-ajax="${ctx}/question/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width180">问题标题</th>
                                    <th class="width40">显示排序</th>
                                    <th class="width100">评估分类</th>
                                    <th class="width100">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
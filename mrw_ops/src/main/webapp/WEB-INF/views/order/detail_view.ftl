<@override name="view_body">

<div class="page-content" id="view_content" data-view="order#">
    <div class="page-head">
        <div class="page-title">
            <h1>
                <small></small>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <#if error??>
                <div class="note note-danger note-shadow">
                        <p> ${error} </p>
                </div>
            </#if>

            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">订单信息</span>
                    </div>
                </div>

                <div class="tab-pane" id="tab_3">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">订单流水号:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.trade_no)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">订单类型:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.order_type_name)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">订单状态:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.status_name)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">交易时间:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.buy_time)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">产品编号:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.product_no)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">产品名称:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.product_name)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">购买会员:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        ${(order.real_name)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">交易金额:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.trade_amount)!} 元
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">手续费:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.fee_amonut)!} 元
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">转让挂单费:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.lodging_amount)!} 元
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->

                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">备注信息:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(order.remark)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-5 col-md-9">
                                            <a href="javascript:history.go(-1);" class="btn btn-default default">返回</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
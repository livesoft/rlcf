<@override name="view_body">

<div class="page-content" id="view_content" data-view="order">
    <div class="page-head">
        <div class="page-title">
            <h1>订单列表
                <small>订单列表管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">订单列表</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-shadow">
                <h4 class="block">订单状态说明</h4>
                <p>1 待处理：用户在浏览产品详情的时候，点击“立即投资”或者“立即购买”按钮，但是未进行立即支付；</p>
                <p>2 待支付：在“待处理”的前提下，用户点击了”立即支付“，并进入收银台；</p>
                <p>3 交易成功：会员通过”收银台“完成支付并成功;</p>
                <p>4 交易失败：会员购买失败；</p>
                <p>5 交易关闭：用户撤销转让后挂单订单状态；</p>
                <p>6 处理中：用户发起的退单；</p>
                <p>7 其他状态（退款中、已删除、已退款、交易完成）为了与银行订单接口状态匹配，目前无意义；</p>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">订单列表</span>
                        <span class="caption-helper">订单列表</span>
                    </div>

                    <@shiro.hasPermission name="order_export">
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">
                                    <i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="${ctx}/order/export"> 导出Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </@shiro.hasPermission>
                </div>
                <div class="portlet-body form">
                    <form action="" class="form-horizontal validate-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="s_order_no">订单流水号:</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="s_order_no"
                                                   placeholder="请输入订单流水号">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">会员:</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="s_member_name"
                                                   placeholder="请输入会员名称">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="s_order_status">订单状态:</label>

                                        <div class="col-md-8">
                                            <select class="select2me form-control" id="s_order_status"
                                                    data-placeholder="订单状态">
                                                <option value=""></option>
                                                <option value="9">待处理</option>
                                                <option value="0">待支付</option>
                                                <option value="1">退款中</option>
                                                <option value="2">处理中</option>
                                                <option value="3">交易成功</option>
                                                <option value="4">交易关闭</option>
                                                <option value="5">已删除</option>
                                                <option value="6">已退款</option>
                                                <option value="7">交易失败</option>
                                                <option value="8">交易完成</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="s_trade_amount_start">订单金额: </label>

                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input type="number" min="0" class="form-control"
                                                       id="s_trade_amount_start">
                                                <span class="input-group-addon">- </span>
                                                <input type="number" min="0" class="form-control"
                                                       id="s_trade_amount_end">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="control-label col-md-4">理财产品:</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="s_product_name"
                                                   placeholder="请输入产品名称">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="s_order_type">订单类型:</label>

                                        <div class="col-md-8">
                                            <select class="select2me form-control" id="s_order_type"
                                                                      data-placeholder="订单类型">
                                            <option value="">全部</option>
                                            <option value="BUY">投资(购买)</option>
                                            <option value="INC">收益</option>
                                            <option value="RPN">退单</option>
                                            <option value="TFR">投资(转让)</option>
                                            <option value="TPY">转让打款</option>
                                            <option value="PFP">募集失败退款</option>
                                            <option value="OFP">交易失败退款</option>
                                            <option value="TPR">转让挂单</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="s_order_time">订单日期:</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="s_order_time" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-offset-11">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                                    class="icon-magnifier mr5"></i>查询</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="order_dt"
                                   data-ajax="${ctx}/order/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width120">订单流水号</th>
                                    <th>产品名称</th>
                                    <th class="width80">会员名称</th>
                                    <th class="width120">订单时间</th>
                                    <th class="width60">订单类型</th>
                                    <th class="width80">交易金额</th>
                                    <th class="width100">手续费/挂单费</th>
                                    <th class="width10">挂单费</th>
                                    <th class="width60">订单状态</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
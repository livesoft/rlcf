<@override name="view_body">

<div class="page-content" id="view_content" data-view="product">
    <div class="page-head">
        <div class="page-title">
            <h1>理财产品
                <small>理财产品管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">理财产品</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财产品</span>
                        <span class="caption-helper">理财产品列表</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">
                                <i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="${ctx}/product/export"> 导出Excel </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="portlet-body ">
                    <div class="well p5 mb5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label p5 col-md-4">产品名称:</label>

                                    <div class="col-md-8 no-p">
                                        <input type="text" class="form-control" id="s_product_name" placeholder="请输入产品名称">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label p5 col-md-4">产品编码:</label>

                                    <div class="col-md-8 no-p">
                                        <input type="text" id="p_code" class="form-control" placeholder="请输入产品编码">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label p5 col-md-4" for="p_type">创建人:</label>
                                    <div class="col-md-8 no-p">
                                        <input type="text" class="form-control" id="s_creater_name" placeholder="请输入创建人名称">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row mt5">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label p5" for="p_yield_start">年利化率: </label>

                                    <div class="col-md-8 no-p">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control" id="p_yield_start">
                                            <span class="input-group-addon">- </span>
                                            <input type="number" min="0" class="form-control" id="p_yield_end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label p5" for="p_due_date_start">起息方式: </label>

                                    <div class="col-md-8 no-p">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control" id="p_due_date_start">
                                            <span class="input-group-addon">- </span>
                                            <input type="number" min="0" class="form-control" id="p_due_date_end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label p5" for="p_price_start">项目本金: </label>

                                    <div class="col-md-8 no-p">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control" id="p_price_start">
                                            <span class="input-group-addon">- </span>
                                            <input type="number" min="0" class="form-control" id="p_price_end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt5">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label p5">起投金额: </label>

                                    <div class="col-md-8 no-p">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control"
                                                   id="p_min_invest_amount_start">
                                            <span class="input-group-addon"> - </span>
                                            <input type="number" min="0" class="form-control"
                                                   id="p_min_invest_amount_end">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label p5">产品状态: </label>

                                    <div class="col-md-8 no-p">
                                        <select class="select2me form-control" id="s_status"
                                                data-placeholder="产品状态">
                                            <option value=""></option>
                                            <option value="0">待发布</option>
                                            <option value="1">销售中</option>
                                            <option value="2">售罄</option>
                                            <option value="3">募集失败</option>
                                            <option value="4">已结束</option>
                                            <option value="6">已删除</option>
                                            <option value="8">已成立</option>
                                            <option value="7">销售结束</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <a id="search_btn" href="javascript:;" class="btn default"><i
                                                class="icon-magnifier mr5"></i>查询</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <@shiro.hasPermission name="product_create">
                         <a id="new" href="${ctx}/product/create" class="btn blue"> <i class="icon-plus mr5"></i>创建</a>
                    </@shiro.hasPermission>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="product_dt"
                                   data-ajax="${ctx}/product/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width120">产品编码</th>
                                    <th>产品名称</th>
                                    <th class="width60">项目本金</th>
                                    <th class="width60">产品期限</th>
                                    <th class="width60">起息方式</th>
                                    <th class="width60"></th>
                                    <th class="width60">风险等级</th>
                                    <th class="width45">进度</th>
                                    <th class="width125">发布时间</th>
                                    <th class="width50">创建人</th>
                                    <th class="width35">状态</th>
                                    <th class="width35">推荐</th>
                                    <th class="width175">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script>
    var product_edit = 0, product_clearing_time = 0, product_recommended = 0, product_intro = 0, product_remove = 0;
    <@shiro.hasPermission name="product_edit">
        product_edit = 1;
    </@shiro.hasPermission>

    <@shiro.hasPermission name="product_clearing_time">
        product_clearing_time = 1;
    </@shiro.hasPermission>

    <@shiro.hasPermission name="product_recommended">
        product_recommended = 1;
    </@shiro.hasPermission>

    <@shiro.hasPermission name="product_intro">
        product_intro = 1;
    </@shiro.hasPermission>

    <@shiro.hasPermission name="product_remove">
        product_remove = 1;
    </@shiro.hasPermission>
</script>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="attr#item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品属性
                <small>${(action=='create')?string('创建','编辑')}产品属性</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/attr">产品属性</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}产品属性</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品属性</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}产品属性</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/attr/save" method="post" id="attr_form">
                        <input type="hidden" name="attr.id" value="${(attr.id)!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 属性名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="name" name="attr.name" value="${(attr.name)!}" maxlength="110"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <#--<div class="form-group">-->
                                <#--<label class="control-label col-md-3" for="code">-->
                                    <#--<span class="required"> * </span> 属性编码-->
                                <#--</label>-->
                                <#--<div class="col-md-4">-->
                                    <#--<input type="text" id="code" name="attr.code" value="${(attr.code)!}"-->
                                           <#--class="form-control" autocomplete="off">-->
                                <#--</div>-->
                            <#--</div>-->
                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_name">
                                    <span class="required"> * </span> 显示名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="display_name" name="attr.display_name" value="${(attr.display_name)!}" maxlength="180"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="data_type">
                                    <span class="required"> * </span> 数据类型
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择数据类型" id="data_type" name="attr.data_type"
                                            class="select2me form-control">
                                        <option value=""></option>
                                        <#list datatypes as dt>
                                            <option value="${dt.code}" <#if attr.data_type?? && attr.data_type ==dt.code>selected="selected" </#if>>${dt.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/attr" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
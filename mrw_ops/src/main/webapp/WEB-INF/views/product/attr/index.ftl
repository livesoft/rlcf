<@override name="view_body">

<div class="page-content" id="view_content" data-view="attr">
    <div class="page-head">
        <div class="page-title">
            <h1>产品属性 <small>产品属性管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品属性</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品属性</span>
                        <span class="caption-helper">产品属性列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">属性名称:</label>
                            <input type="text" class="form-control" id="s_name" placeholder="请输入属性名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-left"><a id="new" href="${ctx}/product/attr/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="attr_dt"
                                   data-ajax="${ctx}/product/attr/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width20"></th>
                                    <th class="width80">名称</th>
                                    <th class="width200">显示名称</th>
                                    <th class="width60">数据类型</th>
                                    <th class="width45">状态</th>
                                    <th class="width90">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@override name="view_scripts">
    <script type="text/javascript">
        g.datatypes = JSON.parse('${datatype_json!'[]'}')
</script>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
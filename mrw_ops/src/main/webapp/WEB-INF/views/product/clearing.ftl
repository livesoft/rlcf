<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#">
    <div class="page-head">
        <div class="page-title">
            <h1>产品结算规则
                <small>产品结算规则</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product">理财产品</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${product_name!}产品结算规则</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">${product_name!}产品结算时间</span>
                        <span class="caption-helper">结算时间</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/clearing_save" method="post" id="c_form">
                        <input type="hidden" name="c.product" value="${c.product!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_cycle">
                                    <span class="required"> * </span> 结算周期
                                </label>

                                <div class="col-md-4">
                                        <select data-placeholder="请选结算周期" id="product" name="c.clearing_cycle"
                                                class="select2me form-control">
                                            <option value="1" <#if c.clearing_cycle?? && c.clearing_cycle == 1>selected </#if>>每月</option>
                                            <option value="2" <#if c.clearing_cycle?? && c.clearing_cycle == 2>selected</#if>>每日</option>
                                            <option value="0" <#if c.clearing_cycle?? && c.clearing_cycle == 0>selected</#if>>到期结算</option>
                                        </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_day">
                                    <span class="required"> * </span> 结算日
                                </label>

                                <div class="col-md-4">
                                    <input type="number" max="31" id="clearing_day" name="c.clearing_day" value="${c.clearing_day!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_time">
                                    <span class="required"> * </span> 结算点
                                </label>

                                <div class="col-md-4">
                                    <input type="number" max="24" id="clearing_time" name="c.clearing_time" value="${c.clearing_time!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">
<div class="page-content" id="view_content" data-view="product#detail_view">
    <div class="page-head">
        <div class="page-title">
            <h1>理财产品信息查看
                <small>理财产品信息</small>
            </h1>
        </div>
    </div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>
                    <span class="caption-subject bold uppercase">理财产品信息查看</span>
                </div>
            </div>

            <div class="portlet-body form">
                <form role="form" class="form-horizontal validate-form" action="${ctx}/product/save" method="post"
                      id="product_form">
                    <input type="hidden" name="action" value="${(action)!}">
                    <input type="hidden" name="product.id" value="${(product.id)!}">
                    <input type="hidden" name="product.remaining_amount" value="${(product.remaining_amount)!'0'}">

                    <div class="form-body">
                        <h3 class="form-section">产品基本信息</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="name">
                                        <span class="required"> * </span> 产品名称
                                    </label>

                                    <div class="col-md-9">
                                        <input type="text" id="name" name="product.name" maxlength="160" value="${(product.name)!}"
                                               class="form-control required " autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="simple_code">
                                        销售编号
                                    </label>

                                    <div class="col-md-9">
                                        <input type="text" id="simple_code" name="product.simple_code" minlength="10" maxlength="10" value="${(product.simple_code)!}"
                                               class="form-control stringCheck" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="brand">
                                        <span class="required"> * </span> 所属品牌
                                    </label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择所属品牌" id="brand" name="product.brand"
                                                class="select2me form-control required">
                                            <option value=""></option>
                                            <#list brand! as b>
                                                <option value="${b.id!}"
                                                        <#if product.brand?? && product.brand == b.id>selected="selected" </#if>>${b.name!}</option>
                                            </#list>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="product_type">
                                        产品类型
                                    </label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择产品类型" id="product_type" name="product.product_type"
                                                class="select2me form-control dict"
                                                data-category="product_type" data-value="${product.product_type!}">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="risk_tolerance">
                                        <span class="required"> * </span> 风险等级
                                    </label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择风险等级" id="risk_tolerance"
                                                name="product.risk_tolerance" data-category="risk_level"
                                                data-value="${(product.risk_tolerance)!}"
                                                class="select2me form-control required dict">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="groups_id"> 产品分组</label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择分组" id="groups_id" name="product.group_id"
                                                class="select2me form-control required">
                                            <option value=""></option>
                                            <#list groups! as g>
                                                <option value="${g.id!}"
                                                        <#if product.group_id?? && product.group_id==g.id>selected="selected" </#if>>${g.name!}</option>
                                            </#list>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="sales_manage_fee">
                                        管理费率
                                    </label>

                                    <div class="col-md-9">
                                        <input type="number" id="sales_manage_fee" name="product.sales_manage_fee" maxlength="160" value="${(product.sales_manage_fee)!}"
                                               class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label col-md-3" for="transfer_holding_days"> 转让限制天数</label>
                                <div class="col-md-9">
                                    <div class="input-group width150 ">
                                        <input type="number" name="product.transfer_holding_days"
                                            <#if !(product.transfer_flag??) || product.transfer_flag == 'N'> disabled="disabled" class="form-control" <#else >class="form-control  required number" </#if>
                                               value="${(product.transfer_holding_days)!}" id="transfer_holding_days">
                                        <span class="input-group-addon"><i class="fa">天</i></span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <label class="control-label col-md-3" for="groups_id">&nbsp;</label>

                                <div class="col-md-9">

                                    <label class="checkbox-inline pl0">
                                        <input type="checkbox" id="cell_phone" name="product.cell_phone" value="Y"
                                               <#if product.cell_phone?? && product.cell_phone=='Y'>checked="checked" </#if>>
                                        手机专享
                                    </label>

                                    <label class="checkbox-inline pl0">
                                        <input type="checkbox" id="new_user" name="product.new_user" value="Y"
                                               <#if product.new_user?? && product.new_user == 'Y'>checked="checked" </#if>>
                                        新手专享
                                    </label>

                                    <label class="checkbox-inline pl0">
                                        <input type="checkbox" id="redeem_flag" name="product.redeem_flag" value="Y"
                                               <#if product.redeem_flag?? && product.redeem_flag == 'Y'>checked="checked" </#if>>
                                        可退款
                                    </label>
                                    <label class="checkbox-inline pl0">
                                        <input type="checkbox" id="transfer_flag" name="product.transfer_flag" value="Y"
                                               <#if product.transfer_flag?? && product.transfer_flag == 'Y'>checked="checked" </#if>>
                                        可转让
                                    </label>

                                </div>

                            </div>
                        </div>

                        <h3 class="form-section">产品收益说明</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="yield">
                                        <span class="required"> * </span> 年利化率
                                    </label>

                                    <div class="col-md-9">
                                        <input type="number" id="yield" min="0" max="100" name="product.yield"
                                               value="${(product.yield)!}"
                                               class="form-control number decimal2  isFloat required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="collection_mode">
                                        <span class="required"> * </span> 收益方式
                                    </label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择收益方式" id="collection_mode"
                                                name="product.collection_mode"
                                                class="select2me form-control required dict"
                                                data-category="collection_mode"
                                                data-value="${product.collection_mode!}">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="start_end"><span
                                            class="required"> * </span>认购日期</label>

                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-medium" id="start_end" name="start_end" readonly
                                               value="<#if product.begin_date?? && product.end_date??>${(product.begin_date)!?string("yyyy/MM/dd")} - ${(product.end_date)!?string("yyyy/MM/dd")}</#if>">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-3" for="sale_time"> 销售时间点</label>

                                <div class="col-md-9">
                                    <select data-placeholder="请选择销售时间点" id="sale_time"
                                            name="product.sale_time"
                                            class="select2me form-control">
                                        <option value=""></option>
                                        <option value="0" <#if product.sale_time! == 0>selected="selected"</#if>>下午12点</option>
                                        <option value="1" <#if product.sale_time! == 1>selected="selected"</#if>>上午1点</option>
                                        <option value="2" <#if product.sale_time! == 2>selected="selected"</#if>>上午2点</option>
                                        <option value="3" <#if product.sale_time! == 3>selected="selected"</#if>>上午3点</option>
                                        <option value="4" <#if product.sale_time! == 4>selected="selected"</#if>>上午4点</option>
                                        <option value="5" <#if product.sale_time! == 5>selected="selected"</#if>>上午5点</option>
                                        <option value="6" <#if product.sale_time! == 6>selected="selected"</#if>>上午6点</option>
                                        <option value="7" <#if product.sale_time! == 7>selected="selected"</#if>>上午7点</option>
                                        <option value="8" <#if product.sale_time! == 8>selected="selected"</#if>>上午8点</option>
                                        <option value="9" <#if product.sale_time! == 9>selected="selected"</#if>>上午9点</option>
                                        <option value="10" <#if product.sale_time! == 10>selected="selected"</#if>>上午10点</option>
                                        <option value="11" <#if product.sale_time! == 11>selected="selected"</#if>>上午11点</option>
                                        <option value="12" <#if product.sale_time! == 12>selected="selected"</#if>>中午12点</option>
                                        <option value="13" <#if product.sale_time! == 13>selected="selected"</#if>>下午1点</option>
                                        <option value="14" <#if product.sale_time! == 14>selected="selected"</#if>>下午2点</option>
                                        <option value="15" <#if product.sale_time! == 15>selected="selected"</#if>>下午3点</option>
                                        <option value="16" <#if product.sale_time! == 16>selected="selected"</#if>>下午4点</option>
                                        <option value="17" <#if product.sale_time! == 17>selected="selected"</#if>>下午5点</option>
                                        <option value="18" <#if product.sale_time! == 18>selected="selected"</#if>>下午6点</option>
                                        <option value="19" <#if product.sale_time! == 19>selected="selected"</#if>>下午7点</option>
                                        <option value="20" <#if product.sale_time! == 20>selected="selected"</#if>>下午8点</option>
                                        <option value="21" <#if product.sale_time! == 21>selected="selected"</#if>>下午9点</option>
                                        <option value="22" <#if product.sale_time! == 22>selected="selected"</#if>>下午10点</option>
                                        <option value="23" <#if product.sale_time! == 23>selected="selected"</#if>>下午11点</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="valuedate">起息日期</label>

                                    <div class="col-md-9">
                                        <div id="start_date" class="input-group input-medium date date-picker"
                                             data-date-format="yyyy-mm-dd"
                                             data-date-start-date="+0d">
                                            <input type="text" class="form-control" id="valuedate"
                                                   name="valuedate" value="<#if product.valuedate??>${(product.valuedate)!?string('yyyy-MM-dd')}</#if>"
                                                   readonly>
												<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i>
                                                </button>
												</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="valuedate_offset">到帐日</label>

                                    <div class="col-md-9">
                                        <div class="radio-list" id="valuedate_offset">
                                            <label class="radio-inline">
                                                <input type="radio" id="valuedate_offset2"
                                                       name="product.valuedate_offset"
                                                       <#if (product.valuedate_offset)?? && product.valuedate_offset ==0>checked </#if>
                                                       value="0"> T+0
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" id="valuedate_offset1"
                                                       name="product.valuedate_offset"
                                                       <#if (product.valuedate_offset)?? && product.valuedate_offset ==1>checked </#if>
                                                       value="1"> T+1
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" id="valuedate_offset2"
                                                       name="product.valuedate_offset"
                                                       <#if (product.valuedate_offset)?? && product.valuedate_offset ==2>checked </#if>
                                                       value="2"> T+2
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="time_limit">
                                        <span class="required"> * </span> 理财期限
                                    </label>

                                    <div class="col-md-8 pr0">
                                        <div class="input-group">
                                            <input type="number" id="time_limit" name="product.time_limit"
                                                   value="${(product.time_limit)!}" class="form-control"
                                                   autocomplete="off" min="0" required>
                                            <span class="input-group-btn">
                                                <select data-placeholder="请选择期限单位" id="time_limit_unit"
                                                        name="product.time_limit_unit" data-category="limit_unit"
                                                        data-value="${(product.time_limit_unit)!}"
                                                        class="select2me form-control width55 dict"></select>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="due_date">
                                        <span class="required"> * </span> 结息日期
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group input-medium">
                                            <input type="text" class="form-control" id="due_date"
                                                   name="due_date" value="${(product.due_date)!?string('yyyy-MM-dd')}" disabled>
												<span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i></button>
												</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="founded_date">
                                        <span class="required"> * </span> 成立日期
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group input-medium">
                                            <input type="text" class="form-control input-medium date date-picker" id="founded_date"
                                                   name="founded_date"
                                                <#if (product.founded_date)??>
                                                   value="${(product.founded_date)!?string('yyyy-MM-dd')}"
                                                </#if>>
												<span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i></button>
												</span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="expire_date">
                                        <span class="required"> * </span> 到期日期
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group input-medium">
                                            <input type="text" class="form-control input-medium date date-picker" id="expire_date"
                                                   name="expire_date"  <#if (product.founded_date)??>
                                                   value="${(product.expire_date)!?string('yyyy-MM-dd')}"
                                            </#if>>
												<span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i></button>
												</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="price">
                                        <span class="required"> * </span> 募集资金
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required"
                                                   name="product.price" value="${(product.price)!}"
                                                   id="price" placeholder="">
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="max_invest_amount">
                                        <span class="required"> * </span> 单笔投资上限
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required max_invest_amount"
                                                   name="product.max_invest_amount"
                                                   value="${(product.max_invest_amount)!}"
                                                   id="max_invest_amount">
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="min_invest_amount">
                                        <span class="required"> * </span> 起投金额
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required min_invest_amount"
                                                   name="product.min_invest_amount"
                                                   value="${(product.min_invest_amount)!}" id="min_invest_amount">
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="increase_amount">
                                        <span class="required"> * </span> 递增金额
                                    </label>

                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required min_invest_amount"
                                                   name="product.increase_amount" value="${(product.increase_amount)!}"
                                                   id="increase_amount">
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="valuedate_offset">
                                        <span class="required"> * </span> 基本天数
                                    </label>

                                    <div class="col-md-9">
                                        <div class="radio-list" id="basic_days">
                                            <label class="radio-inline">
                                                <input type="radio" id="valuedate_offset2" name="product.basic_days"
                                                       value="365" <#if product.basic_days==365>checked</#if>> 365 天
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" id="valuedate_offset1" name="product.basic_days"
                                                       value="360" <#if product.basic_days ==360>checked</#if>> 360 天
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>


                        <h3 class="form-section">产品其他信息</h3>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="labels">
                                        产品标签
                                    </label>

                                    <div class="col-md-9">
                                        <input type="text" id="labels" name="product.labels" maxlength="180"
                                               value="${(product.labels)!}" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3" for="issuer"> 购买渠道</label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择发行机构" id="issuer" name="product.issuer"
                                                class="select2me form-control">
                                            <option value=""></option>
                                            <#list issuer! as iss>
                                                <option value="${iss.id!}"
                                                        <#if product.issuer?? && iss.id==product.issuer>selected="selected" </#if>>${iss.name!}</option>
                                            </#list>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="category">
                                        运作模式
                                    </label>

                                    <div class="col-md-9">
                                        <select data-placeholder="请选择运作模式" id="category" name="product.category"
                                                class="select2me form-control required dict"
                                                data-category="product_category" data-value="${product.category!}">
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="sales_area">
                                        销售地区
                                    </label>

                                    <div class="col-md-9">

                                        <input type="text" id="sales_area" name="product.sales_area" maxlength="500" value="${(product.sales_area)!}"
                                               class="form-control" autocomplete="off">
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="remark">
                                        备注
                                    </label>

                                    <div class="col-md-9">
                                        <textarea class="form-control" rows="3" name="product.remark" id="remark">${product.remark!}</textarea>
                                    </div>
                                </div>
                            </div>

                            <#--<div class="col-md-6">-->
                                <#--<div class="form-group">-->

                                    <#--<label class="control-label col-md-3" for="due_date">产品LOGO</label>-->

                                    <#--<div class="col-md-9">-->
                                        <#--<div class="fileinput fileinput-new" id="logo_uploaded"-->
                                             <#--data-provides="fileinput" data-obj="${(product.logo_attachment)!}"-->
                                             <#--data-name="pic_attachment"></div>-->
                                    <#--</div>-->
                                <#--</div>-->
                            <#--</div>-->
                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-5 col-md-9">
                                <a href="javascript:history.go(-1);" class="btn btn-default default">返回</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@override>

<@override name="view_scripts">
    <script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">
    <link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
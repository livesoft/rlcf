<@override name="view_body">

<div class="page-content" id="view_content" data-view="brand">
    <div class="page-head">
        <div class="page-title">
            <h1>产品品牌 <small>产品品牌</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品品牌</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品品牌管理</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="" class="form-inline pull-right">
                        <label class="" for="s_name">品牌名称:</label>
                        <input type="text" class="form-control" id="s_name" placeholder="请输入品牌名称">
                        <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                class="icon-magnifier mr5"></i>查询</a>

                    </form>
                    <div class="clearfix mb10">
                        <div class="pull-left"><a id="new" href="${ctx}/product/brand/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="brand_dt"
                                   data-ajax="${ctx}/product/brand/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width150">品牌名称</th>
                                    <th class="width100">品牌LOGO</th>
                                    <th class="width40">排序</th>
                                    <th class="width100">创建时间</th>
                                    <th class="width100">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
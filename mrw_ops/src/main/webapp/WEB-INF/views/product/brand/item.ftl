<@override name="view_body">

<div class="page-content" id="view_content" data-view="brand#item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品品牌
                <small>${(action=='create')?string('创建','编辑')}产品品牌</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/brand">产品品牌</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}产品品牌</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品品牌</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}产品品牌</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/brand/save" method="post" id="brand_form">
                        <input type="hidden" name="brand.id" value="${brand.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 品牌名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="title" name="brand.name" maxlength="160" value="${brand.name!}"
                                           class="form-control" autocomplete="off" required maxlength="100">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 品牌LOGO
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(brand.logo_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_order">
                                    显示次序
                                </label>

                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${brand.sort!'1'}" data-step="1"
                                         data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="display_order" name="brand.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="intro">
                                    品牌描述
                                </label>

                                <div class="col-md-6 ">
                                    <textarea id="intro" name="brand.intro"  class="form-control" maxlength="1100">${brand.intro!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/brand" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
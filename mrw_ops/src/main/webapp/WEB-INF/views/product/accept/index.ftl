<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#accept">
    <div class="page-head">
        <div class="page-title">
            <h1>承兑银行 <small>承兑银行</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product">理财产品</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品承兑银行</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品承兑银行</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/accept/save" method="post" id="accept_form">
                        <input type="hidden" name="accept.product" value="${accept.product!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 承兑银行
                                </label>

                                <div class="col-md-4">

                                    <input type="text" maxlength="12" id="name" name="accept.name" value="${accept.name!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo">展示LOGO</label>

                                <div class="col-md-4">
                                    <div id="logo"
                                         data-obj="${(accept.icon_attachment)!}" data-name="logo"></div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
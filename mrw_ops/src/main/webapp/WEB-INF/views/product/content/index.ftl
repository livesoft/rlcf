<@override name="view_body">

<div class="page-content" id="view_content" data-view="zone#content">
    <div class="page-head">
        <div class="page-title">
            <h1>App首页展示 <small>App首页维护</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/zone">APP展示</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">App首页内容</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">App首页内容</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <input type="text" class="form-control" id="s_email" placeholder="请输入字段1">

                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-right"><a id="new" href="${ctx}/product/content/create/${zone!}" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="content_dt"
                                   data-ajax="${ctx}/product/content/dtlist/${zone!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th>图片</th>
                                    <th class="width80">图片高度</th>
                                    <th class="width140">图片宽度比例</th>
                                    <th class="width60">状态</th>
                                    <th class="width140">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
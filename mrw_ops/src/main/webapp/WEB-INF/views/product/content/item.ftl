
<@override name="view_styles">
<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>
<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="zone#content_item">
    <div class="page-head">
        <div class="page-title">
            <h1>App首页维护
                <small>${(action=='create')?string('创建','编辑')} App首页内容</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/content/${c.zone!}">App内容</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')} App内容</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">App首页维护</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')} App首页内容</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/content/save" method="post"
                          id="zone_form">
                        <input type="hidden" name="c.id" value="${c.id!}">
                        <input type="hidden" name="c.zone" value="${c.zone!}">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="image_up">
                                    展示图片
                                </label>

                                <div class="col-md-4">
                                    <div id="image_up" data-obj="${c.image_attachment!}" data-name="attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_order">
                                    显示次序
                                </label>

                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${c.display_order!'0'}" data-step="1"
                                         data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="display_order" name="c.display_order"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/content/${c.zone}" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
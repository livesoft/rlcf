<@override name="view_body">

<div class="page-content" id="view_content" data-view="push#item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品推送
                <small>${(action=="create")?string("创建","编辑")}产品推送</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/push">产品推送</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("创建","编辑")}产品推送</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品推送</span>
                        <span class="caption-helper">${(action=="create")?string("创建","编辑")}产品推送</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/push/save" method="post" id="product_form">
                        <input type="hidden" name="r.id" value="${(r.id)!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>推荐产品
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择项目" id="product" name="sel_product"
                                            class="select2me form-control required">
                                        <#list products! as p>
                                            <option value="${p.id!}-${p.name!}" <#if r.product?? && r.product==p.id>selected="selected" </#if>>${p.name!}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>推荐词
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="txt_tpl" name="r.txt_tpl" value="${(r.txt_tpl)!}"
                                    class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 显示排序
                                </label>
                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(r.sort)!1}" data-step="1" data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="r.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/push" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
</div>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
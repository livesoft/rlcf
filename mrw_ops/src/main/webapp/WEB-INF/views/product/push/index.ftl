<@override name="view_body">

<div class="page-content" id="view_content" data-view="push">
    <div class="page-head">
        <div class="page-title">
            <h1>产品推送 <small>产品推送</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品推送</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品推送</span>
                        <span class="caption-helper">产品推送管理</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">产品名称:</label>
                            <input type="text" class="form-control" id="s_product_name" placeholder="请输入产品名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                        <div class="pull-left"><a id="new" href="${ctx}/product/push/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="push_dt"
                                   data-ajax="${ctx}/product/push/dtlist">
                                <thead>
                                    <th class="width30"></th>
                                    <th class="width150">产品名称</th>
                                    <th class="width60">排序</th>
                                    <th class="width150">推荐时间</th>
                                    <th class="width60">状态</th>
                                    <th class="width140">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>

<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#app_item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品展示
                <small>${(action=='create')?string('创建','编辑')} 产品展示</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product">产品管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/app/${pd.product!}">产品展示管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')} 产品展示</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品展示管理</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')} 产品展示</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/app/save" method="post"
                          id="zone_form">
                        <input type="hidden" name="pd.id" value="${pd.id!}">
                        <input type="hidden" name="pd.product" value="${pd.product!}">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 标题
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="title" name="pd.title" value="${(pd.title)!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="content"> 内容</label>
                                <div class="col-md-4">
                                    <input type="text" id="content" name="pd.content" value="${(pd.content)!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="remark">
                                    备注
                                </label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" id="remark" name="pd.remark">
                                    ${(pd.remark)!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_order">
                                    显示次序
                                </label>

                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${pd.display_order!'0'}" data-step="1"
                                         data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="display_order" name="pd.display_order"
                                                   class="spinner-input form-control" maxlength="3" readonly>
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code"></label>
                                <div class="col-md-9 checkbox-list">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="cell_phone" name="pd.pcweb_flag" value="Y"
                                               <#if pd.pcweb_flag?? && pd.pcweb_flag=='Y'>checked="checked" </#if>>
                                        PCWEB端展示
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/app/${(pd.product)!}" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
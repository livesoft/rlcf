<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#app">
    <div class="page-head">
        <div class="page-title">
            <h1>产品手机展示
                <small>产品手机展示</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product">理财产品管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品手机展示</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品－手机展示</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <a id="new" href="${ctx}/product/app/create/${product!}" class="btn blue">
                        <i class="icon-plus mr5"></i>创建</a>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="app_dt"
                                   data-ajax="${ctx}/product/app/dtlist/${product!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width120">标题</th>
                                    <th>内容</th>
                                    <th class="width140">备注</th>
                                    <th class="width60">地址</th>
                                    <th class="width60">次序</th>
                                    <th class="width140">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
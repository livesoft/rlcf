<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#detail_item">
    <div class="page-head">
        <div class="page-title">
            <h1>理财中心
                <small>理财产品</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/detail/${details.product!}">理财项目</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">理财产品</a>
        </li>
    </ul>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">${(product.name)!}</span>
                    <span class="caption-helper">产品属性</span>
                </div>
            </div>

            <div class="portlet-body form">
                <form role="form" class="form-horizontal validate-form" action="${ctx}/product/detail/save" method="post"
                      id="attr_detail_form">
                    <input type="hidden" name="details.id" value="${(details.id)!}">

                    <div class="form-body">
                        <input type="hidden" name="details.product" value="${(product.id)!}">

                        <div class="form-group">
                            <label class="control-label col-md-3" for="app_item">
                                <span class="required"> * </span>  所属产品展示
                            </label>

                            <div class="col-md-4">
                                <select data-placeholder="请选择产品展示" id="app_item" name="details.app_item"
                                        class="select2me form-control required" data-msg-required="必须填写">
                                    <option value=""></option>
                                    <#list appDetails! as ad>
                                        <option  value="${ad.id!}"
                                                 <#if details.app_item?? &&details.app_item == ad.id>selected="selected"</#if>>${ad_index+1}. ${ad.title!}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="attribute">
                                <span class="required"> * </span> 属性
                            </label>

                            <div class="col-md-4">
                                <select data-placeholder="请选择属性" id="attribute" name="details.attribute"
                                        class="select2me form-control required" data-msg-required="必须填写">
                                    <option value=""></option>
                                    <#list attrs! as attr>
                                        <option  value="${attr.id!}" data-datatype="${attr.data_type}"
                                                 <#if details.attribute?? &&details.attribute == attr.id>selected="selected"</#if>>${attr_index+1}. ${attr.name!}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="attr_div_id">
                            <label class="control-label col-md-3" for="attribute_val">
                                <span class="required"> * </span> 属性内容
                            </label>

                            <div class="col-md-4" id="attr_val">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">显示排序</label>

                            <div class="col-md-9">
                                <input type="hidden" value="${details.attribute_val2!?html}" id="attribute_val2">
                                <input type="hidden" value="${details.attribute_val!?html}" id="attribute_val">
                                <div id="spinner1">
                                    <div class="input-group input-small">
                                        <input type="text" class="spinner-input form-control" maxlength="3"
                                               name="details.display_order" value="${(details.display_order)!}">

                                        <div class="spinner-buttons input-group-btn btn-group-vertical">
                                            <button type="button" class="btn spinner-up btn-xs blue">
                                                <i class="fa fa-angle-up"></i>
                                            </button>
                                            <button type="button" class="btn spinner-down btn-xs blue">
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary blue">保存</button>
                                <a href="${ctx}/product/detail/${details.product!}"
                                   class="btn btn-default default">取消</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@override>
<@override name="view_scripts">
    <script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">
    <link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
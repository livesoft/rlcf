<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#detail">
    <div class="page-head">
        <div class="page-title">
            <h1>理财中心 <small>理财产品</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">理财中心</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品属性</span>
                        <span class="caption-helper">产品属性列表</span>
                    </div>
                    <#--<div class="actions">-->
                        <#--<div class="btn-group">-->
                            <#--<a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">-->
                                <#--<i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>-->
                            <#--</a>-->
                            <#--<ul class="dropdown-menu pull-right">-->
                                <#--<li><a href="#"> 导出Excel </a>-->
                                <#--</li>-->
                            <#--</ul>-->
                        <#--</div>-->
                    <#--</div>-->
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">

                        </form>
                        <div class="pull-right"><a id="new" href="${ctx}/product/detail/create/${product_id!}" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="pro_attr_dt"
                                   data-ajax="${ctx}/product/detail/dtlist/${product_id!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width20"></th>
                                    <th class="width100">属性名称</th>
                                    <th>属性值</th>
                                    <th class="width200">显示次序</th>
                                    <th class="width200">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>
<@override name="view_scripts">
    <script type="text/javascript">
        g.attr_json = JSON.parse('${attr_json!'[]'}')
    </script>

</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="group">
    <div class="page-head">
        <div class="page-title">
            <h1>产品分组 <small>产品分组</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品分组</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品分组管理</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <a id="new" href="${ctx}/product/group/create" class="btn blue"> <i class="icon-plus mr5"></i>创建</a>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="group_dt"
                                   data-ajax="${ctx}/product/group/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th>名称</th>
                                    <th class="width80">LOGO</th>
                                    <th class="width140">担保机构</th>
                                    <th class="width60">排序</th>
                                    <th class="width60">状态</th>
                                    <th class="width30">首页显示</th>
                                    <th class="width180">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
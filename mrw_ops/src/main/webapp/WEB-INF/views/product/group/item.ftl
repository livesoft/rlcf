
<@override name="view_styles">
<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>
<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="group#content_item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品分组管理
                <small>${(action=='create')?string('创建','编辑')} 产品分组</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product/group">产品分组</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')} 产品分组</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品分组管理</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')} 产品分组</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/group/save" method="post"
                          id="zone_form">
                        <input type="hidden" name="g.id" value="${g.id!}">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="name" name="g.name" maxlength="50" value="${(g.name)!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="assure_name">担保机构
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="assure_name" maxlength="160" name="g.assure_name" value="${(g.assure_name)!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="image_up">
                                    LOGO
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="image_up" data-obj="${(g.attachment)!}" data-name="attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_order">
                                    显示次序
                                </label>

                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${g.display_order!'1'}" data-step="1"
                                         data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="display_order" name="g.display_order"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="product_flag">
                                </label>

                                <div class="col-md-4 checkbox-list">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="product_flag" name="g.product_flag" value="Y"
                                                <#if g.product_flag?? && g.product_flag=='Y'>checked </#if>>
                                        产品专享
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="index_show" name="g.index_show" value="Y"
                                                <#if g.index_show?? && g.index_show=='Y'>checked </#if>>
                                        APP首页推荐
                                    </label>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="description">
                                    描述
                                </label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" maxlength="150" id="description" name="g.description">
                                    ${(g.description)!}
                                    </textarea>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product/group" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
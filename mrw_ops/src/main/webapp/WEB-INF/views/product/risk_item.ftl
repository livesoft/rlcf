<@override name="view_body">

<div class="page-content" id="view_content" data-view="product#risk_item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品说明书
                <small>产品说明书</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/product">产品说明书</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品说明书</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品说明书</span>
                        <span class="caption-helper">产品说明书</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/product/risk_save" method="post" id="risk_form">
                        <input type="hidden" name="details.id" value="${(details.id)!}">
                        <input type="hidden" name="details.product" value="${product_id!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    产品说明书
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="details.attribute_val" type="text/plain">${(details.attribute_val)!}</script>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_profit#profit_detail">
    <div class="page-head">
        <div class="page-title">
            <h1>日终收益明细报表 <small></small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/report/profit">日终收益报表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">日终收益明细报表</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">日终收益明细报表</span>
                        <span class="caption-helper">日终收益明细报表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <input type="hidden" name="adate" value="${adate!}">
                            <label class= "">订单号：</label>
                            <input type="text" class="form-control" id="s_order" name="s_order" placeholder="请输入订单号">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/profit/detail_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="profit_detail_dt"
                                   data-ajax="${ctx}/report/profit/detail_dtlist?adate=${adate!}">
                                <thead>
                                <tr>
                                    <th class="width80">收益日期</th>
                                    <th class="width80">用户名称</th>
                                    <th class="width80">购买产品</th>
                                    <th class="width100">电子账户</th>
                                    <th class="width60">投资金额</th>
                                    <th class="width60">收益金额</th>
                                    <th class="width60">调账金额</th>
                                    <th class="width100">订单号</th>
                                    <th class="width60">打款状态</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
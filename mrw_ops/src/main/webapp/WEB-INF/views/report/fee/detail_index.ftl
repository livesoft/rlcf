<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_fee#fee_detail">
    <div class="page-head">
        <div class="page-title">
            <h1>转让手续费 <small>转让手续费</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/report/fee">转让报表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让手续费</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers font-green-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold">合计信息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row static-info">
                        <div class="col-md-2 name">合计投资笔数:</div>
                        <div class="col-md-4 value" ><span id="t_cnt">${(record.t_cnt)!0}</span> 笔</div>

                        <div class="col-md-2 name" >合计手续金额:</div>
                        <div class="col-md-4 value" ><span id="t_handing_charge">${(record.t_handing_charge)!0}</span> 元</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">转让手续费</span>
                        <span class="caption-helper">转让手续费</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left" method="post">
                            <input type="hidden" name="s_date" id="s_date" value="${adate!}">
                            <label class= "">订单号：</label>
                            <input type="text" class="form-control" id="s_order" name="s_order" placeholder="请输入订单号">
                            <label class= "">产品名称：</label>
                            <input type="text" class="form-control" id="s_product_name" name="s_product_name" placeholder="请输入产品名称">
                            <label class= "">是否收费：</label>
                            <select data-placeholder="是否收费" id="free_status" name="free_status"
                                    class="select2me form-control">
                                <option value=""></option>
                                <option value="true">是</option>
                                <option value="false">否</option>
                            </select>
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/fee/detail_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="fee_detail_dt"
                                   data-ajax="${ctx}/report/fee/detail_dtlist?date=${adate!}">
                                <thead>
                                    <th class="width100">交易时间</th>
                                    <th class="width100">用户名称</th>
                                    <th class="width100">电子账户</th>
                                    <th class="width100">产品名称</th>
                                    <th class="width100">交易金额</th>
                                    <th class="width100">费率</th>
                                    <th class="width100">手续费</th>
                                    <th class="width100">转让订单号</th>

                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="raisefail#raisefail_detail">
    <div class="page-head">
        <div class="page-title">
            <h1>购买明细 <small>购买明细</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/report/raisefail">募集失败报表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">购买明细</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">购买明细</span>
                        <span class="caption-helper">购买明细列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <input type="hidden" name="s_product" value="${productid!}">
                            <label class= "">订单号：</label>
                            <input type="text" class="form-control" id="s_order" name="s_order" placeholder="请输入订单号">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/raisefail/detail_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="raisefail_detail_dt"
                                   data-ajax="${ctx}/report/raisefail/detail_dtlist/${productid!}">
                                <thead>
                                    <th class="width100">产品名称</th>
                                    <th class="width100">投资日期</th>
                                    <th class="width100">用户名称</th>
                                    <th class="width100">电子账户</th>
                                    <th class="width100">投资金额</th>
                                    <th class="width100">购买订单号</th>
                                    <th class="width100">打款状态</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
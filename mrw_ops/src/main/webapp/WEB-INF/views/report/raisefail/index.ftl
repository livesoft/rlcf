<@override name="view_body">

<div class="page-content" id="view_content" data-view="raisefail">
    <div class="page-head">
        <div class="page-title">
            <h1>募集失败报表 <small>募集失败报表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">募集失败报表</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers font-green-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold">合计信息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row static-info">
                        <div class="col-md-2 name">
                            合计募集金额:
                        </div>
                        <div class="col-md-4 value" >
                            <span id="t_price">${record.t_price!0}</span> 元
                        </div>
                        <div class="col-md-2 name" >
                            合计投资金额:
                        </div>
                        <div class="col-md-4 value" >
                            <span id="t_total">${record.t_total!0}</span> 元
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">募集失败报表</span>
                        <span class="caption-helper">募集失败报表列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "">产品名称：</label>
                            <input type="text" class="form-control" id="s_product_name" name="s_product_name" placeholder="请输入产品名称">
                            <label class= "">发行机构：</label>
                            <input type="text" class="form-control" id="s_org_name" name="s_org_name" placeholder="请输入发行机构名称">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/raisefail/index_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="raisefail_dt"
                                   data-ajax="${ctx}/report/raisefail/dtlist">
                                <thead>
                                    <th class="width30"></th>
                                    <th class="width100">产品名称</th>
                                    <th class="width100">发行机构</th>
                                    <th class="width100">募集金额</th>
                                    <th class="width100">投资金额</th>
                                    <th class="width160">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_totalproduct">
    <div class="page-head">
        <div class="page-title">
            <h1>产品资金汇总报表 <small>产品资金汇总报表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品资金汇总报表</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers font-green-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold">合计信息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row static-info">
                        <div class="col-md-2 name">合计原始募集金额:</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_price">${record.t_sum_price!0}</span> 元
                        </div>

                        <div class="col-md-2 name" >合计实际募集金额:</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_orignal">${record.t_sum_orignal!0}</span> 元
                        </div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name" >合计发放金额:</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_principal">${record.t_sum_principal!0}</span> 元
                        </div>

                        <div class="col-md-2 name" >合计发放收益额:</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_profit">${record.t_sum_profit!0}</span> 元
                        </div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name" >合计发放总金额:</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_actual">${record.t_sum_actual!0}</span> 元
                        </div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-4 name" >合计调账金额(交易失败合计调账金额):</div>
                        <div class="col-md-4 value" >
                            <span id="t_sum_adjust_amount">
                            ${record.t_sum_adjust_amount!0}
                            </span>
                            <#if (record.t_fail_sum_adjust_amount!0) != 0>
                                (<span id="t_fail_sum_adjust_amount" style="color: red">${record.t_fail_sum_adjust_amount!0}</span>)
                            <#else >
                                (<span id="t_fail_sum_adjust_amount">${record.t_fail_sum_adjust_amount!0}</span>)
                            </#if>
                            元
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品资金汇总报表</span>
                        <span class="caption-helper">产品资金汇总报表列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "">产品名称：</label>
                            <input type="text" class="form-control" id="s_product_name" name="s_product_name" placeholder="请输入产品名称">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="_export" data-href="${ctx}/report/totalproduct/index_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="totalproduct_dt"
                                   data-ajax="${ctx}/report/totalproduct/index_dtlist">
                                <thead>
                                    <th class="width60">产品名称</th>
                                    <th class="width60">原始募集金额</th>
                                    <th class="width60">实际募集金额</th>
                                    <th class="width60">发放金额</th>
                                    <th class="width60">发放收益额</th>
                                    <th class="width120">调账金额(交易失败调账金额)</th>
                                    <th class="width60">发放总金额</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_redeem#redeem_detail">
    <div class="page-head">
        <div class="page-title">
            <h1>每日退单报表 <small>每日退单报表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/report/redeem">日退单赎回报表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">每日退单报表</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">每日退单报表</span>
                        <span class="caption-helper">每日退单报表列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <input type="hidden" name="s_date" value="${adate!}">
                            <label class= "">订单号：</label>
                            <input type="text" class="form-control" id="s_order" name="s_order" placeholder="请输入订单号">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/redeem/detail_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="redeem_detail_dt"
                                   data-ajax="${ctx}/report/redeem/detail_dtlist?date=${adate!}">
                                <thead>
                                    <th class="width100">日期</th>
                                    <th class="width100">退单人</th>
                                    <th class="width100">电子账户</th>
                                    <th class="width100">产品名称</th>
                                    <th class="width100">退单金额</th>
                                    <th class="width100">购买订单</th>
                                    <th class="width100">打款状态</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
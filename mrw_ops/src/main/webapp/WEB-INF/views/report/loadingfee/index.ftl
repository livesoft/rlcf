<@override name="view_body">

<div class="page-content" id="view_content" data-view="loading_fee">
    <div class="page-head">
        <div class="page-title">
            <h1>挂牌费报表 <small>挂牌费报表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">挂牌费报表</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers font-green-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold">合计信息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row static-info">
                        <div class="col-md-2 name">合计挂单数:</div>
                        <div class="col-md-4 value" ><span id="t_trade_cnt">${(record.t_trade_cnt)!0}</span> 笔</div>

                        <div class="col-md-2 name" >合计挂单金额:</div>
                        <div class="col-md-4 value" ><span id="t_trade_sum">${(record.t_trade_sum)!0}</span> 元</div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name">合计成交数:</div>
                        <div class="col-md-4 value" ><span id="t_deal_cnt">${(record.t_deal_cnt)!0}</span> 笔</div>

                        <div class="col-md-2 name" >合计成交金额:</div>
                        <div class="col-md-4 value" ><span id="t_deal_sum">${(record.t_deal_sum)!0}</span> 元</div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name">合计挂牌金额:</div>
                        <div class="col-md-4 value" ><span id="t_fee_sum">${(record.t_fee_sum)!0}</span> 元</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">挂牌费报表</span>
                        <span class="caption-helper">挂牌费报表列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "" for="s_date">日期</label>
                                <input type="text" class="form-control input-medium" id="s_date" name="s_date" readonly>
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="index_export" data-href="${ctx}/report/loadingfee/index_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">

                            <table class="table table-bordered table-striped table-condensed" id="loadingfee_dt"
                                   data-ajax="${ctx}/report/loadingfee/dt_list">
                                <thead>
                                    <th class="width100">日期</th>
                                    <th class="width100">挂单数</th>
                                    <th class="width100">挂单金额</th>
                                    <th class="width100">成交数</th>
                                    <th class="width100">成交金额</th>
                                    <th class="width100">挂牌费</th>
                                    <th class="width160">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
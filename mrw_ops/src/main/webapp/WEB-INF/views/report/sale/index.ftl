<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_sale">
    <div class="page-head">
        <div class="page-title">
            <h1>日终销售报表 <small>日终销售报表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">日终销售报表</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers font-green-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold">合计信息</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="row static-info">
                        <div class="col-md-2 name">
                            合计投资金额:
                        </div>
                        <div class="col-md-4 value" >
                            <span id="t_amount_sum">${record.t_amount_sum!0}</span> 元
                        </div>

                        <div class="col-md-2 name">
                            合计投资笔数:
                        </div>
                        <div class="col-md-4 value" >
                            <span id="t_cnt">${record.t_cnt!0}</span> 笔
                        </div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name">
                            合计投资人数:
                        </div>
                        <div class="col-md-4 value">
                            <span id="t_investor_cnt">${record.t_investor_cnt!0}</span> 人
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">日终销售报表</span>
                        <span class="caption-helper">日终销售报表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "" for="s_date">日期</label>
                                <input type="text" class="form-control input-medium" id="s_date" name="s_date" readonly>
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="index_export" href="javascript:void(0);" class="btn default blue-stripe"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="sale_dt"
                                   data-ajax="${ctx}/report/sale/dt_list">
                                <thead>
                                    <th class="width100">日期</th>
                                    <th class="width100">投资笔数</th>
                                    <th class="width100">投资人数</th>
                                    <th class="width100">投资金额</th>
                                    <th class="width160">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
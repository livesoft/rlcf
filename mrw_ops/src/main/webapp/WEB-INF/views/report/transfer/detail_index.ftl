<@override name="view_body">

<div class="page-content" id="view_content" data-view="report_transfer#transfer_detail">
    <div class="page-head">
        <div class="page-title">
            <h1>转让明细 <small>转让明细</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/report/transfer">日转让报表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让明细</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">转让明细</span>
                        <span class="caption-helper">转让明细</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left" method="post">
                            <input type="hidden" name="s_date" value="${adate!}">
                            <label class= "">订单号：</label>
                            <input type="text" class="form-control" id="s_order" name="s_order" placeholder="请输入订单号">
                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="deail_export" data-href="${ctx}/report/transfer/detail_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="transfer_detail_dt"
                                   data-ajax="${ctx}/report/transfer/detail_dtlist?date=${adate!}">
                                <thead>
                                    <th class="width100">挂单时间</th>
                                    <th class="width100">产品名称</th>
                                    <th class="width100">用户名称</th>
                                    <th class="width100">电子账户</th>
                                    <th class="width100">挂单金额</th>
                                    <th class="width60">成交状态</th>
                                    <th class="width100">成交时间</th>
                                    <th class="width100">成交金额</th>
                                    <th class="width100">转让订单号</th>
                                    <th class="width100">打款状态</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
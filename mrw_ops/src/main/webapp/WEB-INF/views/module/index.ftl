<@override name="view_body">

<div class="page-content" id="view_content" data-view="module">
    <div class="page-head">
        <div class="page-title">
            <h1>模块资源 <small>模块资源管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">模块资源</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">模块资源管理</span>
                        <span class="caption-helper">模块资源列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10"></div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="module_dt"
                                   data-ajax="${ctx}/module/dtlist">
                                <thead>
                                <tr>
                                    <th class="width100">编号</th>
                                    <th>名称</th>
                                    <th class="width100">菜单</th>
                                    <th class="width40">状态</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
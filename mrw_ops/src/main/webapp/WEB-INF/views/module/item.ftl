<@override name="view_body">

<div class="page-content" id="view_content" data-view="module#item">
    <div class="page-head">
        <div class="page-title">
            <h1>模块资源
                <small>${(action=='create')?string('创建','编辑')}资源</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/module">模块资源</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("新建","编辑")}资源</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">模块资源</span>
                        <span class="caption-helper">${(action=="create")?string("新建","编辑")}资源</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/module/save" method="post" id="module_form">
                        <input type="hidden" name="module.id" value="${(module.id)!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="name" name="module.name" value="${(module.name)!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 编码
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="code" name="module.code" value="${(module.code)!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="menu"></label>
                                <div class="col-md-4 checkbox-list">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="menu" name="module.menu" value="1" <#if module.menu?? && module.menu>checked</#if>>
                                        菜单标记
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="important" name="module.important" value="1" <#if module.important?? && module.important>checked </#if>>
                                        重要模块
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/module" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
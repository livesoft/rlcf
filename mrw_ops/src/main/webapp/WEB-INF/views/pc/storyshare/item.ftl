<@override name="view_body">

<div class="page-content" id="view_content" data-view="storyshare#item">
    <div class="page-head">
        <div class="page-title">
            <h1>理财分享
                <small>${(action=='create')?string('创建','编辑')}理财分享</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/pc/storyshare">理财分享</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}理财分享</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财分享</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}理财分享</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/pc/storyshare/save" method="post" id="storyshare_form">
                        <input type="hidden" name="storyshare.id" value="${storyshare.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="160" name="storyshare.title" value="${storyshare.title!}"
                                           class="form-control required" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 姓名
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="name" maxlength="12" name="storyshare.name" value="${storyshare.name!}"
                                           class="form-control required" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 头像
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(storyshare.pic_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="href_url">
                                    链接地址
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="href_url" maxlength="180" name="storyshare.href_url" value="${storyshare.href_url!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 显示排序
                                </label>
                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(storyshare.sort)!1}" data-step="1" data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="storyshare.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    内容
                                </label>

                                <div class="col-md-6 ">
                                    <textarea class="form-control" name="storyshare.content" id="intro" rows="3" maxlength="60">${storyshare.content!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/pc/storyshare" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
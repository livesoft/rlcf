<@override name="view_body">

<div class="page-content" id="view_content" data-view="storyshare">
    <div class="page-head">
        <div class="page-title">
            <h1>理财分享 <small>理财分享管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">理财分享</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财分享</span>
                        <span class="caption-helper">理财分享列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">标题:</label>
                            <input type="text" class="form-control" id="s_title" placeholder="请输入标题名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-left"><a id="new" href="${ctx}/pc/storyshare/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="storyshare_dt"
                                   data-ajax="${ctx}/pc/storyshare/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width100">标题</th>
                                    <th class="width200">姓名</th>
                                    <th class="width100">头像</th>
                                    <th class="width100">排序</th>
                                    <th class="width150">创建时间</th>
                                    <th class="width120">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
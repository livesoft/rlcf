<@override name="view_body">

<div class="page-content" id="view_content" data-view="aboutus#aboutus_item">
    <div class="page-head">
        <div class="page-title">
            <h1>关于我们
                <small>${(action=='create')?string('创建','编辑')}关于我们</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/pc/aboutus">关于我们</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">编辑关于我们</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">关于我们</span>
                        <span class="caption-helper">编辑关于我们</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/pc/aboutus/save" method="post" id="aboutus_form">
                        <input type="hidden" name="aboutus.id" value="${aboutus.id!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="type">
                                     所属目录
                                </label>

                                <div class="col-md-4">
                                    <p class="form-control-static">
                                        ${aboutus.type_name!}
                                    </p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="160" name="aboutus.title" value="${aboutus.title!}"
                                           class="form-control required" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="source">
                                    <span class="required"> * </span> 来源
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="source" maxlength="90" name="aboutus.source" value="${aboutus.source!}"
                                           class="form-control required" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="aboutus.content" type="text/plain">${aboutus.content!}</script>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/pc/aboutus" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
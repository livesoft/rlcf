<@block name="view_scripts">

</@block>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="report">
    <div class="page-head">
        <div class="page-title">
            <h1>平台商户系统工作台
                <small>统计 & 报告</small>
            </h1>
        </div>
    </div>
    <div class="row margin-top-10">



        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
                <div class="visual">
                    <i class="fa fa-briefcase fa-icon-medium"></i>
                </div>
                <div class="details">
                    <div class="number">${product_cnt!0}</div>
                    <div class="desc">理财产品数</div>
                </div>
            </a>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
                <div class="visual">
                    <i class="fa fa-briefcase fa-icon-medium"></i>
                </div>
                <div class="details">
                    <div class="number">${product_cnt!0}</div>
                    <div class="desc">投资总笔数</div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">${member_product_cnt!0}</div>
                    <div class="desc">总投资人数</div>
                </div>
            </a>
        </div>
    </div>
    <div class="row margin-top-10">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">${member_product_cnt!0}</div>
                    <div class="desc">总投资金额</div>
                </div>
            </a>
        </div>



        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">${member_product_cnt!0}</div>
                    <div class="desc">总发放收益额</div>
                </div>
            </a>
        </div>


    </div>
    <div class="clearfix">
    </div>
    <#if amount??>

        <div class="row margin-top-10">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light blue-madison" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-money fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${amount.totalBalance!0}</div>
                        <div class="desc">总金额</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="details">
                        <div class="number">${amount.availableBalance!0}</div>
                        <div class="desc">可用余额</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-money fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${amount.freezeBalance!0}</div>
                        <div class="desc">冻结余额</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-money fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${amount.lastDayBalance!0}</div>
                        <div class="desc">上日余额</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix">
        </div>
    </#if>



    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> 访问统计表</span>
                        <span class="caption-helper">单位：元</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div id="chart1" style="height: 350px;">
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze">产品成交额统计表</span>
                        <span class="caption-helper">单位：元</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div id="chart5" style="height: 350px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

<#--<div class="row">-->
<#--<div class="col-md-12">-->
<#--<div class="portlet light">-->
<#--<div class="portlet-title">-->
<#--<div class="caption">-->
<#--<i class="icon-bar-chart font-green-haze"></i>-->
<#--<span class="caption-subject bold uppercase font-green-haze"> 报表</span>-->
<#--<span class="caption-helper">column and line mix</span>-->
<#--</div>-->
<#--<div class="tools"></div>-->
<#--</div>-->
<#--<div class="portlet-body">-->
<#--<div id="chart2" style="height: 350px;">-->
<#--</div>-->
<#--</div>-->
<#--</div>-->
<#--</div>-->
<#--</div>-->


    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> 品牌销售组成统计</span>
                        <span class="caption-helper">单位：元</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div id="chart3" style="height: 370px;">
                    </div>
                </div>
            </div>
        </div>
    <#--<div class="col-md-6">-->
    <#--<div class="portlet light">-->
    <#--<div class="portlet-title">-->
    <#--<div class="caption">-->
    <#--<i class="icon-bar-chart font-green-haze"></i>-->
    <#--<span class="caption-subject bold uppercase font-green-haze"> 3D Pie Chart</span>-->
    <#--<span class="caption-helper">bar and line chart mix</span>-->
    <#--</div>-->
    <#--<div class="tools"></div>-->
    <#--</div>-->
    <#--<div class="portlet-body">-->
    <#--<div id="chart4" style="height: 350px;">-->
    <#--</div>-->
    <#--</div>-->
    <#--</div>-->
    <#--</div>-->
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze">产品收益发放统计表</span>
                        <span class="caption-helper">单位：元</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div id="chart6" style="height: 350px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
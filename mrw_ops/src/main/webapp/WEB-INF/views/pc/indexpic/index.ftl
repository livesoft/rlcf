<@override name="view_body">

<div class="page-content" id="view_content" data-view="indexpic">
    <div class="page-head">
        <div class="page-title">
            <h1>首页轮播 <small>首页轮播管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">首页轮播</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">首页轮播</span>
                        <span class="caption-helper">首页轮播列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">标题:</label>
                            <input type="text" class="form-control" id="s_title" placeholder="请输入标题名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-left"><a id="new" href="${ctx}/pc/indexpic/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="indexpic_dt"
                                   data-ajax="${ctx}/pc/indexpic/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width100">标题</th>
                                    <th class="width200">链接地址</th>
                                    <th class="width100">图片</th>
                                    <th class="width40">排序</th>
                                    <th class="width150">创建时间</th>
                                    <th class="width120">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
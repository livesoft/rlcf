<@override name="view_body">

<div class="page-content" id="view_content" data-view="indexpic#item">
    <div class="page-head">
        <div class="page-title">
            <h1>首页轮播
                <small>${(action=='create')?string('创建','编辑')}首页轮播</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/pc/indexpic">首页轮播</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}首页轮播</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">首页轮播</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}首页轮播</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/pc/indexpic/save" method="post" id="indexpic_form">
                        <input type="hidden" name="indexpic.id" value="${indexpic.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="180" name="indexpic.title" value="${indexpic.title!}"
                                           class="form-control required" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="href_url">
                                    链接地址
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="href_url" maxlength="480" name="indexpic.href_url" value="${indexpic.href_url!}"
                                           class="form-control" autocomplete="off">
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 轮播图片
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(indexpic.pic_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 显示排序
                                </label>
                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(indexpic.sort)!1}" data-step="1" data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="indexpic.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/pc/indexpic" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
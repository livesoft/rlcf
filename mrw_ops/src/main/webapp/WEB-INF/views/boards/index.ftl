<@override name="view_body">

<div class="page-content" id="view_content" data-view="boards">
    <div class="page-head">
        <div class="page-title">
            <h1>私人定制 <small>私人定制管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">私人定制</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">私人定制</span>
                        <span class="caption-helper">私人定制列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class="" for="s_name">联系电话:</label>
                            <input type="text" class="form-control" id="s_phone" placeholder="请输入联系电话">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="boards_dt"
                                   data-ajax="${ctx}/boards/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width60">姓名</th>
                                    <th class="width60">联系电话</th>
                                    <th class="width150">定制内容</th>
                                    <th class="width100">定制时间</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
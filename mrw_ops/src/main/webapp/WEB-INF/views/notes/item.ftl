<@override name="view_body">

<div class="page-content" id="view_content" data-view="notes#item">
    <div class="page-head">
        <div class="page-title">
            <h1>协议
                <small>${(action=='create')?string('创建','编辑')}协议</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/notes">协议</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}协议</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">协议</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}协议</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/notes/save" method="post" id="notes_form">
                        <input type="hidden" name="notes.id" value="${notes.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="name" maxlength="180" name="notes.name" value="${notes.name!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="category">
                                    <span class="required"> * </span> 类别
                                </label>

                                <div class="col-md-4">
                                    <input type="text" readonly id="category" name="notes.category" value="${notes.category!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    协议内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="notes.note" type="text/plain">${notes.note!}</script>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/notes" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="notes">
    <div class="page-head">
        <div class="page-title">
            <h1>协议 <small>协议管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">协议</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">协议</span>
                        <span class="caption-helper">协议列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class="" for="s_name">标题:</label>
                            <input type="text" class="form-control" id="s_name" placeholder="请输入协议标题">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="notes_dt"
                                   data-ajax="${ctx}/notes/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width200">标题</th>
                                    <th class="width100">类别</th>
                                    <th class="width200">发布时间</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
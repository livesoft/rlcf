<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearingRule">
    <div class="page-head">
        <div class="page-title">
            <h1>产品结算默认规则管理 <small>产品结算默认规则管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品结算默认规则管理</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品结算默认规则管理</span>
                        <span class="caption-helper">产品结算默认规则管理列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="clearingRule_dt"
                                   data-ajax="${ctx}/clearingRule/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width200">收益方式</th>
                                    <th class="width100">结算周期</th>
                                    <th class="width60">结算日</th>
                                    <th class="width100">结算点</th>
                                    <th class="width120">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
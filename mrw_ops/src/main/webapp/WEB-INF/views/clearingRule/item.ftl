<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearingRule#item">
    <div class="page-head">
        <div class="page-title">
            <h1>产品结算规则
                <small>${(action=="create")?string("创建","编辑")}产品结算规则</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/rule">产品结算规则</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("创建","编辑")}机构</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">产品结算规则</span>
                        <span class="caption-helper">${(action=="create")?string("创建","编辑")}机构</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/clearingRule/save" method="post" id="rule_form">
                        <input type="hidden" name="rule.id" value="${rule.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="product_type">
                                    <span class="required"> * </span> 收益方式
                                </label>

                                <div class="col-md-4">
                                    <p class="form-control-static ">
                                        <#if rule.product_type?? && rule.product_type == 1><span class="product_type" p_v="${(rule.product_type)!}">一次性还本付息<span></#if>
                                        <#if rule.product_type?? && rule.product_type == 2><span class="product_type" p_v="${(rule.product_type)!}">每月等额本息<span></#if>
                                        <#if rule.product_type?? && rule.product_type == 3><span class="product_type" p_v="${(rule.product_type)!}">本息滚动投资<span></#if>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_cycle">
                                    <span class="required"> * </span> 结算周期
                                </label>

                                <div class="col-md-4">
                                    <p class="form-control-static ">
                                        <#if rule.clearing_cycle?? && rule.clearing_cycle == 1>每月</#if>
                                        <#if rule.clearing_cycle?? && rule.clearing_cycle == 2>每日</#if>
                                        <#if rule.clearing_cycle?? && rule.clearing_cycle == 0>到期结算</#if>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_day">
                                    <span class="required"> * </span> 结算日
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="clearing_day" name="rule.clearing_day" value="${rule.clearing_day!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="clearing_time">
                                    <span class="required"> * </span> 结算点
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="clearing_time" name="rule.clearing_time" value="${rule.clearing_time!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/clearingRule" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
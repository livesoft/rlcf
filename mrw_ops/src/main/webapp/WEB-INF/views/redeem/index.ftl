<@override name="view_body">

<div class="page-content" id="view_content" data-view="redeem">
    <div class="page-head">
        <div class="page-title">
            <h1>会员退单审核</h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">会员退单审核</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员退单审核</span>
                        <span class="caption-helper">会员退单列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline">
                            <label class="" for="s_name">产品名称:</label>
                            <input type="text" class="form-control input-medium" id="s_product_name" placeholder="请输入产品名称">
                            <label class="" for="s_orderno">订单号:</label>
                            <input type="text" class="form-control input-medium" id="s_orderno" placeholder="请输入订单号">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="redeem_dt"
                                   data-ajax="${ctx}/redeem/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th >产品</th>
                                    <th class="width120">会员名称</th>
                                    <th class="width120">电子账户</th>
                                    <th class="width60">金额</th>
                                    <th class="width135">订单号</th>
                                    <th class="width125">退单时间</th>
                                    <th class="width50">状态</th>
                                    <th class="width120">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="dashboard">
    <div class="page-head">
        <div class="page-title">
            <h1>系统工作台
                <small>统计 & 报告</small>
            </h1>
        </div>
    </div>
    <div class="row margin-top-10">
        <@shiro.hasPermission name="index_member_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="member">
                    <div class="visual">
                        <i class="fa fa-group fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${member_cnt!0}</div>
                        <div class="desc">会员总数</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="index_product_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="product">
                    <div class="visual">
                        <i class="fa fa-briefcase fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${product_cnt!0}</div>
                        <div class="desc">理财产品数</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="index_buy_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="order">
                    <div class="visual">
                        <i class="fa fa-briefcase fa-icon-medium"></i>
                    </div>
                    <div class="details">
                        <div class="number">${buy_cnt!0}</div>
                        <div class="desc">投资总笔数</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="index_sale_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="transfer/product">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">${transfer_product_cnt!0}</div>
                        <div class="desc">挂单笔数</div>
                    </div>
                </a>

            </div>
        </@shiro.hasPermission>
    </div>
    <div class="row margin-top-10">
        <@shiro.hasPermission name="index_total_sum">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light red-intense" href="order">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">${buy_amount_sum!0}</div>
                        <div class="desc">总投资金额</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="index_total_income">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">${profit_sum!0}</div>
                        <div class="desc">总发放收益额</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>

        <@shiro.hasPermission name="index_buyer_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">${buy_person_cnt!0}</div>
                        <div class="desc">总投资人数</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>
    </div>

    <div class="row margin-top-10">

        <@shiro.hasPermission name="index_company_count">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light  yellow" href="pc/report">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">江苏银行</div>
                        <div class="desc">发行机构</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-light  yellow" href="pc/report">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">兴业银行</div>
                        <div class="desc">发行机构</div>
                    </div>
                </a>
            </div>
        </@shiro.hasPermission>
    </div>
    <div class="clearfix">
    </div>

    <@shiro.hasPermission name="index_visit_chat">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>

                            <span class="caption-subject bold uppercase font-green-haze"> 访问统计表</span>
                            <span class="caption-helper">单位：元</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="icon-list" data-original-title="" title="更多">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart1" style="height: 350px;">
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="index_deal_chat">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>

                            <span class="caption-subject bold uppercase font-green-haze">平台成交额统计表</span>
                            <span class="caption-helper">单位：元</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="icon-list" data-original-title="" title="更多"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart2" style="height: 350px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </@shiro.hasPermission>


    <div class="row">
        <@shiro.hasPermission name="index_brand_chat">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>

                            <span class="caption-subject bold uppercase font-green-haze"> 品牌销售组成统计</span>
                            <span class="caption-helper">单位：元</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="icon-list" data-original-title="" title="更多"></a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: block;">
                        <div id="chart3" style="height: 370px;">
                        </div>
                    </div>
                </div>
            </div>
        </@shiro.hasPermission>


        <@shiro.hasPermission name="index_company_chat">
            <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>

                            <span class="caption-subject bold uppercase font-green-haze"> 发行机构销售组成统计</span>
                            <span class="caption-helper">单位：元</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="icon-list" data-original-title="" title="更多"></a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: block;">
                        <div id="chart4" style="height: 370px;">
                        </div>
                    </div>
                </div>
            </div>
        </@shiro.hasPermission>
    </div>

    <@shiro.hasPermission name="index_platincome_chat">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>

                            <span class="caption-subject bold uppercase font-green-haze">平台收益发放统计表</span>
                            <span class="caption-helper">单位：元</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="icon-list" data-original-title="" title="更多"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart5" style="height: 350px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </@shiro.hasPermission>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>

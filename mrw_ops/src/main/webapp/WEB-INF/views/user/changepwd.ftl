<@override name="view_body">

<div class="page-content" id="view_content" data-view="changepwd">
    <div class="page-head">
        <div class="page-title">
            <h1>密码修改
                <small>密码修改设置</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">密码修改</a>
        </li>
    </ul>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">密码修改</span>
                    <span class="caption-helper">密码修改</span>
                </div>
            </div>

            <div class="portlet-body form">
                <form chagepwd="form" class="form-horizontal validate-form ajax-form" action="${ctx}/user/changepwd_save" method="post" id="chagepwd_form">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="old_pwd">
                                <span class="required"> * </span>原始密码
                            </label>
                            <div class="col-md-4">
                                <input type="password" id="old_pwd" name="old_pwd"
                                       class="form-control required" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3" for="new_pwd">
                                <span class="required"> * </span>新密码
                            </label>
                            <div class="col-md-4">
                                <input type="password" id="new_pwd" name="new_pwd"
                                       class="form-control required" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3" for="confirm_pwd">
                                <span class="required"> * </span>确认密码
                            </label>
                            <div class="col-md-4">
                                <input type="password" id="confirm_pwd" name="confirm_pwd"
                                       class="form-control required eq_pwd" autocomplete="off" p_v="#new_pwd">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary blue">保存</button>
                                <a href="${ctx}" class="btn btn-default default">取消</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
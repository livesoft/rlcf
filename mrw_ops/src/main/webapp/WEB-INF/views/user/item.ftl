<@override name="view_body">

<div class="page-content" id="view_content" data-view="user#item">
    <div class="page-head">
        <div class="page-title">
            <h1>系统配置
                <small>${(action=='create')?string('新建','编辑')}管理员</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/user">管理员列表</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('新建','编辑')}管理员</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">管理员列表</span>
                        <span class="caption-helper">${(action=='create')?string('新建','编辑')} 管理员</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/user/save" method="post" id="user_form">
                        <input type="hidden" name="user.id" value="${user.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="username">
                                    <span class="required"> * </span> 用户名称
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="username" name="user.username"
                                           data-required="1"
                                           data-msg-remote="您输入的用户名已经使用，请重新输入"
                                           value="${user.username!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="nickname">
                                    真实姓名
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="nickname" name="user.nickname" value="${user.nickname!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <#if action=='create'>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="password">
                                        <span class="required"> * </span> 登录密码
                                    </label>

                                    <div class="col-md-4">
                                        <input type="password" id="password" name="password" value=""
                                               class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3" for="retry_password">
                                        <span class="required"> * </span> 确认密码
                                    </label>

                                    <div class="col-md-4">
                                        <input type="password" id="retry_password" name="retry_password" value=""
                                               class="form-control eq_pwd" autocomplete="off" p_v="#password">
                                    </div>
                                </div>
                            </#if>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="gender">
                                     性别
                                </label>

                                <div class="col-md-4">
                                    <div class="radio-list required" id="gender">
                                        <label class="radio-inline">
                                            <input type="radio" name="user.gender" <#if user.gender?? && user.gender == 1> checked </#if> value="1"/> 男 </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="user.gender" <#if user.gender?? && user.gender == 2> checked </#if> value="2"/> 女 </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="email">
                                    常用邮箱
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="email" name="user.email" value="${user.email!}"
                                           class="form-control email" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="phone">
                                    联系电话
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="phone" name="user.phone" value="${user.phone!}"
                                           class="form-control isTel" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="position">
                                    职位
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="position" name="user.position" value="${user.position!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">选择角色</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <#list roles! as r>
                                                <label><input type="checkbox" name="user.role" value="${r.id!}" <#if user.role?? && (user.role?contains(r.id))>checked </#if>>${r.name!}</label>
                                            </#list>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/user" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
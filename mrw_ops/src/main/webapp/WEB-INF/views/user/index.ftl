<@override name="view_body">

<div class="page-content" id="view_content" data-view="user">
    <div class="page-head">
        <div class="page-title">
            <h1>系统配置 <small>管理员列表</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">管理员列表</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">管理员列表</span>
                        <#--<span class="caption-helper">管理员列表</span>-->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">用户名称:</label>
                            <input type="text" class="form-control" id="s_username" placeholder="用户名称">
                            <label class="" for="s_name">联系电话:</label>
                            <input type="text" class="form-control" id="s_phone" placeholder="联系电话">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                        <div class="pull-left"><a id="new" href="${ctx}/user/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="user_dt"
                                   data-ajax="${ctx}/user/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width80">用户名称</th>
                                    <th class="width140">联系电话</th>
                                    <th>昵称</th>
                                    <th class="width70">电子邮箱</th>
                                    <th class="width135">状态</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
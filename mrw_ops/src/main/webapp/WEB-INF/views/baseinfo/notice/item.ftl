<@override name="view_body">

<div class="page-content" id="view_content" data-view="notice#item">
    <div class="page-head">
        <div class="page-title">
            <h1>公告管理
                <small>公告管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/baseinfo/notice">公告管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("创建","编辑")}公告</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">公告管理</span>
                        <span class="caption-helper">${(action=="create")?string("创建","编辑")}公告</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/baseinfo/notice/save" method="post" id="notice_form">
                        <input type="hidden" name="notice.id" value="${notice.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 公告标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="160" name="notice.title" value="${notice.title!}"
                                           class="form-control" autocomplete="off" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    公告内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="notice.content"
                                            type="text/plain">${notice.content!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 公告图标
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(notice.pic_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 多附件
                                </label>

                                <div class="col-md-4">
                                    <div id="multi-container" class="no-p" data-name="notice.attachments" data-value="${(notice.attachments)!}"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/baseinfo/notice" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
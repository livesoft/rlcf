<@override name="view_body">

<div class="page-content" id="view_content" data-view="ismg">
    <div class="page-head">
        <div class="page-title">
            <h1>短信网关
                <small>短信网关设置</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">短信网关设置</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase"> 系统设置 </span>
                        <span class="caption-helper"> 短信网关设置 </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form ajax-form" action="${ctx}/baseinfo/ismg/save" method="post"
                          id="zone_form">
                        <input type="hidden" name="ismg.id" value="${(ismg.id)!}">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 网关地址
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="code" name="ismg.url"
                                           value="${(ismg.url)!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="username">
                                    <span class="required"> * </span> 登录名
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="username" name="ismg.username"
                                           value="${(ismg.username)!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="password">
                                    <span class="required"> * </span> 登录密码
                                </label>

                                <div class="col-md-4">
                                    <input type="password" id="password" name="ismg.password"
                                           value="${(ismg.password)!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="port">网关端口</label>

                                <div class="col-md-4">
                                    <input type="text" id="port" name="ismg.port"
                                           value="${(ismg.port)!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="tel">联系电话 </label>

                                <div class="col-md-4">
                                    <input type="text" id="tel" name="ismg.tel"
                                           value="${(ismg.tel)!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">
<div class="page-content" id="view_content" data-view="dict">
    <div class="page-head">
        <div class="page-title">
            <h1>系统设置
                <small>字典维护</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">字典维护</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-6">

            <section class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-tree font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp">数据字典树</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <ul id="dict_tree" class="ztree" style="width:360px; overflow:auto;"></ul>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp">字典信息</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form-horizontal" action="${ctx}/baseinfo/dict/save" method="post"
                          id="dict_form">
                        <input type="hidden" value="" name="d.id" id="id">
                        <input type="hidden" value="0" name="d.parent" id="parent">

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span> 字典编码
                                </label>

                                <div class="col-md-9">
                                    <input type="text" id="code" name="d.code"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 字典值
                                </label>

                                <div class="col-md-9">
                                    <input type="text" id="name" name="d.name"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="chinese_name">
                                    中文名称
                                </label>

                                <div class="col-md-9">
                                    <input type="text" id="chinese_name" name="d.chinese_name"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="sort">
                                    显示次序
                                </label>

                                <div class="col-md-9">
                                    <div class="spinnerme"  data-step="1"
                                         data-min="1" data-max="120">
                                        <div class="input-group">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="d.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="bank#item">
    <div class="page-head">
        <div class="page-title">
            <h1>公告管理
                <small>公告管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/baseinfo/bank">银行信息维护</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("创建","编辑")} 银行信息</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">银行信息</span>
                        <span class="caption-helper">${(action=="create")?string("新增","编辑")}银行信息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/baseinfo/bank/save" method="post" id="notice_form">
                        <input type="hidden" name="bank.id" value="${bank.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">银行名称
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="name" name="bank.name" value="${bank.name!}"
                                           class="form-control" autocomplete="off" required>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo">银行LOGO</label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo"
                                         data-obj="${(bank.logo_attachment)!}" data-name="logo"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="intro">
                                    描述
                                </label>

                                <div class="col-md-6 ">
                                    <textarea class="form-control" name="bank.intro" id="intro">${bank.intro!}</textarea>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/baseinfo/bank" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="bank">
    <div class="page-head">
        <div class="page-title">
            <h1>银行信息
                <small>银行基本信息</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li><a href="#">银行信息</a></li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">银行信息管理</span>
                        <span class="caption-helper">银行信息列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <a id="new" href="${ctx}/baseinfo/bank/create" class="btn blue">
                            <i class="icon-plus mr5"></i>新增</a>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="bank_dt"
                                   data-ajax="${ctx}/baseinfo/bank/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width100">名称</th>
                                    <th >图片</th>
                                    <th class="width120">创建时间</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
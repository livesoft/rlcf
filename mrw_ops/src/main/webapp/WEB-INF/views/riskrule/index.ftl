<@override name="view_body">

<div class="page-content" id="view_content" data-view="riskrule">
    <div class="page-head">
        <div class="page-title">
            <h1>风险等级 <small>风险等级关联管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">风险等级关联</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">风险等级关联</span>
                        <span class="caption-helper">风险等级关联列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="riskrule_dt"
                                   data-ajax="${ctx}/riskrule/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th>风险评级</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
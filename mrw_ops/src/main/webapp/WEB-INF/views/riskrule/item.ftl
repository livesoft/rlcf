<@override name="view_body">

<div class="page-content" id="view_content" data-view="riskrule#item">
    <div class="page-head">
        <div class="page-title">
            <h1>风险等级
                <small>风险等级</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/riskrule">风险等级</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">风险等级</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">风险等级</span>
                        <span class="caption-helper">风险等级</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/riskrule/save" method="post" id="riskrule_form">
                        <input type="hidden" name="rule.risk_tolerance" value="${rule.risk_tolerance!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 风险等级
                                </label>
                                <p class="form-control-static">
                                    ${rule.risk_tolerance_name!}
                                </p>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="risk_level">
                                    <span class="required"> * </span> 产品风险
                                </label>

                                <div class="col-md-4">
                                    <select data-placeholder="请选择风险承受能力" multiple id="risk_level" name="risk_level"
                                            class="select2me form-control required">
                                        <option value=""></option>
                                        <#list risk_level! as g>
                                            <option value="${g.code!}" <#if rule.risk_level?? && rule.risk_level?contains(g.code)>selected </#if>>${g.name!}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="optionsRadios5">
                                    <span class="required"> * </span> 方式
                                </label>

                                <div class="col-md-4">
                                    <div class="radio-list">
                                        <#--<label class="radio-inline">-->
                                            <#--<input type="radio" name="rule.type" id="optionsRadios4" dv="deal_size" value="1" <#if rule.type?? && rule.type == "1">checked </#if>> 次数 </label>-->

                                        <label class="radio-inline">
                                            <input type="radio" name="rule.type" id="optionsRadios5" dv="amount" value="2" <#if rule.amount?? && rule.type == "2">checked </#if>> 金额
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3" for="deal_size">
                            </label>
                            <div class="col-md-4">
                                <input type="number" id="deal_size" name="rule.deal" value="${(rule.deal)!}"
                                       class="form-control required my_input" autocomplete="off" >
                                <input type="number" id="amount" name="rule.amount" value="${(rule.amount)!}"
                                       class="form-control required my_input" autocomplete="off" >
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/riskrule" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
</@override>


<@extends name="*/_layout/admin/basic.ftl"></@extends>
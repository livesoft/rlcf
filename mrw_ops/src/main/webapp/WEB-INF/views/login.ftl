<#compress >
<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>融理财富平台－后台维护登录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="${ctx}/static/styles/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/static/styles/simple-line-icons.min.css">
    <link rel="stylesheet" href="${ctx}/static/styles/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/styles/select2.css">
    <link rel="stylesheet" href="${ctx}/static/styles/uniform.default.css">
    <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-fileinput.css">
    <link rel="stylesheet" href="${ctx}/static/styles/dataTables.bootstrap.css">
    <link rel="stylesheet" href="${ctx}/static/styles/datepicker3.css">
    <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="${ctx}/static/styles/clockface.css">
    <link rel="stylesheet" href="${ctx}/static/styles/daterangepicker-bs3.css">
    <link rel="stylesheet" href="${ctx}/static/styles/dropzone.css">
    <link rel="stylesheet" href="${ctx}/static/styles/jquery.fancybox.css">
    <link rel="stylesheet" href="${ctx}/static/styles/components.css">
    <link rel="stylesheet" href="${ctx}/static/styles/plugins.css">
    <link rel="stylesheet" href="${ctx}/static/styles/layout.css">
    <link rel="stylesheet" href="${ctx}/static/styles/light.css">
    <link rel="stylesheet" href="${ctx}/static/styles/ztree_metro.css">
    <link rel="stylesheet" href="${ctx}/static/styles/define.css">
    <link href="${ctx}/static/styles/login.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        var g = {ctx: '${ctx}/', 'assetsPath': '${ctx}/static/'};
    </script>
</head>
<body class="login">
<div class="menu-toggler sidebar-toggler">
</div>
<div class="logo">
    <a href="${ctx}">
        <img src="${ctx}/static/img/logo-big.png" alt=""/>
    </a>
</div>
<div class="content">
    <form class="login-form" action="${ctx}/login" method="post">
        <h3 class="form-title">管理登录</h3>

        <div class="alert alert-danger <#if !message??>display-hide</#if> ">
            <button class="close" data-close="alert"></button>
			<span> ${message!'请输入用户名称和登录密码. '}</span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">用户名称：</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
                   placeholder="用户名称" name="username" value="${username!}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">登录密码：</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   placeholder="登录密码" name="password"/>
        </div>
        <div class="form-actions text-center">
            <button type="submit" class="btn btn-success">登 录</button>
        </div>

    </form>
</div>
<div class="copyright">
    2015 © Rongli. 融理财富平台运维管理系统.
</div>
<!--[if lt IE 9]>
<script src="${ctx}/static/scripts/ie/respond.min.js"></script>
<script src="${ctx}/static/scripts/ie/excanvas.min.js"></script>
<![endif]-->
<script src="${ctx}/static/scripts/framework.min.js" type="text/javascript"></script>
<script src="${ctx}/static/scripts/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        MbaJF.init();
        Layout.init();

        $('.login-form').validate({
            errorElement: 'span',
            errorClass  : 'help-block',
            focusInvalid: false,
            rules       : {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: "请输入用户名称。"
                },
                password: {
                    required: "请输入密码。"
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });


        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                var $login = $('.login-form');
                if ($login.validate().form()) {
                    $login.submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    });
</script>
</body>
</html>
</#compress>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="news#item">
    <div class="page-head">
        <div class="page-title">
            <h1>理财资讯
                <small>${(action=='create')?string('创建','编辑')}理财资讯</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/news">理财资讯</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}理财资讯</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财资讯</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}理财资讯</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/news/save" method="post" id="news_form">
                        <input type="hidden" name="news.id" value="${news.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 资讯标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="160" name="news.title" value="${news.title!}"
                                           class="form-control" autocomplete="off" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>点击类型
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择点击类型" id="click_mode" name="news.click_mode"
                                            class="select2me form-control required">
                                        <option value="H5" <#if news.click_mode?? && news.click_mode=="H5">selected="selected" </#if>>HTML5页面</option>
                                        <option value="PRODUCT" <#if news.click_mode?? && news.click_mode=="PRODUCT">selected="selected" </#if>>产品</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>置顶
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="是否指定" id="top_flag" name="news.top_flag"
                                            class="select2me form-control required">
                                        <option value="N" <#if news.top_flag?? && news.top_flag=="N">selected="selected" </#if>>否</option>
                                        <option value="Y" <#if news.top_flag?? && news.top_flag=="Y">selected="selected" </#if>>是</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    资讯内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="news.content" type="text/plain">${news.content!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 资讯图标
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(news.pic_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 多附件
                                </label>

                                <div class="col-md-4">
                                    <div id="multi-container" class="no-p" data-name="news.top_attachments" data-value="${(news.top_attachments)!}"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/news" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="merchant">
    <div class="page-head">
        <div class="page-title">
            <h1>账户管理 </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">账户管理</a>
        </li>
    </ul>

    <div class="row">

    <@shiro.hasPermission name="charging">
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-purple-plum">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 提现</span>
                        <span class="caption-helper">账户提现功能</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form id="extract_form" action="${ctx}/merchant/withdrawals" data-msg-success="提现成功" class="form-horizontal" method="post">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="amount">提现金额</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required number" id="amount" name="amount" value=""  placeholder="" >
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="edesc">提现备注</label>
                                    <div class="col-md-8">
                                        <textarea name="desc" rows="3" class="form-control required" id="edesc" maxlength="120"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="extract_btn" class="btn green">确定</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="withdrawals">
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-purple-plum">
                        <i class="icon-speech"></i>
                        <span class="caption-subject bold uppercase"> 充值</span>
                        <span class="caption-helper">账户充值功能</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form id="recharge_form" action="${ctx}/merchant/charging" data-msg-success="充值成功" class="form-horizontal" method="post">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">充值金额</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="number" min="0" class="form-control required number" name="amount" value=""  placeholder="" >
                                            <span class="input-group-addon"><i class="fa">元</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="edesc">充值备注</label>
                                    <div class="col-md-8">
                                        <textarea name="desc" rows="3" class="form-control required" id="edesc" maxlength="120"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn green">确定</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

    </@shiro.hasPermission>
    </div>

</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
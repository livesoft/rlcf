<@override name="view_body">

<div class="page-content" id="view_content" data-view="merchant#record">
    <div class="page-head">
        <div class="page-title">
            <h1>系统设置 <small>账户纪录</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">账户纪录</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">账户纪录</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline">
                            <label class="" for="start_end">操作时间:</label>
                                <input type="text" class="form-control input-medium" id="start_end" name="start_end" value="" readonly>

                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="mechant_dt"
                                   data-ajax="${ctx}/merchant/recordlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30">序号</th>
                                    <th class="width140">类型</th>
                                    <th class="width80">金额</th>
                                    <th>备注</th>
                                    <th class="width80">状态</th>
                                    <th class="width180">操作时间</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
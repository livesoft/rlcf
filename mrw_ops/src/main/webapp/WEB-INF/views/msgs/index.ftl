<@override name="view_body">

<div class="page-content" id="view_content" data-view="msgs">
    <div class="page-head">
        <div class="page-title">
            <h1>消息管理 <small>消息管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">消息管理</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">消息管理</span>
                        <span class="caption-helper">消息列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class="" for="s_name">接收人姓名:</label>
                            <input type="text" class="form-control" id="s_nick_name" placeholder="请输入接收人姓名">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                        <div class="pull-right"><a id="new" href="${ctx}/msgs/create" class="btn blue">
                            <i class="icon-plus mr5"></i>发送消息</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="msgs_dt"
                                   data-ajax="${ctx}/msgs/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width100">消息标题</th>
                                    <th class="width200">接收人</th>
                                    <th class="width100">发送时间</th>
                                    <th class="width100">阅读时间</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
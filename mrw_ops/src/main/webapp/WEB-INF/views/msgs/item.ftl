<@override name="view_body">

<div class="page-content" id="view_content" data-view="msgs#item">
    <div class="page-head">
        <div class="page-title">
            <h1>系统消息
                <small>${(action=='create')?string('创建','编辑')}系统消息</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/msgs">系统消息</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}系统消息</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">系统消息</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}系统消息</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/msgs/save" method="post" id="msgs_form">
                        <input type="hidden" name="msgs.id" value="${msgs.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 消息标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" name="msgs.title" value="${msgs.title!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 消息封面
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(msgs.pic_attachment)!}" data-name="pic_attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    消息内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="msgs.content" type="text/plain">${msgs.content!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    接收人
                                </label>

                                <div class="col-md-6 ">
                                    <select data-placeholder="请选择发送用户" multiple id="receive" name="receive"
                                            class="select2me form-control required">
                                       <#list member! as m>
                                           <option value="${(m.id)!}">${(m.nick_name)!}</option>
                                       </#list>
                                    </select>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/msgs" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
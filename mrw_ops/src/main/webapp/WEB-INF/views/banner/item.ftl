<@override name="view_body">

<div class="page-content" id="view_content" data-view="banner#item">
    <div class="page-head">
        <div class="page-title">
            <h1>焦点图管理
                <small>${(action=='create')?string('创建','编辑')}焦点图</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/banner">焦点图管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("新建","编辑")}焦点图</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财中心</span>
                        <span class="caption-helper">理财产品</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/banner/save" method="post" id="banner_form">
                        <input type="hidden" name="banner.id" value="${(banner.id)!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 链接地址
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="click_url" name="banner.click_url" value="${(banner.click_url)!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    <span class="required"> * </span> 图片
                                </label>
                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-provides="fileinput" data-obj="${(banner.img_attachment)!}" data-name="img_attachment"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>产品
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择产品" id="product" name="banner.product"
                                            class="select2me form-control">
                                        <option value=""></option>
                                        <#list products! as p>
                                            <option value="${p.id!}" <#if banner.product?? && banner.product==p.id>selected="selected" </#if>>${p.name!}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>栏目
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择产品" id="section" name="banner.section"
                                            class="select2me form-control required">
                                       <option value="1" <#if banner.section?? && banner.section==1>selected="selected" </#if>>首页</option>
                                       <option value="2" <#if banner.section?? && banner.section==2>selected="selected" </#if>>理财中心</option>
                                       <option value="3" <#if banner.section?? && banner.section==3>selected="selected" </#if>>积分商城</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    <span class="required"> * </span>点击类型
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择点击类型" id="click_mode" name="banner.click_mode"
                                            class="select2me form-control required">
                                        <option value="H5" <#if banner.click_mode?? && banner.click_mode=="H5">selected="selected" </#if>>HTML5页面</option>
                                        <option value="PRODUCT" <#if banner.click_mode?? && banner.click_mode=="PRODUCT">selected="selected" </#if>>产品</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                     显示排序
                                </label>
                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(banner.sort)!1}" data-step="1" data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="banner.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/product" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
</div>
</@override>
<@override name="view_scripts">
    <script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">

    <link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
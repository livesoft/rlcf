<@override name="view_body">

<div class="page-content" id="view_content" data-view="banner">
    <div class="page-head">
        <div class="page-title">
            <h1>焦点图管理 <small>焦点图管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">焦点图管理</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">焦点图管理</span>
                        <span class="caption-helper">焦点图管理</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10 ">
                        <div class="pull-left"><a id="new" href="${ctx}/banner/create" class="btn blue ">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="banner_dt"
                                   data-ajax="${ctx}/banner/dtlist">
                                <thead>
                                    <th class="width30"></th>
                                    <th class="width100">图片</th>
                                    <th class="width100">链接地址</th>
                                    <th class="width100">栏目</th>
                                    <th class="width40">显示排序</th>
                                    <th class="width40">状态</th>
                                    <th class="width140">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
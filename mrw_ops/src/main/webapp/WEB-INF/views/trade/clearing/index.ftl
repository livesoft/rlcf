<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearing${type!}">
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算 <small>${title!}</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${title!}</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold">${title!}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <div class="btn-group  pull-right">

                            <#if type == 7>
                            <#--产品募集到期结算清单-->

                                <@shiro.hasPermission name="platform_clearing_confirm">
                                    <a id="confirm_btn" href="javascript:void(0);"
                                       class="btn default"><i
                                            class="icon-control-end mr5"></i>财务审核</a>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="platform_clearing_ok">
                                    <a id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca"><i
                                            class="icon-control-end mr5"></i>领导审批</a>
                                </@shiro.hasPermission>
                            </#if>
                        </div>
                        <form action="${ctx}/trade/clearing/export/${type!}" class="form-inline pull-left">
                            <#if type == 8>
                                <input type="text" class="form-control input-medium" id="s_date" placeholder="请选择日期">
                            <#else>
                                <input type="text" class="form-control" id="s_name" placeholder="请输入产品名称">
                                <select id="isuers" class=" form-control select2me" data-placeholder="请选择发行机构">
                                    <option value=""></option>
                                    <#list issers as se>
                                        <option value="${se.id}">${se.name}</option>
                                    </#list>
                                </select>
                            </#if>
                            <#if type == 7>
                                <select id="status" class="form-control select2me" data-placeholder="请选择状态">
                                <option value=""></option>
                                <option value="0">待提现</option>
                                <option value="1">财务已审核</option>
                                <option value="2">领导已审批</option>
                                <option value="3">已提现</option>
                                <option value="4">提现失败</option>
                            </select></#if>
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe">
                                <i class="icon-magnifier mr5"></i>查询</a>
                            <#if type == 5>
                                <@shiro.hasPermission name="cancel_clearing_export">
                                    <button id="export_btn" type="submit" class="btn default green-stripe">
                                        <i class="fa fa-file-excel-o mr5"></i>导出待审核退单纪录</button>
                                </@shiro.hasPermission>
                            </#if>

                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="playmoney_dt"
                                   data-ajax="${ctx}/trade/clearing/dtlist/${type!}">
                                <thead>
                                    <#if type??>
                                    <tr class="heading">
                                        <th class="width30"></th>
                                        <#if type ==3 >
                                            <th>产品名称</th>
                                            <th class="width140">发行机构</th>
                                            <th class="width80">募集金额</th>
                                            <th class="width120">实际募集金额</th>
                                            <th class="width120">收益总计</th>
                                            <th class="width120">投资笔数</th>
                                            <th class="width120">投资人数</th>
                                            <th class="width155">操作</th>
                                        <#elseif type == 4>
                                            <th>产品名称</th>
                                            <th class="width140">发行机构</th>
                                            <th class="width80">募集金额</th>
                                            <th class="width120">转让单数</th>
                                            <th class="width120">转让人数</th>
                                            <th class="width155">操作</th>
                                        <#elseif type == 5>
                                            <th>产品名称</th>
                                            <th class="width140">发行机构</th>
                                            <th class="width80">募集金额</th>
                                            <th class="width120">退单金额</th>
                                            <th class="width120">退单人数</th>
                                            <th class="width155">操作</th>
                                        <#elseif type == 6>
                                            <th>产品名称</th>
                                            <th class="width140">发行机构</th>
                                            <th class="width80">募集金额</th>
                                            <th class="width120">投资笔数</th>
                                            <th class="width120">投资人数</th>
                                            <th class="width155">操作</th>
                                        <#elseif type == 7>
                                            <th>产品名称</th>
                                            <th class="width140">发行机构</th>
                                            <th class="width80">募集金额</th>
                                            <th class="width50">状态</th>
                                            <th class="width120">实际募集金额</th>
                                            <th class="width185">操作</th>
                                        <#elseif type == 8>
                                            <th>日期</th>
                                            <th class="width140">失败笔数</th>
                                            <th class="width80">投资人数</th>
                                            <th class="width50">总金额</th>
                                            <th class="width185">操作</th>
                                        <#else>
                                            <th class="width80">用户名称</th>
                                            <th class="width140">电子账号</th>
                                            <th>产品名称</th>
                                            <th class="width70">金额</th>
                                            <th class="width70">状态</th>
                                            <th class="width125">提交时间</th>
                                            <th class="width125">打款时间</th>
                                            <th class="width155">操作</th>
                                        </#if>
                                    </tr>
                                    </#if>

                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>
<@override name="view_scripts">
    <script type="text/javascript">
        var clearing_type = '${type!}';
        var platform_confirm= 0,platform_ok = 0;

            <#if type == 7>
                <@shiro.hasPermission name="platform_clearing_confirm">
                platform_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="platform_clearing_ok">
                platform_ok = 1;
                </@shiro.hasPermission>
            </#if>
    </script>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
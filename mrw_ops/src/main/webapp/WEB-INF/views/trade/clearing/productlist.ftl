<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearing7#productdtlist">
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算
                <small>${title!}</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/clearing/7">产品募集到期结算清单</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${product.name!}募集到期结算</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold"><a target="_blank" href="${ctx}/product/detail_view/${product.id!}">${product.name!}</a> 募集到期结算</span>
                    </div>

                    <@shiro.hasPermission name="platform_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/7-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                </div>

                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form class="form-inline">
                            <input type="text" class="form-control input-medium" id="s_order" placeholder="请输入订单号">
                            <input type="text" class="form-control input-medium" id="start_end" placeholder="点击选择投资时间"
                                   readonly>

                            <a id="search_btn" href="javascript:;" class="btn default green-stripe">
                                <i class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="productlist_dt"
                                   data-ajax="${ctx}/trade/clearing/productdtlist/${productId!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width120">用户名称</th>
                                    <th class="width140">电子账号</th>
                                    <th class="width140">投资时间</th>
                                    <th class="width70">投资金额</th>
                                    <th>订单号</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
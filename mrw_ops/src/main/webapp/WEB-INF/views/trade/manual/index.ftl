<@override name="view_body">

<div class="page-content" id="view_content" data-view="tradeMoney#">
    <div class="page-head">
        <div class="page-title">
            <h1>人工调账
                <small>理财产品</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/dashboard">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/clearing/${tradeMoney.type!}">${title}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">人工调账</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="portlet grey-cascade box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>基本信息
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            用户名称:
                        </div>
                        <div class="col-md-7 value">
                            ${tradeMoney.real_name!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            电子账号:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.electronic_account!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            产品名称:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.product_name!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            金额:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.amount!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            本金:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.principal!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            收益:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.profit!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            提交时间:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.submit_time!}
                        </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-5 name">
                            订单号:
                        </div>
                        <div class="col-md-7 value">
                        ${tradeMoney.trade_no!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="portlet green-meadow box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>调账信息
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form role="form" class="form-horizontal validate-form" action="${ctx}/trade/manual/save"
                              method="post"                              id="tradeMoney_form">
                            <input type="hidden" value="${back!}" name="back_url">
                            <input type="hidden" name="tradeMoney.id" value="${(tradeMoney.id)!}">
                            <div class="form-group">
                                <label class="control-label col-md-3">调整方式</label>

                                <div class="col-md-9">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="tradeMoney.adjust_mode" checked value="1"
                                                   class="required"> 增加（+）
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="tradeMoney.adjust_mode"
                                                   class="required" value="2"> 减少（-）
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">调整金额</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="number" min="0" class="form-control required" name="tradeMoney.adjust_amount" value=""
                                               id="sign_integral" placeholder="">
                                        <span class="input-group-addon"><i class="fa">元</i></span>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-primary blue">保存</button>
                                                <a href="${ctx}${back!}" class="btn btn-default default">取消</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="financecost">
    <div class="page-head">
        <div class="page-title">
            <h1>财务对账单 <small>财务对账单</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">财务对账单</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-shadow">
                <h4 class="block">银行对账说明</h4>
                <p> 电子账户上日余额 + 银行收入金额 - 银行支出金额 ＝ 电子账户金额 ， 等于表示<span class="badge badge-info">正常</span>  否则 <span class="badge badge-danger">异常</span></p>
                <p> 银行差异金额：等于电子账户金额 -（银行收入金额 - 银行支出金额 + 上日余额），正数代表比电子账户余额多，负数代表比电子账户余额少</p>
                <h4 class="block">平台对账说明</h4>

                <p> 电子账户上日余额 + 平台收入金额 - 平台支出金额 ＝ 电子账户金额 ，等于表示<span class="badge badge-info">正常</span>  否则 <span class="badge badge-danger">异常</span></p>
                <p> 平台差异金额：等于电子账户金额 - （平台收入金额 - 平台支出金额 + 上日余额），正数代表比电子账户余额多，负数代表比电子账户余额少</p>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">财务对账单</span>
                        <span class="caption-helper">财务对账单列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class= "">日期</label>

                            <input type="text" class="form-control input-medium" id="s_date" name="time" placeholder="请选择日期" readonly>

                            <a id="search_btn" href="javascript:void(0);" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                            <a id="index_export" data-href="${ctx}/trade/financecost/index_export" href="javascript:void(0);" class="btn default blue-stripe _export"><i
                                    class="icon-magnifier mr5"></i>导出</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="financecost_dt"
                                   data-ajax="${ctx}/trade/financecost/dtlist">
                                <thead>
                                <tr>
                                    <th>日期</th>
                                    <th class="width100">电子账户余额</th>
                                    <th class="width80">银行收入</th>
                                    <th class="width80">银行支出</th>
                                    <th class="width100">银行差异金额</th>
                                    <th class="width80">平台收入</th>
                                    <th class="width80">平台支出</th>
                                    <th class="width100">平台差异金额</th>
                                    <th class="width120">电子账户上日余额</th>
                                    <th class="width80">平台对账结果</th>
                                    <th class="width80">银行对账结果</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
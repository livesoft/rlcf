<@override name="view_body">

<div class="page-content" id="view_content" data-view="playmoney#" >
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/playmoney">打款清单</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">结算详情</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <#if edit>
                <div class="note note-success note-shadow">
                    <h4 class="block">温馨提示</h4>

                    <p>
                    ${t.real_name!}的 ${t.product_name!} 的收益打款已打款或者已确认打款。
                    </p>
                </div>

            </#if>
                <div class="portlet light form-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">打款信息</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form role="form" class="form-horizontal validate-form" action="${ctx}/trade/playmoney/save"
                              method="post" id="user_form">
                            <input type="hidden" name="t.id" value="${t.id!}">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">产品名称：</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                ${t.product_name!}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">状态</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">待放放金额：</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">募集金额</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">收益额：</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">财务审核时间</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">审核人：</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">领导审批时间</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">审批人：</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">项目结束日期</label>

                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    Nilson
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-primary blue">财务审核</button>
                                        <button type="submit" class="btn btn-primary blue">领导审批</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </#if>

        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
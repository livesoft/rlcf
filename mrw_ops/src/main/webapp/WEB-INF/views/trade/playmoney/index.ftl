<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearing${type!}#playmoney">
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算
                <small>${title!}</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/clearing/${type!}">${title}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">结算清单</a>
        </li>
    </ul>

    <#if type == 8>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold">交易日期</span>
                        </div>
                        <@shiro.hasPermission name="payfail_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}?day=${day!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                        </@shiro.hasPermission>
                    </div>

                    <div class="portlet-body form">
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                交易日期:
                            </div>
                            <div class="col-md-4 value">
                            ${stat.day!}
                            </div>
                            <div class="col-md-2 name">
                                总金额:
                            </div>
                            <div class="col-md-4 value"> ${stat.amount!} 元</div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                交易笔数:
                            </div>
                            <div class="col-md-4 value"> ${stat.orders!} 笔</div>
                            <div class="col-md-2 name">
                                投资人数:
                            </div>
                            <div class="col-md-4 value">${stat.members!} 人
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <#else>

        <#if product??>
        <div class="row">
        <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold">产品信息</span>
                </div>
                <#if type == 6>
                    <@shiro.hasPermission name="fail_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                <#elseif type == 7>
                    <@shiro.hasPermission name="platform_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                <#elseif type == 3>
                    <@shiro.hasPermission name="expire_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                <#elseif type == 4>
                    <@shiro.hasPermission name="transfer_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                <#elseif type == 5>
                    <@shiro.hasPermission name="cancel_clearing_export">
                        <div class="actions">
                            <a href="${ctx}/trade/clearing/export/${type!}-${product.id!}" class="btn btn-default">
                                <i class="fa fa-file-excel-o"></i>
                                <span class="hidden-480">导出</span>
                            </a>
                        </div>
                    </@shiro.hasPermission>
                </#if>
            </div>

        <div class="portlet-body form">
            <div class="row static-info">
                <div class="col-md-2 name">产品名称: </div>
                <div class="col-md-4 value"> ${product.name!} </div>
                <div class="col-md-2 name">产品募集资金: </div>
                <div class="col-md-4 value"> ${product.price!} 元</div>
            </div>

            <#if type ==6>
                <div class="row static-info">
                    <div class="col-md-2 name"> 交易笔数: </div>
                    <div class="col-md-4 value"> ${product.si!} 笔</div>
                    <div class="col-md-2 name"> 投资人数: </div>
                    <div class="col-md-4 value">${product.members!} 人 </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-2 name">
                        退款金额:
                    </div>
                    <div class="col-md-4 value">${product.amount!} 元</div>
                </div>
            <#elseif  type==3>
                <div class="row static-info">
                    <div class="col-md-2 name">
                        交易笔数:
                    </div>
                    <div class="col-md-4 value"> ${product.si!} 笔</div>
                    <div class="col-md-2 name">
                        投资人数:
                    </div>
                    <div class="col-md-4 value">${product.members!} 人
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-2 name"> 发放金额: </div>
                    <div class="col-md-4 value">${product.amount!} 元 </div>
                    <div class="col-md-2 name"> 收益额: </div>
                    <div class="col-md-4 value"> ${product.profit!} 元 </div>
                    <div class="col-md-2 name"> 实际募集金额: </div>
                    <div class="col-md-4 value"> ${product.raised_mount!} 元 </div>
                </div>
            <#elseif  type==4>
                <div class="row static-info">
                    <div class="col-md-2 name">
                        转让笔数:
                    </div>
                    <div class="col-md-4 value"> ${product.si!} 笔</div>
                    <div class="col-md-2 name">
                        转让人数:
                    </div>
                    <div class="col-md-4 value">${product.members!} 人
                    </div>
                </div>

                <div class="row static-info">
                    <div class="col-md-2 name">
                        转让总金额:
                    </div>
                    <div class="col-md-4 value">${product.amount!} 元</div>
                </div>
            <#elseif  type==5>

                <div class="row static-info">
                    <div class="col-md-2 name">
                        退单笔数:
                    </div>
                    <div class="col-md-4 value"> ${product.si!} 笔</div>
                    <div class="col-md-2 name">
                        退单人数:
                    </div>
                    <div class="col-md-4 value">${product.members!} 人
                    </div>
                </div>

                <div class="row static-info">
                    <div class="col-md-2 name">
                        退单总金额:
                    </div>
                    <div class="col-md-4 value">${product.amount!} 元</div>
                </div>
            </div>
            </div>
            </div>
            </#if>
        </#if>

    </div>
    </#if>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold">${title!}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline ">
                            <input type="text" class="form-control input-mediumn" id="s_real_name"
                                   placeholder="请输入会员真实姓名">
                            <input type="text" class="form-control input-mediumn" id="s_order"
                                   placeholder="请输入订单号">
                            <input type="text" class="form-control input-medium" id="start_end" placeholder="点击选择时间"
                                   readonly>
                            <select id="status" class=" form-control select2me" data-placeholder="请选择状态">
                                <option value=""></option>
                                <option value="0">待打款</option>
                                <option value="1">财务已确认</option>
                                <option value="2">领导已批准</option>
                                <option value="3">已打款</option>
                                <option value="4">打款失败</option>
                            </select>
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe">
                                <i class="icon-magnifier mr5"></i>查询</a>

                        </form>
                    </div>
                    <div class="clearfix mb10">
                        <div class="btn-group">

                            <#if type == 3>
                                <@shiro.hasPermission name="expire_clearing_confirm">
                                    <button id="confirm_btn" href="javascript:void(0);"
                                       class="btn default "><i
                                            class="icon-control-end mr5"></i>财务审核</button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="expire_clearing_ok">
                                    <button id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca "><i
                                            class="icon-control-end mr5"></i>领导审批</button>
                                </@shiro.hasPermission>
                            <#elseif type == 4>
                            <#-- 转让 -->
                                <@shiro.hasPermission name="transfer_clearing_confirm">
                                    <button id="confirm_btn" href="javascript:void(0);"
                                       class="btn default "><i
                                            class="icon-control-end mr5"></i>财务审核</button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="transfer_clearing_ok">
                                    <button id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca "><i
                                            class="icon-control-end mr5"></i>领导审批</button>
                                </@shiro.hasPermission>

                            <#elseif type == 5>
                            <#-- 退单 -->
                                <@shiro.hasPermission name="cancel_clearing_confirm">
                                    <button id="confirm_btn" href="javascript:void(0);"
                                       class="btn default"><i
                                            class="icon-control-end mr5"></i>财务审核</button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="cancel_clearing_ok">
                                    <button id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca "><i
                                            class="icon-control-end mr5"></i>领导审批</button>
                                </@shiro.hasPermission>


                            <#elseif type == 6>
                            <#--募集失败-->

                                <@shiro.hasPermission name="fail_clearing_confirm">
                                    <button id="confirm_btn" href="javascript:void(0);"
                                       class="btn default"><i
                                            class="icon-control-end mr5"></i>财务审核</button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="fail_clearing_ok">
                                    <button id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca "><i
                                            class="icon-control-end mr5"></i>领导审批</button>
                                </@shiro.hasPermission>
                            <#elseif type == 8>
                            <#--募集失败-->

                                <@shiro.hasPermission name="payfail_clearing_confirm">
                                    <button id="confirm_btn" href="javascript:void(0);"
                                       class="btn default"><i
                                            class="icon-control-end mr5"></i>财务审核</button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="payfail_clearing_ok">
                                    <button id="ok_btn" href="javascript:void(0);"
                                       class="btn default yellow-casablanca "><i
                                            class="icon-control-end mr5"></i>领导审批</button>
                                </@shiro.hasPermission>
                            </#if>
                        </div>

                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">

                            <table class="table table-bordered table-striped table-condensed" id="playmoney_dt"
                                   <#if type == 8>data-ajax="${ctx}/trade/playmoney/dtlist/${type!}-${productId!}?day=${day!}"
                                   <#else>
                                   data-ajax="${ctx}/trade/playmoney/dtlist/${type!}-${productId!}"
                                   </#if> >
                                <thead>
                                    <#if type??>
                                    <tr class="heading">
                                        <th class="width30"></th>
                                        <#if type==1 || type ==2 || type ==3>
                                            <th class="width65">用户名称</th>
                                            <th class="width135">电子账号</th>
                                            <th>产品名称</th>
                                            <th class="width70">金额</th>
                                            <th class="width60">本金</th>
                                            <th class="width60">收益</th>
                                            <th class="width50">状态</th>
                                            <th class="width120">打款时间</th>
                                            <th class="width120">订单号</th>
                                            <th class="width60">调账</th>
                                        <#elseif type == 5>
                                            <th class="width65">用户名称</th>
                                            <th class="width135">电子账号</th>
                                            <th>产品名称</th>
                                            <th class="width60">本金</th>
                                            <th class="width50">状态</th>
                                            <th class="width120">打款时间</th>
                                            <th class="width120">订单号</th>
                                            <th class="width60">调账</th>
                                        <#else>
                                            <th class="width80">用户名称</th>
                                            <th class="width140">电子账号</th>
                                            <th>产品名称</th>
                                            <th class="width70">金额</th>
                                            <th class="width70">状态</th>
                                            <th class="width125">打款时间</th>
                                            <th class="width125">订单号</th>
                                            <th class="width60">调账</th>
                                        </#if>
                                        <th class="width155">操作</th>
                                    </tr>
                                    </#if>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>
<@override name="view_scripts">
    <script type="text/javascript">
        var type = '${type!}';
        var stime = '${date!?string('yyyy年MM月dd日')}';
        var day = '${day!}';
        var expire_confirm = 0, expire_ok = 0, expire_reconciliations= 0,
                cancel_confirm = 0, cancel_ok = 0,cancel_reconciliations = 0,
                fail_confirm = 0, fail_ok = 0,fail_reconciliations = 0,
                transfer_confirm = 0, transfer_ok = 0, transfer_reconciliations = 0,
                pay_fail_confirm = 0, pay_fail_ok = 0, payfail_reconciliations = 0;
            <#if type == 3>
                <@shiro.hasPermission name="expire_clearing_confirm">
                expire_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="expire_clearing_ok">
                expire_ok = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="expire_reconciliations">
                    expire_reconciliations = 1;
                </@shiro.hasPermission>
            <#elseif type == 4>
                <@shiro.hasPermission name="transfer_clearing_confirm">
                transfer_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="transfer_clearing_ok">
                transfer_ok = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="transfer_reconciliations">
                transfer_reconciliations = 1;
                </@shiro.hasPermission>
            <#elseif type == 5>
                <@shiro.hasPermission name="cancel_clearing_confirm">
                cancel_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="cancel_clearing_ok">
                cancel_ok = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="cancel_reconciliations">
                cancel_reconciliations = 1;
                </@shiro.hasPermission>
            <#elseif type == 6>
                <@shiro.hasPermission name="fail_clearing_confirm">
                fail_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="fail_clearing_ok">
                fail_ok = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="fail_reconciliations">
                fail_reconciliations = 1;
                </@shiro.hasPermission>
            <#elseif type == 8>
                <@shiro.hasPermission name="payfail_clearing_confirm">
                pay_fail_confirm = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="payfail_clearing_ok">
                pay_fail_ok = 1;
                </@shiro.hasPermission>
                <@shiro.hasPermission name="payfail_reconciliations">
                payfail_reconciliations = 1;
                </@shiro.hasPermission>
            </#if>
    </script>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
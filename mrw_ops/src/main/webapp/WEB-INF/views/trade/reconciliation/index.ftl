<@override name="view_body">

<div class="page-content" id="view_content" data-view="reconciliation">
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算 <small>银行对账单</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">银行对账单</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-shadow">
                <h4 class="block">对比结果说明</h4>

                <p> 账单一致： 表示平台账单与银行账单的金额及状态都一致； </p>

                <p> 差异账单： 表示平台账单与银行账单有差异； </p>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">银行对账单</span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="clearfix mb10">
                        <form action="${ctx}/trade/reconciliation/export" method="post" target="_blank" class="form-inline pull-left">
                            <label class="control-label p5">订单号:</label>
                            <input type="text" class="form-control input-medium" id="s_order" name="order" placeholder="请输入订单号">
                            <input type="text" class="form-control input-medium" id="s_date" name="time" placeholder="请选择交易日期" readonly>
                            <select id="type" class=" form-control select2me width100" name="type" data-placeholder="请选择订单类型">
                                <option value="">全部</option>
                                <option value="1">充值</option>
                                <option value="2">提现</option>
                                <option value="3">转账</option>
                                <option value="4">订单支付</option>
                            </select>
                            <label for="exp_check"><input type="checkbox" id="s_exp">差异订单 </label>
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i class="icon-magnifier mr5"></i>查询</a>
                            <button type="submit" class="btn default green-stripe"><i class="fa fa-file-excel-o mr5"></i>导出</button>
                        </form>
                        <a id="sync_btn" href="javascript:void(0);" class="btn default blue-stripe"><i class="icon-loop"></i> 发起对账</a>
                    </div>
                    <div class="row list-separated margin-bottom-10" id="bank_stat">

                    </div>
                    <div class="row list-separated margin-bottom-10" id="pf_stat">

                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="reconciliation_dt"
                                   data-ajax="${ctx}/trade/reconciliation/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30">序号</th>
                                    <th class="width90">银行交易日期</th>
                                    <th>订单号</th>
                                    <th class="width125">银行订单号</th>
                                    <th class="width70">交易状态</th>
                                    <th class="width70">订单类型</th>
                                    <th class="width80">金额</th>
                                    <th class="width80">手续费</th>
                                    <th class="width135">平台交易时间</th>
                                    <th class="width70">对账结果</th>
                                    <th class="width60">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
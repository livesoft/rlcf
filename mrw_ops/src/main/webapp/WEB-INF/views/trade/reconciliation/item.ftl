<@override name="view_body">

<div class="page-content" id="view_content" data-view="playmoney#">
    <div class="page-head">
        <div class="page-title">
            <h1>系统结算
                <small>银行对账单</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/reconciliation">银行对账单</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">对账详情</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-shadow">
                <h4 class="block">对比结果说明</h4>
                <p> 账单一致： 表示平台账单与银行账单的金额及状态都一致； </p>
                <p> 差异账单： 表示平台账单与银行账单有差异； </p>
            </div>
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">对账详情</span>
                        <span class="caption-helper">对账结果:
                            <#if bank_order.outcome == 1><span class="badge badge-info badge-roundless">账单一致</span>
                            <#else><span class="badge badge-danger badge-roundless">差异订单</span>
                            </#if>
                        </span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-6 col-sm-12">

                            <div class="portlet blue-hoki box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>银行账单
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            订单号:
                                        </div>
                                        <div class="col-md-7 value">
                                        ${bank_order.innerorderno!}
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            交易日期:
                                        </div>
                                        <div class="col-md-7 value">
                                        ${bank_order.transdate!}
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            订单状态:
                                        </div>
                                        <div class="col-md-7 value">
                                            <#if bank_order.orderstatus == 0>
                                                <span class="label label-default">待支付</span>
                                            <#elseif bank_order.orderstatus == 1>
                                                <span class="label label-default">退款处理中</span>
                                            <#elseif bank_order.orderstatus == 2>
                                                <span class="label label-info">处理中</span>
                                            <#elseif bank_order.orderstatus == 3>
                                                <span class="label label-success">交易成功</span>
                                            <#elseif bank_order.orderstatus == 4>
                                                <span class="label label-danger">交易关闭</span>
                                            <#elseif bank_order.orderstatus == 5>
                                                <span class="label label-danger">已删除</span>
                                            <#elseif bank_order.orderstatus == 6>
                                                <span class="label label-info">已退款</span>
                                            <#elseif bank_order.orderstatus == 7>
                                                <span class="label label-danger">交易失败</span>
                                            <#elseif bank_order.orderstatus == 8>
                                                <span class="label label-success">交易完成</span>
                                            </#if>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            订单金额:
                                        </div>
                                        <div class="col-md-7 value">
                                        ${bank_order.amount!} 元
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            订单类型:
                                        </div>
                                        <div class="col-md-7 value">
                                            <#if bank_order.ordertype == 0>
                                                <span class="label label-success">订单</span>
                                            <#elseif bank_order.ordertype == 1>
                                                <span class="label label-info">充值</span>
                                            <#elseif bank_order.ordertype == 2>
                                                <span class="label label-info">提现</span>
                                            <#elseif bank_order.ordertype == 3>
                                                <span class="label label-success">转账</span>
                                            <#elseif bank_order.ordertype == 4>
                                                <span class="label label-success">外部订单支付</span>
                                            <#elseif bank_order.ordertype == 5>
                                                <span class="label label-danger">退款</span>
                                            <#elseif bank_order.ordertype == 6>
                                                <span class="label label-info">其他订单</span>
                                            <#elseif bank_order.ordertype == 7>
                                                <span class="label label-info">积分购买</span>
                                            <#elseif bank_order.ordertype == 8>
                                                <span class="label label-info">缴费</span>
                                            <#elseif bank_order.ordertype == 9>
                                                <span class="label label-waring">撤销</span>
                                            </#if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="portlet green-meadow box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>平台订单
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <#if self_order??>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                订单号:
                                            </div>
                                            <div class="col-md-7 value">
                                            ${self_order.trade_no!}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                交易日期:
                                            </div>
                                            <div class="col-md-7 value">
                                            ${(self_order.buy_time)!?string('yyyyMMdd')}
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                订单状态:
                                            </div>
                                            <div class="col-md-7 value">
                                                <#if self_order.status == 0>
                                                    <span class="label label-default">待支付</span>
                                                <#elseif self_order.status == 1>
                                                    <span class="label label-default">退款处理中</span>
                                                <#elseif self_order.status == 2>
                                                    <span class="label label-info">处理中</span>
                                                <#elseif self_order.status == 3>
                                                    <span class="label label-success">交易成功</span>
                                                <#elseif self_order.status == 4>
                                                    <span class="label label-danger">交易关闭</span>
                                                <#elseif self_order.status == 5>
                                                    <span class="label label-danger">已删除</span>
                                                <#elseif self_order.status == 6>
                                                    <span class="label label-info">已退款</span>
                                                <#elseif self_order.status == 7>
                                                    <span class="label label-danger">交易失败</span>
                                                <#elseif self_order.status == 8>
                                                    <span class="label label-success">交易完成</span>
                                                <#elseif self_order.status == 9>
                                                    <span class="label label-default">未支付订单</span>
                                                </#if>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                订单金额:
                                            </div>
                                            <div class="col-md-7 value">
                                            ${self_order.trade_amount!} 元
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                订单产品:
                                            </div>
                                            <div class="col-md-7 value">
                                            ${self_order.product_name!}
                                            </div>
                                        </div>
                                    <#else>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">
                                                <span class="label label-danger">平台账单不存在</span>
                                            </div>
                                        </div>
                                    </#if>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
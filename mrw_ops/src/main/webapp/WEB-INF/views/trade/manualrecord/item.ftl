<@override name="view_body">

<div class="page-content" id="view_content" data-view="project#item">
    <div class="page-head">
        <div class="page-title">
            <h1>理财项目管理
                <small>${(action=='create')?string('创建','编辑')}理财项目</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/project">理财项目</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}理财项目</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">理财项目</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}理财项目</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/project/save" method="post" id="projects_form">
                        <input type="hidden" name="p.id" value="${(p.id)!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span> 项目名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="name" name="p.name" value="${(p.name)!}"
                                           class="form-control required" autocomplete="off">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="type">
                                    <span class="required"> * </span> 项目类型
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择项目类型" id="type" name="p.type"
                                            class="select2me form-control required">
                                        <#list project_type as dt>
                                            <#assign p_type = p.type???c>
                                            <option value="${dt.code}"  <#if p.type?? && p_type ==dt.code>selected="selected" </#if>>${dt.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="issuer">
                                    <span class="required"> * </span> 发行机构
                                </label>
                                <div class="col-md-4">
                                    <select data-placeholder="请选择发行机构" id="issuer" name="issuer"
                                            class="select2me form-control">
                                        <#list issuers! as is>
                                            <option value=""></option>
                                            <option value="${(is.id)!}-${(is.name)!}"  <#if is.id?? && is.id == p.issuer!0>selected="selected" </#if>>${is.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="logo_uploaded">
                                    LOGO图片
                                </label>

                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" id="logo_uploaded" data-obj="${(p.attachment)!}" data-name="attachment"></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3" for="content">
                                    项目描述
                                </label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" id="description" name="p.description">
                                        ${(p.description)!}
                                    </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/project" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/webuploader/webuploader.min.js"></script>
</@override>
<@override name="view_styles">
<link rel="stylesheet" href="${ctx}/static/scripts/webuploader/webuploader.css">
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
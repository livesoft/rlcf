<@override name="view_body">

<div class="page-content" id="view_content" data-view="clearing${(trade_money.type)!}#manualrecord"">
    <div class="page-head">
        <div class="page-title">
            <h1>调账记录 <small>调账记录</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/trade/clearing/${trade_money.type!}">${title}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">人工调账</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">调账记录</span>
                        <span class="caption-helper">调账记录列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-horizontal validate-form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">调整人姓名:</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="s_user_name" placeholder="请输入调整人姓名">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">调整时间:</label>
                                            <div class="col-md-8">
                                                <div class="input-group input-large" id="s_order_time_g">
                                                    <input type="text" class="form-control" id="s_time" readonly>
                                        <span class="input-group-btn">
                                         <button class="btn default date-range-toggle" type="button"><i
                                                 class="fa fa-calendar"></i></button>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-offset-10">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                                        class="icon-magnifier mr5"></i>查询</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="record_dt"
                                   data-ajax="${ctx}/trade/manualrecord/dtlist/${(trade_money.id)!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width100">订单号</th>
                                    <th class="width80">调整方式</th>
                                    <th class="width80">调整金额</th>
                                    <th class="width200">调整时间</th>
                                    <th class="width60">调整人</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="member#item">
    <div class="page-head">
        <div class="page-title">
            <h1>
                <small></small>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员详细信息</span>
                        <span class="caption-helper"></span>
                    </div>
                </div>

                <div class="tab-pane" id="tab_3">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">手机:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        ${(member.phone)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">真实姓名:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.real_name)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">性别:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        <#if (member.gender)?? && (member.gender) ==0>
                                                            女
                                                        <#elseif (member.gender)?? && (member.gender) ==1>
                                                            男
                                                        <#else >
                                                            未知
                                                        </#if>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">邮箱地址:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.email)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">证件类型:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        身份证
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">证件号码:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                         ${(member.cart_no)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">实名认证:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        ${((member.approve)! =='N')?string('已认证','未认证')}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">电子账户:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.electronic_account)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">注册时间:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.regiest_time)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">注册IP:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.regiest_ip)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">最后登录IP:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.last_login_ip)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">最后登录时间:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                        ${(member.last_login_time)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">会员状态:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    <#--${(member.status)! == 1}-->
                                                        <#if (member.status)! ==1>
                                                            使用中
                                                        <#else>
                                                            已禁用
                                                        </#if>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">创建时间:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">
                                                    ${(member.create_time)!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-5 col-md-9">
                                            <a href="javascript:history.go(-1);" class="btn btn-default default">返回</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
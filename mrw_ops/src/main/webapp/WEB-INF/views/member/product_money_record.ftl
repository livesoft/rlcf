<@override name="view_body">

<div class="page-content" id="view_content" data-view="member#promoyrecord">
    <div class="page-head">
        <div class="page-title">
            <h1>会员产品收益
                <small>会员产品收益管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/member">会员管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">产品收益</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员产品收益</span>
                        <span class="caption-helper">会员产品收益列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="" class="form-horizontal validate-form">
                                <input type="hidden" id="s_member" value="${member!}">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">产品名称:</label>
                                                <div class="col-md-8">
                                                    <select data-placeholder="请选择产品" id="s_product"
                                                            class="select2me form-control">
                                                        <option value=""></option>
                                                        <#list products! as pd>
                                                            <option value="${pd.id!}">${pd.name!}</option>
                                                        </#list>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                                        class="icon-magnifier mr5"></i>查询</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                        </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <div class="table-container">
                    <div class="table-responsive no-border">
                        <table class="table table-bordered table-striped table-condensed" id="pmr_dt"
                               data-ajax="${ctx}/member/record_dtlist/${member!}">
                            <thead>
                            <tr class="heading">
                                <th class="width20"></th>
                                <th class="width70">会员</th>
                                <th class="width70">产品</th>
                                <th class="width70">收益率</th>
                                <th class="width45">金额</th>
                                <th class="width70">期数</th>
                                <th class="width80">打款日期</th>
                                <th class="width120">剩余本金</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="member#item">
    <div class="page-head">
        <div class="page-title">
            <h1>会员管理
                <small>实名审核</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/member/member">会员中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">实名审核</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员管理</span>
                        <span class="caption-helper">会员审核</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal" action="${ctx}/member/audit/audit" method="post" id="notice_form">
                        <input type="hidden" name="member" value="${info.member!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    真实姓名:
                                </label>
                                <div class="col-md-4">
                                   <span>${info.real_name!}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    身份证号:
                                </label>
                                <div class="col-md-4">
                                    <span>${info.id_card!}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    认证状态:
                                </label>
                                <div class="col-md-4">
                                    <span>
                                        <#if info.status?? && info.status>
                                            已认证
                                        <#else >
                                            未认证
                                        </#if>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">认证</button>
                                    <a href="${ctx}/member/member" class="btn btn-default default">返回</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@override name="view_scripts">

</@override>
<@override name="view_styles">

</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
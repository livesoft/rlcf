<@override name="view_body">

<div class="page-content" id="view_content" data-view="member#amount">
    <div class="page-head">
        <div class="page-title">
            <h1>会员中心
                <small>会员账户信息</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/member/member">会员中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">会员账户信息</a>
        </li>
    </ul>

    <div class="portlet light bordered">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" role="form">
                <div class="form-body">
                    <h3 class="form-section">账户信息：</h3>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">电子帐号:</label>

                                <div class="col-md-9">
                                    <p class="form-control-static">${(account.electronic_account)!}</p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">累计支出:</label>
                                <div class="col-md-9">  <p class="form-control-static">${(sumEarning)!} 元 </p> </div>

                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">累计收入:</label>

                                <div class="col-md-9">
                                    <p class="form-control-static"> ${(sumExpense)!} 元 </p>
                                </div>
                            </div>
                        </div>
                    </div>



                    <h3 class="form-section">资金流水记录：</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet-body">
                            <div class="table-container">
                                <div class="table-responsive no-border">
                                    <table class="table table-bordered table-striped table-condensed" id="amount_dt"
                                           data-ajax="${ctx}/member/amount/dtlist/${member_id!0}">
                                        <thead>
                                        <tr class="heading">
                                            <th class="width30"></th>
                                            <th class="width120">项目名称</th>
                                            <th class="width70">时间</th>
                                            <th class="width50">支出</th>
                                            <th class="width50">收入</th>
                                            <th class="width135">备注</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
</@override>


<@extends name="*/_layout/admin/basic.ftl"></@extends>
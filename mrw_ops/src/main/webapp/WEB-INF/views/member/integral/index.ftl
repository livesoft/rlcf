<@override name="view_body">

<div class="page-content" id="view_content" data-view="member#integral">
    <div class="page-head">
        <div class="page-title">
            <h1>会员积分明细
                <small>会员中心</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/member">会员中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">会员积分明细</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员中心</span>
                        <span class="caption-helper">会员积分明细</span>
                    </div>
                </div>

                <div class="clearfix mb10">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="" class="form-inline pull-right">
                        <label class="" for="s_type">类型:</label>
                        <select data-placeholder="请选择类型" id="s_type" name="s_type"
                                class="select2me form-control">
                            <option value=""></option>
                            <option value="SIGNIN">每日登录</option>
                            <option value="INVITE">邀请注册奖励</option>
                            <option value="CASH">消费奖励</option>
                        </select>
                        <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i class="icon-magnifier mr5"></i>查询</a>
                    </form>
                    <!-- END FORM-->
                </div>
                </div>

                <#--<div class="portlet-body">-->
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="integral_dt"
                                   data-ajax="${ctx}/member/integral/dtlist/${m_id!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width20"></th>
                                    <th class="width70">会员</th>
                                    <th class="width40">类型</th>
                                    <th class="width60">收入积分</th>
                                    <th class="width60">支出积分</th>
                                    <th class="width60">积分余额</th>
                                    <th class="width120">操作时间</th>
                                    <th>备注</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <#--</div>-->
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
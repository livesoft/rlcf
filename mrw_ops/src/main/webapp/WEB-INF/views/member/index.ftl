<@override name="view_body">

<div class="page-content" id="view_content" data-view="member">
    <div class="page-head">
        <div class="page-title">
            <h1>会员管理
                <small>会员中心</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">会员管理</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">会员中心</span>
                        <span class="caption-helper">会员管理</span>
                    </div>

                    <@shiro.hasPermission name="member_export">
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">
                                    <i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="${ctx}/member/export"> 导出Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </@shiro.hasPermission>
                </div>

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="" class="form-horizontal validate-form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">手机:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="s_phone" class="form-control" placeholder="请输入手机号码">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">邮箱:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="s_email" class="form-control" placeholder="请输入邮箱">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">性别:</label>
                                        <div class="col-md-8">
                                            <select class="form-control select2me" id="s_gender">
                                                <option value="">全部</option>
                                                <option value="9">未知</option>
                                                <option value="1">男</option>
                                                <option value="0">女</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                                    class="icon-magnifier mr5"></i>查询</a>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>

                <#--<div class="portlet-body">-->
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="member_dt"
                                   data-ajax="${ctx}/member/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width20"></th>
                                    <th class="width70">手机号</th>
                                    <th class="width70">用户名</th>
                                    <th class="width70">真实姓名</th>
                                    <th class="width70">电子账户</th>
                                    <th class="width45">性别</th>
                                    <th class="width70">邮箱</th>
                                    <th class="width120">注册时间</th>
                                    <th class="width120">会员状态</th>
                                    <th class="width120">积分</th>
                                    <th class="width160">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <#--</div>-->
            </div>
        </div>
    </div>
</@override>

<@override name="view_scripts">
    <script type="text/javascript">
        var member_lock = 0;
    <@shiro.hasPermission name="member_lock">
        member_lock =1;
    </@shiro.hasPermission>
    </script>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="role">
    <div class="page-head">
        <div class="page-title">
            <h1>角色管理 <small>角色管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">角色管理</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">角色管理</span>
                        <span class="caption-helper">角色管理列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <a id="new" href="${ctx}/role/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="role_dt"
                                   data-ajax="${ctx}/role/dtlist">
                                <thead>
                                    <th class="width30"></th>
                                    <th class="width150">角色名称</th>
                                    <th class="width500">角色描述</th>
                                    <th class="width120">操作</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
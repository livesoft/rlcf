<@override name="view_body">

<div class="page-content" id="view_content" data-view="role#item">
    <div class="page-head">
        <div class="page-title">
            <h1>角色管理
                <small>${(action=='create')?string('创建','编辑')}角色</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/role">角色管理</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=="create")?string("新建","编辑")}角色</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">角色管理</span>
                        <span class="caption-helper">${(action=="create")?string("新建","编辑")}角色</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/role/save" method="post" id="role_form">
                        <input type="hidden" name="role.id" value="${(role.id)!}">
                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="name">
                                    <span class="required"> * </span>角色名称
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="name" name="role.name" value="${(role.name)!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="code">
                                    角色描述
                                </label>
                                <div class="col-md-4">
                                    <textarea class="form-control" rows="3" id="description" name="role.description">
                                        ${(role.description)!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">资源选择</label>
                                <div class="col-md-9">

                                    <ul id="module_tree" class="ztree form-control" style="width:360px; overflow:auto; height: 220px;"></ul>
                                    <input type="hidden" id="permissions" name="role.permissions" value="${role.permissions!}">
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/role" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
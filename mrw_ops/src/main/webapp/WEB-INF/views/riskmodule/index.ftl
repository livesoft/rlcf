<@override name="view_body">

<div class="page-content" id="view_content" data-view="riskmodel">
    <div class="page-head">
        <div class="page-title">
            <h1>风险评估模型 <small>风险评估模型管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">风险评估模型</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">风险评估模型</span>
                        <span class="caption-helper">风险评估模型列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="risk-model_dt" data-ajax="${ctx}/riskmodule/dtlist">
                                <thead>
                                    <tr class="heading">
                                        <th class="width30"></th>
                                        <th class="width200">风险等级</th>
                                        <th class="width100">最小分值</th>
                                        <th class="width60">最大分值</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
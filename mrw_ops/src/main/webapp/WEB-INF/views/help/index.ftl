<@override name="view_body">

<div class="page-content" id="view_content" data-view="help">
    <div class="page-head">
        <div class="page-title">
            <h1>帮助中心 <small>帮助中心管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">帮助中心</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">帮助中心</span>
                        <span class="caption-helper">帮助中心列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">标题:</label>
                            <input type="text" class="form-control" id="s_title" placeholder="请输入帮助中心标题">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                        <div class="pull-left"><a id="help" href="${ctx}/help/create" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="help_dt"
                                   data-ajax="${ctx}/help/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width150">标题</th>
                                    <th class="width60">图标CSS</th>
                                    <th class="width150">所属标题</th>
                                    <th class="width60">排序</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
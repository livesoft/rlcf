<@override name="view_body">

<div class="page-content" id="view_content" data-view="help#">
    <div class="page-head">
        <div class="page-title">
            <h1>帮助中心
                <small>${(action=='create')?string('创建','编辑')}帮助中心</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/help">帮助中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}帮助中心</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">帮助中心</span>
                        <span class="caption-helper">${(action=='create')?string('创建','编辑')}帮助</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/help/save" method="post" id="help_form">
                        <input type="hidden" name="help.id" value="${help.id!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 帮助标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" maxlength="25" name="help.title" value="${help.title!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="icon">
                                    图片信息
                                </label>
                                <div class="col-md-4">
                                    <input type="text" id="icon" maxlength="16" name="help.icon" value="${help.icon!}"
                                           class="form-control" autocomplete="off">
                                </div>
                            </div>

                          <#if !has_child>
                              <div class="form-group">
                                  <label class="control-label col-md-3" for="parent">
                                      上级标题
                                  </label>
                                  <div class="col-md-4">
                                      <select data-placeholder="请选择上级标题" id="parent" name="help.parent"
                                              class="select2me form-control">
                                          <option></option>
                                          <#list parent! as p>
                                              <#if help.id! != p.id>
                                                  <option value="${(p.id)!}" <#if help.parent?? && p.id==help.parent>selected </#if>>${(p.title)!}</option>
                                              </#if>
                                          </#list>
                                      </select>
                                  </div>
                              </div>
                          </#if>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="display_order">
                                    显示次序
                                </label>

                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(help.sort)!'1'}" data-step="1"
                                         data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="display_order" name="help.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/help" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
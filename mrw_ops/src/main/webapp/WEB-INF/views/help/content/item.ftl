<@override name="view_body">

<div class="page-content" id="view_content" data-view="help#content_item">
    <div class="page-head">
        <div class="page-title">
            <h1>帮助内容
                <small>${(action=='create')?string('创建','编辑')}帮助内容</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/help">帮助中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/content">帮助内容</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">${(action=='create')?string('创建','编辑')}帮助内容</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">帮助内容</span>
                        <span class="caption-contenter">${(action=='create')?string('创建','编辑')}帮助</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form" action="${ctx}/help/content/save" method="post" id="content_form">
                        <input type="hidden" name="content.id" value="${content.id!}">
                        <input type="hidden" name="content.nav" value="${content.nav!}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="nav">
                                    <span class="required"> * </span> 所属帮助导航
                                </label>
                                <div class="col-md-4">
                                   <p>${content.nav_name!}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="title">
                                    <span class="required"> * </span> 帮助标题
                                </label>

                                <div class="col-md-4">
                                    <input type="text" id="title" name="content.title" value="${content.title!}"
                                           class="form-control" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="conent">
                                    帮助内容
                                </label>

                                <div class="col-md-6 ">
                                    <script id="content" name="content.conent" type="text/plain">${content.conent!}</script>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="sort">
                                    <span class="required"> * </span> 显示排序
                                </label>
                                <div class="col-md-4">
                                    <div class="spinnerme" data-value="${(content.sort)!1}" data-step="1" data-min="1" data-max="20">
                                        <div class="input-group" style="width:150px;">
                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-up blue">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <input type="text" id="sort" name="content.sort"
                                                   class="spinner-input form-control" maxlength="3" readonly>

                                            <div class="spinner-buttons input-group-btn">
                                                <button type="button" class="btn spinner-down red">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                    <a href="${ctx}/help/content/${(content.nav)!}" class="btn btn-default default">取消</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>
<@override name="view_scripts">
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/scripts/ueditor/ueditor.all.min.js"></script>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
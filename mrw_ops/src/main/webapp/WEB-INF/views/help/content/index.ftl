<@override name="view_body">

<div class="page-content" id="view_content" data-view="help_content">
    <div class="page-head">
        <div class="page-title">
            <h1>帮助内容 <small>帮助内容管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/help">帮助中心</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">帮助内容</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">帮助内容</span>
                        <span class="caption-help_contenter">帮助内容列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-left">
                            <label class="" for="s_name">标题:</label>
                            <input type="text" class="form-control" id="s_title" placeholder="请输入帮助内容标题">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                        <div class="pull-right"><a id="help_content" href="${ctx}/help/content/create/${help_id!0}" class="btn blue">
                            <i class="icon-plus mr5"></i>创建</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="help_content_dt"
                                   data-ajax="${ctx}/help/content/dtlist/${(help_id)!0}">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width200">内容标题</th>
                                    <th class="width200">所属导航标题</th>
                                    <th class="width200">排序</th>
                                    <th class="width155">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
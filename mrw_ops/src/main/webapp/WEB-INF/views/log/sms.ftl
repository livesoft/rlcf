<@override name="view_body">

<div class="page-content" id="view_content" data-view="log-sms">
    <div class="page-head">
        <div class="page-title">
            <h1>日志报表 <small>短信发送日志</small> </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">短信发送日志</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">短信发送日志</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-default btn-circle" href="#" data-toggle="dropdown">
                                <i class="fa fa-share"></i> 工具 <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="${ctx}/log/sms/export"> 导出Excel </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline">
                            <label class="" for="s_name">发送号码:</label>
                            <input type="text" id="s_phone" class="form-control" id="s_email" placeholder="请输入发送号码">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="sms_dt"
                                   data-ajax="${ctx}/log/sms/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width80">发送号码</th>
                                    <th>发送内容</th>
                                    <th class="width60">发送状态</th>
                                    <th class="width120">发送时间</th>
                                    <th class="width220">异常信息</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
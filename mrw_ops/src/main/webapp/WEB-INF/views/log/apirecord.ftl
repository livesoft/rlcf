<@override name="view_body">

<div class="page-content" id="view_content" data-view="log-apirecord">
    <div class="page-head">
        <div class="page-title">
            <h1>日志报表 <small>银行接口调用日志</small> </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">银行接口调用日志</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">银行接口调用日志</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline">
                            <label class="" for="s_name">发送号码:</label>
                            <input type="text" id="s_param" class="form-control" placeholder="请输入报文参数">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>

                        </form>
                    </div>
                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="api_dt"
                                   data-ajax="${ctx}/log/apirecord/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width10"></th>
                                    <th class="width80">接口名称</th>
                                    <th>报文参数</th>
                                    <th class="width60">返回报文</th>
                                    <th class="width120">调用时间</th>
                                    <th class="width220">异常信息</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
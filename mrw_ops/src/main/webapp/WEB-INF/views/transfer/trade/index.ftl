<@override name="view_body">

<div class="page-content" id="view_content" data-view="transfer_product#transfer_trade">
    <div class="page-head">
        <div class="page-title">
            <h1>转让交易 <small>转让交易管理</small></h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/transfer/product">转让专区</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让详情</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">转让交易</span>
                        <span class="caption-helper">转让交易列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-inline pull-right">
                            <label class="" for="s_name">产品名称:</label>
                            <input type="text" class="form-control" id="s_product_name" placeholder="请输入产品名称">
                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i
                                    class="icon-magnifier mr5"></i>查询</a>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="transfer_trade_dt"
                                   data-ajax="${ctx}/transfer/trade/dtlist/${tranfer!}">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th class="width120">订单号</th>
                                    <th class="width120">转让产品</th>
                                    <th class="width60">转让方</th>
                                    <th class="width60">受让方</th>
                                    <th class="width60">交易金额</th>
                                    <th class="width60">交易时间</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
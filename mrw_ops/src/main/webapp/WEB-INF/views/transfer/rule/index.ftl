<@override name="view_body">

<div class="page-content" id="view_content" data-view="transfer_rule">
    <div class="page-head">
        <div class="page-title">
            <h1>转让规则
                <small>转让规则设置</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让规则设置</a>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-shadow">
                <h4 class="block">转让规则设置说明</h4>

                <p> 手续费比率如果设置为0 表示转让时不收取手续费 </p>

                <p> 转让时效如果设置为0 表示转让时效无限制 </p>
            </div>
            <div class="portlet light form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">转让规则</span>
                        <span class="caption-helper">转让规则设置</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" class="form-horizontal validate-form ajax-form" action="${ctx}/transfer/rule/save"
                          method="post" id="transfer_rule_form">
                        <input type="hidden" name="fees.id" value="${(fees.id)!}">

                        <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-3" for="lodging_fee">
                                    每日挂单费:
                                </label>

                                <div class="col-md-4">

                                    <input type="text" value="${(fees.lodging_fee)!}" class="form-control required number" name="fees.lodging_fee" id="lodging_fee">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="ratio">
                                    手续费(转让价格的百分比)：
                                </label>

                                <div class="col-md-4">
                                    <div class="input-group" style="width:150px;">
                                        <input type="text" id="ratio" name="fees.ratio" value="${(fees.ratio)!0}"
                                               data-rule-max="100" data-rule-min="0"
                                               class="spinner-input form-control" maxlength="5">

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3" for="free_times">
                                    免费天数（单位：天）：
                                </label>

                                <div class="col-md-4">
                                    <input type="text" value="${(fees.free_times)!}" class="form-control required number" name="fees.free_times" id="free_times">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="aging">
                                    转让时效（单位：小时）：
                                </label>

                                <div class="col-md-4">
                                    <input type="text" value="${(fees.aging)!}" class="form-control required number" name="fees.aging" id="aging">
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary blue">保存</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
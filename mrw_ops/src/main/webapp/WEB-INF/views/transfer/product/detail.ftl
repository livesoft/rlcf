<@override name="view_body">

<div class="page-content" id="view_content" data-view="transfer_product">
    <div class="page-head">
        <div class="page-title">
            <h1>转让产品详细
                <small></small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="${ctx}/transfer/product">转让专区</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让产品详细</a>
        </li>
    </ul>

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form class="form-horizontal" role="form">
                    <div class="form-body">
                        <h2 class="margin-bottom-20"> 转让产品详细信息 </h2>

                        <h3 class="form-section">基本信息</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">产品名称:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.product_name)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让价格:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.price)!} 元
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让状态:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.status_name)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">转让时效:</label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">
                                            ${(detail.aging)!} 小时
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                        <#--<div class="col-md-6">-->
                        <#--<div class="form-group">-->
                        <#--<label class="control-label col-md-3">受让方收益率:</label>-->
                        <#--<div class="col-md-9">-->
                        <#--<p class="form-control-static">-->
                        <#--${(detail.yield)!}-->
                        <#--</p>-->
                        <#--</div>-->
                        <#--</div>-->
                        <#--</div>-->
                        <#--<!--/span&ndash;&gt;-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">剩余天数:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.remaining_days)!} 天
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让方:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.phone)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">持有天数:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.hold_days)!} 天
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">手续费:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.handing_charge)!} 元
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让时间:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.transfer_time)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让形式:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        <#--TODO LH 一口价-->
                                        <#--${(detail.mode)!}-->
                                            一口价
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h3 class="form-section">交易信息</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">购买人:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.tt_assignee_phone)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">购买时间:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.tt_create_time)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">转让价格:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.tt_amount)!0}元
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">订单号:</label>

                                    <div class="col-md-9">
                                        <p class="form-control-static">
                                        ${(detail.tt_order_id)!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">

                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</@override>
<@extends name="*/_layout/admin/basic.ftl"></@extends>
<@override name="view_body">

<div class="page-content" id="view_content" data-view="transfer_product">
    <div class="page-head">
        <div class="page-title">
            <h1>转让专区
                <small>转让专区管理</small>
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="${ctx}/">首页</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">转让专区</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">转让专区</span>
                        <span class="caption-helper">转让专区列表</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="clearfix mb10">
                        <form action="" class="form-horizontal validate-form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group mb0">
                                            <label class="control-label col-md-4">产品名称:</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="s_product_name"
                                                       placeholder="请输入产品名称">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group mb0">
                                            <label class="col-md-4 control-label" for="s_price_start">转让价格: </label>
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <input type="number" min="0" class="form-control" id="s_price_start">
                                                    <span class="input-group-addon">- </span>
                                                    <input type="number" min="0" class="form-control" id="s_price_end">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group mb0">
                                            <label class="control-label col-md-4" for="s_trade_no">订单号:</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="s_trade_no" placeholder="请输入订单号">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row pt0">
                                    <div class="col-md-4">
                                        <div class="form-group mb0">
                                            <label class="control-label col-md-4" for="transfer_time">转让时间:</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="transfer_time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group mb0">
                                            <label class="control-label col-md-4" for="s_transfer_phone">联系电话:</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="s_transfer_phone"
                                                       placeholder="请输入转让人电话号码">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group mb0">
                                            <label class="control-label col-md-4" for="s_status">转让状态:</label>

                                            <div class="col-md-8">
                                                <select class="select2me form-control" id="s_status"
                                                        data-placeholder="转让状态">
                                                    <option value=""></option>
                                                    <option value="0">待审核</option>
                                                    <option value="1">交易中</option>
                                                    <option value="2">交易成功</option>
                                                    <option value="3">转让失效</option>
                                                    <option value="4">转让撤销</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label class="control-label col-md-4" for="s_member_name">会员名称:</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="s_member_name"  placeholder="请输入会员名称">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <a id="search_btn" href="javascript:;" class="btn default green-stripe"><i class="icon-magnifier mr5"></i>查询</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="table-container">
                        <div class="table-responsive no-border">
                            <table class="table table-bordered table-striped table-condensed" id="transfer_product_dt"
                                   data-ajax="${ctx}/transfer/product/dtlist">
                                <thead>
                                <tr class="heading">
                                    <th class="width30"></th>
                                    <th>转让产品</th>
                                    <th class="width60">转让价格</th>
                                    <th class="width50">转让方</th>
                                    <th class="width50">手续费</th>
                                    <th class="width60">持有天数</th>
                                    <th class="width60">剩余天数</th>
                                    <th class="width120">转让时间</th>
                                    <th class="width120">订单号</th>
                                    <th class="width50">状态</th>
                                    <th class="width70">操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@override>

<@extends name="*/_layout/admin/basic.ftl"></@extends>
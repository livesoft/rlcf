<#compress>
<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="description" content="后台运维系统">
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta name="renderer" content="webkit">
        <title>后台维护 - 系统</title>

        <link rel="stylesheet" href="${ctx}/static/styles/font-awesome.min.css">
        <link rel="stylesheet" href="${ctx}/static/styles/simple-line-icons.min.css">
        <link rel="stylesheet" href="${ctx}/static/styles/bootstrap.min.css">
        <link rel="stylesheet" href="${ctx}/static/styles/select2.css">
        <link rel="stylesheet" href="${ctx}/static/styles/uniform.default.css">
        <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-fileinput.css">
        <link rel="stylesheet" href="${ctx}/static/styles/dataTables.bootstrap.css">
        <link rel="stylesheet" href="${ctx}/static/styles/datepicker3.css">
        <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="${ctx}/static/styles/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="${ctx}/static/styles/clockface.css">
        <link rel="stylesheet" href="${ctx}/static/styles/daterangepicker-bs3.css">
        <link rel="stylesheet" href="${ctx}/static/styles/dropzone.css">
        <link rel="stylesheet" href="${ctx}/static/styles/jquery.fancybox.css">
        <link rel="stylesheet" href="${ctx}/static/styles/components.css">
        <link rel="stylesheet" href="${ctx}/static/styles/plugins.css">
        <link rel="stylesheet" href="${ctx}/static/styles/layout.css">
        <link rel="stylesheet" href="${ctx}/static/styles/light.css">
        <link rel="stylesheet" href="${ctx}/static/styles/ztree_metro.css">
        <link rel="stylesheet" href="${ctx}/static/styles/define.css">

        <style type="text/css">

            #browser_container {
                display: none;
                position: absolute;
                top:0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 100000;
                background: #f5f5f5;
                border: 1px solid #e3e3e3;
            }

            .main-content {
                width: 990px;
                margin: 0 auto;
                padding: 50px 0;
            }

            .main-content ul {
                list-style: none;
                margin-top: 60px;
                padding: 0;
                text-align: center;
            }

            .main-content li {
                display: inline-block;
                width: 88px;
                height: 88px;
                margin-right: 30px;
                margin-left: 30px;
                text-align: center;
                border: 1px solid #f5f5f5;
            }

            .main-content li:hover {
                border-radius: 10px;
                border: 1px solid #ccc;
            }

            .main-content li p {
                color: #000;
            }

            .main-content li img {
                width: 58px;
                height: 58px;
            }

        </style>

        <@block name="view_styles"></@block>
        <script type="text/javascript">
            var g = {ctx: '${ctx}/', dict_url: '${ctx}/baseinfo/dict/show/', 'assetsPath': '${ctx}/static/'};
        </script>

    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo">

        <div id="browser_container" class="browser_container">
            <div class="main-content">
                <div style="text-align: center;">
                    <div class="ie-title">
                        <img src="http://res.tongbanjie.com/build/imgs/browser/old_ie.png">
                        <h3>您的IE浏览器太老啦</h3>
                    </div>
                </div>
                <div class="ie-content" style="text-align: center;">
                    <span>为了更安全、快捷地体验后台融理系统，我们强烈建议您升级到最新版本的</span>
            <span>
                <a href="http://www.microsoft.com/zh-cn/download/internet-explorer-10-details.aspx" target="_blank">Internet Explore</a>或使用其他浏览器。
            </span>
                    <div class="ie-target" style="margin-top: 10px;">
                        <a href="javascript:void(0);" id="browser_goon_visit">继续访问&gt;</a>
                    </div>
                </div>
                <ul class="clearfix">
                    <li>
                        <a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" target="_blank">
                            <img src="${ctx}/static/img/browser/i.png" alt="下载IE11" title="下载IE11">
                            <p>IE11</p>
                        </a>
                    </li>
                    <li>
                        <a href="http://support.apple.com/kb/DL1531?viewlocale=zh_CN&amp;locale=zh_CN" target="_blank">
                            <img src="${ctx}/static/img/browser/s.png" alt="下载Safari" title="下载Safari">
                            <p>Safari</p>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.firefox.com.cn/download/" target="_blank">
                            <img src="${ctx}/static/img/browser/f.png" alt="下载Firefox" title="下载Firefox">
                            <p>Firefox</p>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.google.cn/intl/zh-CN/chrome/browser/" target="_blank">
                            <img src="${ctx}/static/img/browser/c.png" alt="下载Chrome" title="下载Chrome">
                            <p>Chrome</p>
                        </a>
                    </li>
                    <li>
                        <a href="http://chrome.360.cn/" target="_blank">
                            <img src="${ctx}/static/img/browser/360.jpg" alt="急速360Chrome" title="下载急速360">
                            <p>360</p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>

        <div id="layout_container">
            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner">
                    <div class="page-logo">
                        <a href="${ctx}/index">
                            <img src="${ctx}/static/img/logo-light.png" alt="logo" class="logo-default"/>
                        </a>

                        <div class="menu-toggler sidebar-toggler"></div>
                    </div>
                    <a href="javascript:void(0);" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
                    <div class="page-top">
                        <div class="top-menu">
                            <ul class="nav navbar-nav pull-right">
                                <li class="separator hide">
                                </li>
                                <@shiro.hasPermission name="view_account">
                                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                    <a href="javascript:void(0);" id="showAccounts" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                        <i class="icon-credit-card"></i>

                                        <span class="badge badge-success"> 查看平台账户 </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="external">
                                            <h3>
                                                <span class="bold">平台账户金额</span>
                                            </h3>
                                            <a id="refreshAccounts" href="javascript:void(0);">刷新</a>
                                        </li>
                                        <li>
                                            <div id="account-drop-loading" style="height: 250px;line-height: 250px;text-align: center;color: #888888;">
                                                正在查询，请稍等...
                                            </div>
                                            <ul id="account-drop-list" class="dropdown-menu-list scroller" style="height: 250px; overflow: auto; width: auto;display: none;"
                                                data-handle-color="#637283" data-initialized="1">

                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                </@shiro.hasPermission>

                                <!--账单结束-->
                                <li class="dropdown dropdown-user dropdown-dark">

                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                       data-close-others="true">
                                        <span class="username username-hide-on-mobile"> <@shiro.principal></@shiro.principal> </span>
                                        <img alt="" class="img-circle" src="${ctx}/static/img/avatar9.jpg"/>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">

                                        <@shiro.hasPermission name="charging">
                                            <li>
                                                <a href="${ctx}/merchant"> <i class="icon-credit-card"></i>账户充值 </a>
                                            </li>
                                        </@shiro.hasPermission>
                                        <@shiro.hasPermission name="withdrawals">
                                            <li>
                                                <a href="${ctx}/merchant"> <i class="icon-credit-card"></i> 账户提现 </a>
                                            </li>
                                        </@shiro.hasPermission>

                                        <@shiro.hasPermission name="merchantrs">
                                            <li>
                                                <a href="${ctx}/merchant/record"><i class="icon-directions"></i>账户纪录
                                                </a>
                                            </li>
                                        </@shiro.hasPermission>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="${ctx}/user/changepwd"><i class="icon-calendar"></i> 修改密码 </a>
                                        </li>
                                        <li>
                                            <a href="${ctx}/logout">
                                                <i class="icon-key"></i> 注销退出
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="page-container pl10 pr10 pt5">
                <div class="page-sidebar-wrapper">
                    <#include 'menu.ftl'>
                </div>
                <div class="page-content-wrapper">
                    <@block name="view_body"></@block>
                </div>
            </div>
        </div>

        <!--[if lt IE 9]>
        <script src="${ctx}/static/scripts/fixie/respond.min.js"></script>
        <script src="${ctx}/static/scripts/fixie/excanvas.min.js"></script>
        <![endif]-->
        <script src="${ctx}/static/scripts/framework.min.js"></script>
        <script src="${ctx}/static/scripts/framework-extra.min.js"></script>
        <script src="${ctx}/static/scripts/json.js"></script>
        <script src="${ctx}/static/scripts/jquery.cookie.js"></script>
        <@block name="view_scripts"></@block>
        <script src="${ctx}/static/scripts/chart/highcharts.js"></script>
        <script src="${ctx}/static/scripts/app.min.js"></script>
        <script src="${ctx}/static/scripts/constants.js"></script>
        <script src="${ctx}/static/scripts/rongli.js"></script>
        <script src="${ctx}/baseinfo/dict/js"></script>
        <@hasAnyPermissions name="payfail_clearing_ok,fail_clearing_ok,cancel_clearing_ok,expire_clearing_ok,platform_clearing_ok">
                <script src="${ctx}/static/app/admin/heartbeat.js"></script>
        </@hasAnyPermissions>
        <script type="text/javascript" src="${ctx}/static/scripts/require.js"
                data-main="${ctx}/static/app/admin/rongli"></script>

    </body>

</html>
</#compress>
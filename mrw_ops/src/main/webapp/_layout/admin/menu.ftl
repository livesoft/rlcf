<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200">
    <@shiro.hasPermission name="dashboard">
        <li class="start" data-view="dashboard">
            <a href="${ctx}/dashboard">
                <i class="icon-home"></i>
                <span class="title">系统首页</span>
            </a>
        </li>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="product_menu">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-basket"></i>
                <span class="title">产品维护</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="group">
                    <li data-view="group"><a href="${ctx}/product/group"><i class="icon-tag"></i> 产品分组 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="brand">
                    <li data-view="brand"><a href="${ctx}/product/brand"><i class="icon-list"></i> 产品品牌 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product">
                    <li data-view="product"><a href="${ctx}/product"><i class="icon-handbag"></i> 产品管理 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="attr">
                    <li data-view="attr"><a href="${ctx}/product/attr"><i class="icon-list"></i> 产品属性 </a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="member_menu">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-users"></i>
                <span class="title">会员管理</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="member">
                    <li data-view="member"><a href="${ctx}/member"><i class="icon-users"></i> 会员中心</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="invite">
                    <li data-view="invite"><a href="${ctx}/invite"><i class="icon-users"></i>邀请统计</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="redeem">
                    <li data-view="redeem"><a href="${ctx}/redeem"><i class="icon-users"></i>退单申请</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="boards">
                    <li data-view="boards"><a href="${ctx}/boards"><i class="icon-users"></i>私人定制</a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="order">
        <li data-view="order">
            <a href="${ctx}/order"><i class="icon-basket"></i><span class="title">订单管理</span></a>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="transfer_product">
        <li data-view="transfer_product">
            <a href="${ctx}/transfer/product"><i class="icon-shuffle"></i><span class="title">转让专区</span></a>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="clearing_center">
        <li>
            <a href="javascript:;">
                <i class="icon-shield"></i>
                <span class="title">结算中心</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">

                <@shiro.hasPermission name="platform_clearing">
                    <li data-view="clearing7"><a href="${ctx}/trade/clearing/7"><i class="icon-directions"></i>
                        产品募集到期结算 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="expire_clearing">
                    <li data-view="clearing3"><a href="${ctx}/trade/clearing/3"><i class="icon-directions"></i>
                        到期收益发放结算 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="cancel_clearing">
                    <li data-view="clearing5"><a href="${ctx}/trade/clearing/5"><i class="icon-directions"></i> 退单资金结算
                    </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="fail_clearing">
                    <li data-view="clearing6"><a href="${ctx}/trade/clearing/6"><i class="icon-directions"></i>
                        募集失败资金返还结算 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="transfer_clearing">
                    <li data-view="clearing4"><a href="${ctx}/trade/clearing/4"><i class="icon-directions"></i>
                        转让交易资金结算 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="payfail_clearing">
                    <li data-view="clearing8"><a href="${ctx}/trade/clearing/8"><i class="icon-directions"></i>
                        电子账户交易差错结算 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="check_account">
                    <li data-view="reconciliation"><a href="${ctx}/trade/reconciliation"><i class="icon-check"></i>
                        银行对账单 </a></li>
                </@shiro.hasPermission>
            <@shiro.hasPermission name="financecost">
                    <li data-view="financecost"><a href="${ctx}/trade/financecost"><i class="icon-check"></i>
                        财务对账单 </a></li>
            </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="shop_integral">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-present"></i>
                <span class="title">积分商城</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li><a href="${ctx}/gift/category"><i class="icon-calendar"></i> 礼品类型</a></li>
                <li><a href="${ctx}/gift"><i class="icon-badge"></i> 礼品管理</a></li>
            </ul>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="content_manage">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-screen-tablet"></i>
                <span class="title">内容维护</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="hotnews">
                    <li data-view="news"><a href="${ctx}/news"><i class="icon-docs"></i>理财资讯</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="notice">
                    <li data-view="notice"><a href="${ctx}/baseinfo/notice"><i class="icon-loop"></i>公告管理</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="notes">
                    <li data-view="notes"><a href="${ctx}/notes"><i class="icon-loop"></i>协议管理</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="pc_content_manage">
                    <li>
                        <a href="javascript:void(0);">
                            <i class="icon-settings"></i> PC内容维护 <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <@shiro.hasPermission name="indexpic">
                                <li data-view="indexpic"><a href="${ctx}/pc/indexpic"> <i
                                        class="icon-graph"></i>轮播图维护</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="storyshare">
                                <li data-view="storyshare"><a href="${ctx}/pc/storyshare"> <i class="fa fa-users"></i>理财分享</a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="aboutus">
                                <li data-view="aboutus"><a href="${ctx}/pc/aboutus"> <i class="fa fa-users"></i>关于我们</a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="help_center">
                                <li data-view="help"><a href="${ctx}/help"> <i class="fa fa-users"></i>帮助中心</a></li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>

                <@shiro.hasPermission name="app_content_manage">
                    <li>
                        <a href="javascript:void(0);">
                            <i class="icon-settings"></i> APP内容维护 <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <@shiro.hasPermission name="app_banner">
                                <li data-view="banner"><a href="${ctx}/banner"><i class="icon-target"></i> 轮播图</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="push_msg">
                                <li data-view="push"><a href="${ctx}/product/push"><i class="icon-target"></i> 推送消息</a>
                                </li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>

                <@shiro.hasPermission name="question">
                    <li data-view="question"><a href="${ctx}/question"><i class="icon-book-open"></i> 风险评估问题</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="riskmodule">
                    <li data-view="riskmodel"><a href="${ctx}/riskmodule"><i class="icon-book-open"></i> 风险评估模型</a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="user_manage">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-user"></i>
                <span class="title">用户管理</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="user_manage">
                    <li data-view="user"><a href="${ctx}/user"> <i class="fa fa-users"></i>用户维护</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="customer">
                    <li><a href="${ctx}/customer"> <i class="icon-cup"></i>客服管理</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="role">
                    <li data-view="role"><a href="${ctx}/role"><i class="icon-docs"></i> 角色管理</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="resource">
                    <li data-view="module"><a href="${ctx}/module"><i class="icon-docs"></i> 资源管理</a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="message_manage">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-envelope"></i>
                <span class="title">消息管理</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="message_send">
                    <li data-view="msgs"><a href="${ctx}/msgs"><i class="icon-docs"></i>站内消息</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sms_send">
                    <li><a href="${ctx}/customer"> <i class="icon-cup"></i>短信发送</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="email_send">
                    <li data-view="role"><a href="${ctx}/role"><i class="icon-docs"></i> 邮件发送</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="ismg">
                    <li data-view="ismg"><a href="${ctx}/baseinfo/ismg"><i class="icon-equalizer"></i> 短信网关</a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
    </@shiro.hasPermission>

    <@shiro.hasPermission name="reports">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-bar-chart"></i>
                <span class="title">统计报表</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="report_totalproduct">
                    <li data-view="report_totalproduct"><a href="${ctx}/report/totalproduct"><i class="icon-equalizer"></i> 产品资金汇总报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_sale">
                    <li data-view="report_sale"><a href="${ctx}/report/sale"><i class="icon-equalizer"></i> 日终销售报表</a></li>
                </@shiro.hasPermission>
                <#--<li data-view="report_sale_product"><a href="${ctx}/report/sale"><i class="icon-equalizer"></i> 产品销售报表</a></li>-->
                <@shiro.hasPermission name="product_profit">
                    <li data-view="report_profit"><a href="${ctx}/report/profit"><i class="icon-equalizer"></i> 日终收益报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_cancel">
                    <li data-view="report_redeem"><a href="${ctx}/report/redeem"><i class="icon-equalizer"></i> 日退单报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_fail">
                    <li data-view="raisefail"><a href="${ctx}/report/raisefail"><i class="icon-equalizer"></i> 产品募集失败报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_transfer">
                    <li data-view="report_transfer"><a href="${ctx}/report/transfer"><i class="icon-equalizer"></i> 日转让报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_listing">
                    <li data-view="loading_fee"><a href="${ctx}/report/loadingfee"><i class="icon-equalizer"></i> 挂牌费报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="product_fee">
                    <li data-view="report_fee"><a href="${ctx}/report/fee"><i class="icon-equalizer"></i> 手续费报表</a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sms_record">
                    <li data-view="log-sms"><a href="${ctx}/log/sms"><i class="icon-speech"></i> 短信发送报表 </a></li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="api_record">
                    <li data-view="log-apirecord"><a href="${ctx}/log/apirecord"><i class="icon-speech"></i> 银行接口调用报表 </a></li>
                </@shiro.hasPermission>
            </ul>
        </li>

    </@shiro.hasPermission>
    <@shiro.hasPermission name="settings">
        <li>
            <a href="javascript:void(0);">
                <i class="icon-rocket"></i>
                <span class="title">系统设置</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <@shiro.hasPermission name="common_settings">
                    <li data-view="notice"><a href="${ctx}/baseinfo/notice"><i class="icon-loop"></i>公共参数设置</a></li>
                </@shiro.hasPermission>

                <@shiro.hasPermission name="biz_rule_settings">
                    <li>
                        <a href="javascript:void(0);">
                            <i class="icon-settings"></i> 业务规则设置 <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <@shiro.hasPermission name="integral_rule">
                                <li data-view="points"><a href="${ctx}/points"><i class="icon-note"></i> 积分规则设置 </a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="member_level">
                                <li data-view="push"><a href="${ctx}/product/push"><i class="icon-target"></i>
                                    会员等级规则</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="transfer_rule">
                                <li data-view="transfer_rule"><a href="${ctx}/transfer/rule"><i class="icon-target"></i>
                                    产品转让规则</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="clearing_rule">
                                <li data-view="clearingRule"><a href="${ctx}/clearingRule"><i class="icon-target"></i>
                                    产品结算规则</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="risk_rule">
                                <li data-view="riskrule"><a href="${ctx}/riskrule"><i class="icon-target"></i>
                                    风险等级规则</a></li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>

                <@shiro.hasPermission name="basic_date_manage">
                    <li>
                        <a href="javascript:;">
                            <i class="icon-settings"></i> 基础数据维护 <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <@shiro.hasPermission name="bank_info">
                                <li data-view="bank"><a href="${ctx}/baseinfo/bank"><i class="icon-bell"></i>银行信息维护</a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="issuer">
                                <li data-view="issuer"><a href="${ctx}/issuer"><i class="icon-map"></i> 发行机构</a></li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="dict">
                                <li data-view="dict"><a href="${ctx}/baseinfo/dict"><i class="icon-disc"></i> 字典维护</a>
                                </li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>

                <@shiro.hasPermission name="version">
                    <li data-view="news"><a href="${ctx}/version"><i class="icon-docs"></i>升级管理</a></li>
                </@shiro.hasPermission>
            </ul>
        </li>
        </@shiro.hasPermission>
    <@shiro.hasPermission name="syslog">
        <li data-view="syslog">
            <a href="${ctx}/syslog"><i class="icon-docs"></i><span class="title">系统日志</span></a>
        </li>
    </@shiro.hasPermission>

        <li data-view="about">
            <a href="${ctx}/about"><i class="icon-docs"></i><span class="title">系统说明</span></a>
        </li>

    </ul>
</div>
define(function (require, exports, module) {
    return {
        init: function () {
            var logo_uploader = new Uploader();
            logo_uploader.init({
                src       : $('#logo'),
                choose_btn: '上传图标',
                change_btn: '重新上传',
                remove_btn: '删除图标',
                file_mode : 'res'
            });


        }
    };
});
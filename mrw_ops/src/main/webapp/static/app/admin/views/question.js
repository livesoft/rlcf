/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./question/action');

    var category_obj ={

    };

    var Module = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#question_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title'
                    }, {
                        data: 'sort'
                    }, {
                        data: 'category_name'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {

                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                edit_url: g.ctx + "question/edit/" + full["id"],
                                delete_url: g.ctx + "question/delete/" + full["id"],
                                option_url: g.ctx + "question/option/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [2, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Module.init();
        }
    };
});
/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/27/15 00:04
 */
define(function (require, exports, module) {
    var DT = new Datatable();

    var status = {
        "-1" :'失败',
        "1" :'成功',
        '0': ''
    };
    return {
        init: function () {
            DT.init({
                src      : $('#sms_dt'),
                dataTable: {
                    columns   : [
                        {
                            data: 'phone'
                        }, {
                            data      : 'content',
                            orderable : false,
                            searchable: false
                        }, {
                            data: 'status'
                        }, {
                            data: 'dateline'
                        },{
                            data: 'exception_txt',
                            orderable : false,
                            searchable: false
                        }
                    ],
                    columnDefs: [
                        {
                            'targets': 2,
                            render   : function (data, type, full, row) {
                                return status[data];
                            }
                        }
                    ],
                    "order"   : [
                        [0, 'desc']
                    ]
                }
            });

            $('#search_btn').click(function(e) {
                DT.setAjaxParam("multiquery", "1");
                DT.setAjaxParam("s-phone-like", $('#s_phone').val());
                DT.getDataTable().ajax.reload();
                DT.clearAjaxParams();
            });
        }


    };
});
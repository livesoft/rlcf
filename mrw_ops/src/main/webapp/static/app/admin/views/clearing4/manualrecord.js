/*
 * Copyright 2014 mobiao.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {
    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src: $('#record_dt'),
                dataTable: {
                    columns: [{
                        data: 'trade_no'
                    }, {
                        data: 'adjust_mode'
                    }, {
                        data: 'amount'
                    }, {
                        data: 'adjust_time'
                    }, {
                        data: 'username'
                    }],
                    columnDefs: [
                        {
                            targets: 1,
                            render : function (data, type, full, meta) {
                                if(data==constantsStatus.rade_reconciliation.PLUS){
                                    return '<span class="badge badge-danger">增加(+)</span>';
                                }else if(data==constantsStatus.rade_reconciliation.MINUS){
                                    return '<span class="badge badge-info">减去(-)</span>';
                                }else{
                                    return '未知';
                                }
                            }
                        }
                    ],
                    "order": [
                        [3, 'desc']
                    ]
                }
            });
            $('#s_time').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                format: 'YYYY/MM/DD'
            });

            $('#search_btn').click(function (e) {
                DT.setAjaxParam("multiquery", "1");
                DT.setAjaxParam("s-ru.username-like", $('#s_user_name').val());
                DT.setAjaxParam("s_time", $('#s_time').val());
                DT.getDataTable().ajax.reload();
            });
        }
    };
});
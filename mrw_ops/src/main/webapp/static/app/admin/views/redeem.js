/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_audit');
    var href_hbs = require('hbs!./common/list_href');

    var Redeem = function () {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src           : $("#redeem_dt"),
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : {
                    columns   : [{
                        data: 'member_product'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'real_name'
                    },{
                        data: 'electronic_account'
                    }, {
                        data: 'amount'
                    }, {
                        data: 'trade_no'
                    }, {
                        data: 'redeem_time'
                    }, {
                        data: 'status'
                    }, {
                        data: 'member'
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['member'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 7,
                            render : function (data, type, full, meta) {
                                return constantsStatus.redeems[data];
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            var opt = {
                                status   : full['status'] == 1,
                                audit_url: g.ctx + 'redeem/audit/' + full["member_product"] + '-' + full['member']
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order"   : [
                        [6, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-rp.product_name-like", $.trim($('#s_product_name').val()));
                dt.setAjaxParam("s-o.trade_no-like", $.trim($('#s_orderno').val()));
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });

            //审核点击
            $("#audit").live("click", function (e) {
                var audit_url = $(this).attr("audit_url");
                bootbox.confirm("你确定要执行该操作吗", function (result) {
                    if (result) {
                        $.getJSON(
                            audit_url,
                            function (json) {
                                if (json.status == "OK") {
                                    dt.getDataTable().ajax.reload();
                                } else {
                                    bootbox.alert("操作失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function () {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function () {
            Redeem.init();
        }
    };
});
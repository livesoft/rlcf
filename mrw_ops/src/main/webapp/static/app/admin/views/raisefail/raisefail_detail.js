/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#raisefail_detail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'product_name'
                    }, {
                        data: 'buy_time'
                    }, {
                        data: 'real_name'
                    }, {
                        data: 'electronic_account'
                    }, {
                        data: 'trade_amount'
                    },{
                        data: 'trade_no'
                    },{
                        data: 'playmoney_status'
                    }],
                    columnDefs: [
                        {
                            targets: 0,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['member'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: [5],
                            render : function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 6,
                            render: function (data, type, full, meta) {
                                return constantsStatus.member_playmoney[data]
                            }
                        }
                    ],
                    "order": [
                        [4, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_order = $('#s_order').val();
                
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-o.trade_no-like", s_order);
                dt.getDataTable().ajax.reload();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
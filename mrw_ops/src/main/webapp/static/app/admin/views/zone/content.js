/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!../common/list_img'),
        action_hbs = require('hbs!../common/action');
    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src      : $('#content_dt'),
                dataTable: {
                    columns   : [{
                        data: 'id'
                    }, {
                        data      : 'image_url',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'image_height',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'image_width_scale',
                        orderable : false,
                        searchable: false
                    }, {
                        data: 'enable_flag'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render : function (data, type, full, meta) {
                                return img_hbs({img_url: g.ctx + data});
                            }
                        },
                        {
                            targets: 4,
                            render : function (data, type, full, meta) {
                                return data ? '启用' : '禁用';
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {

                            return action_hbs({
                                edit_url  : g.ctx + 'product/content/edit/' + full['id'],
                                delete_url: g.ctx + 'product/content/delete/' + full['id']
                            });
                        })
                    ],
                    "order"   : [
                        [3, 'desc']
                    ]
                }
            })
        }
    };
});
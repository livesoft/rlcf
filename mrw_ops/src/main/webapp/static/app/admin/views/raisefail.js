/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!./common/list_img');
    var action_hbs = require('hbs!./common/action_one');

    var Help = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#raisefail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name'
                    }, {
                        data: 'issuser_name'
                    }, {
                        data: 'price'
                    }, {
                        data: 'total'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    name:'查看明细',
                                    url: g.ctx + 'report/raisefail/detail_index/' + full['id']
                                });
                        })
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                var s_product_name = $('#s_product_name').val();
                dt.setAjaxParam("s-p.name-like", s_product_name);
                var s_org_name = $('#s_org_name').val();
                dt.setAjaxParam("s-i.name-like", s_org_name);
                dt.getDataTable().ajax.reload();

                $.post(g.ctx +"report/raisefail/sum_json",{
                    s_product_name:s_product_name,
                    s_org_name:s_org_name
                }).done(function(obj){
                    $("#t_total").text(obj.t_total==null?0:obj.t_total);
                    $("#t_price").text(obj.t_price==null?0:obj.t_price);
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Help.init();
        }
    };
});
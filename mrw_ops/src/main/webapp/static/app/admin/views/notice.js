/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 * 
 * path: 
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');
    var img_hbs = require('hbs!./common/list_img');

    var Notice = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#notice_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'pic',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'summary'
                    }, {
                        data: 'pubdate'
                    }, {
                        data: 'publisher'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render:function(data,type,full,meta){
                                if(data){
                                    return img_hbs({img_url: g.ctx + data});
                                }else{
                                    return '暂无';
                                }
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'baseinfo/notice/edit/' + full['id']
                                , delete_url :  g.ctx + 'baseinfo/notice/delete/' + full['id']});
                        })
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-title-like", $('#s_title').val());
                dt.setAjaxParam("pubdate", $('#s_pubdate').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Notice.init();
        }
    };
});
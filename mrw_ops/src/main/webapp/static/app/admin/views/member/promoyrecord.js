/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var Record = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#pmr_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'nick_name',
                        orderable: false
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'yield'
                    }, {
                        data: 'amount'
                    }, {
                        data: 'no'
                    }, {
                        data: 'exec_time'
                    }, {
                        data: 'principal'
                    }],
                    columnDefs: [
                        DTKit.col_check
                    ],
                    "order": [
                        [6, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("member", $('#s_member').val());
                dt.setAjaxParam("product", $('#s_product').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Record.init();
        }
    };
});
/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 4/12/15 20:58
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');

    var dt = new Datatable();
    return {
        init: function () {
            $('#start_end').daterangepicker({
                opens : (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
            var dataTable = {
                columns   : [{
                    data      : 'real_name',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'electronic_account'
                }, {
                    data: 'buy_time'
                }, {
                    data: 'amount'
                }, {
                    data: 'trade_no'
                }],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'member/detail_view/'+full['id'],
                                dis_name:data
                            });
                        }
                    },
                    {
                        targets: [3],
                        render : function (data, type, full, meta) {
                            return (data ? data : '0') + '元';
                        }
                    },
                    {
                        targets: [4],
                        render : function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                dis_name:data
                            });
                        }
                    }
                ],
                "order"   : [
                    [2, 'desc']
                ]
            };

            var $playmoneyDt = $("#productlist_dt");

            dt.init({
                src           : $playmoneyDt,
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });


            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-o.trade_no-like", $.trim($('#s_order').val()));

                var date = $('#start_end').val();
                dt.setAjaxParam("time", date ? date : '');
                dt.getDataTable().ajax.reload();
            });
        }
    };
});
/**
 * Created by liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {
    return {
        init: function () {

            var content = UE.getEditor('content'
                , {
                    initialFrameHeight: 450,
                    elementPathEnabled: false, // 不启用元素路径
                    wordCount: false, //关闭字数统计
                    toolbars          : [[ 'fontsize',  '|', 'undo', 'redo','blockquote', 'horizontal', '|', 'removeformat', '|', 'insertvideo', 'simpleupload', 'insertimage'],
                        ['bold', 'italic', 'underline', 'forecolor', 'backcolor', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight', '|', 'insertorderedlist', 'insertunorderedlist', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter']]
                });

            content.setDisabled();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');

    var Aboutus = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#aboutus_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'type_name',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'title',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'source',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'create_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({
                                edit_url: g.ctx + 'pc/aboutus/edit/' + full['id'],
                                delete_url:g.ctx + 'pc/aboutus/delete/' + full['id']
                            });
                        })
                    ],
                    "order": [
                        [1, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-title-like", $('#s_title').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Aboutus.init();
        }
    };
});
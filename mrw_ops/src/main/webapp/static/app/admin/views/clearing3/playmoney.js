/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    var href_hbs = require('hbs!../common/list_href');

    $.ajaxSetup({
        global: false,
        cache: false,
        beforeSend: function( xhr ) {
            MbaJF.blockUI({
                message: '正在处理中...'
            });
        },
        complete: function(){
            MbaJF.unblockUI();
        }
    });
    var action_hbs = require('hbs!../trade/playmoney/action');
    var dt = new Datatable();


    return {
        init: function () {
            $('#start_end').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });

            var datacount = 0, sh = 0, ld = 0;

            var dataTable = {
                pageLength: 25,
                columns   : [{
                    data: 'id'
                }, {
                    data      : 'real_name',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'electronic_account'
                }, {
                    data      : 'product_name',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'amount'
                }, {
                    data: 'principal'
                }, {
                    data: 'profit'
                }, {
                    data: 'status'
                }, {
                    data: 'money_time'
                }, {
                    data: 'trade_no'
                }, {
                    data: 'adjust_amount'
                }, {
                    data: null
                }],
                columnDefs: [
                    DTKit.col_check,
                    {
                        targets: 1,
                        render: function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'member/detail_view/'+full['member'],
                                dis_name:data
                            });
                        }
                    },
                    {
                        targets: 7,
                        render : function (data, type, full, meta) {
                            return constantsStatus.playmoneys[data];
                        }
                    },
                    {
                        targets: [9],
                        render : function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                dis_name:data
                            });
                        }
                    },
                    {
                        targets: 10,
                        render : function (data, type, full, meta) {
                            if(full['adjust_mode']==1){
                                return  '<span class="badge badge-danger"> +' + data +'</span>'
                            }else if(full['adjust_mode']==2){
                                return   '<span class="badge badge-info">-'+ data+ '</span>'
                            }else{
                                return ''
                            }
                        }
                    },

                    DTKit.renderActionColumn(function (data, type, full, row) {
                        full['ctx'] = g.ctx;

                        full['confirm_p'] = expire_confirm == 1;
                        full['ok_p'] = expire_ok == 1;

                        full['confirm'] = full['status'] == 0 || full['status'] == 4;
                        full['ok'] = full['status'] == 1;

                        full['reconciliations'] = expire_reconciliations==1;
                        return action_hbs(full);
                    })
                ],
                "order"   : [
                    [9,'desc']
                    ],

                createdRow: function (row, record, index) {
                    record.status != 0 && ++sh;
                    record.status != 1 && ++ld;
                    datacount++;
                }
            };

            var $playmoneyDt = $("#playmoney_dt");

            dt.init({
                src           : $playmoneyDt,
                onSuccess     : function (grid) {
                    $('#confirm_btn, #ok_btn').prop('disabled', false);
                    if (datacount == sh) {
                        $('#confirm_btn').prop('disabled', true);
                    }
                    if (datacount == ld) {
                        $('#ok_btn').prop('disabled', true);
                    }
                    sh = 0;
                    ld = 0;
                    datacount = 0;
                },
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });

            $playmoneyDt.on('click', '.confirm_btn', function (e) {
                e.preventDefault();
                var id = $(this).data('pk');
                bootbox.confirm('确定审核通过该条结算信息么？', function (r) {
                    if (r) {
                        $.ajax({
                            url  : g.ctx + 'trade/playmoney/confirm/'+type,
                            type : 'POST',
                            data : {data: id},
                            cache: false,
                            beforeSend: function( xhr ) {
                                MbaJF.blockUI({
                                    message: '正在处理中...'
                                });
                            }
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '财务审核通过操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }

                            MbaJF.unblockUI();
                        }).fail(function() {
                            MbaJF.unblockUI();
                        })
                    }
                });
                return false;
            });
            $playmoneyDt.on('click', '.ok_btn', function (e) {
                e.preventDefault();
                var id = $(this).data('pk');
                bootbox.confirm('确定审批?审批成功后系统将于 ' + stime + ' 21时30分自动执行打款操作!', function (r) {
                    if (r) {
                        $.ajax({
                            url  : g.ctx + 'trade/playmoney/approve/'+type,
                            type : 'POST',
                            data : {data: id},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '审批操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });

            $('#confirm_btn').click(function (e) {
                e.preventDefault();

                var selectedRows = dt.getSelectedRows();
                bootbox.confirm('确定审核通过该条结算信息么？', function (r) {
                    if (r) {
                        $.ajax({
                            url  : g.ctx + 'trade/playmoney/confirm',
                            type : 'POST',
                            data : {data: selectedRows.join(',')},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '财务审核通过操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });
            $('#ok_btn').click(function (e) {
                e.preventDefault();

                var selectedRows = dt.getSelectedRows();
                bootbox.confirm('确定审批?审批成功后系统将于 ' + stime + ' 21时30分自动执行打款操作!', function (r) {
                    if (r) {
                        $.ajax({
                            url  : g.ctx + 'trade/playmoney/approve/'+type,
                            type : 'POST',
                            data : {data: selectedRows.join(',')},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '审批操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });
            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-m.real_name-like", $('#s_real_name').val());
                dt.setAjaxParam("s-o.trade_no-like", $('#s_order').val());

                var time = $('#start_end').val();
                dt.setAjaxParam("time", time);
                var status = $('#status').val();
                dt.setAjaxParam("s-tm.status", status);
                dt.getDataTable().ajax.reload();
            });
        }
    };
});
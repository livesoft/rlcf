/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');

    var Module = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#role_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name'
                    }, {
                        data: 'description'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                edit_url: g.ctx + "role/edit/" + full["id"],
                                delete_url: g.ctx + "role/delete/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [2, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            //启用、禁用点击
            $("#onoff").live("click",function(e){
                var id = $(this).attr("d_id");
                var status_url = $(this).attr("status_url");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            status_url,
                            {
                                id:id,
                                set_val:set_val
                            },
                            function(json){
                                if(json.status =="OK"){
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("修改失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Module.init();
        }
    };
});
/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!../common/list_img'),
        action_hbs = require('hbs!../common/action');
    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src      : $('#app_dt'),
                dataTable: {
                    columns   : [{
                        data: 'id'
                    }, {
                        data      : 'title',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'content',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'remark',
                        orderable : false,
                        searchable: false
                    }, {
                        data: 'url'
                    }, {
                        data: 'display_order'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            return action_hbs({
                                edit_url  : g.ctx + 'product/app/edit/' + full['id'],
                                delete_url: g.ctx + 'product/app/delete/' + full['id']
                            });
                        })
                    ],
                    "order"   : [
                        [5, 'asc']
                    ]
                }
            })
        }
    };
});
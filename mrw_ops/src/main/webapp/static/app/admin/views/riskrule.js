/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_edit');

    // 初始化
    var dt = new Datatable();

    return {
        init: function() {
            dt.init({
                src: $("#riskrule_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data:'risk_tolerance'
                    },{
                        data: 'risk_tolerance_name'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    edit_url: g.ctx + 'riskrule/edit/' + full['risk_tolerance']
                                });
                        })
                    ],
                    "order": [
                        [1, "desc"]
                    ]
                }
            });
        }
    };
});
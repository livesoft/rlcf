define(function(require, exports, module) {

    //现状图
    function initChart1 () {
        $('#chart1').highcharts({
            title: false,
            xAxis: {
                categories: ['3-29','3-30','3-31','4-1','4-2','4-3','4-4','4-5','4-6','4-8','4-9','4-10','4-11','4-12','4-13','4-15']
            },
            yAxis: {
                title: {
                    text: '访问次数'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                margin: 30,
                verticalAlign: 'top',
                borderWidth: 0
            },
            credits: {
                enabled:false
            },
            series: [{
                name: '产品访问次数',
                data: [11, 11, 15, 13, 12, 13, 10,14,16,11,12,12,1,2,1,22]
            }]
        });

    }

    function initChart5 () {

        $('#chart5').highcharts({
            title: false,
            xAxis: {
                categories: ['3-29','3-30','3-31','4-1','4-2','4-3','4-4','4-5','4-6','4-8','4-9','4-10','4-11','4-12','4-13','4-15']
            },
            yAxis: {
                title: {
                    text: '产品成交额'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                margin: 30,
                verticalAlign: 'top',
                borderWidth: 0
            },
            credits: {
                enabled:false
            },
            series: [{
                name: '产品成交额',
                data: [11, 11, 15, 13, 12, 13, 10,14,16,11,12,12,1,2,1,22]
            }]
        });

    }

    function initChart6 () {

        $('#chart6').highcharts({
            title: false,
            xAxis: {
                categories: ['3-29','3-30','3-31','4-1','4-2','4-3','4-4','4-5','4-6','4-8','4-9','4-10','4-11','4-12','4-13','4-15']
            },
            yAxis: {
                title: {
                    text: '产品收益'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                margin: 30,
                verticalAlign: 'top',
                borderWidth: 0
            },
            credits: {
                enabled:false
            },
            series: [{
                name: '产品收益发放',
                data: [11, 11, 15, 13, 12, 13, 10,14,16,11,12,12,1,2,1,22]
            }]
        });

    }


    //饼状图
    function initChart3 () {
        $('#chart3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: false,
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled:false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000'
                    }
                }
            },
            series: [{
                type: 'pie',
                name:'访问来源',
                data: [
                    ['票据', 335],
                    ['宝宝', 310],
                    ['证券', 234],
                    ['保险', 335]
                ]
            }]
        });
    }

    return {
        init: function() {
            initChart1();
          //  initChart2();
            initChart3();
          //  initChart4();
            initChart5();
            initChart6();
        }
    };
});

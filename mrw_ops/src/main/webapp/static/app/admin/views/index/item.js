define(function(require, exports, module) {
    var formvalidate = new FormValidate();

    return {
        init: function() {

            $("#mask_phone").inputmask("mask", {
                "mask": "(999) 999-9999"
            });
             $("#range_1").ionRangeSlider({
                type: "single",
                step: 100,
                postfix: " 年",
                from: 55000,
                hideText: true
            });

            $("#range_2").ionRangeSlider({
                min: 0,
                max: 5000,
                from: 1000,
                to: 4000,
                type: 'double',
                step: 1,
                prefix: "$",
                prettify: false,
                hasGrid: true
            });
            formvalidate.init({
                src: '#test_form',
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    city: {
                        required: true
                    },
                    datepicker: {
                        required: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    membership: {
                        required: true
                    },
                    service: {
                        required: true,
                        minlength: 2
                    }
                },
                messages: { // custom messages for radio buttons and checkboxes
                    membership: {
                        required: "请选择您的性别"
                    },
                    service: {
                        required: "Please select  at least 2 types of Service",
                        minlength: jQuery.validator.format("Please select  at least {0} types of Service")
                    }
                }
            })
        }
    };
});

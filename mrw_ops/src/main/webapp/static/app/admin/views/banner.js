/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_onoff');
    var img_hbs = require('hbs!./common/list_img');
    //数据库中对应的从1开始
    var section = {
        1:'首页',
        2:'理财中心',
        3:'积分商城'
    }

    var status_label = {
        0:"<span class='label label-default'>待审核</span>",
        1:"<span class='label label-success'>审核通过</span>"
    }

    var Product = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#banner_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'img'
                    }, {
                        data: 'click_url'
                    }, {
                        data: 'section'
                    }, {
                        data: 'sort'
                    }, {
                        data: 'status'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render:function(data,type,full,meta){
                                return img_hbs({img_url: g.ctx + data});
                            }
                        },
                        {
                            targets:3,
                            render:function(data,type,full,meta){
                                return section[data];
                            }
                        },
                        {
                            targets: 5,
                            render:function(data,type,full,meta){
                                //return data
                                return status_label[data];
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                id:full["id"],
                                status:full["status"]==1,
                                status_url: g.ctx + "banner/setStatus",
                                edit_url: g.ctx + "banner/edit/" + full["id"],
                                delete_url: g.ctx + "banner/delete/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [4, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            //启用、禁用点击
            $("#onoff").live("click",function(e){
                var id = $(this).attr("d_id");
                var status_url = $(this).attr("status_url");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            status_url,
                            {
                                id:id,
                                set_val:set_val
                            },
                            function(json){
                                if(json.status =="OK"){
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("修改失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Product.init();
        }
    };
});
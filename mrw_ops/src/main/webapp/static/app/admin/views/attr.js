/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_onoff');

    var status_label = {
        0:"<span class='label label-default'>禁用</span>",
        1:"<span class='label label-success'>启用</span>"
    }

    var Attr = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#attr_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name'
                    }, {
                        data: 'display_name'
                    }, {
                        data: 'data_type'
                    }, {
                        data: 'status'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 3,
                            render:function(data,type,full,meta){
                                var data_type_txt = '';
                                $.each(g.datatypes,function(idx,val){
                                    if(val.code == data){
                                        data_type_txt = val.name;
                                        return false;
                                    }
                                });
                                return data_type_txt;
                            }
                        },
                        {
                            targets: 4,
                            render:function(data,type,full,meta){
                              return status_label[data]
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                status:full['status']=='1',
                                status_url: g.ctx + "product/attr/setStatus",
                                id:full['id'],
                                edit_url: g.ctx + "product/attr/edit/" + full["id"],
                                delete_url: g.ctx + "product/attr/delete/"+full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [1, "asc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-name-like", $('#s_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });

            //启用、禁用点击
            $("#onoff").live("click",function(e){
                var id = $(this).attr("d_id");
                var status_url = $(this).attr("status_url");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            status_url,
                            {
                                id:id,
                                set_val:set_val
                            },
                            function(json){
                                if(json.status =="OK"){
                                    bootbox.alert("修改成功");
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("修改失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Attr.init();
        }
    };
});
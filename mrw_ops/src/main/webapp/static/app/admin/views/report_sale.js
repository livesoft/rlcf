/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_one');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#sale_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'a_date'
                    }, {
                        data: 'cnt'
                    }, {
                        data: 'investor_cnt'
                    }, {
                        data: 'amount_sum'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        {
                            targets: 0,
                            render:function(data,type,full,meta) {
                                return data.split(" ",1)
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    name:'查看明细',
                                    url: g.ctx + 'report/sale/detail_index?date='+ full['a_date']
                                });
                        })
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }



        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_date = $('#s_date').val();
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s_date", s_date);
                dt.getDataTable().ajax.reload();

                $.ajax({
                    url: g.ctx + "report/sale/count_json",
                    type:'POST',
                    data:{
                        s_date:s_date
                    }
                }).done(function(obj){
                    $("#t_amount_sum").text(obj.t_amount_sum == null?0:obj.t_amount_sum);
                    $("#t_cnt").text(obj.t_cnt== null?0:obj.t_cnt);
                    $("#t_investor_cnt").text(obj.t_investor_cnt==null?0:obj.t_investor_cnt);
                });
            });

            $('#index_export').click(function(e) {
                var url = g.ctx + "report/sale/export";
                var closest = $(this).closest("form");
                closest.attr("action",url);
                closest.submit();
            });

            $('#s_date').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
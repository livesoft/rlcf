/**
 * Created by liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {
    return {
        init: function () {

            var content = UE.getEditor('content'
                , {

                    initialFrameHeight: 450,
                    elementPathEnabled: false, // 不启用元素路径
                    wordCount: false, //关闭字数统计
                    toolbars          : [[ 'fontsize',  '|', 'undo', 'redo','blockquote', 'horizontal', '|', 'removeformat', '|', 'insertvideo', 'simpleupload', 'insertimage'],
                        ['bold', 'italic', 'underline', 'forecolor', 'backcolor', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight', '|', 'insertorderedlist', 'insertunorderedlist', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter']]
                });

            var logo_uploader = new Uploader();
            logo_uploader.init({
                src       : $('#logo_uploaded'),
                choose_btn: '上传图标',
                change_btn: '重新上传',
                remove_btn: '删除图标',
                thumbnail : {
                    height: 30
                },
                file_mode : 'res'
            });

            var screenshotsUploader = new Uploader();
            screenshotsUploader.init({
                src       : $('#multi-container'),
                choose_btn: '上传文件',
                remove_btn: '删除文件',
                single: false,
                file_mode : 'res'
            });
        }
    };
});
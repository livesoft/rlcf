/*
 * Copyright 2014 mobiao.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {

    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src: $('#invite_dt'),
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'phone',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'cnt',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'integral',
                        orderable: false,
                        searchable: false
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return data
                            }
                        }
                    ],
                    "order": [
                        [3, 'desc']
                    ]
                }
            });

            $('#search_btn').click(function (e) {
                DT.setAjaxParam("multiquery", "1");
                DT.setAjaxParam("s-m.phone-like", $('#s_phone').val());
                DT.getDataTable().ajax.reload();
                DT.clearAjaxParams();
            });
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#profit_detail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'money_time'
                    },{
                        data: 'real_name'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'electronic_account'
                    }, {
                        data: 'principal'
                    }, {
                        data: 'profit'
                    }, {
                        data: 'adjust_amount'
                    }, {
                        data: 'trade_no'
                    }, {
                        data: 'status'
                    }],
                    columnDefs: [
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['member'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 7,
                            render : function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 6,
                            render : function (data, type, full, meta) {
                                if(full['adjust_mode']==1){
                                    return  '<span class="badge badge-danger"> +' + data +'</span>'
                                }else if(full['adjust_mode']==2){
                                    return   '<span class="badge badge-info">-'+ data+ '</span>'
                                }else{
                                    return ''
                                }
                            }
                        },
                        {
                            targets: 8,
                            render: function (data, type, full, meta) {
                                return "已打款"
                            }
                        },

                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-r_o.trade_no-like", $('#s_order').val());
                dt.getDataTable().ajax.reload();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
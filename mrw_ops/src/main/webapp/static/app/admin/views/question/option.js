/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!../common/action');

    var Module = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#option_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title'
                    }, {
                        data: 'content'
                    }, {
                        data: 'score'
                    },{
                        data: 'sort'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {

                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                edit_url: g.ctx + "question/option_edit/" + full["id"],
                                delete_url: g.ctx + "question/option_delete/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [4, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Module.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./member/action');
    var phone_hbs = require('hbs!./member/phone');
    var elec_account_hbs = require('hbs!./member/elec_account');
    var list_href = require('hbs!./common/list_href');

    var Notice = function () {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#member_dt"),
                onSuccess: function (grid) {
                },
                onError: function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'phone',
                        orderable: false
                    }, {
                        data: 'nick_name'
                    }, {
                        data: 'real_name'
                    }, {
                        data: 'electronic_account'
                    }, {
                        data: 'gender'
                    }, {
                        data: 'email'
                    }, {
                        data: 'regiest_time'
                    }, {
                        data: 'status'
                    }, {
                        data: 'integral'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return list_href({
                                    href_value: g.ctx + 'member/detail_view/'+full['id'],
                                    dis_name:data
                                });
                            }

                        }, {//电子账户
                            targets: 4,
                            render: function (data, type, full, meta) {
                                if(data == "" || data ==undefined){
                                    return "未绑定"
                                }else{
                                    return elec_account_hbs({
                                        url: g.ctx + 'member/account/getAccount/' + full['id'],
                                        data: data
                                    });
                                }

                            }
                        }, {
                            targets: 5,
                            render: function (data, type, full, meta) {
                                if (data == 0) {
                                    return '女';
                                } else if (data == 1) {
                                    return '男';
                                } else {
                                    return "未知";
                                }
                            }
                        },
                        {
                            targets: 8,
                            "visible": false
                        },{
                            targets: 9,
                            render: function (data, type, full, meta) {
                                if(data){
                                    return list_href({
                                        href_value: g.ctx + "member/integral/"+full["id"],
                                        dis_name:data
                                    })
                                }else{
                                    return 0
                                }

                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            var ops = {
                                member_lock:member_lock==1,
                                status: full['status'] == 1, id: full['id'],
                                audit_url: g.ctx + 'member/audit/item/' + full['id'],
                                promoy_record_url: g.ctx + 'member/product_money_record/' + full['id']
                            }
                            return action_hbs(ops);
                        })
                    ],
                    "order": [
                        [7, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-phone-like", $('#s_phone').val());
                dt.setAjaxParam("s-email-like", $('#s_email').val());
                dt.setAjaxParam("s-gender", $('#s_gender').val());
                dt.getDataTable().ajax.reload();
            });

            //启用、禁用点击
            $("#onoff").live("click", function (e) {
                var id = $(this).attr("d_id");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗", function (result) {
                    if (result) {
                        $.getJSON(
                            g.ctx + "member/setStatus",
                            {
                                id: id,
                                set_val: set_val
                            },
                            function (json) {
                                if (json.status == "OK") {
                                    dt.getDataTable().ajax.reload();
                                } else {
                                    bootbox.alert("设置失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function () {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function () {
            Notice.init();
        }
    };
});
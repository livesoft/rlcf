/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!./common/list_img');
    var action_hbs = require('hbs!./common/action');

    var StroyShare = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#storyshare_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'name',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'pic'
                    },{
                        data: 'sort'
                    },{
                        data: 'create_time'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 3,
                            render:function(data,type,full,meta) {
                                if(data){
                                    return img_hbs({img_url: g.ctx + data});
                                }else{
                                    return '暂无';
                                }
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'pc/storyshare/edit/' + full['id']
                                , delete_url :  g.ctx + 'pc/storyshare/delete/' + full['id']});
                        })
                    ],
                    "order": [
                        [4, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-title-like", $('#s_title').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            StroyShare.init();
        }
    };
});
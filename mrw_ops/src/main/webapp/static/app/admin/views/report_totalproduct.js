/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#totalproduct_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'product_name'
                    },{
                        data: 'sum_price'
                    }, {
                        data: 'sum_principal'
                    }, {
                        data: 'sum_orignal'
                    }, {
                        data: 'sum_profit'
                    }, {
                        data: 'sum_adjust_amount'
                    }, {
                        data: 'sum_actual'
                    }],
                    columnDefs: [
                        {
                            targets: 5,
                            render: function (data, type, full, meta) {
                                if(full['fail_sum_adjust_amount'] !=0){
                                    //var v = '<span class="badge badge-info">'+ full['fail_sum_adjust_amount'] +'</span>'
                                    var v = "<span style='color:red'>"+ full['fail_sum_adjust_amount'] +"</span>"
                                    return data+'('+ v +')'
                                }else{
                                    return data+'('+full['fail_sum_adjust_amount'] +')'
                                }
                            }
                        }
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                var product_name = $("#s_product_name").val();
                dt.setAjaxParam("s-p.name-like", product_name);
                dt.getDataTable().ajax.reload();

                var url = g.ctx + "report/totalproduct/index_sum";
                $.post(url,{
                    product_name :product_name
                }).done(function(obj){
                    $("#t_sum_price").text(obj.t_sum_price==null?0:obj.t_sum_price);
                    $("#t_sum_orignal").text(obj.t_sum_orignal==null?0:obj.t_sum_orignal);
                    $("#t_sum_principal").text(obj.t_sum_principal==null?0:obj.t_sum_principal);
                    $("#t_sum_profit").text(obj.t_sum_profit==null?0:obj.t_sum_profit);
                    $("#t_sum_adjust_amount").text(obj.t_sum_adjust_amount==null?0:obj.t_sum_adjust_amount);
                    $("#t_sum_actual").text(obj.t_sum_actual==null?0:obj.t_sum_actual);
                    $("#t_fail_sum_adjust_amount").text(obj.t_fail_sum_adjust_amount==null?0:obj.t_fail_sum_adjust_amount);
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    var Integral = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#integral_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'real_name',
                        orderable: false
                    }, {
                        data: 'type'
                    }, {
                        data: 'earning'
                    }, {
                        data: 'expense'
                    }, {
                        data: 'balance'
                    }, {
                        data: 'create_time'
                    },{
                        data: 'remark'
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render:function(data,type,full,meta) {
                                if(data){
                                    return constantsStatus.intergrals[data];
                                }else{
                                    return '';
                                }
                            }
                        },
                        {
                            targets: 3,
                            render:function(data,type,full,meta) {
                                if(data){
                                    return '<span class="label label-success">'+ "+" + data+'</span>';
                                }else{
                                    return data
                                }
                            }
                        },
                        {
                            targets: 4,
                            render:function(data,type,full,meta) {
                                if(data){
                                    return '<span class="label label-default">'+"-"+data+'</span>';
                                }else{
                                    return data
                                }
                            }
                        }
                    ],
                    "order": [
                        [6, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-records.type", $('#s_type').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Integral.init();
        }
    };
});
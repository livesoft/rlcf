/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./transfer_product/action');
    var href_hbs = require('hbs!./common/list_href');

    var Product = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#transfer_product_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'price'
                    }, {
                        data: 'nick_name'
                    }, {
                        data: 'handing_charge'
                    }, {
                        data: 'hold_days'
                    }, {
                        data: 'remaining_days'
                    }, {
                        data: 'transfer_time'
                    },  {
                        data: 'trade_no'
                    }, {
                        data: 'status'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets:1,
                            render:function(data,type,full,meta){
                                return href_hbs({
                                    href_value: g.ctx +"transfer/product/detail/"+full["id"],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets:9,
                            render:function(data,type,full,meta){
                                return constantsStatus.transfer[data]
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                status:full["status"]==0,
                                audit_url: g.ctx + "transfer/product/audit/" + full["id"],
                                trade_url: g.ctx + "transfer/trade/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [7, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-t_p.product_name-like", $('#s_product_name').val());
                dt.setAjaxParam("s-t_p.status", $('#s_status').val());
                dt.setAjaxParam("s-m.real_name-like", $.trim($('#s_member_name').val()));
                dt.setAjaxParam("s-m.phone-like", $.trim($('#s_transfer_phone').val()));
                dt.setAjaxParam("s-o.trade_no-like", $.trim($('#s_trade_no').val()));
                //转让时间
                dt.setAjaxParam("transfer_time", $('#transfer_time').val());
                //转让价格
                dt.setAjaxParam("price_start", $('#s_price_start').val());
                dt.setAjaxParam("price_end", $('#s_price_end').val());
                dt.getDataTable().ajax.reload();
            });

            //审核点击
            $("#audit").live("click",function(e){
                var  audit_url = $(this).attr("audit_url");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            audit_url,
                            function(json){
                                if(json.status =="OK"){
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("操作失败");
                                }
                            }
                        );
                    }
                });
            });

            $('#transfer_time').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Product.init();
        }
    };
});
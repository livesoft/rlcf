/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    var dt = new Datatable();

    return {
        init: function () {


            dt.init({
                src      : $("#module_dt"),
                onSuccess: function (grid) {},
                onError  : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns   : [{
                        data: 'code'
                    }, {
                        data: 'name'
                    }, {
                        data: 'menu'
                    }, {
                        data: 'status'
                    }],
                    columnDefs: [
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return data ? "是" : "否";
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, full, meta) {
                                return data ? "使用中" : "停用";
                            }
                        }
                    ],
                    "order"   : [
                        [0, "asc"]
                    ]
                }
            });

        }
    };
});
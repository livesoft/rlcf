/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    var min_hbs = require('hbs!./riskmodule/list_min');
    var max_hbs = require('hbs!./riskmodule/list_max');

    var Module = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#risk-model_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'risk_level'
                    },{
                        data: 'min_score'
                    }, {
                        data: 'max_score'
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render:function(data,type,full,meta){
                                $.each(sys_dict.risk_tolerance,function(index, element){
                                   if(element.code == data ){
                                        data = element.name
                                   }
                                });
                                return data
                            }
                        },
                        {
                            targets: 2,
                            render:function(data,type,full,meta){
                                return min_hbs({min_val:data,id:full["id"]});
                            }
                        },
                        {
                            targets: 3,
                            render:function(data,type,full,meta){
                                return max_hbs({max_val:data,id:full["id"]});
                            }

                        }
                    ],
                    "order": [
                        [2, "asc"]
                    ]
                }
            });
        }

        function bindEvents() {
            //最小值修改
            $("td input.min").live("blur",function(){
                var min_val = $(this).val();
                var id = $(this).attr("r_id");
                if(!min_val == 0 ){
                    $.ajax({
                        type: "POST",
                        url: g.ctx + "riskmodule/setMinValue",
                        data: { id: id, val: min_val}
                    })
                        .fail(function() {
                            alert( "error" );
                        })
                }
            });

            //最大值修改
            $("td input.max").live("blur",function(){
                var max_val = $(this).val();
                var id = $(this).attr("r_id");
                if(!max_val == 0 ){
                    $.ajax({
                        type: "POST",
                        url: g.ctx + "riskmodule/setMaxValue",
                        data: { id: id, val: max_val}
                    })
                        .fail(function() {bootbox.alert("error");
                        })
                }
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();

    return {
        init: function() {
            Module.init();
        }
    };
});
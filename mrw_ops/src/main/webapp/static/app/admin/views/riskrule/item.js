/**
 * Created by
 * liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {

    return {
        init: function () {

            function extracted(val) {
                $(".my_input").attr("disabled", true);
                $(".my_input").css('display', 'none');

                $("#" + val).attr("disabled", false);
                $("#" + val).css('display', 'block');
            }

            var cur_value = $("input[name='rule.type']:checked").attr("dv");
            extracted(cur_value);


            $("input[name='rule.type']").change(function(){
                var val = $(this).attr("dv");
                extracted(val);
            });
        }
    };
});
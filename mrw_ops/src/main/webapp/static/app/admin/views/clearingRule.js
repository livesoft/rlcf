/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 * 
 * path: 
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_edit');

    var product_type_obj = {
        1:"一次性还本付息",
        2:"每月等额本息",
        3:"本息滚动投资"
    }

    var clearing_cycle_obj = {
        1:"每月",
        2:"每日",
        0:"到期结算"
    }

    var Clearing = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#clearingRule_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'product_type',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'clearing_cycle',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'clearing_day',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'clearing_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets:1,
                            render:function(data){
                                return product_type_obj[data]
                            }
                        },
                        {
                            targets:2,
                            render:function(data){
                                return  clearing_cycle_obj[data]
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'clearingRule/edit/' + full['id']});
                        })
                    ],
                    "order": [
                        [1, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-name-like", $('#s_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Clearing.init();
        }
    };
});
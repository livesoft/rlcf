/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 * 
 * path: 
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');
    var img_hbs = require('hbs!./common/list_img');

    var Issuer = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#issuer_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'logo',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'clear_rate',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'create_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render:function(data,type,full,meta){
                                if(!data){
                                    return '暂无图片';
                                }
                                return img_hbs({img_url: g.ctx + data});
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'issuer/edit/' + full['id']
                                , delete_url :  g.ctx + 'issuer/delete/' + full['id']});
                        })
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-name-like", $('#s_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Issuer.init();
        }
    };
});
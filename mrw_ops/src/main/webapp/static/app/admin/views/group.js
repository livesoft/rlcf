/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!./common/list_img'),
        action_hbs = require('hbs!./group/action');

    var status_label = {
        0:"<span class='label label-default'>禁用</span>",
        1:"<span class='label label-success'>启用</span>"
    };

    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src      : $('#group_dt'),
                dataTable: {
                    columns   : [{
                        data: 'id'
                    }, {
                        data      : 'name',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'logo',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'assure_name',
                        orderable : false,
                        searchable: false
                    }, {
                        data: 'display_order'
                    }, {
                        data: 'status'
                    }, {
                        data: 'index_show'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {targets: 6, visible : false},
                        {
                            targets: 2,
                            render : function (data, type, full, meta) {
                                if(data){
                                    return img_hbs({img_url: g.ctx + data});
                                }else{
                                    return '暂无'
                                }

                            }
                        },
                        {
                            targets: 5,
                            render : function (data, type, full, meta) {
                                return status_label[data] ;
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            full['index_show'] = full['index_show'] == 'Y';
                            full['ctx'] = g.ctx;
                            return action_hbs( full );
                        })
                    ],
                    "order"   : [
                        [4, 'asc']
                    ]
                }
            })
        }
    };
});
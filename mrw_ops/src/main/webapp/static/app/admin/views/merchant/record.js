/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');


    var dt = new Datatable();


    return {
        init: function () {
            var dataTable = {
                columns   : [{
                    data: 'id'
                }, {
                    data: 'order_type'
                }, {
                    data: 'trade_amount'
                }, {
                    data      : 'remark',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'status'
                }, {
                    data: 'record_time'
                }],
                columnDefs: [
                    {data:null,width:"58px",defaultContent:1,orderable:!1,searchable:!1,title:"序号",targets:0},
                    {
                        targets:1,
                        render : function (data, type, full, meta) {
                            return constantsStatus.ordertypes[data];
                        }
                    },
                    {
                        targets: 4,
                        render : function (data, type, full, meta) {
                            return data == 3 ? '<span class="label label-success">成功</span>' : '<span class="label label-danger">失败</span>';
                        }
                    }
                ],
                "order"   : [
                    [5, "desc"]
                ]
            };


            dt.init({
                src           : $("#mechant_dt"),
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });
            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                var time = $('#start_end').val();
                if (time) {
                    dt.setAjaxParam("time", time);
                }

                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });

            $('#start_end').daterangepicker({
                opens : (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });

        }
    };
});
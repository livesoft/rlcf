/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    var href_hbs = require('hbs!./common/list_href');

    var action_hbs = require('hbs!./trade/clearing/action7');
    var dt = new Datatable();

    var confirm_url = g.ctx + 'trade/playmoney/confirm/7';
    var approve_url = g.ctx + 'trade/playmoney/approve/7';

    var playmoneys = [
        '<span class="label label-default">待提现</span>',
        '<span class="label label-info">财务已审核</span>',
        '<span class="label label-info">领导已批准</span>',
        '<span class="label label-success">已提现</span>',
        '<span class="label label-danger">提现失败</span>'
    ];

    return {
        init: function () {
            $('#start_end').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
            var dataTable = {
                columns   : [{
                    data: 'id'
                }, {
                    data      : 'name',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'suname',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'price'
                }, {
                    data: 'status'
                }, {
                    data: 'raised_mount'
                }, {
                    data: null
                }],
                columnDefs: [
                    DTKit.col_check,
                    {
                        targets: 1,
                        render: function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'product/detail_view/'+full['product'],
                                dis_name:data
                            });
                        }
                    },
                    {
                        targets: 4,
                        render : function (data, type, full, meta) {
                            return playmoneys[data];
                        }
                    },
                    {
                        targets: [5],
                        render : function (data, type, full, meta) {
                            return (data ? data : '0') + '元';
                        }
                    },
                    DTKit.renderActionColumn(function (data, type, full, row) {
                        full['ctx'] = g.ctx;

                        full['confirm_p'] = platform_confirm == 1;
                        full['ok_p'] = platform_ok == 1;

                        full['confirm'] = full['status'] == 0 || full['status'] == 4;
                        full['ok'] = full['status'] == 1;
                        return action_hbs(full);
                    })
                ],
                "order"   : [
                    [0,'desc']
                    ]
            };

            var $playmoneyDt = $("#playmoney_dt");

            dt.init({
                src           : $playmoneyDt,
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });

            $playmoneyDt.on('click', '.confirm_btn', function (e) {
                e.preventDefault();
                var id = $(this).data('pk');
                bootbox.confirm('确认进行打款操作么？', function (r) {
                    if (r) {
                        $.ajax({
                            url  : confirm_url,
                            type : 'POST',
                            data : {data: id},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '确认操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });
            $playmoneyDt.on('click', '.ok_btn', function (e) {
                e.preventDefault();
                var id = $(this).data('pk');
                bootbox.confirm('您确定批准该打款纪录么?', function (r) {
                    if (r) {
                        $.ajax({
                            url  : approve_url,
                            type : 'POST',
                            data : {data: id},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '确认操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });

            $('#confirm_btn').click(function (e) {
                e.preventDefault();

                var selectedRows = dt.getSelectedRows();
                bootbox.confirm('确认进行打款操作么?', function (r) {
                    if (r) {
                        $.ajax({
                            url  : confirm_url,
                            type : 'POST',
                            data : {data: selectedRows.join(',')},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '确认打款操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });
            $('#ok_btn').click(function (e) {
                e.preventDefault();

                var selectedRows = dt.getSelectedRows();
                bootbox.confirm('您确定批准该打款纪录么?', function (r) {
                    if (r) {
                        $.ajax({
                            url  : approve_url,
                            type : 'POST',
                            data : {data: selectedRows.join(',')},
                            cache: false
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                MbaJF.alert({
                                    type   : 'success',
                                    message: '确认打款操作成功'
                                });
                                dt.getDataTable().ajax.reload();
                                dt.clearAjaxParams();
                            } else {
                                MbaJF.alert({
                                    type   : 'warning',
                                    message: rst.message
                                });
                            }
                        })
                    }
                });
                return false;
            });
            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("name", $('#s_name').val());
                dt.setAjaxParam("issuer", $('#isuers').val());

                var status = $('#status').val();
                if(status){
                    dt.setAjaxParam("s-tm.status", status);
                } else {
                    dt.setAjaxParam("s-tm.status", '');
                }
                dt.getDataTable().ajax.reload();
            });
        }
    };
});
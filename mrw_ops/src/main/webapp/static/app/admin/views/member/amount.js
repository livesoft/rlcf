/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    var Amount = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#amount_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name',
                        orderable: false
                    }, {
                        data: 'record_time'
                    }, {
                        data: 'earning'
                    }, {
                        data: 'expense'
                    }, {
                        data: 'remark'
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 3,
                            render:function(data,type,full,meta){
                                if(data == 0){
                                    return "";
                                }else{
                                    return '<span class="badge badge-danger">'+data+'</span>';
                                }

                            }
                        },{
                            targets: 4,
                            render:function(data,type,full,meta){
                                if(data == 0){
                                    return "";
                                }else{
                                    return '<span class="badge badge-info">'+data+'</span>';
                                }
                            }
                        }
                    ],
                    "order": [
                        [2, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("pubdate", $('#s_pubdate').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Amount.init();
        }
    };
});
/**
 * Created by liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {
    return {
        init: function () {
            var logo_uploader = new Uploader();
            logo_uploader.init({
                src       : $('#logo_uploaded'),
                choose_btn: '上传图标',
                change_btn: '重新上传',
                remove_btn: '删除图标',
                thumbnail : {
                    height: 30
                },
                file_mode : 'res'
            });
        }
    };
});
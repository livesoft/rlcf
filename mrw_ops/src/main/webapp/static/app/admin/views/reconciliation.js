/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var bank_stat_hbs = require('hbs!./trade/clearing/bank_stat'),
        pf_stat_hbs = require('hbs!./trade/clearing/pf_stat');
    var href_hbs = require('hbs!./common/list_href');

    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

    var outcoms = [
        '',
        '<span class="badge badge-info badge-roundless">账单一致</span>',
        '<span class="badge badge-danger badge-roundless">差异订单</span>',
        '<span class="badge badge-danger badge-roundless">差异订单</span>',
        '<span class="badge badge-danger badge-roundless">差异订单</span>',
        ''
    ];

    var types = {
        0: '<span class="badge badge-info badge-roundless">订单</span>',
        1: '<span class="badge badge-info badge-roundless">充值</span>',
        2: '<span class="badge badge-info badge-roundless">提现</span>',
        3: '<span class="badge badge-info badge-roundless">转账</span>',
        4: '<span class="badge badge-info badge-roundless">外部订单支付</span>',
        5: '<span class="badge badge-info badge-roundless">退款</span>',
        6: '<span class="badge badge-info badge-roundless">其他订单</span>',
        7: '<span class="badge badge-info badge-roundless">积分购买</span>',
        8: '<span class="badge badge-info badge-roundless">缴费</span>',
        9: '<span class="badge badge-info badge-roundless">撤销</span>'
    };
    var dt = new Datatable();

    var stat = function () {
        $.ajax({
            url       : g.ctx + 'trade/reconciliation/stat',
            data      : {
                order: $.trim($('#order').val()),
                time : $.trim($('#s_date').val()),
                type : $.trim($('#type').val())
            },
            beforeSend: function (xhr) {
                MbaJF.blockUI({
                    message: '正在读取统计数据...'
                });
            },
            complete  : function () {
                MbaJF.unblockUI();
            }
        }).done(function (rst) {
            if (rst.status == 'OK') {
                var data = rst.data;
                var bank_html = bank_stat_hbs(data);
                $('#bank_stat').html(bank_html);
                $('#pf_stat').html(pf_stat_hbs(data));
            }
        })
    };

    return {
        init: function() {

            stat();
            $('#s_date').daterangepicker({
                opens : (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });

            $('#sync_btn').click(function (e) {
                bootbox.confirm('您确定要立即进行银行对账么？', function (r) {
                    if (r) {
                        $.ajax({
                            type: 'POST',
                            url : g.ctx + 'trade/reconciliation/sync'
                        }).done(function (rst) {
                            if (rst.status == 'OK') {
                                bootbox.alert('操作成功！');
                            }
                        });
                    }
                })
            });
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-rca.outerorderno-like", $.trim($('#s_order').val()));
                dt.setAjaxParam("time", $.trim($('#s_date').val()));
                dt.setAjaxParam("s-rca.ordertype", $.trim($('#type').val()));
                if($('#s_exp').is(':checked')){
                    dt.setAjaxParam("s-rca.outcome-ne", '1');
                } else {
                    dt.setAjaxParam("s-rca.outcome-ne", '');
                }

                dt.getDataTable().ajax.reload();
                stat();
            });

            dt.init({
                src: $("#reconciliation_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [ {
                        data: 'id'
                    },{
                        data: 'transdate'
                    }, {
                        data: 'outerorderno'
                    }, {
                        data: 'innerorderno'
                    }, {
                        data: 'orderstatus'
                    }, {
                        data: 'ordertype'
                    }, {
                        data: 'amount'
                    }, {
                        data: 'fee'
                    }, {
                        data: 'buy_time'
                    }, {
                        data: 'outcome'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        {
                            data          : null,
                            width         : "58px",
                            defaultContent: 1,
                            orderable     : !1,
                            searchable    : !1,
                            title         : "序号",
                            targets       : 0
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['outerorderno'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, full, meta) {
                                return constantsStatus.orderstatus[data];
                            }
                        },
                        {
                            targets: 5,
                            render: function (data, type, full, meta) {
                                return types[data];
                            }
                        },
                        {
                            targets: 9,
                            render: function (data, type, full, meta) {
                                return outcoms[data];
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return '<a href="{0}trade/reconciliation/item/{1}" class="btn default blue-stripe btn-xs"><i class="icon-list mr5"></i>详情</a>'.format(g.ctx, full['id']);
                        })
                    ],
                    "order": [
                        [9, "asc"]
                    ]
                }
            });
        }
    };
});
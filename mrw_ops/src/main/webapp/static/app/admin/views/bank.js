/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');
    var img_hbs = require('hbs!./common/list_img');

    var dt = new Datatable();

    return {
        init: function () {
            dt.init({
                src           : $("#bank_dt"),
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : {
                    columns   : [{
                        data: 'id'
                    }, {
                        data      : 'name',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'logo',
                        orderable : false,
                        searchable: false
                    }, {
                        data: 'create_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render : function (data, type, full, meta) {
                                return img_hbs({img_url: g.ctx + data});
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            return action_hbs({
                                edit_url    : g.ctx + 'baseinfo/bank/edit/' + full['id']
                                , delete_url: g.ctx + 'baseinfo/bank/delete/' + full['id']
                            });
                        })
                    ],
                    "order"   : [
                        [2, "desc"]
                    ]
                }
            });
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./help/action');
    var img_hbs = require('hbs!./common/list_img');

    var Help = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#help_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'icon',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'parent_name'
                    }, {
                        data: 'sort'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    edit_url: g.ctx + 'help/edit/' + full['id'],
                                    delete_url :  g.ctx + 'help/delete/' + full['id'],
                                    detail_url :  g.ctx + 'help/content/' + full['id']
                                });
                        })
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-a.title-like", $('#s_title').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Help.init();
        }
    };
});
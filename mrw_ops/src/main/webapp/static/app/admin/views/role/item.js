/**
 * Created by
 * liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {
    var setting = {
        check   : {
            enable: true,
            chkboxType: { "Y" : "p", "N" : "" }
        },
        data    : {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onCheck: onCheck
        }
    };
    var $permissions = $('#permissions');

    function onCheck(e, treeId, treeNode) {
        var zTree      = $.fn.zTree.getZTreeObj("module_tree"),
            checkCount = zTree.getCheckedNodes(true);
        if (checkCount.length > 0) {
            var modules = [];
            for (var i = 0; i < checkCount.length; i++) {
                var item = checkCount[i];
                modules.push(item['id']);
            }
            $permissions.val(modules.join(','))

        }
    }
    return {
        init: function () {
            $.ajax({url: g.ctx + 'module/tree', cache: false}).done(function (rst) {

                    var modules = $.parseJSON(rst);

                    var check_nodes = $permissions.val();
                    if (check_nodes) {
                        for (var i = 0; i < modules.length; i++) {
                            var module = modules[i];
                            if (check_nodes.indexOf(module['id']) > 0) {
                                module['checked'] = true;
                            }
                        }
                    }
                    $.fn.zTree.init($("#module_tree"), setting, modules);


                }
            )
        }
    };
});
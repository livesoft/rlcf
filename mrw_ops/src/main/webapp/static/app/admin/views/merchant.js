/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 4/7/15 10:22
 */
define(function (require, exports, module) {
    return {
        init: function () {

            $('#extract_form, #recharge_form').each(function () {
                $(this).validate({
                    meta: 'validate',
                    errorElement: 'span',
                    errorClass: 'help-block help-block-error',
                    focusInvalid: false,
                    ignore: "",
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else if (element.parent().find('.chosen-container-single').size() > 0) {
                            error.appendTo(element.parent().find('.chosen-container-single'));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },

                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },

                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                    },

                    submitHandler: function (form) {
                        var $cur_form = $(form);
                        var default_tip_txt = $cur_form.data('msg-success');
                        default_tip_txt = !default_tip_txt ? '保存成功' : default_tip_txt;
                        MbaJF.blockUI({
                            target: $cur_form.parents('.portlet'),
                            boxed: true
                        });
                        $cur_form.ajaxSubmit({
                            success: function (responseText, statusText, xhr, $form) {
                                var alert_opts = {
                                    place: 'append',
                                    type: (responseText.status == 'OK' ) ? 'success' : 'warning',
                                    message: (responseText.status == 'OK' ) ? default_tip_txt : responseText.message,
                                    close: true,
                                    reset: true,
                                    focus: true,
                                    closeInSeconds: 5,
                                    icon: 'check'
                                };
                                MbaJF.unblockUI($cur_form.parents('.portlet'));
                                MbaJF.alert(alert_opts);
                                $cur_form[0].reset();
                            },
                            error: function () {
                                MbaJF.unblockUI($cur_form.parents('.portlet'));
                            }
                        });
                    }
                });
            })
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!./common/list_href');

    var Msgs = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#msgs_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'title',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'nick_name',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'send_time'
                    },{
                        data: 'read_time'
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render:function(data,type,full,meta) {
                                return href_hbs({dis_name:data,href_value: g.ctx + "msgs/detail/"+full["id"] });
                            }
                        }
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-member.nick_name-like", $('#s_nick_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Msgs.init();
        }
    };
});
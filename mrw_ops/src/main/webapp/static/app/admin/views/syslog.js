/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    var Help = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#syslog_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'module_name',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'content',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'record_time'
                    }, {
                        data: 'operator_name'
                    }],
                    columnDefs: [
                        DTKit.col_check
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-operator_name-like", $('#operator_name').val());
                dt.setAjaxParam("s_date", $('#s_date').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });


        }

        return {
            init: function() {
                $('#s_date').daterangepicker({
                    opens: (MbaJF.isRTL() ? 'left' : 'right'),
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Help.init();
        }
    };
});
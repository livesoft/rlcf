/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#financecost_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 't_date'
                    }, {
                        data: 'balance'
                    }, {
                        data: 'bank_in_amount'
                    }, {
                        data: 'bank_out_amount'
                    }, {
                        data: 'bank_diff',
                        orderable: false
                    }, {
                        data: 'order_in_amount'
                    }, {
                        data: 'order_out_amount'
                    }, {
                        data: 'order_diff',
                        orderable: false
                    }, {
                        data: 'last_day'
                    }, {
                        data: 'rst_order',
                        orderable: false
                    }, {
                        data: 'rst_bank',
                        orderable: false
                    }],
                    columnDefs: [
                        {
                            targets: 0,
                            render: function (data, type, full, meta) {
                                return data.split(" ",1)
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, full, meta) {
                                //银行差异金额：等于电子账户金额 -（银行收入 - 银行支出 + 上日余额），正数代表比电子账户余额多，负数代表比电子余额少
                               if(data != 0 ){
                                   return '<span class="badge badge-danger">' + data +'</span>'
                               }else{
                                   return data
                               }
                            }
                        },
                        {
                            targets: 7,
                            render: function (data, type, full, meta) {
                                //平台差异金额：等于电子账户金额 - （平台收入 - 平台支出 + 上日余额），正数代表比电子账户余额多，负数代表比电子余额少
                                if(data != 0 ){
                                    return '<span class="badge badge-danger">' + data +'</span>'
                                }else{
                                    return data
                                }
                            }
                        },
                        {
                            targets: 9,
                            render: function (data, type, full, meta) {
                                //上日余额 + 平台当日收入金额 - 平台支出金额 ＝ 电子账户余额 （等于表示平台账单正常）
                                if(data){
                                    return '<span class="badge badge-info">正常</span>'
                                }else{
                                    return '<span class="badge badge-danger">异常</span>'
                                }
                            }
                        },
                        {
                            targets: 10,
                            render: function (data, type, full, meta) {
                                //上日余额 + 银行当日收入金额 - 银行支出金额 ＝ 电子账户余额 （等于表示银行对账正常）
                                if(data){
                                    return '<span class="badge badge-info">正常</span>'
                                }else{
                                    return '<span class="badge badge-danger">异常</span>'
                                }
                            }
                        }
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_date = $('#s_date').val();
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s_date", s_date);
                dt.getDataTable().ajax.reload();
            });

            $('#s_date').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
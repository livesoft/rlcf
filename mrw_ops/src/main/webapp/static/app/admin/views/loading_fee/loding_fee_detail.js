/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#loadingfee_detail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'trade_time'
                    },{
                        data: 'real_name'
                    },{
                        data: 'electronic_account'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'price'
                    }, {
                        data: 'rule_free_times'
                    }, {
                        data: 'lodging_hours'
                    }, {
                        data: 'rule_lodging_fee'
                    }, {
                        data: 'lodging_fee'
                    }, {
                        data: 'trade_no'
                    }],
                    columnDefs: [
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['assignor'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: [9],
                            render : function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                    ],
                    "order": [
                        [1, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_date = $('#s_date').val();
                var s_product_name = $('#s_product_name').val();
                var free_status = $('#free_status').val();
                var s_order = $('#s_order').val();

                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s_date", s_date);
                dt.setAjaxParam("s_order", s_order);
                dt.setAjaxParam("s_product_name", s_product_name);
                dt.setAjaxParam("free_status", free_status);
                dt.getDataTable().ajax.reload();

                $.getJSON(g.ctx +"report/loadingfee/sum_detail_json",{
                    s_date:s_date,
                    s_order:s_order,
                    s_product_name:s_product_name,
                    free_status:free_status
                }).done(function(obj){
                    $("#t_cnt").text(obj.t_cnt==null?0:obj.t_cnt);
                    $("#t_lodging_fee").text(obj.t_lodging_fee==null?0:obj.t_lodging_fee);
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
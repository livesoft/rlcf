/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');

    var status_label = {
        0:"<span class='label label-default'>禁用</span>",
        1:"<span class='label label-success'>启用</span>"
    };

    var User = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#user_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'username',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'phone',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'nickname',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'email'
                    },  {
                        data: 'status'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 5,
                            render : function (data, type, full, meta) {
                                return status_label[data]
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'user/edit/' + full['id']
                                , delete_url :  g.ctx + 'user/delete/' + full['id']});
                        })
                    ],
                    "order": [
                        [1, "asc"]
                    ]
                }
            });
        }





        return {
            init: function() {
                initDT();
                // 查询
                $('#search_btn').click(function(e) {
                    dt.setAjaxParam("multiquery", "1");
                    dt.setAjaxParam("s-phone-like", $('#s_phone').val());
                    dt.setAjaxParam("s-username-like", $('#s_username').val());
                    dt.getDataTable().ajax.reload();
                    dt.clearAjaxParams();
                });
            }
        };
    }();
    return {
        init: function() {
            User.init();
        }
    };
});
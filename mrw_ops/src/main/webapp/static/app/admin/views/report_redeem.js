/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_one');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#redeem_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'adate'
                    }, {
                        data: 'cnt'
                    }, {
                        data: 'amount_sum'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        {
                            targets: 0,
                            render : function (data, type, full, meta) {
                                return data.split(" ",1)
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    name:'查看明细',
                                    url: g.ctx + 'report/redeem/detail_index?date='+ full['adate']
                                });
                        })
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                var s_date = $('#s_date').val();
                dt.setAjaxParam("s_date", s_date);
                dt.getDataTable().ajax.reload();

                $.post(g.ctx +"report/redeem/count_json",{
                    s_date:s_date
                }).done(function(obj){
                    $("#t_cnt").text(obj.t_cnt==null?0:obj.t_cnt);
                    $("#t_amount_sum").text(obj.t_amount_sum==null?0:obj.t_amount_sum);
                });
            });

            $('#s_date').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action_one');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#transfer_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'a_date'
                    }, {
                        data: 'trade_cnt'
                    }, {
                        data: 'trade_sum'
                    }, {
                        data: 'deal_cnt'
                    }, {
                        data: 'deal_sum'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        {
                            targets: 0,
                            render: function (data, type, full, meta) {
                                return data.split(" ",1)
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs(
                                {
                                    name:'查看明细',
                                    url: g.ctx + 'report/transfer/detail_index?date='+ full['a_date']
                                });
                        })
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_date = $('#s_date').val();
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("a_date", s_date);
                dt.getDataTable().ajax.reload();

                $.getJSON(g.ctx + "report/transfer/sum_json",{
                    s_date:s_date
                }).done(function(obj){
                    $("#t_trade_cnt").text(obj.t_trade_cnt==null?0:obj.t_trade_cnt);
                    $("#t_trade_sum").text(obj.t_trade_sum==null?0:obj.t_trade_sum);
                    $("#t_deal_cnt").text(obj.t_deal_cnt==null?0:obj.t_deal_cnt);
                    $("#t_deal_sum").text(obj.t_deal_sum==null?0:obj.t_deal_sum);
                });
            });

            $('#s_date').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!./common/list_href');
    var Attr = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#order_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id',
                        orderable: false
                    }, {
                        data: 'trade_no'
                    }, {
                        data: 'product_name',
                        orderable: false
                    }, {
                        data: 'real_name',
                        orderable: false
                    }, {
                        data: 'buy_time'
                    }, {
                        data: 'order_type'
                    }, {
                        data: 'trade_amount'
                    }, {
                        data: 'fee_amonut'
                    }, {
                        data: 'lodging_amount',
                        orderable: false
                    }, {
                        data: 'status'
                    }],
                    columnDefs: [
                        {targets: [0,8], visible: false},
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['member'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 5,
                            render:function(data,type,full,meta){
                                return constantsStatus.ordertypes[data]
                            }
                        },
                        {
                            targets: 7,
                            render:function(data,type,full,meta){
                                var lodging = full['lodging_amount'];
                                return (data ? data : '0.00') + '/' + (lodging ? lodging : '0.00')
                            }
                        },
                        {
                            targets: 9,
                            render:function(data,type,full,meta){
                                return constantsStatus.orderstatus[data]
                            }
                        }
                    ],
                    "order": [
                        [4, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-o.product_name-like", $('#s_product_name').val());
                dt.setAjaxParam("s-m.real_name-like", $('#s_member_name').val());
                dt.setAjaxParam("s-o.status", $('#s_order_status').val());
                dt.setAjaxParam("buy_time", $('#s_order_time').val());
                //订单金额
                dt.setAjaxParam("trade_amount_start", $('#s_trade_amount_start').val());
                dt.setAjaxParam("trade_amount_end", $('#s_trade_amount_end').val());
                dt.setAjaxParam("s-o.trade_no-like", $('#s_order_no').val());
                dt.setAjaxParam("s-o.order_type", $('#s_order_type').val());
                dt.getDataTable().ajax.reload();
            });
            $('#s_order_time').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Attr.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

    var action_hbs = require('hbs!./trade/clearing/action8');
    var dt = new Datatable();


    return {
        init: function () {
            $('#s_date').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });
            var dataTable = {
                columns   : [{
                    data: null
                }, {
                    data: 'day'
                }, {
                    data: 'orders'
                }, {
                    data: 'members'
                },  {
                    data: 'amount'
                }, {
                    data: null
                }],
                columnDefs: [
                    {
                        data          : null,
                        width         : "58px",
                        defaultContent: 1,
                        orderable     : !1,
                        searchable    : !1,
                        title         : "序号",
                        targets       : 0
                    },
                    {
                        targets: [4],
                        render : function (data, type, full, meta) {
                            return (data ? data : '0') + '元';
                        }
                    },
                    DTKit.renderActionColumn(function (data, type, full, row) {
                        full['ctx'] = g.ctx;
                        full['type'] = clearing_type;
                        return action_hbs(full);
                    })
                ],
                "order"   : [
                    [1,'desc']
                    ]
            };

            var $playmoneyDt = $("#playmoney_dt");

            dt.init({
                src           : $playmoneyDt,
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });

            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("date_time", $.trim($('#s_date').val()));
                dt.getDataTable().ajax.reload();
            });
        }
    };
});
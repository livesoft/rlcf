/*
 * Copyright 2014 mobiao.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/27/15 00:04
 */
define(function (require, exports, module) {
    var DT = new Datatable();

    return {
        init: function () {
            DT.init({
                src      : $('#api_dt'),
                dataTable: {
                    columns   : [
                        {
                            data: 'id'
                        },
                        {
                            data: 'name'
                        }, {
                            data      : 'params',
                            orderable : false,
                            searchable: false
                        }, {
                            data: 'response',
                            orderable : false,
                            searchable: false
                        }, {
                            data: 'call_time'
                        }, {
                            data      : 'exp_txt',
                            orderable : false,
                            searchable: false
                        }
                    ],
                    columnDefs: [
                        {
                            data          : null,
                            width         : "58px",
                            defaultContent: 1,
                            orderable     : !1,
                            searchable    : !1,
                            title         : "序号",
                            targets       : 0
                        },
                        {
                            targets: 2, render: function (data) {
                            return '<div class="note note-info width250 no-p" style="word-wrap: break-word; word-break: normal; "><p>' + data + '</p></div>'
                        }
                        },
                        {
                            targets: 3, render: function (data) {
                            return '<div class="note note-success width250 no-p" style="word-wrap: break-word; word-break: normal; "><p>' + data + '</p></div>'
                        }
                        }
                    ],
                    "order"   : [
                        [4, 'desc']
                    ]
                }
            });

            $('#search_btn').click(function (e) {
                DT.setAjaxParam("multiquery", "1");
                DT.setAjaxParam("s-params-like", $.trim($('#s_param').val()));
                DT.getDataTable().ajax.reload();
                DT.clearAjaxParams();
            });
        }


    };
});
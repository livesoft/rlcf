/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {

    return {
        init: function() {
            $('body').addClass('page-sidebar-closed');
            $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
            $(":input").prop("readonly", true);
            $("select").prop("disabled", true);
            $(":radio").prop("disabled", true);
            $(":checkbox").prop("disabled", true);
        }
    };
});
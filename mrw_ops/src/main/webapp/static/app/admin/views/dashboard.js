define(function(require, exports, module) {

    //访问统计表 现状图
    function initChart1 () {
        $('#chart1').highcharts({
            title: false,
            xAxis: {
                categories: ['3-29','3-30','3-31','4-1','4-2','4-3','4-4','4-5','4-6','4-8','4-9','4-10','4-11','4-12','4-13','4-15']
            },
            yAxis: {
                title: false,
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                margin: 30,
                verticalAlign: 'top',
                borderWidth: 0
            },
            credits: {
                enabled:false
            },
            series: [{
                name: 'PC产品访问次数',
                data: [11, 11, 15, 13, 12, 13, 10,14,16,11,12,12,1,2,1,22]
            }, {
                name: 'APP访问统计',
                data: [21, 19, 88, 23, 22, 66, 16,19,26,21,22,22,8,99,11,32]
            }, {
                name: '微信访问统计',
                data: [21, 19, 25, 23, 22, 55, 16,19,33,21,77,22,8,12,11,32]
            }, {
                name: '移动访问统计',
                data: [21, 19, 25, 23, 22, 77, 16,19,26,21,22,22,44,12,11,32]
            }, {
                name: '联通访问统计',
                data: [22, 19, 25, 23, 22, 33, 44,19,77,21,22,22,8,12,11,32]
            }]
        });
    }

    //平台成交额统计表
    function initChart2 () {
        $.ajax({
            url: g.ctx + 'dashboard/dealAmountChat',
            success: function (data) {
                var status = data.status;
                data.data = status != 'OK' ? [] : data.data;
                var categories = [], series = [];
                for (var i = 0; i < data.data.length; i++) {
                    var item = data.data[i];
                    categories.push(item.adate);
                    series.push(item.amount_sum);
                }
                $('#chart2').highcharts({
                    title: false,
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: false,
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        margin: 30,
                        verticalAlign: 'top',
                        borderWidth: 0
                    },
                    credits: {
                        enabled:false
                    },
                    series: [{
                        name: '成交额',
                        data: series
                    }]
                });
            }
        });
    }

    //品牌销售组成统计 饼状图
    function initChart3 () {

        $.ajax({
            url: g.ctx + 'dashboard/brandProductChat',
            success: function (data) {
                var status = data.status;
                data.data = status != 'OK' ? [] : data.data;
                var root = [];
                for (var i = 0; i < data.data.length; i++) {
                    var item = data.data[i];
                    root.push([item.name, item.cnt]);
                }
                $('#chart3').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: false,
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled:false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name:'产品数量',
                        data: root
                    }]
                });
            }
        });
    }

    //发行机构销售组成统计 饼状图
    function initChart4 () {

        $.ajax({
            url: g.ctx + 'dashboard/orgProductChat',
            success: function (data) {
                var status = data.status;
                data.data = status != 'OK' ? [] : data.data;
                var root = [];
                for (var i = 0; i < data.data.length; i++) {
                    var item = data.data[i];
                    root.push([item.name, item.cnt]);
                }

                $('#chart4').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: false,
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled:false
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name:'产品数量',
                        data: root
                    }]
                });
            }
        });
    }

    //平台收益发放统计表
    function initChart5 () {
        $.ajax({
            url: g.ctx + 'dashboard/profitChat',
            success: function (data) {
                var status = data.status;
                data.data = status != 'OK' ? [] : data.data;
                var categories = [], series = [];
                for (var i = 0; i < data.data.length; i++) {
                    var item = data.data[i];
                    categories.push(item.adate);
                    series.push(item.amount_sum);
                }
                $('#chart5').highcharts({
                    title: false,
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: false,
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        margin: 30,
                        verticalAlign: 'top',
                        borderWidth: 0
                    },
                    credits: {
                        enabled:false
                    },
                    series: [{
                        name: '平台收益发放',
                        data: series
                    }]
                });
            }
        });
    }

    return {
        init: function() {
            initChart1();
            initChart2();
            initChart3();
            initChart4();
            initChart5();
        }
    };
});

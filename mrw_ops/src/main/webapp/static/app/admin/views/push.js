/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./common/action');

    var status = ['待审核','审核通过'];

    var status_label = {
        0:"<span class='label label-default'>待审核</span>",
        1:"<span class='label label-success'>审核通过</span>"
    }

    var Product = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#push_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'sort'
                    }, {
                        data: 'recommend_time'
                    }, {
                        data: 'status'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 4,
                            render:function(data,type,full,meta){
                                return status_label[data];
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                edit_url: g.ctx + "product/push/edit/" + full["id"],
                                delete_url: g.ctx + "product/push/delete/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [2, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-product_name-like", $('#s_product_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });

            //审核点击
            $("#audit").live("click",function(e){
                var  audit_url = $(this).attr("audit_url");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            audit_url,
                            function(json){
                                if(json.status =="OK"){
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("操作失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Product.init();
        }
    };
});
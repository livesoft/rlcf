/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!../common/action_onoff');

    var Attr_detail = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#pro_attr_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: "attribute",
                        orderable: false
                    }, {
                        data: 'attribute_val',
                        orderable: false
                    }, {
                        data: 'display_order'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 1,
                            render : function (data, type, full, meta) {
                                var attr_name = '';
                                $.each(g.attr_json, function (idx, val) {
                                    if (val.id == data) {
                                        attr_name = val.name;
                                        return false;
                                    }
                                });
                                return attr_name;
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                status:full['display_flag']=='1',
                                status_url: g.ctx + "product/detail/setStatus",
                                id:full['id'],
                                edit_url: g.ctx + "product/detail/edit/" + full["id"],
                                delete_url: g.ctx + "product/detail/delete/" + full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ],
                    "order": [
                        [3, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {

            //启用、禁用点击
            $("#onoff").live("click",function(e){
                var id = $(this).attr("d_id");
                var status_url = $(this).attr("status_url");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            status_url,
                            {
                                id:id,
                                set_val:set_val
                            },
                            function(json){
                                if(json.status =="OK"){
                                    bootbox.alert("修改成功");
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("修改失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Attr_detail.init();
        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./notes/action');

    var Notes = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#notes_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'category'
                    }, {
                        data: 'create_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,

                        DTKit.renderActionColumn(function(data, type, full, row) {
                            return action_hbs({edit_url: g.ctx + 'notes/edit/' + full['id']});
                        })
                    ],
                    "order": [
                        [2, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-name-like", $('#s_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Notes.init();
        }
    };
});
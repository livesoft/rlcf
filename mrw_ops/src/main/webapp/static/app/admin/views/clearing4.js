/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/4/15 12:39
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
    var href_hbs = require('hbs!./common/list_href');
    var action_hbs = require('hbs!./trade/clearing/action3');
    var dt = new Datatable();


    return {
        init: function () {

            var dataTable = {
                columns   : [{
                    data: 'id'
                }, {
                    data      : 'name',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'suname',
                    orderable : false,
                    searchable: false
                }, {
                    data: 'price'
                },  {
                    data: 'si'
                }, {
                    data: 'members'
                }, {
                    data: null
                }],
                columnDefs: [
                    {
                        data          : null,
                        width         : "58px",
                        defaultContent: 1,
                        orderable     : !1,
                        searchable    : !1,
                        title         : "序号",
                        targets       : 0
                    },
                    {
                        targets: 1,
                        render: function (data, type, full, meta) {
                            return href_hbs({
                                href_value: g.ctx + 'product/detail_view/'+full['id'],
                                dis_name:data
                            });
                        }
                    },
                    {
                        targets: [3],
                        render : function (data, type, full, meta) {
                            return (data ? data : '0') + '单';
                        }
                    },
                    {
                        targets: [4],
                        render : function (data, type, full, meta) {
                            return (data ? data : '0') + '元';
                        }
                    },
                    DTKit.renderActionColumn(function (data, type, full, row) {
                        full['ctx'] = g.ctx;
                        full['type'] = clearing_type;
                        return action_hbs(full);
                    })
                ],
                "order"   : [
                    [1,'desc']
                    ]
            };

            var $playmoneyDt = $("#playmoney_dt");

            dt.init({
                src           : $playmoneyDt,
                onSuccess     : function (grid) {},
                onError       : function (grid) {
                },
                loadingMessage: '加载中...',
                dataTable     : dataTable
            });

            // 查询
            $('#search_btn').click(function (e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("name", $('#s_name').val());
                dt.setAjaxParam("issuer", $('#isuers').val());

                var status = $('#status').val();
                if(status){
                    dt.setAjaxParam("s-tm.status", status);
                } else {
                    dt.setAjaxParam("s-tm.status", '');
                }
                dt.getDataTable().ajax.reload();
            });
        }
    };
});
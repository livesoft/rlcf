/*
 * Copyright 2014 mobiao.inc. All right reserved.
 * 
 * path: 
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/8/15 17:07
 */
define(function (require, exports, module) {

    var action_hbs = require('hbs!./zone/action');

    var status_labels = [
        '<span class="label label-default">停用</span>',
        '<span class="label label-success">启用</span>'
    ];

    // 初始化
    var ZoneDT = new Datatable();
    return {
        init: function () {
            ZoneDT.init({
                src      : $('#zone_dt'),
                dataTable: {
                    columns   : [{
                        data: 'id'
                    }, {
                        data      : 'title',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'title_color',
                        orderable : false,
                        searchable: false
                    }, {
                        data      : 'display_order',
                        orderable : false,
                        searchable: false
                    }, {
                        data: 'status'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render : function (data, type, full, meta) {
                                return '<span style="background-color: {0}">标题</span>'.format(data);
                            }
                        },
                        {
                            targets: 4,
                            render : function (data, type, full, meta) {
                                return status_labels[data];
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {

                            return action_hbs({
                                edit_url   : g.ctx + 'zone/edit/' + full['id'],
                                delete_url : g.ctx + 'zone/delete/' + full['id'],
                                content_url: g.ctx + 'zone/content/' + full['id']
                            });
                        })
                    ],
                    "order"   : [
                        [3, 'desc']
                    ]
                }
            });

            // 查询
            $('#search_btn').click(function(e) {
                ZoneDT.setAjaxParam("multiquery", "1");
                ZoneDT.setAjaxParam("s-title-like", $('#s_title').val());
                ZoneDT.getDataTable().ajax.reload();
                ZoneDT.clearAjaxParams();
            });

        }
    };
});
/*
 * Copyright 2014 mobiao.inc. All right reserved.
 *
 * path:
 * author: sagyf yang
 * version: 1.0.0
 * date: 2/26/15 00:13
 */
define(function (require, exports, module) {
    var img_hbs = require('hbs!./common/list_img'),
        action_hbs = require('hbs!./common/action');

    // 初始化
    var DT = new Datatable();
    return {
        init: function () {
            DT.init({
                src: $('#brand_dt'),
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'logo',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'sort',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'create_time'
                    }, {
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                if(data){
                                    return img_hbs({img_url: g.ctx + data});
                                } else {
                                    return '暂无LOGO';
                                }
                            }
                        },
                        DTKit.renderActionColumn(function (data, type, full, row) {
                            return action_hbs({
                                edit_url: g.ctx + 'product/brand/edit/' + full['id'],
                                delete_url: g.ctx + 'product/brand/delete/' + full['id']
                            });
                        })
                    ],
                    "order": [
                        [3, 'asc']
                    ]
                }
            });

            $('#search_btn').click(function (e) {
                DT.setAjaxParam("multiquery", "1");
                DT.setAjaxParam("s-name-like", $('#s_name').val());
                DT.getDataTable().ajax.reload();
                DT.clearAjaxParams();
            });
        }
    };
});
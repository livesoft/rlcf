/**
 * Created by
 * liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');


    return {

        applyEndDate: function (first) {
            var date = $.trim($('#valuedate').val());
            if (!date) {
                return;
            }
            var due_date_el = $('#due_date');
            if (first) {
                due_date_el.val(due_date_el.attr('value'));
            } else {
                var count = parseInt($("#time_limit").val()) || 0;
                var type = $("#time_limit_unit").find('option:selected').attr('value');
                var val = moment(date).add(type == 'day' ? 'days' : 'months', count);
                val = moment(val).add('day', -1);
                val = moment(val).format('YYYY-MM-DD');
                due_date_el.val(val).attr('value', val);
            }
        },

        init: function () {
            var me = this;

            $('#start_date').on('changeDate', function(ev){
                me.applyEndDate();
            });

            $('#time_limit_unit').change(function () {
                me.applyEndDate();
            });

            $('#time_limit').change(function () {
                me.applyEndDate();
            });

            me.applyEndDate(true);



            $('#transfer_flag').click(function (e) {
                var $transferHoldingDays = $('#transfer_holding_days');
                if ($(this).is(':checked')) {
                    $transferHoldingDays.removeProp('disabled').addClass('required');
                } else {
                    $transferHoldingDays.attr('disabled', 'disabled').removeClass('required');
                }
            });

            $('#start_end').daterangepicker({
                opens: (MbaJF.isRTL() ? 'left' : 'right'),
                format: 'YYYY/MM/DD'
            });

            var logo_uploader = new Uploader();
            logo_uploader.init({
                src       : $('#logo_uploaded'),
                choose_btn: '上传图标',
                change_btn: '重新上传',
                remove_btn: '删除图标',
                thumbnail : {
                    height: 30
                },
                file_mode : 'res'
            });

        }
    };
});
/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var action_hbs = require('hbs!./project/action');
    var img_hbs = require('hbs!./common/list_img');

    //var project_type = ["","宝宝类","募集类","投资类"];

    var project_type = {
        1:"宝宝类",
        2:"募集类",
        3:"投资类"
    }

    var status_label = {
        0:"<span class='label label-default'>禁用</span>",
        1:"<span class='label label-success'>启用</span>"
    }

    var Project = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#projects_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'id'
                    }, {
                        data: 'name'
                    }, {
                        data: 'logo'
                    }, {
                        data: 'type'
                    }, {
                        data: 'description'
                    }, {
                        data: 'products'
                    }, {
                        data: 'status'
                    },{
                        data: null
                    }],
                    columnDefs: [
                        DTKit.col_check,
                        {
                            targets: 2,
                            render:function(data,type,full,meta){
                               return img_hbs({img_url: g.ctx + data});
                            }
                        },
                        {
                            targets: 3,
                            render:function(data,type,full,meta){
                                return project_type[data];
                            }
                        },
                        {
                            targets: 6,
                            render:function(data,type,full,meta){
                                return status_label[data]
                            }
                        },
                        DTKit.renderActionColumn(function(data, type, full, row) {
                            var opt = {
                                status:full['status']=='1',
                                status_url: g.ctx + "project/setStatus",
                                id:full['id'],
                                edit_url: g.ctx + "project/edit/" + full["id"],
                                delete_url: g.ctx + "project/delete/"+full["id"]
                            };
                            return action_hbs(opt);
                        })
                    ] ,
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-name-like", $('#s_name').val());
                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();
            });

            //启用、禁用点击
            $("#onoff").live("click",function(e){
                var id = $(this).attr("d_id");
                var status_url = $(this).attr("status_url");
                var set_val = $(this).attr("d_value");

                bootbox.confirm("你确定要执行该操作吗",function(result){
                    if(result){
                        $.getJSON(
                            status_url,
                            {
                                id:id,
                                set_val:set_val
                            },
                            function(json){
                                if(json.status =="OK"){
                                    dt.getDataTable().ajax.reload();
                                }else{
                                    bootbox.alert("修改失败");
                                }
                            }
                        );
                    }
                });
            });
        }

        return {
            init: function() {
                $('#s_pubdate').daterangepicker({
                    timePicker: false,
                    timePickerIncrement: 30,
                    format: 'YYYY/MM/DD'
                });
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            Project.init();
        }
    };
});
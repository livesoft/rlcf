/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');
    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#fee_detail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'trade_time'
                    },{
                        data: 'real_name'
                    },{
                        data: 'electronic_account'
                    }, {
                        data: 'product_name'
                    }, {
                        data: 'price'
                    }, {
                        data: 'ratio'
                    }, {
                        data: 'handing_charge'
                    }, {
                        data: 'trade_no'
                    }],
                    columnDefs: [
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['assignor'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 5,
                            render : function (data, type, full, meta) {
                                return data + '%'
                            }
                        },{
                            targets: [7],
                            render : function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                var s_date = $('#s_date').val();
                var s_product_name = $('#s_product_name').val();
                var free_status = $('#free_status').val();
                var s_order = $('#s_order').val();

                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s_date", s_date);
                dt.setAjaxParam("s_order", s_order);
                dt.setAjaxParam("s_product_name", s_product_name);
                dt.setAjaxParam("free_status", free_status);

                dt.getDataTable().ajax.reload();
                dt.clearAjaxParams();

                $.getJSON(g.ctx +"report/fee/sum_detail_json",{
                    s_date:s_date,
                    s_order:s_order,
                    s_product_name:s_product_name,
                    free_status:free_status
                }).done(function(obj){
                    $("#t_cnt").text(obj.t_cnt==null?0:obj.t_cnt);
                    $("#t_handing_charge").text(obj.t_handing_charge==null?0:obj.t_handing_charge);
                });
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
/**
 * Created by
 * liuhui on 15/2/5.
 *
 */
define(function (require, exports, module) {

    var $attribute = $('#attribute'),
        $attrVal = $('#attr_val');

    var attribute_val = $('#attribute_val').val(),
        attribute_val2 = $('#attribute_val2').val();

    var attr_format = {
        'int'    : '<input id="control_id" type="number" class="form-control number" name="details.attribute_val"  placeholder="请输入数字" value="{0}">',
        'string' : '<textarea id="control_id" class="form-control" rows="3" name="details.attribute_val" >{0}</textarea>',
        'richtxt': '<script id="control_id" name="details.attribute_val" style="width:540px;height:260px;" type="text/plain">{0}</script>',
        'images' : '<div id="control_id" class="no-p" data-name="details.attribute_val" data-value="{0}"></div>',
        'image'  : '<div id="control_id"  data-obj="{0}" data-name="details.attribute_val"></div>'
    };

    return {
        init: function () {

            $('#spinner1').spinner();


            //属性初始化
            var current_datatype = $attribute.find('option:selected').data('datatype');
            if(current_datatype){
                initAttrControl(current_datatype);
            }


            function initAttrControl(datatype) {
                // 如果已经执行过先销毁
                if($attrVal.find('.edui-editor').length > 0){
                    if (datatype != 'richtxt') {
                        return;
                    }
                    UE.getEditor('control_id').destroy();
                }
                $attrVal.empty();
                var html_format = attr_format[datatype];
                var html = attribute_val ? html_format.format(attribute_val) : html_format.format('');
                $attrVal.html(html);

                switch (datatype) {
                    case 'richtxt':
                        UE.getEditor('control_id',
                            {
                                elementPathEnabled: false, // 不启用元素路径
                                wordCount         : false, //关闭字数统计
                                allowDivTransToP: false,
                                toolbars          : [['fontsize', '|', 'undo', 'redo', 'blockquote', 'horizontal', '|', 'removeformat', '|', 'insertvideo', 'simpleupload', 'insertimage', 'source'],
                                    ['bold', 'italic', 'underline', 'forecolor', 'backcolor', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight', '|', 'insertorderedlist', 'insertunorderedlist', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter']]
                            });
                        break;
                    case 'image':
                        var singleUpload = new Uploader();
                        singleUpload.init({
                            src       : $('#control_id'),
                            choose_btn: '上传文件',
                            remove_btn: '删除文件',
                            single    : true,
                            file_mode : 'res'
                        });
                        break;
                    case 'images':
                        var mulitUpload = new Uploader();
                        mulitUpload.init({
                            src       : $('#control_id'),
                            choose_btn: '上传文件',
                            remove_btn: '删除文件',
                            single    : false,
                            file_mode : 'res'
                        });
                        break;
                }
            }

            $attribute.on('change', function (e) {
                var datatype = $(e.target).find('option:selected').data('datatype');
                initAttrControl(datatype);
            });

        }
    };
});
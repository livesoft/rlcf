/*
 * IntelliJ IDEA: .
 * Copyright 2014 Ifoly.inc. All right reserved.
 *
 * path:
 * author: liuhui
 * version: 1.0.0
 */
define(function (require, exports, module) {
    var href_hbs = require('hbs!../common/list_href');

    var TABLE = function() {
        // 初始化
        var dt = new Datatable();

        function initDT() {
            dt.init({
                src: $("#sale_detail_dt"),
                onSuccess: function(grid) {},
                onError: function(grid) {
                },
                loadingMessage: '加载中...',
                dataTable: {
                    columns: [{
                        data: 'confirm_date'
                    },{
                        data: 'trade_no'
                    },{
                        data: 'order_type'
                    },{
                        data: 'product_name'
                    }, {
                        data: 'real_name'
                    }, {
                        data: 'electronic_account'
                    }, {
                        data: 'trade_amount'
                    }, {
                        data: 'status'
                    }],
                    columnDefs: [
                        {
                            targets: [1],
                            render : function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'order/detail_view/'+full['trade_no'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return constantsStatus.ordertypes[data]
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'product/detail_view/'+full['product'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, full, meta) {
                                return href_hbs({
                                    href_value: g.ctx + 'member/detail_view/'+full['meb_id'],
                                    dis_name:data
                                });
                            }
                        },
                        {
                            targets: 7,
                            render: function (data, type, full, meta) {
                               return constantsStatus.orderstatus[data]
                            }
                        }
                    ],
                    "order": [
                        [0, "desc"]
                    ]
                }
            });
        }

        function bindEvents() {
            // 查询
            $('#search_btn').click(function(e) {
                dt.setAjaxParam("multiquery", "1");
                dt.setAjaxParam("s-ord.trade_no-like", $('#s_order').val());
                dt.setAjaxParam("s-ord.order_type", $('#s_order_type').val());
                dt.getDataTable().ajax.reload();
            });

            $('#s_order_time').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'YYYY/MM/DD'
            });
        }

        return {
            init: function() {
                initDT();
                bindEvents();
            }
        };
    }();
    return {
        init: function() {
            TABLE.init();
        }
    };
});
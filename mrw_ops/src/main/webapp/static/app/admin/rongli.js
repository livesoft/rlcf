var paths = {
    'hbs': 'scripts/libs/hbs',
    'viw_path': 'app/admin/rongli_path',
    'webuploader': 'scripts/webuploader/webuploader'
};

var shim = {'webuploader': {
    exports: 'WebUploader'
}};

require.config({
    locale: "zh_cn",
    baseUrl: g.ctx + 'static/',
    paths: paths,
    hbs: {
        templateExtension: 'hbs',
        disableI18n: false
    },
    shim: shim,
    urlArgs: "_=" + (new Date()).getTime()
});


require(['viw_path', 'hbs!app/admin/views/common/account_item'],function (Path, account_item_hbs) {

    $('#browser_goon_visit').bind('click', function () {
        $.cookie('ops_browser_goon_visit', 1);
        $('#layout_container').show();
        $('#browser_container').remove();
    });

    if ($.browser.msie && $.browser.version < 9 && $.cookie('ops_browser_goon_visit') != 1) {
        $.cookie('ops_browser_goon_visit', 1);
        $('#layout_container').hide();
        $('#browser_container').show();
    } else {
        $('#layout_container').show();
        $('#browser_container').hide();
    }

    Path.initialize();

    $('#showAccounts').bind('click', function (e) {
        loadAccountList();
    });

    $('#refreshAccounts').bind('click', function (e) {
        loadAccountList();
        return false;
    });

    function loadAccountList () {
        var accountList = $('#account-drop-list');
        var loading = $('#account-drop-loading');
        loading.show().siblings().hide();
        $.ajax({
            url    : g.ctx + 'amount',
            success: function (data) {
                accountList.fadeIn().siblings().hide();
                if (data.status == 'OK') {
                    accountList.html(account_item_hbs(data.data));
                } else {

                }
            }
        });
    }

    return {};
});

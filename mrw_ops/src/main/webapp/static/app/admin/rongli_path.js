define(function(require, exports, module) {
    var prefix_path = 'app/admin/views/',
        content_id = 'div#view_content';

    var initialize = function() {
        MbaJF.init();
        Layout.init();


        // view 类似这样 game#create 表示 game模块下的下级页面
        // view 类似这样 game/category#create 表示game/category下的下级界面
        var $content = $(content_id);
        var data_view = $content.data('view');
        var view_splits = data_view.split('#');
        var view, menu_id;
        if (view_splits.length > 1) {
            view = data_view.replace('#', '/');
            menu_id = view_splits[0];
        } else {
            view = view_splits[0];
            menu_id = view;
        }
        var $menu = $('ul.page-sidebar-menu').find('li[data-view="' + menu_id + '"]');
        if ($menu.length > 0) {
            $menu.addClass('active');
            // 判断是否为子菜单
            var $sub = $menu.parents('ul.sub-menu');
            if ($sub.length > 0) {
                // 取得父级菜单
                var $parent_menu = $sub.closest('li');
                $parent_menu.addClass('active').addClass('open');
            }
        }
        if(view_splits.length >=2){
            if (!view_splits[1] || view_splits[1].length <= 0) {
                return;
            }
        }
        var viewPath = prefix_path + view;
        require([viewPath], function(r_view) {
            r_view.init();
        });


    };
    return {
        initialize: initialize
    };
});

var constantsStatus = {
    product: ['<span class="label label-default"> 待发布 </span>',
        '<span class="label label-primary"> 销售中 </span>',
        '<span class="label label-success"> 售罄 </span>',
        '<span class="label label-warning"> 募集失败 </span>',
        '<span class="label label-info"> 已结束 </span>',
        '<span class="label label-info"></span>',
        '<span class="label label-default"> 已删除</span>',
        '<span class="label label-default"> 销售结束</span>',
        '<span class="label label-success"> 已成立 </span>'
    ],
    member : [''],
    redeems : [
        '',
        '<span class="label label-default">待审核</span>',
        '<span class="label label-info">待退款</span>',
        '<span class="label label-success">已退单</span>'
    ],
    playmoneys: [
        '<span class="label label-default">待打款</span>',
        '<span class="label label-info">财务已审核</span>',
        '<span class="label label-info">领导已批准</span>',
        '<span class="label label-success">已打款</span>',
        '<span class="label label-danger">打款失败</span>'
    ],
    orderstatus    : [
        '<span class="label label-default">待支付</span>',
        '<span class="label label-default">退款处理中</span>',
        '<span class="label label-info">处理中</span>',
        '<span class="label label-success">交易成功</span>',
        '<span class="label label-danger">交易关闭</span>',
        '<span class="label label-danger">已删除</span>',
        '<span class="label label-info">已退款</span>',
        '<span class="label label-danger">交易失败</span>',
        '<span class="label label-success">交易完成</span>',
        '<span class="label label-info">待处理</span>'
    ],
    ordertypes     : {
        "BUY": '<span class="badge badge-primary badge-roundless">投资(购买)</span>',
        'INC': '<span class="badge badge-info badge-roundless">收益</span>',
        'RPN': '<span class="badge badge-warning badge-roundless">退单</span>',
        'TPR': '<span class="badge badge-success badge-roundless">转让挂单</span>',
        'TFR': '<span class="badge badge-info badge-roundless">投资(转让)</span>',
        'TPY': '<span class="badge badge-info badge-roundless">转让打款</span>',
        'WRS': '<span class="badge badge-info badge-roundless">账户提现</span>',
        'CRG': '<span class="badge badge-success badge-roundless">账户充值</span>',
        'OFP': '<span class="badge badge-success badge-roundless">交易失败退款</span>',
        'PFP': '<span class="badge badge-danger badge-roundless">产品募集失败退款</span>'
    },
    risklevel : ['', '极低风险', '较低风险', '中等风险', '较高风险', '高风险'],

    intergrals: {
        'SIGNIN': '每日登录',
        'INVITE': '邀请注册奖励',
        'CASH'  : '消费奖励'
    },

    transfer:{
        0:'待审核',
        1:'交易中',
        2:'交易成功',
        3:'转让失效',
        4:"转让撤销"
    },

    /**
     * 会员理财产品 打款状态
     */
    member_playmoney:{
        0:"未打款",
        1:"提交打款",
        2:"打款成功"
    },

    /**
     * 结算金额调账 方式
     */
    rade_reconciliation:{
        'PLUS':1,
        'MINUS':2
    }
};
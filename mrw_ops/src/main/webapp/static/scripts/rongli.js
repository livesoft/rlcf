
/*
 产品新增中form验证
 */
jQuery.validator.addMethod("max_invest_amount", function (value, element) {
    var priceVal = $("#price").val();
    return this.optional(element) || value <= parseFloat(priceVal);
}, "单笔投资上限不能大于产品本金");

jQuery.validator.addMethod("min_invest_amount", function (value, element) {
    var max_invest_amount = parseFloat($("#max_invest_amount").val());
    return this.optional(element) || value < max_invest_amount;
}, "起投投资金额不能大于单笔投资上限");


jQuery.validator.addMethod("increase_amount", function (value, element) {
    var max_invest_amount = parseFloat($("#max_invest_amount").val());
    return this.optional(element) || value < max_invest_amount;
}, "递增金额不能大于单笔投资上限");


// 小数点2位
jQuery.validator.addMethod("decimal2", function (value, element) {
    return this.optional(element) || /^\d+(\.\d{2})?$/.test(value);
}, "小数点后需要输入两位");

jQuery.validator.addMethod("eq_pwd", function (value, element) {
    var p = $(element).attr("p_v");
    var first = $(p).val();
    return this.optional(element) || first == value
}, "新密码两次密码输入不一致");

//统计页面的导出按钮使用
$("a._export").click(function(){
    var href = $(this).data("href");
    var closest = $(this).closest("form");
    closest.attr("action",href);
    closest.submit();
});




package app.kits;

import com.google.common.collect.Maps;
import com.jfinal.kit.PathKit;

import java.io.File;
import java.util.Map;

import static app.models.order.TradeMoney.CANCEL_TYPE;
import static app.models.order.TradeMoney.DUE_TYPE;
import static app.models.order.TradeMoney.FAIL_TYPE;
import static app.models.order.TradeMoney.PAY_FAIL_TYPE;
import static app.models.order.TradeMoney.RAISED_SUCCESS_TYPE;
import static app.models.order.TradeMoney.TRANSFER_TYPE;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ReportTplPathKit {

    private static String bank_check_account;

    private static Map<Integer, String> TYPE_EXCEL = Maps.newHashMapWithExpectedSize(5);

    public static void init() {
        String reportPath = PathKit.getWebRootPath() + File.separator + "reports" + File.separator;
        String clearing_product_cancel_file = reportPath + "clearing_product_cancel.xlsx";
        String clearing_product_fail_file = reportPath + "clearing_product_fail.xlsx";
        String clearing_product_profit_file = reportPath + "clearing_product_profit.xlsx";
        String clearing_product_success_file = reportPath + "clearing_product_success.xlsx";
        String clearing_product_transfer_file = reportPath + "clearing_product_transfer.xlsx";
        bank_check_account = reportPath + "bank_check_account.xlsx";
        String clearing_daily_fail_file = reportPath + "clearing_daily_fail.xlsx";
        TYPE_EXCEL.put(DUE_TYPE, clearing_product_profit_file);
        TYPE_EXCEL.put(FAIL_TYPE, clearing_product_fail_file);
        TYPE_EXCEL.put(TRANSFER_TYPE, clearing_product_transfer_file);
        TYPE_EXCEL.put(CANCEL_TYPE, clearing_product_cancel_file);
        TYPE_EXCEL.put(RAISED_SUCCESS_TYPE, clearing_product_success_file);
        TYPE_EXCEL.put(PAY_FAIL_TYPE, clearing_daily_fail_file);
    }


    public static String getExcelByType(int type){
        return TYPE_EXCEL.get(type);
    }

    public static String getBank_check_account() {
        return bank_check_account;
    }
}

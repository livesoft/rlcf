package app;

import app.directive.HasAnyPermissionsDirective;
import com.jfinal.render.FreeMarkerRender;
import freemarker.template.Configuration;
import goja.mvc.AppLoadEvent;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ApplicationStartup implements AppLoadEvent {
    @Override
    public void load() {

        Configuration configuration = FreeMarkerRender.getConfiguration();
        configuration.setSharedVariable("hasAnyPermissions", new HasAnyPermissionsDirective());
    }
}

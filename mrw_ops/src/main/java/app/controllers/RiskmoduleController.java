package app.controllers;

import app.Const;
import app.models.basic.Dict;
import app.models.basic.RiskModel;
import com.google.common.collect.Sets;
import com.jfinal.plugin.activerecord.Db;
import goja.mvc.Controller;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 评估模型管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class RiskmoduleController extends Controller {

    public void index() {
        //风险承受
        List<Dict> tolerance = Dict.dao.risk_tolerance();

        List<RiskModel> riskModels = RiskModel.dao.findAll();

        if (riskModels.size() <= 0) {
            insertData(tolerance);
        } else {
            Set<String> tolSet = Sets.newHashSet();
            Set<String> riskSet = Sets.newHashSet();
            for (RiskModel riskModel : riskModels) {
                riskSet.add(riskModel.getStr("risk_level"));
            }
            for (Dict dict : tolerance) {
                tolSet.add(dict.getStr(Const.FIELD_CODE));
            }

            //风险承受字典表有但是评估模型中没有 新增
            Sets.SetView<String> tol = Sets.symmetricDifference(tolSet, riskSet);
            Set<String> tempSet = Sets.newHashSet();
            for (String s : tol) {
                RiskModel riskModel = new RiskModel();
                riskModel.set("risk_level",s).save();
                //直接riskSet.add(s)不允许，会报错，使用一个临时set进行保存
                tempSet.add(s);
            }
            //把从字典表中新增的set和评估模型set合并
            Sets.SetView union = Sets.union(riskSet, tempSet);

            //合并后的评估模型中有但是风险承受字典表没有 删除
            Sets.SetView<String> model = Sets.symmetricDifference(union,tolSet);
            for (String s : model) {
                RiskModel.dao.deleteByRisk_level(s);
            }
        }
    }

    private void insertData(List<Dict> tolerance) {
        Object[][] params = new Object[tolerance.size()][];
        //按照系统字典表中进行插入
        for (int i = 0; i < tolerance.size(); i++) {
            Object[] param = new Object[5];
            param[0] = 0;
            param[1] = 0;
            param[2] = 0;
            param[3] = tolerance.get(i).getStr(Const.FIELD_CODE);
            param[4] = new java.sql.Date(DateTime.now().toDate().getTime());
            params[i]=param;
        }
        Db.batch(SqlKit.sql("riskmodel.batch_insert"), params, 50);
    }

    public void dtlist(){
        final DTCriterias criterias = getCriterias();
        renderDataTables(criterias, RiskModel.class);
    }

    /**
     * 设置最小值
     */
    public void  setMinValue(){
        String id = getPara("id","0");
        String value = getPara("val","0");
        RiskModel riskModel = RiskModel.dao.findById(id);
        boolean update = riskModel.set("min_score", value).update();
        if(update){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }

    }

    /**
     * 设置最大值
     */
    public void  setMaxValue(){
        String id = getPara("id","0");
        String value = getPara("val","0");
        RiskModel riskModel = RiskModel.dao.findById(id);
        boolean update = riskModel.set("max_score",value).update();
        if(update){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

}

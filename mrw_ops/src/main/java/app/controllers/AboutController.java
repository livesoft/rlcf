package app.controllers;

import com.jfinal.core.ActionKey;
import goja.mvc.Controller;
import goja.security.shiro.Securitys;

/**
 * <p>
 * 后台系统说明
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class AboutController extends Controller {

    @ActionKey("/")
    public void index(){
        if(!Securitys.isLogin()){
            redirect("/login");
        }
    }
}

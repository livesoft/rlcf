package app.controllers;

import app.Const;
import app.kit.CommonKit;
import app.models.basic.Hotnews;
import app.models.sys.User;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;
import org.joda.time.DateTime;

/**
 * <p>
 *     理财咨询管理
 * </p>
 * liuhui on 15/2/5.
 */
public class NewsController extends Controller {

    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        String pubdate = getPara("pubdate");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        if(!Strings.isNullOrEmpty(pubdate)){
            criterias.setParam("pubdate", Condition.BETWEEN,pubdate.split(" - "));
        }
        renderDataTables(criterias, Hotnews.class);
    }

    @Before(GET.class)
    public void create() {
        Hotnews news = new Hotnews();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("news",news);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        Hotnews news = Hotnews.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("news",news);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        Hotnews news = getModel(Hotnews.class,"news");

        if (!Strings.isNullOrEmpty(getPara("pic_attachment"))) {
            news.set("pic", getPara("pic_attachment_url"));
            news.set("pic_attachment", getPara("pic_attachment"));
        }

        if (DaoKit.isNew(news)) {
            news.set("pubdate", DateTime.now().toDate());
            news.set("publisher", loginUser.getName());
            news.set("summary", CommonKit.getSummary(news.getStr("content")));
            news.set(Const.FIELD_STATUS, 1);
            news.save();
        } else {
            news.set("summary", CommonKit.getSummary(news.getStr("content")));
            news.update();
        }
        redirect("/news");
    }

    public void delete(){
        int id = getParaToInt(0, 0);
        boolean status = Hotnews.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

}

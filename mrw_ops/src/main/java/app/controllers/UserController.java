/*
 * Copyright (c) 2014-2015. Mobiao Group.
 */

package app.controllers;

import app.Const;
import app.models.sys.Role;
import app.models.sys.User;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.encry.DigestsKit;
import goja.encry.EncodeKit;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;
import org.apache.commons.lang3.StringUtils;

import static app.Const.NO;
import static app.Const.YES;

/**
 * <p>
 * The url /user Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class UserController extends Controller {

    /**
     * The index route.
     * the url /user
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {

    }

    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        criterias.setParam("customer", 0);
        criterias.setParam("username", Condition.NE, "admin");
        renderDataTables(criterias, User.class);
    }

    public void checkUserName() {
        String username = getPara("user.username");
        final User user = User.dao.findByUserName(username);
        renderJson(user == null);
    }


    @Before(GET.class)
    public void create() {
        User user = new User();
        user.set("gender",1);
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr(user);

        //用户角色
        setAttr("roles",Role.dao.findIdAndName());
        render("item.ftl");
    }


    @Before(GET.class)
    public void edit() {
        User user = User.dao.findById(getPara());
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr(user);
        //用户角色
        setAttr("roles",Role.dao.findIdAndName());
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        User user = getModel(User.class);
        String[] paraValues = getParaValues("user.role");
        if(null != paraValues){
            user.set("role", Joiner.on(",").join(paraValues));
        }else{
            user.set("role", "");
        }


        String password = getPara("password");
        if (DaoKit.isNew(user)) {
            //设置密码
            byte[] salt = DigestsKit.generateSalt(EncodeKit.SALT_SIZE);
            user.set("salt", EncodeKit.encodeHex(salt));
            byte[] hashPassword = DigestsKit.sha1(password.getBytes(), salt, EncodeKit.HASH_INTERATIONS);
            user.set("password", EncodeKit.encodeHex(hashPassword));
            user.set(Const.FIELD_STATUS, 1);
            user.save();
        } else {
            user.update();
        }
        redirect("/user");
    }


    public void delete() {
        if (User.dao.deleteById(getPara())) {
            renderAjaxSuccess();
        } else {
            renderAjaxFailure("删除用户失败!");
        }
    }

    /**
     * 修改密码
     */
    public void changepwd(){
        AppUser<User> loginUser = Securitys.getLogin();
        setAttr("user",loginUser);
    }

    public void checkPwd(){
        String old_pwd = getPara("old_pwd");

        AppUser<User> loginUser = Securitys.getLogin();
        User user = loginUser.user;
        String password = user.getStr("password");
        byte[] salt = EncodeKit.decodeHex(user.getStr("salt"));
        byte[] hashPassword = DigestsKit.sha1(old_pwd.getBytes(), salt, EncodeKit.HASH_INTERATIONS);
        boolean equals = StringUtils.equals(EncodeKit.encodeHex(hashPassword), password);
        if(equals){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

    public void changepwd_save(){
        String old_pwd = getPara("old_pwd");
        String new_pwd = getPara("new_pwd");

        AppUser<User> loginUser = Securitys.getLogin();
        User user = loginUser.user;
        String password = user.getStr("password");
        byte[] salt = EncodeKit.decodeHex(user.getStr("salt"));
        byte[] hashPassword = DigestsKit.sha1(old_pwd.getBytes(), salt, EncodeKit.HASH_INTERATIONS);
        boolean equals = StringUtils.equals(EncodeKit.encodeHex(hashPassword), password);
        if(!equals){
            renderAjaxFailure("密码修改失败，请确保输入正确的原始密码");
            return;
        }

        salt = DigestsKit.generateSalt(EncodeKit.SALT_SIZE);
        user.set("salt", EncodeKit.encodeHex(salt));
        hashPassword = DigestsKit.sha1(new_pwd.getBytes(), salt, EncodeKit.HASH_INTERATIONS);
        user.set("password", EncodeKit.encodeHex(hashPassword));
        boolean update = user.update();
        if(update){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }

    }
}
package app.controllers;

import app.kit.CommonKit;
import app.models.OperLog;
import com.google.common.base.Strings;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import org.joda.time.DateTime;

import java.util.List;


/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class SyslogController extends Controller {

    public void index(){

    }

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            criterias.setParam("record_time", Condition.BETWEEN,date.toArray());
        }

        renderDataTables(criterias, OperLog.class);
    }
}

package app.controllers;

import app.models.member.Boards;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;

/**
 * <p>
 * <p/>
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class BoardsController extends Controller{

    public void index(){}

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, Boards.class);
    }
}

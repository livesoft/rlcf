package app.controllers;

import app.Const;
import app.constant.CommonStatusMap;
import app.constant.OrderConstant;
import app.kit.CommonKit;
import app.models.order.Order;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.tuples.Pair;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 订单列表管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class OrderController extends Controller {

    static Map<String,String> typeMap = Maps.newHashMap();
    static {
        typeMap.put(OrderConstant.BUY_TYPE,"投资(购买)");
        typeMap.put(OrderConstant.INC_TYPE,"收益");
        typeMap.put(OrderConstant.RPN_TYPE,"退单");
        typeMap.put(OrderConstant.TFR_TYPE,"投资(转让)");
        typeMap.put(OrderConstant.TFR_PAY_TYPE,"转让打款");
        typeMap.put(OrderConstant.CHARGING,"账户充值");
        typeMap.put(OrderConstant.WITHDRAWALS,"账户提现");
        typeMap.put(OrderConstant.PRODUCTFAIL,"产品募集失败退款");
    }

    static Map<Integer,String> statusMap = Maps.newHashMap();
    static {
        statusMap.put(OrderConstant.TOBEPAY,"待支付");
        statusMap.put(OrderConstant.REFUNDING,"退款处理中");
        statusMap.put(OrderConstant.HANDING,"处理中");
        statusMap.put(OrderConstant.TRADE_SUCCESS,"交易成功");
        statusMap.put(OrderConstant.TRADE_CLOSE,"交易关闭");
        statusMap.put(OrderConstant.DEL,"已删除");
        statusMap.put(OrderConstant.REFUNDED,"已退款");
        statusMap.put(OrderConstant.TRADE_FAILURE,"交易失败");
        statusMap.put(OrderConstant.TRADE_COMPLETE,"交易完成");
        statusMap.put(OrderConstant.NEW,"待处理");
    }


    @Before(GET.class)
    public void index(){}

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist(){
        //订单交易金额
        String amount_start = getPara("trade_amount_start");
        String amount_end = getPara("trade_amount_end");

//        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:SS");

        String buy_time = getPara("buy_time");

        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        if(!Strings.isNullOrEmpty(buy_time)){
            Pair<DateTime, DateTime> objects = CommonKit.rangeDatetime(buy_time);
            criterias.setParam("o.buy_time", Condition.BETWEEN,objects.toArray());
        }

        if (!Strings.isNullOrEmpty(amount_start)) {
            criterias.setParam("o.trade_amount", Condition.GTEQ, amount_start);
        }
        if (!Strings.isNullOrEmpty(amount_end)) {
            criterias.setParam("o.trade_amount", Condition.LTEQ, amount_end);
        }

//        criterias.setParam("o.status", Condition.NE, OrderConstant.NEW);
        renderDataTables(criterias, "order");
    }

    public void detail_view() {
        String trade_no = getPara(0, "0");
        Order order = Order.dao.findDetailById(trade_no);
        if(null != order){
            String order_type = order.getStr("order_type");
            int status_val = order.getNumber(Const.FIELD_STATUS).intValue();
            order.put("order_type_name", CommonStatusMap.getChineseOrderType(order_type));
            order.put("status_name", statusMap.get(status_val));
        }else{
            setAttr("error","该订单不存在");
        }
        setAttr("order", order);
        render("detail_view.ftl");
    }

    /**
     * 数据导出
     */
    public void export() {
        List<Order> orders = Order.dao.findExcelExport();
        for (Order order : orders) {
            String order_type = order.getStr("order_type");
            order.put("type_name", typeMap.get(order_type));

            int status_val = order.getNumber(Const.FIELD_STATUS).intValue();
            order.put("status_name", statusMap.get(status_val));
        }

        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("odr", orders);
        JxlsRender beans = JxlsRender.me(Const.Report.ORDER_PATH).filename("订单表格.xls").beans(_excel_datas);
        render(beans);

    }
}

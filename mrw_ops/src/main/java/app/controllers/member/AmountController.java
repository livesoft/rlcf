package app.controllers.member;

import app.models.member.AccountAmount;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;

/**
 * 账户资金记录
 * Created by
 * liuhui on 15/2/9.
 */
public class AmountController extends Controller {

    public void index(){

    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        String member_id = getPara(0, "0");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        criterias.setParam("member",Condition.EQ ,member_id);
        renderDataTables(criterias, AccountAmount.class);
    }

}

package app.controllers.member;

import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;

/**
 * <p>
 * 会员积分
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class IntegralController extends Controller {

    public void index(){
        setAttr("m_id", getPara(0,"0"));
    }

    /**
     * 根据传入的会员显示datatable
     */
    public void dtlist(){
        int mid = getParaToInt(0, 0);
        final DTCriterias criterias = getCriterias();
        criterias.setParam("records.member", Condition.EQ,mid);
        renderDataTables(criterias, "integralrecord");
    }
}

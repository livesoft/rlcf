package app.controllers.member;

import app.Const;
import app.models.member.Identity;
import app.models.member.Member;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import goja.mvc.Controller;

import java.sql.SQLException;

/**
 * 会员实名认证
 * Created by
 * liuhui on 15/2/8.
 */
public class AuditController extends Controller {

    public void index(){}


    /**
     * 查看实名信息
     */
    public void item(){
        String id = getPara(0, "0");
        Identity info =Identity.dao.findById(id);
        if(null == info){
            info = new Identity();
        }
        setAttr("info",info);
        render("item.ftl");
    }

    /**
     * 实名认证操作
     */
    public void audit(){
        final String member_id = getPara(Const.FIELD_MEMBER, "0");
        boolean tx = Db.tx(new IAtom(
        ) {
            @Override
            public boolean run() throws SQLException {
                boolean identity = Identity.dao.auditOk(member_id);
                boolean member = Member.dao.auditOk(member_id);
                return identity && member;
            }
        });
        redirect("/member/member");
    }

}

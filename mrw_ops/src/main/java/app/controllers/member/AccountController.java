package app.controllers.member;

import app.Const;
import app.models.member.Account;
import app.models.member.AccountAmount;
import app.models.member.BankCard;
import goja.mvc.Controller;

/**
 * Created by
 * liuhui on 15/2/9.
 */
public class AccountController extends Controller {

    public void index(){}


    public void getAccount(){
        //会员id
        String member_id = getPara(0, "0");
        //会员账户
        Account account = Account.dao.findById(member_id);
        //资金记录
        AccountAmount amount = AccountAmount.dao.getAmountByAccount(member_id);
        //该会员的电子账户下所有银行卡
        long card_size = BankCard.dao.getTotSizeByAccount(member_id);
        //统计会员收入总额
        double sumEarning = AccountAmount.dao.sumEarningByMember(member_id);
        //统计会员支出总额
        double sumExpense = AccountAmount.dao.sumExpenseByMember(member_id);
        setAttr("account", account);
        setAttr(Const.FIELD_AMOUNT,amount);
        setAttr("card_size",card_size);
        setAttr("sumEarning",sumEarning);
        setAttr("sumExpense",sumExpense);
        setAttr("member_id",member_id);
        render("item.ftl");
    }
}

package app.controllers;

import app.Const;
import app.kit.CommonKit;
import app.models.sys.Role;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import goja.Func;
import goja.StringPool;
import goja.mvc.Controller;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;

import java.sql.SQLException;
import java.util.List;

import static goja.StringPool.PK_COLUMN;


/**
 * <p>
 * 角色管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class RoleController extends Controller {

    public static final String FORM_ATTR = "role";

    public void index(){

    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        renderDataTables(criterias, Role.class);
    }


    @Before(GET.class)
    public void create() {
        Role role = new Role();
        setAttr(FORM_ATTR, role);
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        Role role = Role.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr(FORM_ATTR, role);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        final Role role = getModel(Role.class);
        //权限列表
        String permissions = role.getStr("permissions");
        final List<String> permissionList;
        if (!Strings.isNullOrEmpty(permissions)) {
            role.set("permissions", permissions);
            permissionList = Func.COMMA_SPLITTER.splitToList(permissions);
        } else {
            role.set("permissions", StringPool.EMPTY);
            permissionList = Lists.newArrayList();
        }


        if (Strings.isNullOrEmpty(role.getStr(PK_COLUMN))) {
            role.set(PK_COLUMN, CommonKit.uuid());
            Db.tx(new IAtom() {
                @Override
                public boolean run() throws SQLException {
                    if (role.save()) {
                        Object[][] params = new Object[permissionList.size()][];
                        for (int i = 0; i < permissionList.size(); i++) {
                            String permission = permissionList.get(i);
                            Object[] param = new Object[2];
                            param[0] = role.getStr(PK_COLUMN);
                            param[1] = permission;
                            params[i] = param;
                        }
                        Db.batch(SqlKit.sql("role.installRight"), params, 50);
                        return true;
                    }
                    return false;
                }
            });

        } else {
            Db.tx(new IAtom() {
                @Override
                public boolean run() throws SQLException {
                    if (Role.dao.deleteByRole(role.get(PK_COLUMN))) {
                        if (role.update()) {
                            Object[][] params = new Object[permissionList.size()][];
                            for (int i = 0; i < permissionList.size(); i++) {
                                String permission = permissionList.get(i);
                                Object[] param = new Object[2];
                                param[0] = role.getStr(PK_COLUMN);
                                param[1] = permission;
                                params[i] = param;
                            }
                            Db.batch(SqlKit.sql("role.installRight"), params, 50);
                            return true;
                        }
                    }
                    return false;
                }
            });
        }
        redirect("/role");
    }

    /**
     * 删除
     */
    public void delete(){
        String id = getPara(0, StringPool.EMPTY);
        if (Strings.isNullOrEmpty(id)) {
            renderAjaxFailure();
        }
        boolean status = Role.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

}

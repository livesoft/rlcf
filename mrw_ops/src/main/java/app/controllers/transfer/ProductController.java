package app.controllers.transfer;

import app.Const;
import app.constant.MemberConstant;
import app.constant.TransferConstant;
import app.kit.CommonKit;
import app.models.member.MemberProduct;
import app.models.transfer.TransferProduct;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import org.joda.time.DateTime;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;


/**
 * <p>
 *  转让产品处理
 * <p/>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductController extends Controller {

    /**
     * The index route.
     * the url /transfer/product
     */
    public void index(){}


    public void dtlist() {

        String buy_time = getPara("transfer_time");

        //转让价格
        String price_start = getPara("price_start");
        String price_end = getPara("price_end");

        final DTCriterias criterias = getCriterias();
        if (!Strings.isNullOrEmpty(price_start)) {
            criterias.setParam("t_p.price", Condition.GTEQ, price_start);
        }
        if (!Strings.isNullOrEmpty(price_end)) {
            criterias.setParam("t_p.price", Condition.LTEQ, price_end);
        }

        if(!Strings.isNullOrEmpty(buy_time)){
            List<DateTime> date = CommonKit.string2DateTimeList(buy_time, "yyyy/MM/dd");
            criterias.setParam("t_p.transfer_time", Condition.BETWEEN,date.toArray());
        }

        renderDataTables(criterias,"transfer_product");
    }

    /**
     * 审核
     */
    public void audit(){
        int id = getParaToInt(0, 0);
        if (id > 0) {
            renderAjaxFailure();
            return;
        }
        final TransferProduct product = TransferProduct.dao.findById(id);

        product.set(Const.FIELD_STATUS, TransferConstant.STATUE_TRADING);
        //审核时间
        Date current_date = DateTime.now().toDate();
        product.set("audit_time", current_date);
        //挂单时间
        product.set("put_time",current_date);
        final MemberProduct member_product = MemberProduct.dao.findById(product.getNumber("member_product").intValue());

        boolean rs = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                boolean m_pro = false;
                boolean tp_status = product.update();
                if(null != member_product){
                    m_pro = member_product.set(Const.FIELD_STATUS, MemberConstant.TRANSFER_AUDIT_SUCCESS).update();
                }
                return tp_status && m_pro;
            }
        });
        if(rs){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

    /**
     * 转让产品详细信息
     */
    @Before(GET.class)
    public void detail(){
        String id = getPara(0, "0");
        //获取明细和其他的明细
        Record detail = TransferProduct.dao.getPrdtAndMdlById(id);
        int status = detail.getNumber("status").intValue();
        switch (status){
            case TransferConstant.STATUE_PAUSE:detail.set("status_name","待审核"); break;
            case TransferConstant.STATUE_TRADING:detail.set("status_name","交易中"); break;
            case TransferConstant.STATUE_SUCCESS:detail.set("status_name","交易成功"); break;
            case TransferConstant.STATUE_FAIL:detail.set("status_name","转让失效"); break;
            default: detail.set("status_name","");
        }
        setAttr("detail", detail);
        //表单展示
        render("detail.ftl");
    }
}

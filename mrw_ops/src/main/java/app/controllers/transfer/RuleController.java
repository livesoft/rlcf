package app.controllers.transfer;

import app.models.transfer.TransferRule;
import goja.StringPool;
import goja.mvc.Controller;

/**
 * <p>
 * The url transfer/rule Controller.
 * </p>
 * 转让规则维护
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class RuleController extends Controller {

    /**
     * The index route.
     * the url /transfer/rule
     * the view in index.ftl
     */
    public void index() {

        TransferRule fees = TransferRule.dao.findDefault();
        if (fees == null) {
            fees = new TransferRule();
            fees.set(StringPool.PK_COLUMN, TransferRule.DEFAULT_RULE_ID);
            fees.set("ratio", 0.2);
            fees.set("aging", 24);
            fees.save();
        }
        setAttr("fees", fees);
    }


    public void save() {

        TransferRule transferRule = getModel(TransferRule.class, "fees");

        transferRule.update();

        renderAjaxSuccess();
    }
}
package app.controllers.transfer;

import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;

/**
 * <p>
 * 转让产品的转让交易信息
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class TradeController extends Controller {

    /**
     * The index route.
     * the url /product/app
     */
    public void index(){
        //转让编号
        String tranfer = getPara(0, "0");
        setAttr("tranfer",tranfer);
    }

    public void dtlist(){
        //转让编号
        String tranfer = getPara(0, "0");
        final DTCriterias criterias = getCriterias();
        criterias.setParam("t_t.tranfer", Condition.EQ,tranfer);
        renderDataTables(criterias, "transfer_trade");
    }
}

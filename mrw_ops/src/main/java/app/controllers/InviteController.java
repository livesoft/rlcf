package app.controllers;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;


/**
 * <p>
 * 会员邀请统计
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class InviteController extends Controller {

    @Before(GET.class)
    public void index(){}

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, "invite");
    }
}

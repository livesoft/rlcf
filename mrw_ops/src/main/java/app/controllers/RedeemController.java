package app.controllers;

import app.kit.TypeKit;
import app.models.member.MemberProduct;
import app.models.member.RedeemProduct;
import app.models.order.TradeMoney;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;

import java.sql.SQLException;

import static app.Const.FIELD_AMOUNT;
import static app.Const.FIELD_MEMBER;
import static app.Const.FIELD_PRODUCT;
import static app.Const.FIELD_STATUS;
import static app.Const.FIELD_ORDER_ID;
import static app.constant.MemberConstant.REDEEM_SUCCESS;
import static app.constant.MemberConstant.REDEM_OK;
import static app.constant.ProductConstant.ONLINE;
import static app.models.member.MemberProduct.FIELD_PRODUCT_STATUS;
import static app.models.member.MemberProduct.FIELD_REDEEM_STATUS;
import static app.models.member.MemberProduct.dao;

/**
 * <p>
 * 赎回审核处理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class RedeemController extends Controller{

    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问rlo_redeem_product
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, "redeemproduct");
    }

    /**
     * 赎回审核
     */
    public void audit(){
        //会员产品id
        int redeemProductId = getParaToInt(0, 0);
        if (redeemProductId <= 0) {
            renderAjaxFailure();
            return;
        }
        final RedeemProduct redeemProduct = RedeemProduct.dao.findByMemberProduct(redeemProductId, getParaToInt(1, 0));
        if(TypeKit.getInt(redeemProduct, FIELD_PRODUCT_STATUS) != ONLINE){
            renderAjaxFailure("当前产品不在认购期内，无法退单。");
            return;
        }


        redeemProduct.set(FIELD_STATUS, REDEM_OK);
        int memberProductId = TypeKit.getInt(redeemProduct, "member_product");
        final MemberProduct member_product = dao.findById(memberProductId);
        if (member_product == null) {
            renderAjaxFailure();
            return;
        }
        member_product.set(FIELD_REDEEM_STATUS, REDEEM_SUCCESS);

        // 写入打款纪录
        final TradeMoney tradeMoney = TradeMoney.createCancelPlayMoney(TypeKit.getInt(redeemProduct, FIELD_MEMBER),
                redeemProduct.getStr("membercode"),
                TypeKit.getInt(redeemProduct, FIELD_PRODUCT),
                redeemProduct.getBigDecimal(FIELD_AMOUNT),
                memberProductId);

        tradeMoney.set(FIELD_ORDER_ID, TypeKit.getInt(redeemProduct, FIELD_ORDER_ID));


        boolean rs = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                return redeemProduct.update() && member_product.update() && tradeMoney.save();
            }
        });
        if(rs){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }


}

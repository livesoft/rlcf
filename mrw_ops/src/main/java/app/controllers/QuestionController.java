package app.controllers;

import app.Const;
import app.models.basic.RiskCategory;
import app.models.basic.RiskOptions;
import app.models.basic.RiskProblem;
import app.models.sys.User;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;

import java.util.List;

/**
 * <p>
 * 风险评估问题管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class QuestionController extends Controller {
    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias,"riskproblem");
    }

    @Before(GET.class)
    public void create() {
        RiskProblem question = new RiskProblem();
        List<RiskCategory> categories = RiskCategory.dao.findAll();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("problem",question);
        setAttr("categories",categories);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        List<RiskCategory> categories = RiskCategory.dao.findAll();
        RiskProblem question = RiskProblem.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("question",question);
        setAttr("categories",categories);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        RiskProblem problem = getModel(RiskProblem.class,"question");

        if (DaoKit.isNew(problem)) {
            problem.save();
        } else {
            problem.update();
        }
        redirect("/question");
    }

    public void delete(){
        int id = getParaToInt(0, 0);
        boolean status = RiskProblem.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

    /**
     * 选项首页
     */
    public void option(){
        //获取问题ID
        String qustion_id = getPara(0,"0");
        setAttr("qustion_id",qustion_id);
        render("option.ftl");
    }

    /**
     * 选项列表 dt
     */
    public void option_dtlist(){
        //获取问题ID
        String qustion_id = getPara(0, "0");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        criterias.setParam("o.problem", Condition.EQ,qustion_id);
        renderDataTables(criterias, "riskoption");
    }

    /**
     * 选项创建
     */
    public void option_create(){
        //创建时需获取问题ID
        String qustion_id = getPara(0, "0");
        RiskProblem question = RiskProblem.dao.findById(qustion_id);
        if(null == question){
            return;
        }
        RiskOptions option = new RiskOptions();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("question",question);
        setAttr("option",option);
        render("option_item.ftl");
    }

    /**
     * 选项编辑
     */
    public void option_edit(){
        //选项ID
        String option_id = getPara(0, "0");
        RiskOptions option = RiskOptions.dao.findById(option_id);
        if(null == option){
            return;
        }
        RiskProblem question = RiskProblem.dao.findById(option.get("problem"));
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("option",option);
        setAttr("question",question);
        render("option_item.ftl");
    }

    /**
     * 选项新增/保存
     */
    public void option_save(){
        RiskOptions option = getModel(RiskOptions.class, "option");
        if (DaoKit.isNew(option)) {
            option.save();
        }else {
            option.update();
        }
        redirect("/question/option/" + option.get("problem"));
    }

    public void option_delete(){
        int id = getParaToInt(0, 0);
        boolean status = RiskOptions.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }
}

package app.controllers;

import app.Const;
import app.constant.MemberConstant;
import app.kit.TypeKit;
import app.models.basic.Dict;
import app.models.member.Member;
import app.models.product.Product;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;

import java.util.List;
import java.util.Map;

/**
 * 注册会员管理
 * Created by
 * liuhui on 15/2/8.
 */
public class MemberController extends Controller {

    public void index(){

    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        renderDataTables(criterias, Const.FIELD_MEMBER);
    }

    /**
     * 设置启用禁用状态
     */
    public void setStatus(){
        String id = getPara("id","0");
        String set_val = getPara("set_val");
        boolean updateStatus = Member.dao.updateStatus(set_val, id);
        if(updateStatus){

            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

    /**
     * 信息列表
     */
    public void item(){
        String member_id = getPara(0,"0");
        Member member = Member.dao.findById(member_id);
        setAttr(Const.FIELD_MEMBER,member);
        render("item.ftl");
    }

    public void detail_view(){
        String member_id = getPara(0,"0");
        Member member = Member.dao.findById(member_id);
        setAttr(Const.FIELD_MEMBER,member);
        render("detail_view.ftl");
    }

    /**
     * 产品收益记录index
     */
    public void product_money_record(){
        String member_id = getPara(0, "0");
        List<Product> products = Product.dao.findNameAndIdAvalible();
        setAttr("products",products);
        setAttr(Const.FIELD_MEMBER, member_id);
        render("product_money_record.ftl");
    }

    public void record_dtlist(){
        final DTCriterias criterias = getCriterias();
        String member_id = getPara(0, "0");
        //指定会员
        if(!Strings.isNullOrEmpty(member_id)){
            criterias.setParam("r.member", Condition.EQ, member_id);
        }
        String product = getPara(Const.FIELD_PRODUCT);
        if (!Strings.isNullOrEmpty(product)) {
            criterias.setParam("r.product", Condition.EQ, product);
        }
        renderDataTables(criterias, "memberproduct");
    }

    /**
     * 会员导出
     */
    public void export(){
        Map<Integer,String> status_map = Maps.newHashMap();
        status_map.put(0, "删除");
        status_map.put(1, "正常");
        status_map.put(2, "禁用");
        status_map.put(3, "锁定");

        Map<String, Map<String, Dict>> dictMaps = Dict.dao.getDictMaps();
        List<Member> members = Member.dao.findExcelExport();
        for (Member member : members) {
            int gender = TypeKit.getInt(member, "gender");
            switch (gender){
                case MemberConstant.GENDER.Man:
                    member.put("genderName", "男");
                    break;
                case MemberConstant.GENDER.Women:
                    member.put("genderName", "女");
                    break;
                case MemberConstant.GENDER.UNKNOW:
                    member.put("genderName", "未知");
                    break;
            }
            Dict risk_tolerance = dictMaps.get(Const.FIELD_RISK_TOLERANCE).get(member.getNumber("risk_tolerance").intValue() + "");
            member.put("risk_toleranceName", null==risk_tolerance?"":risk_tolerance.getStr("name"));
            member.put("statusName", status_map.get(member.getNumber(Const.FIELD_STATUS).intValue()));
        }
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put(Const.FIELD_MEMBER, members);
        JxlsRender beans = JxlsRender.me(Const.Report.MEMBER_PATH).filename("会员列表.xls").beans(_excel_datas);
        render(beans);
    }
}

package app.controllers.pc;

import app.Const;
import app.models.pc.StoryShare;
import app.models.sys.User;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;
import org.joda.time.DateTime;

/**
 * <p>
 * 理财分享管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class StoryshareController  extends Controller {

    /**
     * The index route.
     * the url /indexpic
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, StoryShare.class);
    }

    @Before(GET.class)
    public void create() {
        StoryShare storyshare = new StoryShare();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("storyshare",storyshare);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        StoryShare storyshare = StoryShare.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("storyshare",storyshare);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        StoryShare share = getModel(StoryShare.class,"storyshare");

        if (!Strings.isNullOrEmpty(getPara("pic_attachment"))) {
            share.set("pic", getPara("pic_attachment_url"));
            share.set("pic_attachment", getPara("pic_attachment"));
        }

        if (DaoKit.isNew(share)) {
            share.set(Const.FIELD_CREATE_TIME, DateTime.now().toDate());
            share.save();
        } else {
            share.update();
        }
        redirect("/pc/storyshare");
    }

    public void delete(){
        int id = getParaToInt(0, 0);
        boolean status = StoryShare.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }
}

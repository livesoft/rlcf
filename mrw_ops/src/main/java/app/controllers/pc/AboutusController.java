package app.controllers.pc;

import app.Const;
import app.models.basic.Dict;
import app.models.pc.AboutUs;
import app.models.pc.IndexPic;
import app.models.sys.User;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;

import java.util.List;

/**
 * <p>
 * PC端 关于我们 普通文本管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class AboutusController extends Controller{

    public void index(){}


    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, AboutUs.class);
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        AboutUs aboutus = AboutUs.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("aboutus",aboutus);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        AboutUs aboutus = getModel(AboutUs.class,"aboutus");
        String type = aboutus.getStr(Const.FIELD_TYPE);
        List<Dict> dicts = Dict.dao.aboutus();
        for (Dict dict : dicts) {
            if(dict.getStr(Const.FIELD_CODE).equals(type)){
                aboutus.set("type_name",dict.getStr(Const.FIELD_NAME));
                break;
            }
        }

        if (DaoKit.isNew(aboutus)) {
            aboutus.save();
        } else {
            aboutus.update();
        }
        redirect("/pc/aboutus");
    }

    public void delete(){
        int id = getParaToInt(0, 0);
        boolean status = AboutUs.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }
}

package app.controllers;

import app.Const;
import app.models.basic.Msgs;
import app.models.member.Member;
import app.models.sys.User;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.plugin.activerecord.Db;
import goja.mvc.Controller;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;

import java.sql.Timestamp;

/**
 * <p>
 * 消息管理
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class MsgsController extends Controller {

    public static final int READ = 1;
    public static final int UNREAD = 0;


    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias,"msgs");
    }

    @Before(GET.class)
    public void create() {
        Msgs msgs = new Msgs();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr(Const.FIELD_MEMBER, Member.dao.findIdAndNameByStatus());
        setAttr("msgs",msgs);
        render("item.ftl");
    }

    @Before(GET.class)
    public void detail() {
        String id = getPara(0, "0");
        Msgs msgs = Msgs.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("msgs",msgs);
        render("detail.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();

        Msgs msgs = getModel(Msgs.class,"msgs");
        //接收人
        String[] receives = getParaValues("receive");
        //发送时间
        Timestamp now =new java.sql.Timestamp(new java.util.Date().getTime());

        int objLength = 9;
        if (!Strings.isNullOrEmpty(getPara("pic_attachment"))) {
            objLength = 11;
            msgs.set("pic", getPara("pic_attachment_url"));
            msgs.set("pic_attachment", Ints.tryParse(getPara("pic_attachment")));
        }

        Object[][] val_attr = new Object[receives.length][];
        for (int i = 0; i < receives.length; i++) {
            Object[] _param = new Object[objLength];
            _param[0] = loginUser.getId();
            _param[1] = Ints.tryParse(receives[i]);
            _param[2] = loginUser.getId();
            _param[3] = Ints.tryParse(receives[i]);
            _param[4] = Const.MSGS_TYPE.SYS;
            _param[5] = msgs.getStr("title");
            _param[6] = msgs.getStr("content");
            _param[7] = now;
            _param[8] = UNREAD;
            if(objLength == 11){
                _param[9] = msgs.getStr("pic");
                _param[10] = msgs.getInt("pic_attachment");
            }
            val_attr[i] = _param;
        }

        if(objLength==11){
            Db.batch(SqlKit.sql("msgs.batch_insert_pic"), val_attr, 100);
        }else{
            Db.batch(SqlKit.sql("msgs.batch_insert"), val_attr, 100);
        }

        redirect("/msgs");
    }

}

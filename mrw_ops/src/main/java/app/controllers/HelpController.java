package app.controllers;

import app.Const;
import app.models.basic.HelpNav;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;

import java.util.UUID;

/**
 * <p>
 * 帮助导航
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class HelpController extends Controller{

    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, "helpnav");
    }

    @Before(GET.class)
    public void create() {
        HelpNav helpNav = new HelpNav();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr("parent",HelpNav.dao.findParent());
        setAttr("has_child",false);
        setAttr("help",helpNav);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        HelpNav helpNav = HelpNav.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("parent",HelpNav.dao.findParent());
        setAttr("has_child",HelpNav.dao.findChildsById(id).size() > 0);
        setAttr("help",helpNav);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        HelpNav helpNav = getModel(HelpNav.class,"help");

        if (Strings.isNullOrEmpty(helpNav.getStr("id"))) {
            helpNav.set("id", UUID.randomUUID().toString().replace("-",""));
            helpNav.save();
        } else {
            helpNav.update();
        }
        redirect("/help");
    }

    public void delete(){
        String id = getPara(0, "0");
        boolean status = HelpNav.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }
}

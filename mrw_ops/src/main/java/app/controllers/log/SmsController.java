package app.controllers.log;

import app.Const;
import app.models.analyze.Smsrecord;
import com.google.common.collect.Maps;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.rapid.datatables.DTCriterias;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * The url log/sms Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class SmsController extends Controller {

    /**
     * The index route.
     * the url /log/sms
     */
    public void index() {
        render("../sms.ftl");
    }


    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        renderDataTables(criterias, Smsrecord.class);
    }

    /**
     * 导出
     */
    public void export() {
        Map<Integer,String> status_map = Maps.newHashMap();
        status_map.put(0,"发送成功");

        List<Smsrecord> smsrecordList = Smsrecord.dao.findExcelExport();
        Map<String, Object> _excel_datas = Maps.newHashMap();
        for (Smsrecord sms : smsrecordList) {
            sms.put("status_name",status_map.get(sms.getNumber(Const.FIELD_STATUS).intValue()));
        }
        _excel_datas.put("sms", smsrecordList);
        JxlsRender beans = JxlsRender.me(Const.Report.LOGSMS_PATH).filename("短信发送日志.xls").beans(_excel_datas);
        render(beans);
    }
}
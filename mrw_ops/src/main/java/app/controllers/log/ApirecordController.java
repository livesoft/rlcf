package app.controllers.log;

import app.models.analyze.Interfacecallrecord;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;

/**
 * <p>
 * The url log/apirecord Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ApirecordController extends Controller {

    /**
     * The index route.
     * the url /log/apirecord
     * the view in index.ftl
     */
    public void index() {
        render("../apirecord.ftl");
    }

    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        renderDataTables(criterias, Interfacecallrecord.class);
    }
}
package app.controllers.report;

import app.Const;
import app.kit.CommonKit;
import app.models.member.MemberProduct;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.datatables.DTResponse;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 赎回（退单）报表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class RedeemController extends Controller {

    public void index(){
        Record record = getSum();
        setAttr(record);
    }

    public void dt_list(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        //查询条件
        String s_date = getPara("s_date");
        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            criterias.setParam("adate", Condition.BETWEEN,date.toArray());
        }
        renderDataTables(criterias, "report_redeem");
    }

    public void detail_index(){
        String adate = getPara("date", DateTime.now().toString("yyyy-MM-dd hh:MM:ss"));
        setAttr("adate",adate);
    }

    public void count_json(){
        Record record = getSum();
        renderJson(record);
    }

    private Record getSum() {
        String s_date = getPara("s_date");

        String sql_column_sum = SqlKit.sql("report_redeem.sql_column_sum");
        String sql_where = SqlKit.sql("report_redeem.where");

        Record record = new Record();

        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql_where= sql_where + " AND adate between ? AND ?";
            record = Db.findFirst(sql_column_sum + sql_where, date.get(0), date.get(1));
        }else {
            record = Db.findFirst(sql_column_sum + sql_where);
        }
        return record;
    }

    public void index_export(){
        String s_date = getPara("s_date");

        String sql_column = SqlKit.sql("report_redeem.column");
        String sql_where = SqlKit.sql("report_redeem.where");

        List<Record> records = Lists.newArrayList();

        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql_where= sql_where + " AND adate between ? AND ?";
            records = Db.find(sql_column + sql_where,date.get(0),date.get(1));
        }else {
            records = Db.find(sql_column + sql_where);
        }
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_REDEEM_PATH).filename("日退单报表.xls").beans(_excel_datas);
        render(beans);
    }

    public void detail_dtlist(){
        String adate = getPara("date",DateTime.now().toString("yyyy-MM-dd hh24:mm:ss"));
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        List<Object> params = Lists.newArrayList();
        params.add(adate);
        Page<Record> datas = DaoKit.paginate(SqlKit.sql("redeem_detail.where"), SqlKit.sql("redeem_detail.column"), criterias, params);
        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);
    }

    public void detail_export(){
        //明细必须的时间条件
        String s_date = getPara("s_date","");
        List params = Lists.newArrayList();
        params.add(s_date);

        String sql_column = SqlKit.sql("redeem_detail.column");
        String sql_where = SqlKit.sql("redeem_detail.where");

        if(!Strings.isNullOrEmpty(getPara("s_order"))){
            sql_where = sql_where + "AND r_o.trade_no like ?";
            params.add(DaoKit.like(getPara("s_order")));
        }

        List<Record> records = Db.find(sql_column + sql_where, params.toArray());
        MemberProduct.dao.setPlayStatusName(records);

        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_REDEEM_DETAIL_PATH).filename("每日退单报表.xls").beans(_excel_datas);
        render(beans);

    }
}

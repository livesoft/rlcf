package app.controllers.report;

import app.Const;
import app.constant.CommonStatusMap;
import app.kit.CommonKit;
import app.models.member.MemberProduct;
import app.models.order.TradeMoney;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 收益报表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class ProfitController extends Controller{

    public void index(){
        Record t_sum = getSum();
        setAttr("t_sum",t_sum);
    }

    public void dt_list(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            criterias.setParam("adate", Condition.BETWEEN,date.toArray());
        }
        List<Object> param = Lists.newArrayList();
        param.add(TradeMoney.SUCCESS_STATUS);
        param.add(TradeMoney.DUE_TYPE);

        renderDataTables(criterias, "report_profit",param);
    }

    public void count_json(){
        Record sum = getSum();
        renderJson(sum);
    }

    /**
     * 首页导出
     */
    public void index_export(){
        List<Record> list = getExportList();
        Map<String, Object> _excel_datas = Maps.newHashMap();

//        MemberProduct.dao.setPlayStatusName(list);
        _excel_datas.put("report", list);
        JxlsRender beans = JxlsRender.me(Const.Report.PRODUCT_PROFIT_PATH).filename("产品收益报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List<Record> getExportList() {
        List<Record> list = Lists.newArrayList();
        String sql_select = SqlKit.sql("report_profit.column");
        String sql_where = String.format(SqlKit.sql("report_profit.where"));

        List<Object> param = Lists.newArrayList();
        param.add(TradeMoney.SUCCESS_STATUS);
        param.add(TradeMoney.DUE_TYPE);

        String sql = sql_select + sql_where;
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql = sql + "AND adate between ? AND ?";
            param.add(date.get(0));
            param.add(date.get(1));
        }
        list = Db.find(sql,param.toArray());
        return list;
    }

    /**
     * 获取统计总数对象
     * @return
     */
    private Record getSum() {
        Record record =new Record();
        String sql_column_sum = SqlKit.sql("report_profit.sql_column_sum");
        String sql_where = String.format(SqlKit.sql("report_profit.where"));
        List<Object> param = Lists.newArrayList();
        param.add(TradeMoney.SUCCESS_STATUS);
        param.add(TradeMoney.DUE_TYPE);

        String sql = sql_column_sum + sql_where;
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql = sql + " AND adate between ? AND ?";
            param.add(date.get(0));
            param.add(date.get(1));
        }
        record = Db.findFirst(sql,param.toArray());
        return record;
    }

    public void detail_index(){
        String adate = getPara("date", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        setAttr("adate",adate);
    }

    public void detail_dtlist(){
        String adate = getPara("adate", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        List<Object> params = Lists.newArrayList();
        params.add(adate);
        renderDataTables(criterias, "profit_detail", params);
    }

    public void detail_export(){
        List<Record> list = getDetailExportList();
        for (Record record : list) {
            //调账方式
            String adjust_mode_name = CommonStatusMap.getAdjustMode(record.getNumber("adjust_mode").intValue());
            //调整金额
            double adjust_amount = record.getNumber("adjust_amount").doubleValue();
            record.set("adjust_amount_name",adjust_mode_name+adjust_amount);
            record.set("status_name","已打款");
        }
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", list);
        JxlsRender beans = JxlsRender.me(Const.Report.PRODUCT_PROFIT_DETAIL_PATH).filename("收益明细报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List<Record> getDetailExportList() {
        String adate = getPara("adate", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        List<Record> list = Lists.newArrayList();
        String sql_select = SqlKit.sql("profit_detail.column");
        String sql_where = String.format(SqlKit.sql("profit_detail.where"));
        List params = Lists.newArrayList();
        params.add(adate);

        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_order"))){
            sql_where = sql_where  + " AND r_o.trade_no like ?" ;
            params.add(DaoKit.like(getPara("s_order")));
        }

        String sql = sql_select + sql_where;
        return Db.find(sql, params.toArray());
    }
}

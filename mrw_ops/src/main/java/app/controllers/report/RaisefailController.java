package app.controllers.report;

import app.Const;
import app.models.member.MemberProduct;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 募集失败报表
 * 产品表中的产品状态是募集失败
 * 订单表中商品是该产品的
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class RaisefailController extends Controller{

    public void index(){
        Record record = getSumRecord();
        setAttr(record);
    }

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, "raisefail");
    }

    public void detail_index(){
        String productid = getPara(0,"0");
        setAttr("productid",productid);
    }

    public void index_export(){
        List<Record> records = getIndexExport();
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.RAISEFAIL_PATH).filename("募集失败报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List<Record> getIndexExport(){
        String s_product_name = getPara("s_product_name");
        String s_org_name = getPara("s_org_name");

        String sql_column = SqlKit.sql("raisefail.column");
        StringBuffer sql_where = new StringBuffer(SqlKit.sql("raisefail.where"));

        List<Object> params = Lists.newArrayList();
        if(!Strings.isNullOrEmpty(s_product_name)){
            sql_where.append(" AND p.name like ?");
            params.add(DaoKit.like(s_product_name));
        }

        if(!Strings.isNullOrEmpty(s_org_name)){
            sql_where.append(" AND i.name like ?");
            params.add(DaoKit.like(s_org_name));
        }

        if(params.size() > 0){
            return Db.find(sql_column + sql_where,params.toArray());
        }else {
            return  Db.find(sql_column + sql_where);
        }
    }

    public void sum_json(){
        Record record = getSumRecord();
        renderJson(record);
    }


    private Record getSumRecord(){
        String s_product_name = getPara("s_product_name");
        String s_org_name = getPara("s_org_name");

        String sql_column_sum = SqlKit.sql("raisefail.column_sum");
        StringBuffer sql_where = new StringBuffer(SqlKit.sql("raisefail.where"));

        List<Object> params = Lists.newArrayList();
        if(!Strings.isNullOrEmpty(s_product_name)){
            sql_where.append(" AND p.name like ?");
            params.add(DaoKit.like(s_product_name));
        }

        if(!Strings.isNullOrEmpty(s_org_name)){
            sql_where.append(" AND i.name like ?");
            params.add(DaoKit.like(s_org_name));
        }

        if(params.size() > 0){
            return Db.findFirst(sql_column_sum + sql_where,params.toArray());
        }else {
            return Db.findFirst(sql_column_sum + sql_where);
        }
    }


    public void detail_dtlist(){
        String productid = getPara(0,"0");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        criterias.setParam("o.product", Condition.EQ,productid);
        renderDataTables(criterias, "raisefail_detail");
    }

    public void detail_export(){
        List<Record> records = getDetailExportList();
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.PRODUCT_BUY_PATH).filename("购买明细.xls").beans(_excel_datas);
        render(beans);
    }

    private  List<Record> getDetailExportList() {
        String s_product = getPara("s_product","0");
        String s_order = getPara("s_order");

        String sql_column = SqlKit.sql("raisefail_detail.column");
        StringBuffer sql_where = new StringBuffer(SqlKit.sql("raisefail_detail.where"));

        List<Object> params = Lists.newArrayList();
        sql_where.append(" AND o.product = ?");
        params.add(s_product);

        if(!Strings.isNullOrEmpty(s_order)){
            sql_where.append(" AND o.trade_no like ?");
            params.add(DaoKit.like(s_order));
        }

        String sql = sql_column + sql_where;
        List<Record> records = Db.find(sql, params.toArray());
        MemberProduct.dao.setPlayStatusName(records);
        return records;
    }
}

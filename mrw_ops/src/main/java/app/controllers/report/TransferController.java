package app.controllers.report;

import app.Const;
import app.constant.TransferConstant;
import app.kit.CommonKit;
import app.models.member.MemberProduct;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.datatables.DTResponse;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 转让报表
 * 挂单
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class TransferController extends Controller{

    public void index(){
        Record record = getSumObj();
        setAttr(record);
    }

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        //查询条件
        String s_date = getPara("a_date");
        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            criterias.setParam("a_date", Condition.BETWEEN,date.toArray());
        }

        renderDataTables(criterias, "report_transfer");
    }

    public void index_export(){
        String s_date = getPara("s_date");
        List<Record> records = getIndexExportList(s_date);
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_TRANSFER_PATH).filename("日转让报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List<Record> getIndexExportList(String s_date) {
        String sql_column = SqlKit.sql("report_transfer.column");
        StringBuilder sql_where = new StringBuilder(SqlKit.sql("report_transfer.where"));
        List<Record> records;
        List<Object> params = Lists.newArrayList();

        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            sql_where.append(" AND a_date BETWEEN ? AND ?");
            params.add(date.get(0));
            params.add(date.get(1));
        }
        if(params.size() > 0){
            records = Db.find(sql_column + sql_where, params.toArray());
        }else {
            records = Db.find(sql_column +sql_where);
        }
        return records;
    }


    public void sum_json(){
        Record sumObj = getSumObj();
        renderJson(sumObj);
    }

    private Record getSumObj() {
        String s_date = getPara("s_date");

        String sql_column_sum = SqlKit.sql("report_transfer.column_sum");
        StringBuilder sql_where = new StringBuilder(SqlKit.sql("report_transfer.where"));
        List<Object> params = Lists.newArrayList();
        Record record;

        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            sql_where.append(" AND a_date BETWEEN ? AND ?");
            params.add(date.get(0));
            params.add(date.get(1));
        }
        if(params.size() > 0){
            record = Db.findFirst(sql_column_sum + sql_where, params.toArray());
        }else {
            record = Db.findFirst(sql_column_sum + sql_where);
        }
        return record;
    }

    public void detail_index(){
        String adate = getPara("date", DateTime.now().toString("yyyy-MM-dd hh:MM:ss"));
        setAttr("adate",adate);
    }

    public void detail_dtlist(){
        String adate = getPara("date",DateTime.now().toString("yyyy-mm-dd hh24:mm:ss"));
        List<Object> params = Lists.newArrayList();
        params.add(adate);
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        Page<Record> datas = DaoKit.paginate(SqlKit.sql("report_transfer_detail.where"),SqlKit.sql("report_transfer_detail.column"),criterias,params);
        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);
    }

    public void detail_export(){
        String adate = getPara("s_date", DateTime.now().toString("yyyy-mm-dd hh24:mm:ss"));
        List<Object> params = Lists.newArrayList();
        params.add(adate);

        String sql_column = SqlKit.sql("report_transfer_detail.column");
        StringBuilder sql_where = new StringBuilder(SqlKit.sql("report_transfer_detail.where"));

        String s_order = getPara("s_order");
        if(!Strings.isNullOrEmpty(s_order)){
            sql_where.append(" AND r_o.trade_no like ?");
            params.add(DaoKit.like(s_order));
        }
        List<Record> records = Db.find(sql_column + sql_where, params.toArray());
        setStatusName(records);
        Map<String, Object> _excel_datas = Maps.newHashMap();

        MemberProduct.dao.setPlayStatusName(records);
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_TRANSFER_DETAIL_PATH).filename("每日转让明细报表.xls").beans(_excel_datas);
        render(beans);

    }

    private void setStatusName(List<Record> records) {
        if(null !=records&&records.size() > 0 ){
            for (Record record : records) {
                int status = record.getNumber("status").intValue();
                String status_name = "";
                switch (status){
                    case TransferConstant.STATUE_PAUSE:status_name="待审核";break;
                    case TransferConstant.STATUE_TRADING:status_name="交易中";break;
                    case TransferConstant.STATUE_SUCCESS:status_name="交易成功";break;
                    case TransferConstant.STATUE_FAIL:status_name="转让失效";break;
                    case TransferConstant.STATUE_CANCEL:status_name="转让撤销";break;
                }
                record.set("status_name", status_name);
            }
        }
    }
}

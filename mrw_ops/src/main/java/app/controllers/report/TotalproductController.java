package app.controllers.report;

import app.Const;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 产品资金汇总报表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class TotalproductController extends Controller {

    public void index(){
        Record record = getTotalSumRecord();
        setAttr("record",record);
    }

    public void index_dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, "totalproduct");
    }

    public void index_sum(){
        Record record = getTotalSumRecord();
        renderJson(record);
    }

    private Record getTotalSumRecord() {
        String sql = SqlKit.sql("totalproduct.column_sum") + SqlKit.sql("totalproduct.where");

        if (!Strings.isNullOrEmpty(getPara("product_name"))) {
            sql = sql + " AND p.name like ?";
            return Db.findFirst(sql,DaoKit.like(getPara("product_name")));
        }else{
            return Db.findFirst(sql);
        }
    }

    public void index_export(){
        String sql = SqlKit.sql("totalproduct.column") + SqlKit.sql("totalproduct.where");
        List<Record> records ;
        if (!Strings.isNullOrEmpty(getPara("s_product_name"))){
            sql = sql + " AND p.name like ?";
            records = Db.find(sql,DaoKit.like(getPara("s_product_name")));
        }else{
            records = Db.find(sql);
        }

        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.TOTAL_PRODUCT_PATH).filename("产品资金汇总报表.xls").beans(_excel_datas);
        render(beans);
    }
}

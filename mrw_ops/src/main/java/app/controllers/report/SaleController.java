package app.controllers.report;

import app.constant.MemberConstant;
import app.constant.OrderConstant;
import app.kit.CommonKit;
import app.kit.TypeKit;
import app.models.member.MemberProduct;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.datatables.DTResponse;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

import static app.Const.*;

/**
 * <p>
 * 销售报表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class SaleController extends Controller{

    int status = OrderConstant.TRADE_SUCCESS;
    int redem_status = MemberConstant.REDEM_SUCCESS;


    public void index(){
        Record record = getSum();
        setAttr("record",record);
    }

    public void dt_list(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            criterias.setParam("a_date", Condition.BETWEEN,date.toArray());
        }
        String sql_where = String.format(SqlKit.sql("report_sale.where"), OrderConstant.BUY_TYPE,OrderConstant.INC_TYPE,status,redem_status);
        Page<Record> datas = DaoKit.paginate(sql_where, SqlKit.sql("report_sale.column"), criterias, Lists.newArrayListWithCapacity(1));
        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);
    }

    /**
     * 数据导出
     */
    public void export() {
        List list =getList();
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", list);
        JxlsRender beans = JxlsRender.me(Report.DAY_SALE_PATH).filename("日终销售报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List getList() {
        List list = Lists.newArrayList();
        String sql_select = SqlKit.sql("report_sale.column");
        String sql_where = String.format(SqlKit.sql("report_sale.where"), OrderConstant.BUY_TYPE,OrderConstant.INC_TYPE,status,redem_status);

        String sql = sql_select + sql_where;
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql = sql + "AND a_date between ? AND ?";
            list = Db.find(sql, date.get(0), date.get(1));
        }else{
            list = Db.find(sql_select + sql_where);
        }
        return list;
    }

    /**
     * 统计量JSON
     */
    public void count_json(){
        Record record = getSum();
        renderJson(record);
    }

    /**
     * 获取统计总数对象
     * @return
     */
    private Record getSum() {
        Record record =new Record();
        String sql_column_sum = SqlKit.sql("report_sale.column_sum");
        String sql_where = String.format(SqlKit.sql("report_sale.where"), OrderConstant.BUY_TYPE,OrderConstant.INC_TYPE,status,redem_status);

        String sql = sql_column_sum + sql_where;
        //查询条件
        if(!Strings.isNullOrEmpty(getPara("s_date"))){
            List<DateTime> date = CommonKit.string2DateTimeList(getPara("s_date"), "yyyy/MM/dd");
            sql = sql + "AND a_date between ? AND ?";
            record = Db.findFirst(sql, date.get(0), date.get(1));
        }else{
            record = Db.findFirst(sql_column_sum + sql_where);
        }
        return record;
    }

    /**
     * 明细首页
     */
    public void detail_index(){
        String adate = getPara("date", DateTime.now().toString("yyyy-MM-dd hh:MM:ss"));
        setAttr("adate",adate);
    }

    public void detail_dtlist(){
        //传入的日期
        String adate = getPara("date",DateTime.now().toString("yyyy-MM-dd hh:MM:ss"));
        List<Object> params = Lists.newArrayList();
        params.add(adate);
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        String sql_where = String.format(SqlKit.sql("report_sale_detail.where"), OrderConstant.BUY_TYPE,OrderConstant.INC_TYPE,status,redem_status);

        Page<Record> datas = DaoKit.paginate(sql_where, SqlKit.sql("report_sale_detail.column"), criterias, params);
        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);
    }

    public void detail_export(){
        String adate = getPara("s_date",DateTime.now().toString("yyyy-mm-dd hh24:mm:ss"));
        List<Object> params = Lists.newArrayList();
        params.add(adate);

        String sql_column = SqlKit.sql("report_sale_detail.column");
        String sql_where = String.format(SqlKit.sql("report_sale_detail.where"), OrderConstant.BUY_TYPE,OrderConstant.INC_TYPE,status,redem_status);

        if(!Strings.isNullOrEmpty(getPara("s_order"))){
            sql_where = sql_where + "AND ord.trade_amount like ?";
            params.add(DaoKit.like(getPara("s_order")));
        }

        if(!Strings.isNullOrEmpty(getPara("s_order_type"))){
            sql_where = sql_where + "AND ord.order_type = ?";
            params.add(getPara("s_order_type"));
        }

        String sql = sql_column + sql_where;

        List<Record> records = Db.find(sql, params.toArray());
        MemberProduct.dao.setPlayStatusName(records);
        MemberProduct.dao.setOrderTypeName(records);

        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Report.DAY_SALE_DETAIL_PATH).filename("每日销售报表.xls").beans(_excel_datas);
        render(beans);
    }
}

package app.controllers.report;

import app.Const;
import app.kit.CommonKit;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.datatables.DTResponse;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 手续费报表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class FeeController extends Controller{

    public void index(){
        Record record = getSumObj();
        setAttr(record);
    }

    private Record getSumObj() {
        String s_date = getPara("s_date");

        String sql_column_sum = SqlKit.sql("report_fee.column_sum");
        StringBuffer sql_where = new StringBuffer(SqlKit.sql("report_fee.where"));
        List params = Lists.newArrayList();
        Record record = new Record();

        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            sql_where.append(" AND a_date BETWEEN ? AND ?");
            params.add(date.get(0));
            params.add(date.get(1));
        }
        if(params.size() > 0){
            record = Db.findFirst(sql_column_sum + sql_where, params.toArray());
        }else {
            record = Db.findFirst(sql_column_sum + sql_where);
        }
        return record;
    }

    public void sum_json(){
        Record record = getSumObj();
        renderJson(record);
    }

    public void dt_list(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        //查询条件
        String s_date = getPara("s_date");
        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            criterias.setParam("a_date", Condition.BETWEEN,date.toArray());
        }
        renderDataTables(criterias,"report_fee");
    }

    public void index_export(){
        String s_date = getPara("s_date");

        List<Record> records = getIndexExportList(s_date);

        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_FEE_PATH).filename("手续费报表.xls").beans(_excel_datas);
        render(beans);
    }

    private List<Record> getIndexExportList(String s_date) {
        String sql_column = SqlKit.sql("report_fee.column");
        StringBuffer sql_where = new StringBuffer(SqlKit.sql("report_fee.where"));
        List<Record> records = Lists.newArrayList();
        List<Object> params = Lists.newArrayList();

        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            sql_where.append(" AND a_date BETWEEN ? AND ?");
            params.add(date.get(0));
            params.add(date.get(1));
        }

        if(params.size() > 0){
            records = Db.find(sql_column + sql_where, params.toArray());
        }else {
            records = Db.find(sql_column +sql_where);
        }
        return records;
    }

    public void detail_index(){
        String adate = getPara("date", DateTime.now().toString("yyyy-MM-dd hh:MM:ss"));
        Record record = getDetailSum(adate);
        setAttr(record);
        setAttr("adate",adate);
    }

    private Record getDetailSum(String adate) {
        String sql_column_sum = SqlKit.sql("report_fee_detail.column_sum");
        List<Object> params = Lists.newArrayList();
        params.add(adate);

        StringBuilder sql_where = getDetailSqlWhere(params);
        Record record = new Record();
        if(params.size() > 0 ){
            record = Db.findFirst(sql_column_sum + sql_where, params.toArray());
        }else {
            record = Db.findFirst(sql_column_sum + sql_where);
        }
        return record;
    }

    private StringBuilder getDetailSqlWhere(List<Object> params) {
        StringBuilder sql_where = new StringBuilder(SqlKit.sql("loding_fee_detail.where"));

        String s_order = getPara("s_order");
        if(!Strings.isNullOrEmpty(s_order)){
            sql_where.append(" AND r_o.trade_no like ?");
            params.add(DaoKit.like(s_order));
        }
        String s_product_name = getPara("s_product_name");
        if(!Strings.isNullOrEmpty(s_product_name)){
            sql_where.append(" AND  t_p.product_name like ?");
            params.add(DaoKit.like(s_product_name));
        }
        Boolean free_status = getParaToBoolean("free_status");
        if(null != free_status){
            if(free_status){
                sql_where.append(" AND  t_p.handing_charge > 0");
            }else {
                sql_where.append(" AND  t_p.handing_charge = 0");
            }
        }
        return sql_where;
    }


    public void detail_dtlist(){
        String adate = getPara("date","0");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        List<Object> params = Lists.newArrayList();
        params.add(adate);

        StringBuilder sql_where = getDetailSqlWhere(params);

        Page<Record> datas = DaoKit.paginate(sql_where.toString(), SqlKit.sql("report_fee_detail.column"), criterias, params);
        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);
    }

    public void sum_detail_json(){
        String adate = getPara("s_date",DateTime.now().toString("yyyy-mm-dd hh24:mm:ss"));
        Record record = getDetailSum(adate);
        renderJson(record);
    }

    public void detail_export(){
        String adate = getPara("s_date", DateTime.now().toString("yyyy-mm-dd hh24:mm:ss"));
        List<Object> params = Lists.newArrayList();
        params.add(adate);

        String sql_column = SqlKit.sql("report_fee_detail.column");
        StringBuilder sql_where = getDetailSqlWhere(params);

        List<Record> records = Db.find(sql_column + sql_where, params.toArray());
        Map<String, Object> _excel_datas = Maps.newHashMap();
        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.DAY_FEE_DETAIL_PATH).filename("手续费明细报表.xls").beans(_excel_datas);
        render(beans);
    }
}

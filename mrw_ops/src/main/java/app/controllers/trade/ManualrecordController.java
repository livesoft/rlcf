package app.controllers.trade;

import app.kit.CommonKit;
import app.kit.TypeKit;
import app.models.order.TradeMoney;
import com.google.common.base.Strings;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.tuples.Pair;
import org.joda.time.DateTime;

import static app.models.order.TradeMoney.*;
import static app.models.order.TradeMoney.RAISED_SUCCESS_TYPE;

/**
 * <p>
 * 调账记录
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class ManualrecordController extends Controller{

    public void index(){
        String trade_money = getPara(0, "");
        TradeMoney tradeMoney = TradeMoney.dao.findById(trade_money);

        String title = "结算中心";
        //交易金额结算id
        String trade_money_id = getPara(0, "0");
        if(null != tradeMoney){
            int type = TypeKit.getInt(tradeMoney, "type");
            switch (type) {
//            case DAILY_TYPE:
//                title = "产品募集到期结算";
//                break;
                case DUE_TYPE:
                    title = "产品到期收益结算清单";
                    break;
                case FAIL_TYPE:
                    title = "募集失败资金返还清单";
                    break;
                case TRANSFER_TYPE:
                    title = "转让资金结算清单";
                    break;
                case CANCEL_TYPE:
                    title = "产品退单资金结算清单";
                    break;
                case RAISED_SUCCESS_TYPE:
                    title = "产品募集到期结算清单";
                    break;
                default:
                    title = "结算中心";
                    break;
            }
        }
        setAttr("title",title);
        setAttr("trade_money", tradeMoney);
    }

    public void dtlist(){
        String trade_money = getPara(0, "");

        String buy_time = getPara("s_time");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());

        if(!Strings.isNullOrEmpty(buy_time)){
            Pair<DateTime, DateTime> objects = CommonKit.rangeDatetime(buy_time);
            criterias.setParam("rtr.adjust_time", Condition.BETWEEN,objects.toArray());
        }
        criterias.setParam("rtr.trade_money", Condition.EQ,trade_money);
        renderDataTables(criterias, "trade_reconciliation");
    }
}

package app.controllers.trade;

import app.constant.OrderConstant;
import app.constant.ProductConstant;
import app.kit.CommonKit;
import app.kits.ReportTplPathKit;
import app.models.product.Issuer;
import app.models.product.Product;
import app.services.ExportService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import goja.security.shiro.Securitys;
import goja.tuples.Pair;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

import static app.models.order.TradeMoney.CANCEL_TYPE;
import static app.models.order.TradeMoney.DUE_TYPE;
import static app.models.order.TradeMoney.FAIL_TYPE;
import static app.models.order.TradeMoney.PAY_FAIL_TYPE;
import static app.models.order.TradeMoney.RAISED_SUCCESS_TYPE;
import static app.models.order.TradeMoney.TRANSFER_TYPE;

/**
 * <p>
 * The url trade/clearing Controller.
 * 结算中心首页展示
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ClearingController extends Controller {

    /**
     * The index route.
     * the url /trade/clearing
     * the view in index.ftl
     */
    public void index() {
        //        打款类型
        int type = getParaToInt(0, 0);
        String title;
        switch (type) {
//            case DAILY_TYPE:
//                title = "产品募集到期结算";
//                break;
            case DUE_TYPE:
                title = "产品到期收益结算清单";
                break;
            case FAIL_TYPE:
                title = "募集失败资金返还清单";
                break;
            case TRANSFER_TYPE:
                title = "转让资金结算清单";
                break;
            case CANCEL_TYPE:
                title = "产品退单资金结算清单";
                break;
            case RAISED_SUCCESS_TYPE:
                title = "产品募集到期结算清单";
                break;
            case PAY_FAIL_TYPE:
                title = "交易失败结算清单";
                break;
            default:
                title = "结算中心";
                break;
        }
        //发行机构
        setAttr("issers", Issuer.dao.findAll());
        setAttr("title", title);
        setAttr("type", type);
    }


    public void dtlist() {
        //清单类型
        int type = getParaToInt(0, 0);
        final DTCriterias criterias = getCriterias();
        if (type > 0) {
            String productName = getPara("name");
            String productIssuer = getPara("issuer");
            if (!Strings.isNullOrEmpty(productName)) {
                // 产品名称搜素
                criterias.setParam("p.name", Condition.LIKE, DaoKit.like(productName));
            }
            if (!Strings.isNullOrEmpty(productIssuer)) {
                // 产品机构
                criterias.setParam("p.issuer", productIssuer);
            }
            List<Object> params = Lists.newArrayList();
            params.add(type);
            switch (type) {
//                case DAILY_TYPE:
//                    break;
                case DUE_TYPE:
                    criterias.setParam("p.status", ProductConstant.END);
                    renderDataTables(criterias, "clearing.due", params);
                    break;
                case FAIL_TYPE:
                    criterias.setParam("p.status", ProductConstant.FAIL);
                    renderDataTables(criterias, "clearing.due", params);
                    break;
                case TRANSFER_TYPE:
                case CANCEL_TYPE:
                    renderDataTables(criterias, "clearing.due", params);
                    break;
                case PAY_FAIL_TYPE:
                    String days = getPara("date_time");
                    if (!Strings.isNullOrEmpty(days)) {
                        Pair<String, String> rangeDatetime = CommonKit.rangeDatestr(days);
                        if (rangeDatetime != null) {
                            criterias.setParam("t.buy_time", Condition.BETWEEN, new Object[]{rangeDatetime.getValue0(), rangeDatetime.getValue1()});
                        }
                    }
                    renderDataTables(criterias, "clearing.payfail", params);
                    break;
                case RAISED_SUCCESS_TYPE:
                    List<Object> o_params = Lists.newArrayList();
                    o_params.add(type);
                    o_params.add(ProductConstant.FOUND);
                    o_params.add(ProductConstant.END);
                    renderDataTables(criterias, "clearing.success", o_params);
                    break;
                default:
                    break;
            }
            return;
        }
        renderEmptyDataTables(criterias);
    }

    public void productlist() {

        int product = getParaToInt(0, 0);
        if (product > 0) {
            setAttr("productId", product);
            setAttr("product", Product.dao.findById(product));
        } else {
            renderNull();
        }

    }

    public void productdtlist() {
        final DTCriterias criterias = getCriterias();
        int product = getParaToInt(0, 0);

        if (product > 0) {
            String time_q = getPara("time");
            if (!Strings.isNullOrEmpty(time_q)) {
                Pair<DateTime, DateTime> rangeDatetime = CommonKit.rangeDatetime(time_q);
                if (rangeDatetime != null) {
                    criterias.setParam("rmp.buy_time", Condition.BETWEEN, new Object[]{rangeDatetime.getValue0(), rangeDatetime.getValue1()});
                }
            }
            criterias.setParam("rmp.product", product);
            criterias.setParam("o.order_type", OrderConstant.BUY_TYPE);
            renderDataTables(criterias, "clearing.productdtlist");
        } else {
            renderEmptyDataTables(criterias);
        }
    }

    /**
     * 导出请求
     */
    public void export() {

        int type = getParaToInt(0, 0);
        if (type > 0) {
            String tpl_file = ReportTplPathKit.getExcelByType(type);
            if (Strings.isNullOrEmpty(tpl_file)) {
                renderNull();
            } else {
                String loginName = Securitys.getLogin().name;
                String productName = getPara("name");
                String productIssuer = getPara("issuer");
                List<Object> params = Lists.newArrayList();
                StringBuilder whereSql = new StringBuilder();
                params.add(type);
                if (!Strings.isNullOrEmpty(productName)) {
                    // 产品名称搜素
                    whereSql.append(" AND p.name like ?");
                    params.add(DaoKit.like(productName));
                }
                if (!Strings.isNullOrEmpty(productIssuer)) {
                    // 产品机构
                    whereSql.append(" AND p.issuer = ?");
                    params.add(productIssuer);
                }
                int productId = getParaToInt(1, 0);
                Map<String, Object> beans = Maps.newHashMap();
                switch (type) {
                    case DUE_TYPE: {
                        ExportService.me.invokeDue(productId, loginName, params, whereSql, beans);
                        render(JxlsRender.me(tpl_file).filename("到期收益发放结算单审核.xlsx").beans(beans));
                        break;
                    }
                    case FAIL_TYPE: {
                        ExportService.me.invokeFail(productId, loginName, beans);
                        render(JxlsRender.me(tpl_file).filename("募集失败资金返还清单审核.xlsx").beans(beans));
                        break;
                    }
                    case TRANSFER_TYPE: {
                        ExportService.me.invokeTranser(productId, loginName, beans);
                        render(JxlsRender.me(tpl_file).filename("转让资金结算清单审核.xlsx").beans(beans));
                        break;
                    }
                    case CANCEL_TYPE: {
                        // 默认导出一周的数据
                        ExportService.me.invokeCancel(productId, loginName, whereSql, beans);
                        render(JxlsRender.me(tpl_file).filename("产品退单资金结算清单审核.xlsx").beans(beans));
                        break;
                    }
                    case PAY_FAIL_TYPE: {
                        String day = getPara("day");
                        ExportService.me.invokePayFail(day, loginName, beans);
                        render(JxlsRender.me(tpl_file).filename("电子账户" + day + "交易差错审核.xlsx").beans(beans));
                        break;
                    }
                    case RAISED_SUCCESS_TYPE:
                    {
                        ExportService.me.invokeRaisedSuccess(productId, loginName, beans);
                        render(JxlsRender.me(tpl_file).filename("募集成功结算单审核.xlsx").beans(beans));
                        break;
                    }
                    default:
                        break;
                }
            }
        }

    }

}
package app.controllers.trade;

import app.Const;
import app.kit.CommonKit;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import goja.mvc.Controller;
import goja.mvc.render.JxlsRender;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.datatables.DTResponse;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 财务对账
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class FinancecostController extends Controller {

    public void index(){

    }

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
//      查询条件
        String s_date = getPara("s_date");
        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            criterias.setParam("e_account.t_date", Condition.BETWEEN,date.toArray());
        }
        Page<Record> datas = DaoKit.paginate("financecost", criterias);
        setPageData(datas);

        DTResponse response = DTResponse.build(criterias, datas.getList(), datas.getTotalRow(), datas.getTotalRow());
        renderJson(response);

    }

    private void setPageData(Page<Record> datas) {
        //处理银行差异金额、平台差异金额、平台对账结果、银行对账结果
        for (Record record : datas.getList()) {
            //上日余额
            BigDecimal last_day = getBigdecimal(record.getBigDecimal("last_day"));
            //电子账户余额
            BigDecimal balance = getBigdecimal(record.getBigDecimal("balance"));
            //银行收入
            BigDecimal bank_in_amount = getBigdecimal(record.getBigDecimal("bank_in_amount"));
            //银行支出
            BigDecimal bank_out_amount = getBigdecimal(record.getBigDecimal("bank_out_amount"));
            //平台收入
            BigDecimal order_in_amount = getBigdecimal(record.getBigDecimal("order_in_amount"));
            //平台支出
            BigDecimal order_out_amount = getBigdecimal(record.getBigDecimal("order_out_amount"));

            record.set("rst_order", getBigdecimal(last_day.add(order_in_amount).subtract(order_out_amount)).equals(getBigdecimal(balance)));

            record.set("rst_bank", getBigdecimal(last_day.add(bank_in_amount).subtract(bank_out_amount)).equals(getBigdecimal(balance)));

            //银行差异金额：等于电子账户金额 -（银行收入 - 银行支出 + 上日余额），正数代表比电子账户余额多，负数代表比电子余额少
            BigDecimal bank_diff = getBigdecimal(balance.subtract(bank_in_amount.subtract(bank_out_amount).add(last_day)));
            record.set("bank_diff",bank_diff);

            //平台差异金额：等于电子账户金额 - （平台收入 - 平台支出 + 上日余额），正数代表比电子账户余额多，负数代表比电子余额少
            BigDecimal order_diff = getBigdecimal(balance.subtract(order_in_amount.subtract(order_out_amount).add(last_day)));
            record.set("order_diff",order_diff);

        }
    }

    public void index_export(){
        String s_date = getPara("s_date");
        String sql_column = SqlKit.sql("financecost.column");
        String sql_where = SqlKit.sql("financecost.where");
        String sql_order = "ORDER BY e_account.t_date DESC";

        List<Record> records ;

        if(!Strings.isNullOrEmpty(s_date)){
            List<DateTime> date = CommonKit.string2DateTimeList(s_date, "yyyy/MM/dd");
            sql_where = sql_where + "AND t_date BETWEEN ? AND ?";
            records = Db.find(sql_column + sql_where + sql_order, date.toArray());
        }else{
            records = Db.find(sql_column + sql_where + sql_order);
        }

        Map<String, Object> _excel_datas = Maps.newHashMap();
        //上日余额 + 平台当日收入金额 - 平台支出金额 ＝ 电子账户余额 （等于表示平台账单正常）
        //上日余额 + 银行当日收入金额 - 银行支出金额 ＝ 电子账户余额 （等于表示银行对账正常）
        for (Record record : records) {
            //上日余额
            BigDecimal last_day = getBigdecimal(record.getBigDecimal("last_day"));
            //电子账户余额
            BigDecimal balance = getBigdecimal(record.getBigDecimal("balance"));
            //银行收入
            BigDecimal bank_in_amount = getBigdecimal(record.getBigDecimal("bank_in_amount"));
            //银行支出
            BigDecimal bank_out_amount = getBigdecimal(record.getBigDecimal("bank_out_amount"));
            //平台收入
            BigDecimal order_in_amount = getBigdecimal(record.getBigDecimal("order_in_amount"));
            //平台支出
            BigDecimal order_out_amount = getBigdecimal(record.getBigDecimal("order_out_amount"));

            if (getBigdecimal(last_day.add(order_in_amount).subtract(order_out_amount)).equals(getBigdecimal(balance)) ) {
                record.set("rst_order","正常");
            }else {
                record.set("rst_order","异常");
            }

            if (getBigdecimal(last_day.add(bank_in_amount).subtract(bank_out_amount)).equals(getBigdecimal(balance))) {
                record.set("rst_bank","正常");
            }else {
                record.set("rst_bank","异常");
            }
        }

        _excel_datas.put("report", records);
        JxlsRender beans = JxlsRender.me(Const.Report.FINANCE_COST_PATH).filename("财务对账单.xls").beans(_excel_datas);
        render(beans);
    }

    public BigDecimal getBigdecimal(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return BigDecimal.ZERO;
        } else {
            return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
        }
    }

}

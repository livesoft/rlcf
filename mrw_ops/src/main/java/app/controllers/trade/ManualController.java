package app.controllers.trade;

import app.kit.CommonKit;
import app.kit.TypeKit;
import app.models.order.TradeMoney;
import app.models.order.TradeReconciliation;
import com.google.common.base.Strings;
import com.jfinal.plugin.activerecord.Model;
import goja.mvc.Controller;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;
import org.joda.time.DateTime;

import static app.models.order.TradeMoney.CANCEL_TYPE;
import static app.models.order.TradeMoney.DUE_TYPE;
import static app.models.order.TradeMoney.FAIL_TYPE;
import static app.models.order.TradeMoney.PAY_FAIL_TYPE;
import static app.models.order.TradeMoney.RAISED_SUCCESS_TYPE;
import static app.models.order.TradeMoney.TRANSFER_TYPE;

/**
 * <p>
 * 人工调账 交易金额结算
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class ManualController extends Controller {

    public void index() {
        String title = "结算中心";
        //交易金额结算id
        String trade_money_id = getPara(0, "0");
        TradeMoney tradeMoney = TradeMoney.dao.findDetailById(trade_money_id);
        if (tradeMoney == null) {
            renderNull();
            return;
        }
        int type = TypeKit.getInt(tradeMoney, "type");
        switch (type) {
//            case DAILY_TYPE:
//                title = "产品募集到期结算";
//                break;
            case DUE_TYPE:
                title = "产品到期收益结算清单";
                break;
            case FAIL_TYPE:
                title = "募集失败资金返还清单";
                break;
            case TRANSFER_TYPE:
                title = "转让资金结算清单";
                break;
            case CANCEL_TYPE:
                title = "产品退单资金结算清单";
                break;
            case RAISED_SUCCESS_TYPE:
                title = "产品募集到期结算清单";
                break;
            case PAY_FAIL_TYPE:
                title = "交易失败结算清单";
                String day = getPara("day");
                setAttr("day", day);

                setAttr("title", title);
                String back = "/trade/playmoney/" + type + "-8?day=" +day;
                setAttr("back", back);
                setAttr(tradeMoney);
                return;
            default:
                title = "结算中心";
                break;
        }
        setAttr("title", title);
        String back = "/trade/playmoney/" + type + "-" + tradeMoney.getNumber("product");
        setAttr("back", back);
        setAttr(tradeMoney);
    }

    public void save() {
        AppUser<Model> login = Securitys.getLogin();

        TradeMoney tradeMoney = getModel(TradeMoney.class, "tradeMoney");
        if (!DaoKit.isNew(tradeMoney)) {
            boolean update = tradeMoney.update();
            int tradeMoney_id = tradeMoney.getNumber("id").intValue();
            tradeMoney = TradeMoney.dao.findById(tradeMoney_id);
            if (update) {
                int adjust_mode = tradeMoney.getNumber("adjust_mode").intValue();
                double adjust_amount = tradeMoney.getNumber("adjust_amount").doubleValue();

                TradeReconciliation tradeReconciliation = new TradeReconciliation();
                tradeReconciliation.set("id", CommonKit.uuid())
                        .set("trade_money", tradeMoney_id)
                        .set("amount", adjust_amount)
                        .set("adjust_mode", adjust_mode)
                        .set("adjust_time", DateTime.now())
                        .set("adjust_user", login.id)
                        .save();
                String back = getPara("back_url");
                if(Strings.isNullOrEmpty(back)){

                    String url = "/trade/playmoney/" + tradeMoney.getNumber("type").intValue() + "-" + tradeMoney.getNumber("product");
                    redirect(url);
                } else {
                    redirect(back);
                }
            }
        }

    }
}

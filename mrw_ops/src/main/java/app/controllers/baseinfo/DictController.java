package app.controllers.baseinfo;

import app.Const;
import app.dtos.ZTreeDto;
import app.kit.CommonKit;
import app.models.basic.Dict;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.ehcache.CacheKit;
import goja.StringPool;
import goja.lang.Lang;
import goja.mvc.Controller;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

import static app.Const.FIELD_CREATE_TIME;
import static app.Const.YES;
import static goja.StringPool.PK_COLUMN;

/**
 * <p>
 * The url baseinfo/dict Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class DictController extends Controller {

    public static final String FORM_ATTR = "d";


    /**
     * The index route.
     * the url /baseinfo/dict
     * the view in index.ftl
     */
    public void index() {

    }

    public void nodes() {
        String id = getPara(PK_COLUMN, StringPool.ZERO);
        List<ZTreeDto> children_data = Dict.dao.findByParent(id);
        if (!Lang.isEmpty(children_data)) {
            children_data.get(0).open = true;
        }
        renderText(JsonKit.toJson(children_data));
    }


    public void save() {
        final Dict dict = getModel(Dict.class, FORM_ATTR);

        if (Strings.isNullOrEmpty(dict.getStr(StringPool.PK_COLUMN)) || StringUtils.equals(dict.getStr(PK_COLUMN), StringPool.ZERO)) {
            dict.set(FIELD_CREATE_TIME, DateTime.now());
            dict.set(StringPool.PK_COLUMN, CommonKit.uuid());
            dict.save();

            if(!StringUtils.equals(StringPool.ZERO, dict.getStr("parent"))){
                Dict parent = new Dict();
                parent.set(PK_COLUMN, dict.getStr("parent"));
                parent.set("children_flag",YES);
                parent.update();
            }
        } else {

            String id = dict.getStr(PK_COLUMN);
            Dict db_dict = Dict.dao.findById(id);
            if (StringUtils.equals(Const.YES, db_dict.getStr("sys_flag"))) {
                renderAjaxFailure("系统默认字典，无法进行编辑");
                return;
            }

            dict.update();
        }
        // 清理缓存
        CacheKit.remove("sys_dict", "js_cache");
        dict.remove(FIELD_CREATE_TIME);
        renderAjaxSuccess(dict);
    }


    public void delete() {
        if (Dict.dao.deleteById(getPara())) {
            renderAjaxSuccess();
        } else {
            renderAjaxFailure();
        }
    }


    public void show() {
        final String category = getPara();
        if (Strings.isNullOrEmpty(category)) {
            renderAjaxSuccess();
        } else {
            List<Dict> dicts = Dict.dao.findByCategory(category);
            renderAjaxSuccess(dicts);
        }
    }


    /**
     */
//    public void js() {
//        Map<String, List<Dict>> dictMaps = CacheKit.get("sys_dict", "js_cache");
//
//        if(Lang.isEmpty(dictMaps)){
//            dictMaps = Maps.newHashMap();
//
//            Map<Integer, List<Dict>> parentMaps = Maps.newHashMap();
//
//            try {
//                List<Dict> dicts = Dict.dao.findAll();
//                List<Dict> childrens;
//                for (Dict dict : dicts) {
//                    final int parent = dict.getNumber("parent").intValue();
//                    if (parent == 0) {
//                        childrens = Lists.newArrayList();
//                        dictMaps.put(dict.getStr(Const.FIELD_CODE), childrens);
//                        parentMaps.put(dict.getNumber(PK_COLUMN).intValue(), childrens);
//                    } else {
//                        childrens = parentMaps.get(parent);
//                        childrens.add(dict);
//                    }
//                }
//            } finally {
//                parentMaps.clear();
//                parentMaps = null;
//            }
//
//
//            CacheKit.put("sys_dict", "js_cache", dictMaps);
//        }
//
//        renderJavascript("var sys_dict = " + JsonKit.toJson(dictMaps));
//    }

    public void js() {
        Map<String,Map<String,Dict>> dictMaps = CacheKit.get("sys_dict", "js_cache");

        if(Lang.isEmpty(dictMaps)){
            dictMaps = Maps.newHashMap();
            Map<String,Dict> parentMaps = Maps.newHashMap();
            try {
                List<Dict> dicts = Dict.dao.findAll();
                Map<String,Dict> childrens;

                for (int i = 0; i < dicts.size(); i++) {
                    Dict dict = dicts.get(i);
                    final String parent = dict.getStr("parent");
                    if(StringUtils.equals(StringPool.ZERO, parent)){
                        childrens = Maps.newHashMap();
                            dictMaps.put(dict.getStr(Const.FIELD_CODE),childrens);
                        for (Dict d : dicts) {
                            if(StringUtils.equals(d.getStr("parent"), dict.getStr(StringPool.PK_COLUMN))){
                                childrens.put(d.getStr(Const.FIELD_CODE),d);
                            }
                        }
                    }
                }

            } finally {
                parentMaps.clear();
            }
            CacheKit.put("sys_dict", "js_cache", dictMaps);
        }
        renderJavascript("var sys_dict = " + JsonKit.toJson(dictMaps));
    }
}
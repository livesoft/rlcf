package app.controllers.baseinfo;

import app.Const;
import app.kit.CommonKit;
import app.models.basic.Notice;
import app.models.sys.User;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.Condition;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;


/**
 * 公告管理
 * liuhui on 15/2/5.
 */
public class NoticeController extends Controller {

    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {

    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        String pubdate = getPara("pubdate");
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        if(!Strings.isNullOrEmpty(pubdate)){
            criterias.setParam("pubdate", Condition.BETWEEN,pubdate.split(" - "));
        }
        renderDataTables(criterias, Notice.class);
    }

    @Before(GET.class)
    public void create() {
        Notice notice = new Notice();
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr(notice);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        Notice notice = Notice.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("notice",notice);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        Notice notice = getModel(Notice.class);

        if (!Strings.isNullOrEmpty(getPara("pic_attachment"))) {
            notice.set("pic",getPara("pic_attachment_url"));
            notice.set("pic_attachment",getPara("pic_attachment"));
        }

        if (DaoKit.isNew(notice)) {
            notice.set("publisher",loginUser.getName());
            notice.set("summary", CommonKit.getSummary(notice.getStr("content")));
            notice.set(Const.FIELD_STATUS, 1);
            notice.save();
        } else {
            notice.set("summary", CommonKit.getSummary(notice.getStr("content")));
            notice.update();
        }
        redirect("/baseinfo/notice");
    }

    public void delete(){
        int id = getParaToInt(0, 0);
        boolean status = Notice.dao.deleteById(id);
        if(status){
            renderAjaxSuccess();
        }else {
            renderAjaxFailure();
        }
    }

}

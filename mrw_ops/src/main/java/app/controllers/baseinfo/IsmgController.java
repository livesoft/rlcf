package app.controllers.baseinfo;

import app.models.sys.Ismg;
import goja.StringPool;
import goja.mvc.Controller;

/**
 * <p>
 * The url imsg Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class IsmgController extends Controller {

    /**
     * The index route.
     * the url /imsg
     */
    public void index() {
        Ismg ismg = Ismg.dao.findByDefault();
        setAttr("ismg", ismg);
    }


    public void save() {
        Ismg ismg = getModel(Ismg.class);
        ismg.set(StringPool.PK_COLUMN, "9698a4603df4405b84854699a911016f");
        ismg.update();
        renderAjaxSuccess();
    }
}
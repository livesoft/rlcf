package app.controllers.product;

import app.Const;
import app.models.app.ProductDetails;
import app.models.basic.Attachment;
import app.models.product.Details;
import app.models.product.Product;
import app.models.product.ProductAttr;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.JsonKit;
import goja.Func;
import goja.cache.Cache;
import goja.lang.Lang;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.List;

/**
 * 产品属性详细
 * liuhui on 15/2/13.
 */
public class DetailController extends Controller {

    public void index() {
        String product_id = getPara(0);
        setAttr("product_id", product_id);

        List<?> attrs = Cache.get("attrs", List.class);
        if(Lang.isEmpty(attrs)){
            attrs = ProductAttr.dao.findAll();
            Cache.set("attrs", attrs);
        }
        setAttr("attr_json", JsonKit.toJson(attrs));
    }

    @Before(GET.class)
    public void dtlist() {
        String product_id = getPara(0);
        final DTCriterias criterias = getCriterias();
        criterias.setParam(Const.FIELD_PRODUCT, product_id);
        criterias.setParam("risk_flag", Const.NO);
        renderDataTables(criterias, Details.class);
    }

    @Before(GET.class)
    public void create() {
        int product_id = getParaToInt();
        Product product = Product.dao.findById(product_id);
        List<ProductAttr> attrs = ProductAttr.dao.getAvailableAttr();

        List<ProductDetails> appDetails = ProductDetails.dao.findbyProduct(product_id);
        Details details = new Details();

        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        setAttr(Const.FIELD_PRODUCT, product);
        setAttr("attrs", attrs);
        setAttr("details", details);
        setAttr("appDetails", appDetails);
        render("item.ftl");
    }

    @Before(GET.class)
    public void edit() {
        //属性详情id
        String detail_id = getPara(0, "0");
        List<ProductAttr> attrs = ProductAttr.dao.getAvailableAttr();
        Details details = Details.dao.findById(detail_id);
        final int product_id = details.getNumber(Const.FIELD_PRODUCT).intValue();
        Product product = Product.dao.findById(product_id);
        List<ProductDetails> appDetails = ProductDetails.dao.findbyProduct(product_id);

        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("attrs", attrs);
        setAttr(Const.FIELD_PRODUCT, product);
        setAttr("details", details);
        setAttr("appDetails", appDetails);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
//        AppUser<User> loginUser = Securitys.getLogin();
        int product;
        Details details = getModel(Details.class);
        int attribute = details.getNumber("attribute").intValue();
        ProductAttr attr = ProductAttr.dao.findById(attribute);
        if (StringUtils.equals(attr.getStr("data_type"), "images")) {
            List<Attachment> attachments = Attachment.dao.findByPks(details.getStr("attribute_val"));
            final List<String> urls = Lists.transform(attachments, new Function<Attachment, String>() {
                @Override
                public String apply(Attachment input) {
                    return input.getStr("url");
                }
            });
            details.set("attribute_val2", Func.COMMA_JOINER.join(urls));

        } else if( StringUtils.equals(attr.getStr("data_type"),"richtxt")){
            details.set("attribute_val",details.getStr("attribute_val"));
        }
        if (DaoKit.isNew(details)) {
            details.save();
        } else {
            details.update();
        }
        product = details.getNumber(Const.FIELD_PRODUCT).intValue();
        redirect("/product/detail/" + product);
    }

    public void delete() {
        int id = getParaToInt(0, 0);
        boolean status = Details.dao.deleteById(id);
        if (status) {
            renderAjaxSuccess();
        } else {
            renderAjaxFailure();
        }
    }

    /**
     * 设置产品属性启用禁用状态
     */
    public void setStatus() {

        String id = getPara("id", "0");
        boolean set_val = StringUtils.equals(getPara("set_val"), Const.YES);
        boolean updateStatus = Details.dao.setStatus(set_val, id);
        if (updateStatus) {
            renderAjaxSuccess();
        } else {
            renderAjaxFailure();
        }
    }
}

package app.controllers.product;

import app.Const;
import app.models.app.ZoneContent;
import app.models.product.Group;
import com.google.common.base.Strings;
import com.jfinal.kit.PathKit;
import goja.Logger;
import goja.StringPool;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * The url product/content Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ContentController extends Controller {

    public static final String FORM_ATTR = "c";

    /**
     * The index route.
     * the view in index.ftl
     */
    public void index() {
        int zone = getParaToInt();
        setAttr("zone", zone);
    }


    public void dtlist() {
        final DTCriterias criterias = getCriterias();
        criterias.setParam("zone", getPara());
        renderDataTables(criterias, ZoneContent.class);
    }


    public void create() {
        setAttr(Const.ACTION_ATTR, Const.CREATE_ACTION);
        ZoneContent content = new ZoneContent();
        content.set("zone", getParaToInt());
        setAttr(FORM_ATTR, content);
        render("./item.ftl");
    }

    public void edit() {
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        ZoneContent content = ZoneContent.dao.findById(getPara());
        setAttr(FORM_ATTR, content);
        render("item.ftl");
    }

    public void save() {
        final ZoneContent content = getModel(ZoneContent.class, FORM_ATTR);
        content.set("upload_time", DateTime.now().toDate());

        if (!Strings.isNullOrEmpty(getPara("attachment"))) {
            final String attachment_url = getPara("attachment_url");

            FileInputStream fis = null;
            try {
                fis = new FileInputStream(new File(PathKit.getWebRootPath() + File.separator + attachment_url.replace(StringPool.SLASH, File.separator)));
                BufferedImage buff = ImageIO.read(fis);

                content.set("image_height", buff.getHeight());
                content.set("image_width", buff.getWidth());
            } catch (FileNotFoundException e) {
                Logger.error("The upload file not not found!");
            } catch (IOException e) {
                Logger.error("get upload file width and height has error!");
            }finally {
                IOUtils.closeQuietly(fis);
            }
            content.set("image_url", attachment_url);
            content.set("image_attachment", getPara("attachment"));
        }

        if (DaoKit.isNew(content)) {
            content.set("enable_flag", true);
            content.save();
        } else {
            content.update();
        }

        final int zone_id = content.getNumber("zone").intValue();
        List<ZoneContent> zoneContents = ZoneContent.dao.findByGroup(zone_id);
        Group zone = new Group();
        zone.set(StringPool.PK_COLUMN, zone_id);
        zone.set("index_imgs", zoneContents.size());
        zone.update();

        redirect("/product/content/" + zone_id);
    }


    public void delete() {
        if (ZoneContent.dao.deleteById(getPara())) {
            renderAjaxSuccess();
        } else {
            renderAjaxFailure();
        }
    }
}
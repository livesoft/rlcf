package app.controllers.product;

import app.Const;
import app.models.product.Accept;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import goja.mvc.Controller;

/**
 * <p>
 * The url product/accept Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class AcceptController extends Controller {

    /**
     * The index route.
     * the url /product/accept
     * the view in index.ftl
     */
    public void index() {
        int productId = getParaToInt(0, 0);
        Accept accept = Accept.dao.findById(productId);
        if (accept == null) {
            accept = new Accept();
            accept.set(Const.FIELD_PRODUCT, productId);
            accept.set("icon_attachment", 0);
            accept.save();
        }
        setAttr(accept);
    }


    public void save() {
        Accept accept = getModel(Accept.class);
        final String logo_attachment = getPara("logo");
        if (!Strings.isNullOrEmpty(logo_attachment)) {
            accept.set("icon", getPara("logo_url"));
            accept.set("icon_attachment", Ints.tryParse(logo_attachment));
        }

        accept.update();
        redirect("/product");
    }
}
package app.controllers;

import app.Const;
import app.models.basic.ClearingRule;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;

/**
 * <p>
 * 产品默认结算规则
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class ClearingRuleController extends Controller {

    public void index(){
    }

    public void dtlist(){
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, ClearingRule.class);
    }

    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        ClearingRule rule = ClearingRule.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("rule", rule);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        ClearingRule rule = getModel(ClearingRule.class,"rule");

        if (Strings.isNullOrEmpty(rule.getStr("id"))) {
            rule.save();
        } else {
            rule.update();
        }
        redirect("/clearingRule");
    }
}

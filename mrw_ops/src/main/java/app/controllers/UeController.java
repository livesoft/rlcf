package app.controllers;

import goja.mvc.Controller;
import goja.rapid.ueditor.UEHandler;

/**
 * <p>
 * The url ue Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class UeController extends Controller {
    /**
     * The index route.
     * the url /ue
     * the view in index.ftl.html
     */
    public void index() {

        UEHandler.build(this).exec();
    }
}
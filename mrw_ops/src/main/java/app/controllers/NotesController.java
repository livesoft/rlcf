package app.controllers;

import app.Const;
import app.models.basic.Notes;
import app.models.sys.User;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.rapid.datatables.DTCriterias;
import goja.rapid.db.DaoKit;
import goja.security.shiro.AppUser;
import goja.security.shiro.Securitys;

/**
 * <p>
 * 备注信息(各种协议)
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class NotesController extends Controller {

    /**
     * The index route.
     * the url /notice
     * the view in index.ftl.html
     */
    @Before(GET.class)
    public void index() {
    }

    /**
     * 列表访问
     */
    @Before(GET.class)
    public void dtlist() {
        final DTCriterias criterias = DTCriterias.criteriasWithRequest(getRequest());
        renderDataTables(criterias, Notes.class);
    }


    @Before(GET.class)
    public void edit() {
        String id = getPara(0, "0");
        Notes notes = Notes.dao.findById(id);
        setAttr(Const.ACTION_ATTR, Const.EDIT_ACTION);
        setAttr("notes",notes);
        render("item.ftl");
    }

    @Before(POST.class)
    public void save() {
        AppUser<User> loginUser = Securitys.getLogin();
        Notes notes = getModel(Notes.class,"notes");

        if (DaoKit.isNew(notes)) {
            notes.save();
        } else {
            notes.update();
        }
        redirect("/notes");
    }

}

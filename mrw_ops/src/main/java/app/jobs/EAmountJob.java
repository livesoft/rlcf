package app.jobs;

import app.services.CheckAccountService;
import goja.Logger;
import goja.annotation.On;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * <p> </p>
 * 每天的23点40分开始，每隔10分钟跑一次，确保金额
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 40/10 23 * * ?")
public class EAmountJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Logger.info("Started merchant account is started...");
        CheckAccountService.me.checkAmount();
        Logger.info("Ended merchant account is started...");
    }
}

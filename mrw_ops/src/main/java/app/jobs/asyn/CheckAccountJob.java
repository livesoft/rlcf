package app.jobs.asyn;

import app.services.CheckAccountService;
import goja.Logger;
import goja.job.Job;
import org.joda.time.DateTime;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class CheckAccountJob extends Job {

    @Override
    public void doJob() throws Exception {
        super.doJob();
        Logger.info("started now checkaccount ...");
        DateTime start_time, end_time;

        //  1. 生成对账时间

        final DateTime now = DateTime.now();
        start_time = now.plusDays(-7).millisOfDay().withMinimumValue();
        end_time = now.millisOfDay().withMaximumValue();

        //  3. 查询银行对账信息并写入数据
        CheckAccountService.me.checkAccount(start_time, end_time);
        Logger.info("ended now checkaccount ...");
    }
}

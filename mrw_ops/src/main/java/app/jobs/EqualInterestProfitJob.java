//package app.jobs;
//
//import app.Const;
//import app.constant.DictConstant;
//import app.kit.TypeKit;
//import app.models.member.MemberProduct;
//import app.models.member.ProfitDetails;
//import app.models.order.TradeMoney;
//import app.models.product.ProductYields;
//import app.services.profit.ProfitDto;
//import app.services.profit.ProfitService;
//import com.google.common.base.Optional;
//import com.jfinal.plugin.activerecord.Db;
//import com.jfinal.plugin.activerecord.IAtom;
//import goja.Logger;
//import goja.annotation.On;
//import goja.lang.Lang;
//import org.joda.time.DateTime;
//import org.joda.time.Months;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.util.List;
//
//import static app.Const.FIELD_AMOUNT;
//import static app.Const.FIELD_MEMBER;
//import static app.Const.FIELD_PRODUCT;
//import static app.Const.FIELD_YIELD;
//import static app.Const.HUNDRED;
//import static goja.StringPool.PK_COLUMN;
//
///**
// * <p> </p>
// * <p/>
// * 0 10 3 * * ? : 每天的凌晨3点10分进行计算
// *
// * @author sogYF
// * @version 1.0
// * @since JDK 1.6
// */
//@On("0 10 3 * * ?")
//public class EqualInterestProfitJob implements Job {
//
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//        int page = 1;
//
//        boolean run = true;
//
//        int year, day, hour,
//                clearing_time,
//                product, memberId, memberProductId,
//                buyTimeYear, buyTimeMonth, buyTimeDay;
//        List<MemberProduct> memberProducts;
//        float yield;
//        while (run) {
//            DateTime now = DateTime.now();
//            year = now.getYear();
//            day = now.getDayOfMonth();
//            hour = now.getHourOfDay();
//            // 1. 查询会员的持有中的理财产品
//            memberProducts = MemberProduct.dao.findByMaturityYield(page, DictConstant.PROFIT_MONTH);
//
//            run = !Lang.isEmpty(memberProducts);
//
//            BigDecimal amount;
//            if (run) {
//                // 2. 计算收益信息
//
//                for (final MemberProduct memberProduct : memberProducts) {
//                    memberId = TypeKit.getInt(memberProduct, FIELD_MEMBER);
//                    // 购买日期
//                    buyTimeYear = TypeKit.getInt(memberProduct, "buy_time_year");
//                    buyTimeMonth = TypeKit.getInt(memberProduct, "buy_time_month");
//                    buyTimeDay = TypeKit.getInt(memberProduct, "buy_time_day");
//
//                    //  购买本金
//                    amount = memberProduct.getBigDecimal(FIELD_AMOUNT);
//                    // 产品信息
//                    product = TypeKit.getInt(memberProduct, FIELD_PRODUCT);
//
//                    clearing_time = TypeKit.getInt(memberProduct, "clearing_time");
//
//                    // 等额本息，通过从购买日的下一个月开始进行收益打款计算,并根据规则设定，是否现在打款
//                    if (year >= buyTimeYear && now.getMonthOfYear() > buyTimeMonth && day == buyTimeDay && hour == clearing_time) {
//                        // 已经到期，并符合规定的计算时间，那么就开始计算收益
//                        // 查询最后的收益率
//                        ProductYields yields = ProductYields.dao.findByLastUpdate(product);
//                        yield = yields.getBigDecimal(FIELD_YIELD).divide(HUNDRED, 4, BigDecimal.ROUND_HALF_UP).floatValue();
//                        memberProductId = memberProduct.getNumber(PK_COLUMN).intValue();
//                        // 计算当前期数
//                        final int installments = Months.monthsBetween(new DateTime(buyTimeYear, buyTimeMonth, buyTimeDay, 0, 0), now).getMonths() - 1;
//                        Optional<ProfitDto> profit_opt = ProfitService.me.averageCapitalInterest(amount.doubleValue(), memberProduct.getNumber("term").intValue(), yield / 12, installments);
//
//                        if (profit_opt.isPresent()) {
//                            // 3. 收益进入打款表
//                            ProfitDto profitDto = profit_opt.get();
//                            final TradeMoney tradeMoney = TradeMoney.monthProfitMoney(memberId, memberProduct.getStr("membercode"), product, memberProductId, profitDto.getPrincipal(), profitDto.getInterest());
//                            tradeMoney.set(Const.FIELD_TYPE, TradeMoney.MONTH_TYPE);
//
//                            final ProfitDetails profitDetails = ProfitDetails.dao.averageCapitalInterest(memberId, product, memberProductId, profitDto.getInterest(), now, amount, 0, 1);
//                            boolean ok = Db.tx(new IAtom() {
//                                @Override
//                                public boolean run() throws SQLException {
//                                    return tradeMoney.save() && memberProduct.save() && profitDetails.save();
//                                }
//                            });
//                            if (ok) {
//                                Logger.info("写入打款纪录成功，后续会进行打款确认操作！");
//                            } else {
//                                Logger.error("等额本息收益计算打款纪录操作失败 {'memberProduct':{},'product':{}, 'member': {} }", memberProductId, product, memberId);
//                            }
//                        } else {
//                            Logger.error("等额本息收益计算出错 {'memberProduct':{},'product':{}, 'member': {} }", memberProductId, product, memberId);
//                        }
//
//                    }
//
//                }
//
//            }
//
//
//            page++;
//        }
//    }
//}

package app.jobs;

import app.services.ProductService;
import goja.Logger;
import goja.annotation.On;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 0/30 * * * ?")
public final class ProductBoardStatJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        Logger.info("=================== Started Product board stat Job ===================");

        ProductService.me.boardstat();

        Logger.info("=================== Ended Product board stat Job ===================");
    }
}

//package app.jobs;
//
//import app.constant.DictConstant;
//import app.models.member.MemberProduct;
//import app.services.profit.ProfitService;
//import goja.Logger;
//import goja.annotation.On;
//import goja.lang.Lang;
//import org.joda.time.DateTime;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//
//import java.util.List;
//
///**
// * <p>
// * 一次性还本付息,收益计算
// * </p>
// * 0 1 * * * ? , 每小时跑一次
// *
// * @author sogYF
// * @version 1.0
// * @since JDK 1.6
// */
//@On("0 59 23 * * ?")
//public class OnetimeProfitJob  implements Job {
//
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//
//        Logger.info("started calculating yield to maturity...");
//
//        int page = 1;
//
//        boolean run = true;
//
//        int year, month, day, hour;
//        List<MemberProduct> memberProducts;
//        DateTime now = DateTime.now();
//        year = now.getYear();
//        month = now.getMonthOfYear();
//        day = now.getDayOfMonth();
//        hour = now.getHourOfDay();
//        while (run) {
//            // 1. 查询会员的未结束的理财产品
//            memberProducts = MemberProduct.dao.findByMaturityYield(page, DictConstant.PROFIT_ONCE);
//
//            run = !Lang.isEmpty(memberProducts);
//
//            if (run) {
//                // 2. 计算收益信息
//                for (MemberProduct memberProduct : memberProducts) {
//
//                    ProfitService.me.onceTimeCollection(memberProduct, year, month, day);
//                }
//
//            }
//
//
//            page++;
//        }
//        Logger.info("ended calculating yield to maturity...");
//    }
//}

package app.jobs;

import app.services.TradeService;
import goja.Logger;
import goja.annotation.On;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * <p> 错误订单错误</p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 0/30 * * * ?")
public class FailPayJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Logger.info("Started fail pay order....");
        TradeService.me.scanOrder();
        Logger.info("Ended fail pay order....");
    }
}

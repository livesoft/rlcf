//package app.jobs;
//
//import app.Const;
//import app.constant.OrderConstant;
//import app.constant.TransferConstant;
//import app.kit.TypeKit;
//import app.models.member.MemberProduct;
//import app.models.order.Order;
//import app.models.transfer.TransferProduct;
//import app.models.transfer.TransferRule;
//import com.jfinal.plugin.activerecord.Db;
//import com.jfinal.plugin.activerecord.IAtom;
//import goja.Logger;
//import goja.StringPool;
//import goja.annotation.On;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//
//import java.sql.SQLException;
//import java.util.List;
//
//import static app.Const.FIELD_ORDER_ID;
//import static app.constant.MemberConstant.PRODUCT_SALES_SUCCESS;
//import static app.constant.MemberConstant.TRANSFER_TRADE_FAIL;
//import static app.models.member.MemberProduct.FIELD_PRODUCT_STATUS;
//import static app.models.member.MemberProduct.FIELD_TRANSFER_STATUS;
//
///**
// * <p> 转让JOB，挂单时间24小时候未被转让的</p>
// * <p/>
// * 每天晚上12点59开始运行
// *
// * @author sogYF
// * @version 1.0
// * @since JDK 1.6
// */
//@On("0 59 23 * * ?")
//public class TransferJob implements Job {
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//        Logger.info("======================转让产品JOB执行开始==================");
//        // 取得转让规则表
//        TransferRule rule = TransferRule.dao.findDefault();
//        // 转让时效
//        int aging = TypeKit.getInt(rule, "aging");
//        // 小于0 则表示无限挂单
//        if (aging > 0) {
//            //所有交易中的并且挂单时间超过指定时效小时
//            List<TransferProduct> overdue = TransferProduct.dao.findOverdue(aging);
//
//            for (final TransferProduct product : overdue) {
//                //修改转让产品状态
//                product.set(Const.FIELD_STATUS, TransferConstant.STATUE_FAIL);
//
//                //修改我的产品状态
//                int member_product = product.getNumber("member_product").intValue();
//                final MemberProduct memberProduct = new MemberProduct();
//                memberProduct.set(StringPool.PK_COLUMN, member_product);
//                //转让状态
//                memberProduct.set(FIELD_TRANSFER_STATUS, TRANSFER_TRADE_FAIL);
//                //产品状态
//                memberProduct.set(FIELD_PRODUCT_STATUS, PRODUCT_SALES_SUCCESS);
//
//                //修改订单状态
//                final Order order = Order.dao.findById(product.getNumber(FIELD_ORDER_ID).intValue());
//                order.set(Const.FIELD_STATUS, OrderConstant.TRADE_CLOSE);
//
//                Db.tx(new IAtom() {
//                    @Override
//                    public boolean run() throws SQLException {
//                        return product.update() && memberProduct.update() && order.update();
//                    }
//                });
//            }
//        }
//
//
//        Logger.info("======================转让产品JOB执行结束==================");
//    }
//}

//package app.jobs;
//
//import app.Const;
//import app.constant.DictConstant;
//import app.models.member.MemberProduct;
//import app.models.member.ProfitDetails;
//import app.models.product.ProductYields;
//import app.services.profit.ProfitService;
//import goja.StringPool;
//import goja.annotation.On;
//import goja.lang.Lang;
//import org.joda.time.DateTime;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//
//import java.math.BigDecimal;
//import java.util.List;
//
///**
// * <p> 本息滚动投资计算 </p>
// * <p/>
// * <p>0 1 * * * ? 每小时执行一次</p>
// *
// * @author sogYF
// * @version 1.0
// * @since JDK 1.6
// */
//@On("0 1 * * * ?")
//public class RollProfitJob implements Job {
//
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//        int page = 1;
//
//        boolean run = true;
//
//        int hour, clearing_time,
//                product, memberId, memberProductId;
//        List<MemberProduct> memberProducts;
//        while (run) {
//            DateTime now = DateTime.now();
//            hour = now.getHourOfDay();
//            // 1. 查询会员的持有中的理财产品
//            memberProducts = MemberProduct.dao.findByMaturityYield(page, DictConstant.PROFIT_ROLL);
//
//            run = !Lang.isEmpty(memberProducts);
//
//            double amount;
//            if (run) {
//                // 2. 计算收益信息
//
//                for (MemberProduct memberProduct : memberProducts) {
//                    memberId = memberProduct.getNumber(Const.FIELD_MEMBER).intValue();
//
//
//                    //  购买本金
//                    amount = memberProduct.getNumber(Const.FIELD_AMOUNT).doubleValue();
//                    // 产品信息
//                    product = memberProduct.getNumber(Const.FIELD_PRODUCT).intValue();
//
//                    clearing_time = memberProduct.getNumber("clearing_time").intValue();
//
//                    // 等额本息，通过从购买日的下一个月开始进行收益打款计算,并根据规则设定，是否现在打款
//                    if (hour == clearing_time) {
//                        // 已经到期，并符合规定的计算时间，那么就开始计算收益
//                        // 查询最后的万份收益
//                        ProductYields yields = ProductYields.dao.findByLastUpdate(product);
//                        memberProductId = memberProduct.getNumber(StringPool.PK_COLUMN).intValue();
//                        // 计算收益
//                        final double million_income = ProfitService.me.rollProfit(amount, yields.getNumber("million_income").floatValue());
//                        // 本息滚动投资也就是表示 不需要进行打款，只需要计算收益重新设置值就可以了
//                        final BigDecimal new_amount = memberProduct.getBigDecimal(Const.FIELD_AMOUNT).add(new BigDecimal(million_income));
//
//
//                        ProfitDetails.dao.rollProfit(memberId, product, memberProductId, new BigDecimal(million_income), now);
//
//                    }
//
//                }
//
//            }
//
//
//            page++;
//        }
//    }
//}
//

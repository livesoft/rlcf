package app.jobs;

import app.Const;
import app.models.member.Points;
import app.models.order.TradeMoney;
import app.services.PlaymoneyService;
import com.google.common.base.Optional;
import goja.Logger;
import goja.annotation.On;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.List;

import static app.models.order.TradeMoney.OK_STATUS;
import static app.models.order.TradeMoney.dao;

/**
 * <p> </p>
 * 0 30 21 * * ? 每天凌晨21点30分开始进行打款服务
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 30 21 * * ?")
public class PlaymoneyJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        Logger.info("Started to fight money JOB...");
        // 取得购买消费积分规则
        Optional<Points> optional_points = Points.dao.findByCode(Const.POINTS_CODE.COST);
        // 1. 查询所有领导审批的打款的服务,取得所有已经确认后的打款纪录
        List<TradeMoney> tradeMoneys = dao.findByStatus(OK_STATUS);

        for (final TradeMoney tradeMoney : tradeMoneys) {
            PlaymoneyService.me.playmoney(tradeMoney,optional_points);
        }
        Logger.info("Ended to fight money JOB...");
    }

}

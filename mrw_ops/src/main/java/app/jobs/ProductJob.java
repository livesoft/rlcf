package app.jobs;

import app.services.ProductService;
import goja.Logger;
import goja.annotation.On;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * <p> 产品JOB，到期，募集失败等状态处理</p>
 * <p/>
 * 每天晚上12点59开始运行,
 * <p/>
 * 1. 判断产品是否认购期到期，并开始起息；
 * 2. 判断产品是否到期
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 55,59 23 * * ?")
public class ProductJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        Logger.info("=================== Started Product Status Job ===================");
        // 产品失败的条件
        // 1. 时间到期
        // 2. 募集资金没有完成
        // 3. 收益模式为到期一次性支付和每月等额本息
        // 4. 产品状态为上线的产品
        // 产品失败后，需要将会员产品数据放入打款表中，进行结算确定。


        DateTime now = DateTime.now();
//        // 是否到了23.59
//        boolean isDayLastMinute = now.getHourOfDay() == 23 && now.getMinuteOfHour() == 59;
//        if (isDayLastMinute) {

        // 已经超过销售结束时间的产品数据，并收益模式为到期一次性支付和每月等额本息的数据。
        ProductService.me.statusCheck(now);
        Logger.info("=================== Ended Product Status Job ===================");
    }
}

package app.jobs;

import app.services.CheckAccountService;
import goja.annotation.On;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@On("0 1 * * * ?")
public class CheckAccountJob implements Job {


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        DateTime start_time, end_time;

        //  1. 生成对账时间

        final DateTime now = DateTime.now();
        start_time = now.plusHours(-1);
        end_time = now;

        //  3. 查询银行对账信息并写入数据
        CheckAccountService.me.checkAccount(start_time, end_time);

    }
}

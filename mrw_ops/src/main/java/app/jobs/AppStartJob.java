package app.jobs;

import app.kits.ReportTplPathKit;
import goja.mvc.AppLoadEvent;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class AppStartJob implements AppLoadEvent {
    @Override
    public void load() {

        ReportTplPathKit.init();
    }
}

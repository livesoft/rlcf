package app.services;

import com.jfinal.handler.Handler;
import goja.annotation.HandlerBind;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@HandlerBind
public class SessionHandler extends Handler {
    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        int index = StringUtils.lastIndexOfIgnoreCase(target, ";jsessionid");
        target = index == -1 ? target : target.substring(0, index);
        nextHandler.handle(target, request, response, isHandled);
    }
}

package app.services;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import goja.rapid.storage.FileDto;

import java.io.File;

/**
 * <p>文件附件上传逻辑 </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public enum AttachmentMode {


    RES {
        @Override
        public Optional<FileDto> storage(File f) {
            return StorageService.RES_STORAGE.save(f);
        }

        @Override
        public void delete(String path) {
            StorageService.RES_STORAGE.delete(path);
        }
    },

    NORMAL {
        @Override
        public Optional<FileDto> storage(File f) {
            return StorageService.NORMAL_STORAGE.save(f);
        }

        @Override
        public void delete(String path) {
            StorageService.NORMAL_STORAGE.delete(path);
        }
    },

    LOGO {
        @Override
        public Optional<FileDto> storage(File f) {
            return StorageService.LOGO_STORAGE.save(f);
        }

        @Override
        public void delete(String path) {
            StorageService.LOGO_STORAGE.delete(path);
        }
    };


    public static AttachmentMode toModel(String filemode) {
        return Strings.isNullOrEmpty(filemode)
                ? AttachmentMode.NORMAL : AttachmentMode.valueOf(filemode.toUpperCase());
    }

    /**
     * 存储文件
     *
     * @param f 文件
     * @return 文件对象DTO ，如果没有存储成功，使用 {@link com.google.common.base.Optional#isPresent()}来进行判断
     */
    public abstract Optional<FileDto> storage(File f);

    /**
     * 删除指定路径的文件
     *
     * @param path 文件路径
     */
    public abstract void delete(String path);
}

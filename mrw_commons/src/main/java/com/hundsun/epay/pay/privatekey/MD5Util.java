package com.hundsun.epay.pay.privatekey;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

    public MD5Util() {
    }

    public static String getEncryptedPwd(String password)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte pwd[] = (byte[]) null;
        String saltConst = "c5c1d57485c0";
        byte salt[] = new byte[SALT_LENGTH.intValue()];
        salt = saltConst.getBytes("UTF8");
        MessageDigest md = null;
        md = MessageDigest.getInstance("MD5");
        md.update(salt);
        md.update(password.getBytes("UTF-8"));
        byte digest[] = md.digest();
        pwd = new byte[digest.length + SALT_LENGTH.intValue()];
        System.arraycopy(salt, 0, pwd, 0, SALT_LENGTH.intValue());
        System.arraycopy(digest, 0, pwd, SALT_LENGTH.intValue(), digest.length);
        return byteToHexString(pwd);
    }

    public static String byteToHexString(byte b[]) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
            String hex = Integer.toHexString(b[i] & 255);
            if (hex.length() == 1)
                hex = (new StringBuilder(String.valueOf('0'))).append(hex).toString();
            hexString.append(hex.toUpperCase());
        }

        return hexString.toString();
    }


    private static final Integer SALT_LENGTH   = Integer.valueOf(12);
    private static final String  PASSWORD_SALT = "c5c1d57485c0";

}

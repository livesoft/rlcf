package com.hundsun.epay.pay;

import com.jfinal.kit.PathKit;
import goja.GojaConfig;
import goja.StringPool;

import java.io.File;


public class PropertiesUtil {

    public static String KEYPAIR_PATH;

    public static String merchantPersonalKey;
    public static String merchantPrivateKeyFile;
    public static String merchantPrivateKeyPassword;
    public static String jschinaPrivateKeyFile     = StringPool.EMPTY;
    public static String jschinaPrivateKeyPassword = StringPool.EMPTY;
    public static String jschinaPublicKeyFile;


    static {
        if(GojaConfig.getPropertyToBoolean("app.test", false)){

            KEYPAIR_PATH = PathKit.getRootClassPath() + File.separator + "keypair" + File.separator + "dev" + File.separator;
            merchantPersonalKey = GojaConfig.getProperty("bank.merchant.personalKey");
            merchantPrivateKeyFile = KEYPAIR_PATH + "rongli.pfx";
            merchantPrivateKeyPassword = GojaConfig.getProperty("bank.merchant.pwd");
            jschinaPublicKeyFile = KEYPAIR_PATH + "rongli.cer";
        } else {

            KEYPAIR_PATH = PathKit.getRootClassPath() + File.separator + "keypair" + File.separator;
            merchantPersonalKey = GojaConfig.getProperty("bank.merchant.personalKey");
            merchantPrivateKeyFile = KEYPAIR_PATH + "ronglicaifu.com.pfx";
            merchantPrivateKeyPassword = GojaConfig.getProperty("bank.merchant.pwd");
            jschinaPublicKeyFile = KEYPAIR_PATH + "jsbank.cer";
        }

    }

}

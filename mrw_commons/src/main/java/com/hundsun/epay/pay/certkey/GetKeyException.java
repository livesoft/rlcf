package com.hundsun.epay.pay.certkey;

public class GetKeyException extends Exception
{
  static final long serialVersionUID = -2397997919305279067L;
  private String trans;
  private String errCd;
  private String errMsg;

  public GetKeyException()
  {
  }

  public GetKeyException(String trans, String errCd, String errMsg)
  {
    setTrans(trans);
    setErrCd(errCd);
    setErrMsg(errMsg);
  }

  public GetKeyException(String moduleName, String message) {
    this(moduleName, null, message);
  }

  public GetKeyException(String message, Exception e) {
    super(message, e);
  }

  public GetKeyException(String message) {
    super(message);
  }

  public String getErrCd() {
    return this.errCd;
  }

  public void setErrCd(String errCd) {
    this.errCd = errCd;
  }

  public String getErrMsg() {
    return this.errMsg;
  }

  public void setErrMsg(String errMsg) {
    this.errMsg = errMsg;
  }

  public String getTrans() {
    return this.trans;
  }

  public void setTrans(String trans) {
    this.trans = trans;
  }
}
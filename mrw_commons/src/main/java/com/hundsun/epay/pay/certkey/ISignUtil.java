/**
 * @Project: gateway
 * @Title: ISignUtil.java
 * @Package com.hundsun.epay.eps.gateway.util
 * @Description: ISignUtil.java
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-2 下午02:50:27
 * @Copyright: 2014 www.hundsun.com All Rights Reserved.
 * @version V1.0.0.0 
 */
package com.hundsun.epay.pay.certkey;

/**
 * @ClassName ISignUtil
 * @Description ISignUtil
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-2
 */
public interface ISignUtil {
	/** 默认分隔�?*/
	public static final String DEFAULT_SEPARATOR = "|";

	/** 默认编码格式UTF-8 */
	public static final String DEFAULT_CHARSET = "UTF-8";
	/** MD5算法�?*/
	public static final String MD5_ALGORITHM_NAME = "MD5";
	/** RSA算法�?*/
	public static final String RSA_ALGORITHM_NAME = "RSA";
	/** DSA算法�?*/
	public static final String DSA_ALGORITHM_NAME = "DSA";
	/** SHA+RSA算法�?*/
	public static final String SHA_RSA_SIGN_ALGORITHMS = "SHA1WithRSA";
	/** X.509证书 */
	public static final String X509_CERT_NAME = "X.509";
	/** SHA-1算法�?*/
	public static final String DigestAlgorithm = "SHA-1";
	/** 默认缓冲字节1024 */
	public static final int DefaultBufferLength = 1024;
	/** DESede算法�?*/
	public static final String DefaultCipherAlgorithm = "DESede";
	/** DES/CBC/NoPadding算法�?*/
	public static final String DESCipherAlgorighm = "DES/CBC/NoPadding";

}

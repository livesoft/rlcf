package com.hundsun.epay.pay;
/**
 * @Project: SanxiapayEgs
 * @Title: StringUtil.java
 * @Package com.hundsun.epay.egs.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author yujianhong yujianhong@hundsun.com
 * @date 2014-4-4 下午3:19:24
 * @Copyright: 2014 www.hundsun.com All Rights Reserved.
 * @version V1.0.0.0 
 */


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
/**
 * 
 * @ClassName CodeUtil
 * @Description code处理
 * @author Administrator yedm@hundsun.com
 * @date 2014-11-4
 */
@SuppressWarnings("JavaDoc")
public class CodeUtil {
	public static String SIGNTYPE="signType";
	public static String SIGN="sign";
	public static String CONNECTOR="&";
	public static String MULCONNECTOR="&-&";
	public static String ASSIGNMENT="="; 
	
	
	
	

	/**
	 * 
	 * @Method: getJoinStrByParamMap 
	 * @Description: 根据map获得连接字符串，单笔记录的情况
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String getJoinStrByParamMap(Map<String,String> params)throws Exception{
		String result="";
		String[] args=new String[params.size()];
		Set<String> keys=params.keySet();
		Iterator<String> iterator=keys.iterator();
		int i=0;
		while(iterator.hasNext()){
			args[i]=iterator.next();
			i++;
		}
		Arrays.sort(args);
		//重组url
		for(int j=0;j<args.length;j++){
			String key=args[j];
			String value=params.get(key);
			result+=CONNECTOR+key+ASSIGNMENT+value;
		}
		if(result.length()>0){
			result=result.substring(1);
		}
		return result;
	}
	
	
	
	public static String enCode(String s ,String code){
		String eCode="";
		try {
			eCode=URLEncoder.encode(s, code);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return eCode;
	}
	/**
	 * URL转换
	 * @Method: deCode 
	 * @Description: URL转换 
	 * @param @param s
	 * @param @param code
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String deCode(String s ,String code)throws Exception{
		String deCode="";
		try {
			deCode=URLDecoder.decode(s, code);
		}catch(IllegalArgumentException e){
			throw e;
		}catch (UnsupportedEncodingException e) {
			throw e;
		}
		return deCode;
	}
}

/**
 * @Project: gateway
 * @Title: KeyPair.java
 * @Package com.hundsun.epay.eps.gateway.util
 * @Description: KeyPair.java
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-17 上午11:47:42
 * @Copyright: 2014 www.hundsun.com All Rights Reserved.
 * @version V1.0.0.0 
 */
package com.hundsun.epay.pay.certkey;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName KeyPair
 * @Description KeyPair
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-17
 */
@SuppressWarnings("serial")
public class KeyPair implements Serializable {
	/**
	 * 机构编号
	 */
	private String orgNo = null;
	/**
	 * 业务编号
	 */
	private String bizNo = null;
	/**
	 * 私钥证书
	 */
	private PrivateKey privateKey = null;
	/**
	 * 公钥证书
	 */
	private PublicKey publicKey = null;
	/**
	 * 扩展信息
	 */
	private Map<String, Object> extendMap = new HashMap<String, Object>();

	public KeyPair(String orgNo, String bizNo, PrivateKey privateKey,
			PublicKey publicKey) {
		super();
		this.orgNo = orgNo;
		this.bizNo = bizNo;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	public KeyPair() {
		super();
	}

	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public Map<String, Object> getExtendMap() {
		return extendMap;
	}

	public void setExtendMap(Map<String, Object> extendMap) {
		this.extendMap = extendMap;
	}

	@Override
	public String toString() {
		return "KeyPair [orgNo=" + orgNo + ", bizNo=" + bizNo + ", privateKey="
				+ privateKey + ", publicKey=" + publicKey + "]";
	}
	
	/**
	 * 获取扩展信息
	 * @Method: getExtendMapValue 
	 * @Description: 
	 * @param <E>
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <E> E getExtendMapValue(String key) {
		return (E) this.extendMap.get(key);
	}

	/**
	 * 设置扩展信息
	 * @Method: setExtendMapValue 
	 * @Description: 
	 * @param key
	 * @param value
	 */
	public void setExtendMapValue(String key, Object value) {
		this.extendMap.put(key, value);
	}
	
}

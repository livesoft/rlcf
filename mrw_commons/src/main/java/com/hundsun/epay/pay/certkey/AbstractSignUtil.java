/**
 * @Project: gateway
 * @Title: AbstractSignUtil.java
 * @Package com.hundsun.epay.eps.gateway.util
 * @Description: AbstractSignUtil.java
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-2 下午03:29:47
 * @Copyright: 2014 www.hundsun.com All Rights Reserved.
 * @version V1.0.0.0 
 */
package com.hundsun.epay.pay.certkey;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName AbstractSignUtil
 * @Description AbstractSignUtil
 * @author Administrator fansg@hundsun.com
 * @date 2014-4-2
 */
public class AbstractSignUtil implements ISignUtil {

	/**
	 * 私钥证书
	 */
	private PrivateKey privateKey = null;
	/**
	 * 公钥证书
	 */
	private PublicKey publicKey = null;
	/**
	 * 证书对信�?
	 */
	private Map<String, KeyPair> keyPairs = new HashMap<String, KeyPair>();
	
	/** 
	 * @return privateKey 
	 */
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	
	/** 
	 * @param privateKey 要设置的 privateKey 
	 */
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	
	/** 
	 * @return publicKey 
	 */
	public PublicKey getPublicKey() {
		return publicKey;
	}
	
	/** 
	 * @param publicKey 要设置的 publicKey 
	 */
	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	/**
	 * 证书对信�?
	 * @Method: getKeyPairs 
	 * @Description: 
	 * @return
	 */
	public Map<String, KeyPair> getKeyPairs() {
		return keyPairs;
	}

	/**
	 * 证书对信�?
	 * @Method: setKeyPairs 
	 * @Description: 
	 * @param keyPairs
	 */
	public void setKeyPairs(Map<String, KeyPair> keyPairs) {
		this.keyPairs = keyPairs;
	}
	
	/**
	 * 获取证书信息
	 * @Method: getKeyPair 
	 * @Description: 
	 * @param key
	 * @return
	 */
	public KeyPair getKeyPair(String key) {
		return keyPairs.get(key);
	}

	/**
	 * 添加证书信息
	 * @Method: setKeyPair 
	 * @Description: 
	 * @param key
	 * @param keyPair
	 */
	public void setKeyPair(String key, KeyPair keyPair) {
		if (null == keyPair) {
			return;
		}
		if (null == key || key.length() == 0) {
			key = keyPair.getOrgNo() + DEFAULT_SEPARATOR + keyPair.getBizNo();
		}
		this.keyPairs.put(key, keyPair);
	}
	
}

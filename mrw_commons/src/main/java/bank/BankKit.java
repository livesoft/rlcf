package bank;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import goja.StringPool;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class BankKit {

    public static final Splitter URL_AMPERSAND_SPLIT = Splitter.on(StringPool.AMPERSAND).omitEmptyStrings()
            .trimResults();

    public static final Splitter EQUAL_SPLIT = Splitter.on(StringPool.EQUALS).trimResults();

    /**
     * 构建返回参数
     *
     * @param returnContent 返回内容
     * @return 参数
     */
    public static Map<String, String> buildResp(String returnContent) {
        List<String> restuns = URL_AMPERSAND_SPLIT.splitToList(returnContent);
        Map<String, String> results = Maps.newHashMap();
        for (String restun : restuns) {
            if (StringUtils.startsWith(restun, "sign")) {
                continue;
            }
            final List<String> pvalue = EQUAL_SPLIT.splitToList(restun);
            if(pvalue.size() >=2){
                results.put(pvalue.get(0), pvalue.get(1));
            }
        }
        return results;
    }

}

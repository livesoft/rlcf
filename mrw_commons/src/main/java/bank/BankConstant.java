package bank;

import com.google.common.base.Splitter;
import goja.GojaConfig;
import goja.StringPool;
import goja.date.DateFormatter;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface BankConstant {


    String TIME_FORMAT = "HHmmss";


     String MERCHANT_PRODUCT_NO = "0000000000000000";


    DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DateFormatter.YYYYMMDD);


    Map<String, String> BANKSERVICENAME = new HashMap<String, String>() {
        private static final long serialVersionUID = -8784834251583036679L;

        {

            put("checkAccount", "对账文件产生");
            put("transferMerchantAccnt", "账户单笔转账");
            put("getMerchantAccntAmount", "账户余额查询");
            put("refundOrder", "退款/撤销");
            put("getOrderInfo", "订单查询");
            put("withdrawMerchantAccnt", "账户提现");
            put("rechargeMerchantAccnt", "账户充值");
            put("bindUserInfo", "捆绑江苏聚合账户");
            put("unbindUserInfo", "解绑江苏聚合账户");
            put("getBindUserStatus", "查询是否绑定江苏聚合账户");
        }
    };


    /**
     * 接口返回成功标记
     */
    String RS_SUCCESS = "000000";


    String SIGN     = "sign";
    String SIGNTYPE = "signType";


    String RSK_RESPCODE   = "respCode";
    String RSK_RESMSG     = "respMsg";
    String RSK_RESBIZDATE = "respBizDate";


    String BP_CREATE_DATE      = "createDate";
    String BP_CREATE_TIME      = "createTime";
    String BP_BIZ_DATE         = "bizDate";
    String BP_MSG_ID           = "msgID";
    String BP_SVR_CODE         = "svrCode";
    String BP_PARTNER_ID       = "partnerId";
    String BP_CHANNEL_NO       = "channelNo";
    String BP_PUBLIC_KEY_CODE  = "publicKeyCode";
    String BP_FIELD_1          = "field1";
    String BP_FIELD_2          = "field2";
    String BP_FIELD_3          = "field3";
    String BP_VERSION          = "version";
    String BP_CHARSET          = "charset";
    String BP_SERVICE          = "service";
    String BP_PARTNER_BUYER_ID = "partnerBuyerId";


    String VERSION = "v1.0.0";
    String CNY     = "CNY";

    String PARTNERID       = GojaConfig.getProperty("bank.merchant.parterid", "8dbc14c4b1314ca492391e0d94fff0e4");
    String PUBLIC_KEY_CODE = GojaConfig.getProperty("bank.merchant.publickeycode", "00");


    Splitter URLAMPERE_SPLIT = Splitter.on(StringPool.AMPERSAND).trimResults();
}

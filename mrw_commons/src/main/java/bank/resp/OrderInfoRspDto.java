package bank.resp;

import app.Const;
import bank.BankConstant;
import bank.BankKit;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import goja.date.DateFormatter;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class OrderInfoRspDto extends BaseRspDto {

    /**
     * 平台处理时间YYYYMMDDHHMMSS
     */
    private DateTime createTime;

    /**
     * 订单合同信息
     */
    private String orderMemo;
    /**
     * 订单描述
     */
    private String orderDesc;

    /**
     * 外部订单号varchar(32)
     */
    private String outerOrderNo;

    /**
     * 内部订单号varchar(32)
     */
    private String innerOrderNo;

    /**
     * 会员ID
     */
    private String partnerBuyerId;


    /**
     * 订单类型(1充值、2提现、3转账、4外部订单支付、5退款 、9撤销)
     */
    private int orderType;

    /**
     * 手续费，精确到小数点后面两位
     * number(16,2)，例如10元为：10.00
     */
    private BigDecimal fee;

    /**
     * 订单金额，精确到小数点后面两位
     * number(16,2)，例如10元为：10.00
     */
    private BigDecimal amount;

    /**
     * 订单状态：
     * 0-待支付，1-退款处理中，2-处理中（转账类使用），3-交易成功，4-交易关闭，5-删除，6-已退款，7-交易失败，8-交易完成
     */
    private int orderStatus;

    /**
     * 币种，默认CNY
     */
    private String currency;


    public static OrderInfoRspDto create(String rsp) {
        Map<String, String> rspMap = BankKit.buildResp(rsp);
        OrderInfoRspDto orderInfoRspDto = new OrderInfoRspDto();

        orderInfoRspDto.setBaseInfo(rspMap);
        if(orderInfoRspDto.isSuccess()){
            String createTime = rspMap.get("createTime");

            DateTime time = DateTime.parse(createTime, DateTimeFormat.forPattern(DateFormatter.YYYYMMDD + BankConstant.TIME_FORMAT));
            orderInfoRspDto.setCreateTime(time);

            orderInfoRspDto.setOrderDesc(rspMap.get("orderDesc"));
            orderInfoRspDto.setOrderMemo(rspMap.get("orderMemo"));
            orderInfoRspDto.setOuterOrderNo(rspMap.get("outerOrderNo"));
            orderInfoRspDto.setInnerOrderNo(rspMap.get("innerOrderNo"));
            orderInfoRspDto.setPartnerBuyerId(rspMap.get("partnerBuyerId"));
            final String orderType_str = rspMap.get("orderType");
            orderInfoRspDto.setOrderType(Ints.tryParse(orderType_str));
            final String fee_str = rspMap.get("fee");
            final String amount_str = rspMap.get(Const.FIELD_AMOUNT);
            orderInfoRspDto.setAmount(Strings.isNullOrEmpty(amount_str) ? BigDecimal.ZERO : NumberUtils.createBigDecimal(amount_str));
            orderInfoRspDto.setFee(Strings.isNullOrEmpty(fee_str) ? BigDecimal.ZERO : NumberUtils.createBigDecimal(fee_str));

            orderInfoRspDto.setOrderStatus(Ints.tryParse(rspMap.get("orderStatus")));
            orderInfoRspDto.setCurrency(rspMap.get("currency"));
        }

        return orderInfoRspDto;
    }

    public DateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(DateTime createTime) {
        this.createTime = createTime;
    }

    public String getOrderMemo() {
        return orderMemo;
    }

    public void setOrderMemo(String orderMemo) {
        this.orderMemo = orderMemo;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOuterOrderNo() {
        return outerOrderNo;
    }

    public void setOuterOrderNo(String outerOrderNo) {
        this.outerOrderNo = outerOrderNo;
    }

    public String getInnerOrderNo() {
        return innerOrderNo;
    }

    public void setInnerOrderNo(String innerOrderNo) {
        this.innerOrderNo = innerOrderNo;
    }

    public String getPartnerBuyerId() {
        return partnerBuyerId;
    }

    public void setPartnerBuyerId(String partnerBuyerId) {
        this.partnerBuyerId = partnerBuyerId;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("createTime", createTime)
                .add("orderMemo", orderMemo)
                .add("orderDesc", orderDesc)
                .add("outerOrderNo", outerOrderNo)
                .add("innerOrderNo", innerOrderNo)
                .add("partnerBuyerId", partnerBuyerId)
                .add("orderType", orderType)
                .add("fee", fee)
                .add(Const.FIELD_AMOUNT, amount)
                .add("orderStatus", orderStatus)
                .add("currency", currency)
                .add("super", super.toString())
                .toString();
    }
}

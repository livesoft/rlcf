package bank.resp;

import bank.BankKit;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class MerchantAccntRspDto extends BaseRspDto {

    /**
     * 金额，精确到小数点后面两位
     number(16,2)，例如10元为：10.00
     */
    private double amount;

    /**
     * 订单状态：
     0-待支付，1-退款处理中，2-处理中，3-交易成功，4-交易关闭，5-删除，6-已退款，7-交易失败，8-交易完成
     */
    private int orderStatus;

    /**
     * 手续费，暂时默认填0。精确到小数点后面两位number(16,2)，例如10元为：10.00
     */
    private String fee;


    private String outerOrderNo;
    private String innerOrderNo;

    private String currency;


    public static MerchantAccntRspDto create(String resp) {
        final Map<String, String> resMap = BankKit.buildResp(resp);

        MerchantAccntRspDto rechargeMerchantAccntRspDto = new MerchantAccntRspDto();
        String amount_str = resMap.get("amount");
        rechargeMerchantAccntRspDto.setAmount(Strings.isNullOrEmpty(amount_str) ? 0.0 : Doubles.tryParse(amount_str));
        rechargeMerchantAccntRspDto.setFee(resMap.get("fee"));
        rechargeMerchantAccntRspDto.setOrderStatus(Ints.tryParse(MoreObjects.firstNonNull(resMap.get("orderStatus"), "0")));
        rechargeMerchantAccntRspDto.setOuterOrderNo(resMap.get("outerOrderNo"));
        rechargeMerchantAccntRspDto.setInnerOrderNo(resMap.get("innerOrderNo"));
        rechargeMerchantAccntRspDto.setCurrency(resMap.get("currency"));

        rechargeMerchantAccntRspDto.setBaseInfo(resMap);

        return rechargeMerchantAccntRspDto;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getOuterOrderNo() {
        return outerOrderNo;
    }

    public void setOuterOrderNo(String outerOrderNo) {
        this.outerOrderNo = outerOrderNo;
    }

    public String getInnerOrderNo() {
        return innerOrderNo;
    }

    public void setInnerOrderNo(String innerOrderNo) {
        this.innerOrderNo = innerOrderNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

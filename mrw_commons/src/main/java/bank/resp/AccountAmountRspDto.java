package bank.resp;

import bank.BankKit;
import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class AccountAmountRspDto extends BaseRspDto implements Serializable {


    private static final long serialVersionUID = -3918618050618357374L;
    /**
     * 子账户产品代码
     * 1001-活期借记（外部会使用到）
     * 1002-贷记账户
     * 1003-积分账户
     * 1004-储值卡账户
     * 1005-电子钱包账户
     * 9001-内部账户（外部商户会使用到）
     * 2001-存钱罐
     */
    private String     subAcctProductId;
    /**
     * 子账户产品名称
     */
    private String     subAcctProductName;
    /**
     * 总余额,BigDecimal，单位为元
     */
    private BigDecimal totalBalance;
    /**
     * 可用余额, BigDecimal，单位为元
     */
    private BigDecimal availableBalance;
    /**
     * 冻结余额, BigDecimal，单位为元
     */
    private BigDecimal freezeBalance;
    /**
     * 上日余额, BigDecimal，单位为元
     */
    private BigDecimal lastDayBalance;
    /**
     * 币种，默认CNY
     */
    private String     currency;


    public static AccountAmountRspDto create(String rsp) {
        Map<String, String> rspMap = BankKit.buildResp(rsp);
        AccountAmountRspDto accountAmountRspDto = new AccountAmountRspDto();

        accountAmountRspDto.setBaseInfo(rspMap);
        if (accountAmountRspDto.isSuccess()) {
            String subAcctProductId = rspMap.get("subAcctProductId");

            accountAmountRspDto.setSubAcctProductId(subAcctProductId);
            accountAmountRspDto.setSubAcctProductName(rspMap.get("subAcctProductName"));
            accountAmountRspDto.setCurrency(rspMap.get("currency"));
            String str_totalBalance = rspMap.get("totalBalance");
            accountAmountRspDto.setTotalBalance(NumberUtils.createBigDecimal(str_totalBalance));
            String str_availableBalance = rspMap.get("availableBalance");
            accountAmountRspDto.setAvailableBalance(NumberUtils.createBigDecimal(str_availableBalance));
            String str_freezeBalance = rspMap.get("freezeBalance");
            accountAmountRspDto.setFreezeBalance(NumberUtils.createBigDecimal(str_freezeBalance));
            String str_lastDayBalance = rspMap.get("lastDayBalance");
            accountAmountRspDto.setLastDayBalance(NumberUtils.createBigDecimal(str_lastDayBalance));

        }

        return accountAmountRspDto;
    }

    public String getSubAcctProductId() {
        return subAcctProductId;
    }

    public void setSubAcctProductId(String subAcctProductId) {
        this.subAcctProductId = subAcctProductId;
    }

    public String getSubAcctProductName() {
        return subAcctProductName;
    }

    public void setSubAcctProductName(String subAcctProductName) {
        this.subAcctProductName = subAcctProductName;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getFreezeBalance() {
        return freezeBalance;
    }

    public void setFreezeBalance(BigDecimal freezeBalance) {
        this.freezeBalance = freezeBalance;
    }

    public BigDecimal getLastDayBalance() {
        return lastDayBalance;
    }

    public void setLastDayBalance(BigDecimal lastDayBalance) {
        this.lastDayBalance = lastDayBalance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("subAcctProductId", subAcctProductId)
                .add("subAcctProductName", subAcctProductName)
                .add("totalBalance", totalBalance)
                .add("availableBalance", availableBalance)
                .add("freezeBalance", freezeBalance)
                .add("lastDayBalance", lastDayBalance)
                .add("currency", currency)
                .add("super", super.toString())
                .toString();
    }
}

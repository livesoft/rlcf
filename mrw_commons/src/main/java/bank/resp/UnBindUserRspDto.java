package bank.resp;

import bank.BankKit;
import com.google.common.base.MoreObjects;
import com.google.common.primitives.Ints;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class UnBindUserRspDto extends BaseRspDto {

    /**
     * 绑定状态（0未绑定、1绑定、2已解绑）
     */
    private int bindStatus;


    /**
     * 用户在商户网站的唯一标志
     */
    private String partnerBuyerId;


    public static UnBindUserRspDto build(String rsp) {
        Map<String, String> rspMap = BankKit.buildResp(rsp);
        UnBindUserRspDto unBindUserRspDto = new UnBindUserRspDto();
        unBindUserRspDto.setBindStatus(Ints.tryParse(MoreObjects.firstNonNull(rspMap.get("bindStatus"), "-1")));
        unBindUserRspDto.setPartnerBuyerId(rspMap.get("partnerBuyerId"));
        unBindUserRspDto.setBaseInfo(rspMap);
        return unBindUserRspDto;
    }

    public int getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(int bindStatus) {
        this.bindStatus = bindStatus;
    }

    public String getPartnerBuyerId() {
        return partnerBuyerId;
    }

    public void setPartnerBuyerId(String partnerBuyerId) {
        this.partnerBuyerId = partnerBuyerId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("super", super.toString())
                .add("bindStatus", bindStatus)
                .add("partnerBuyerId", partnerBuyerId)
                .toString();
    }
}

package bank.resp;

import bank.BankConstant;
import bank.BankKit;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import goja.StringPool;

import java.util.List;

/**
 * <p> 对账DTO</p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ReconciliationDto {
    /**
     * 总体信息
     */
    private final ReconciliationOverallDto overallDto;

    private final List<ReconciliationItemDto> items;

    private ReconciliationDto(ReconciliationOverallDto overallDto, List<ReconciliationItemDto> items) {
        this.overallDto = overallDto;
        this.items = items;
    }


    public static ReconciliationDto parseResponse(String res) {
        Preconditions.checkNotNull(res, "响应内容不能为空");

        List<String> lists = Splitter.on("&-&").trimResults().splitToList(res);
        int i = 0;
        String first_item = lists.get(i);
        List<ReconciliationItemDto> itemDtos = Lists.newArrayList();
        for (i = 1; i < lists.size(); i++) {

            String line_item = lists.get(i);
            itemDtos.add(ReconciliationItemDto.create(BankConstant.URLAMPERE_SPLIT.withKeyValueSeparator(StringPool.EQUALS).split(line_item)));
        }

        ReconciliationOverallDto overall = ReconciliationOverallDto.create(BankKit.buildResp(first_item));
        return new ReconciliationDto(overall, itemDtos);
    }


    public ReconciliationOverallDto getOverallDto() {
        return overallDto;
    }

    public List<ReconciliationItemDto> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("overallDto", overallDto)
                .add("items", items)
                .toString();
    }
}

package bank.resp;

import bank.BankConstant;
import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public abstract class BaseRspDto {

    /**
     * 合作商户号
     */
    protected String partnerId;


    /**
     * 应答方业务日期
     */
    protected String respBizDate;


    /**
     * 响应码：000000-成功；非000000即为失败
     */
    protected String respCode;

    /**
     * 响应信息
     */
    protected String respMsg;
    /**
     * 签名类型
     */
    protected String signType;
    /**
     * 签名串
     */
    protected String sign;

    /**
     * 是否操作成功
     */
    protected boolean success;

    /**
     * 预留字段1
     */
    protected String field1;

    /**
     * 预留字段2
     */
    protected String field2;

    /**
     * 预留字段3
     */
    protected String field3;


    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getRespBizDate() {
        return respBizDate;
    }

    public void setRespBizDate(String respBizDate) {
        this.respBizDate = respBizDate;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("partnerId", partnerId)
                .add("field1", field1)
                .add("field2", field2)
                .add("field3", field3)
                .add("respBizDate", respBizDate)
                .add("respCode", respCode)
                .add("respMsg", respMsg)
                .add("signType", signType)
                .add("sign", sign)
                .add("success", success)
                .toString();
    }

    public void setBaseInfo(Map<String, String> rspMap) {
        this.setRespMsg(rspMap.get(BankConstant.RSK_RESMSG));
        final String respCode1 = rspMap.get(BankConstant.RSK_RESPCODE);
        this.setRespCode(respCode1);
        this.setSuccess(StringUtils.equals(respCode1, BankConstant.RS_SUCCESS));
        this.setField1(rspMap.get(BankConstant.BP_FIELD_1));
        this.setField2(rspMap.get(BankConstant.BP_FIELD_2));
        this.setField3(rspMap.get(BankConstant.BP_FIELD_2));
        this.setRespBizDate(rspMap.get(BankConstant.RSK_RESBIZDATE));
    }

}

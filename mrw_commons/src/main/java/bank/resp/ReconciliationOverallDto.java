package bank.resp;

import com.google.common.base.MoreObjects;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ReconciliationOverallDto {

    /**
     * 总记录数
     */
    private final int     totalRows;
    /**
     * 每页多少条
     */
    private final int     pageSize;
    /**
     * 当前第几页
     */
    private final int     currentPage;
    /**
     * 总页码数
     */
    private final int     totalPages;
    /**
     * 是否有下一页：0否 1是
     */
    private final boolean hasNext;
    /**
     * 是否有下一页：0否 1是
     */
    private final boolean hasPrevious;
    /**
     * 本页开始的记录位置
     */
    private final long    pageStartRow;

    /**
     * 本页结束的记录位置
     */
    private final long pageEndRow;

    private ReconciliationOverallDto(int totalRows, int pageSize, int currentPage, int totalPages, boolean hasNext, boolean hasPrevious, long pageStartRow, long pageEndRow) {
        this.totalRows = totalRows;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.hasNext = hasNext;
        this.hasPrevious = hasPrevious;
        this.pageStartRow = pageStartRow;
        this.pageEndRow = pageEndRow;
    }


    public static ReconciliationOverallDto create(Map<String, String> bank_return_contents) {
        String totalRows = MoreObjects.firstNonNull(bank_return_contents.get("totalRows"), "0");
        String pageSize = MoreObjects.firstNonNull(bank_return_contents.get("pageSize"), "0");
        String currentPage = MoreObjects.firstNonNull(bank_return_contents.get("currentPage"), "0");
        String totalPages = MoreObjects.firstNonNull(bank_return_contents.get("totalPages"), "0");
        boolean hasNext = StringUtils.equals("1", MoreObjects.firstNonNull(bank_return_contents.get("hasNext"), "0"));
        boolean hasPrevious = StringUtils.equals("1", MoreObjects.firstNonNull(bank_return_contents.get("hasPrevious"), "0"));
        String pageStartRow = MoreObjects.firstNonNull(bank_return_contents.get("pageStartRow"), "0");
        String pageEndRow = MoreObjects.firstNonNull(bank_return_contents.get("pageEndRow"), "0");

        return new ReconciliationOverallDto(Ints.tryParse(totalRows), Ints.tryParse(pageSize),
                Ints.tryParse(currentPage), Ints.tryParse(totalPages),
                hasNext, hasPrevious,
                Longs.tryParse(pageStartRow), Longs.tryParse(pageEndRow));
    }

    public int getTotalRows() {
        return totalRows;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public boolean isHasPrevious() {
        return hasPrevious;
    }

    public long getPageStartRow() {
        return pageStartRow;
    }

    public long getPageEndRow() {
        return pageEndRow;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalRows", totalRows)
                .add("pageSize", pageSize)
                .add("currentPage", currentPage)
                .add("totalPages", totalPages)
                .add("hasNext", hasNext)
                .add("hasPrevious", hasPrevious)
                .add("pageStartRow", pageStartRow)
                .add("pageEndRow", pageEndRow)
                .toString();
    }
}

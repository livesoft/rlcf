package bank.resp;

import app.Const;
import com.google.common.base.MoreObjects;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ReconciliationItemDto {

    /**
     * 平台交易日期
     * YYYYMMDD
     */
    private final String transDate;
    /**
     * 请求方业务日期YYYYMMDD
     */
    private final String reqbizDate;

    /**
     * 外部订单号（商城订单号）varchar(32)
     */
    private final String outerOrderNo;
    /**
     * 内部订单号（平台订单号）varchar(32)
     */
    private final String innerOrderNo;

    /**
     * 交易状态
     * 0-待支付，1-申请代付，2-处理中，3-交易成功，4-交易关闭，5-删除，6-已退款
     */
    private final int orderStatus;

    /**
     * 订单类型(0AA订单、1充值、2提现、3转账、4外部订单支付、5退款 、6、其他订单 7积分购买、8缴费、9撤销)
     */
    private final String orderType;


    /**
     * 金额，精确到小数点后面两位
     * number(16,2)，例如10元为：10.00
     */
    private final BigDecimal amount;


    /**
     * 手续费，精确到小数点后面两位
     * number(16,2)，例如10元为：10.00
     */
    private final BigDecimal fee;

    /**
     * 币种，默认CNY
     */
    private final String currency;

    public ReconciliationItemDto(String transDate, String reqbizDate, String outerOrderNo, String innerOrderNo,
                                 int orderStatus, String orderType, BigDecimal amount, BigDecimal fee, String currency) {
        this.transDate = transDate;
        this.reqbizDate = reqbizDate;
        this.outerOrderNo = outerOrderNo;
        this.innerOrderNo = innerOrderNo;
        this.orderStatus = orderStatus;
        this.orderType = orderType;
        this.amount = amount;
        this.fee = fee;
        this.currency = currency;
    }

    public static ReconciliationItemDto create(Map<String, String> bank_return_contents) {

        String transDate = bank_return_contents.get("transDate");
        String reqbizDate = bank_return_contents.get("reqbizDate");
        String outerOrderNo = bank_return_contents.get("outerOrderNo");
        String innerOrderNo = bank_return_contents.get("innerOrderNo");
        String orderType = bank_return_contents.get("orderType");
        String currency = bank_return_contents.get("currency");
        String orderStatus = MoreObjects.firstNonNull(bank_return_contents.get("orderStatus"), "0");
        String amount = bank_return_contents.get(Const.FIELD_AMOUNT);
        String fee = bank_return_contents.get("fee");
        return new ReconciliationItemDto(transDate, reqbizDate, outerOrderNo, innerOrderNo, Ints.tryParse(orderStatus)
                , orderType,
                NumberUtils.createBigDecimal(amount),
                NumberUtils.createBigDecimal(fee),
                currency);
    }

    public String getTransDate() {
        return transDate;
    }

    public String getReqbizDate() {
        return reqbizDate;
    }

    public String getOuterOrderNo() {
        return outerOrderNo;
    }

    public String getInnerOrderNo() {
        return innerOrderNo;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getFee() {
        return fee;
    }


    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("transDate", transDate)
                .add("reqbizDate", reqbizDate)
                .add("outerOrderNo", outerOrderNo)
                .add("innerOrderNo", innerOrderNo)
                .add("orderStatus", orderStatus)
                .add("orderType", orderType)
                .add(Const.FIELD_AMOUNT, amount)
                .add("fee", fee)
                .add("currency", currency)
                .toString();
    }
}

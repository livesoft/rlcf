package bank.resp;

import bank.BankConstant;
import bank.BankKit;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import goja.date.DateFormatter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class BindUserStatusRspDto extends BaseRspDto {
    /**
     * 绑定状态（0未绑定、1绑定、2已解绑）
     */
    private int bindStatus;

    /**
     * 绑定/解绑最后修改时间
     * YYYYMMDDHHMMSS
     */
    private DateTime modifyTime;

    /**
     * 用户在商户网站的唯一标志
     */
    private String partnerBuyerId;


    public static BindUserStatusRspDto build(String rsp) {
        Map<String, String> rspMap = BankKit.buildResp(rsp);
        BindUserStatusRspDto bindUserStatusRspDto = new BindUserStatusRspDto();
        bindUserStatusRspDto.setBindStatus(Ints.tryParse(MoreObjects.firstNonNull(rspMap.get("bindStatus"), "-1")));
        bindUserStatusRspDto.setPartnerBuyerId(rspMap.get("partnerBuyerId"));
        String modifyTime = rspMap.get("modifyTime");
        if(!Strings.isNullOrEmpty(modifyTime)){
            DateTime time = DateTime.parse(modifyTime, DateTimeFormat.forPattern(DateFormatter.YYYYMMDD + BankConstant.TIME_FORMAT));
            bindUserStatusRspDto.setModifyTime(time);
        }
        bindUserStatusRspDto.setBaseInfo(rspMap);
        return bindUserStatusRspDto;
    }

    public int getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(int bindStatus) {
        this.bindStatus = bindStatus;
    }

    public DateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(DateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getPartnerBuyerId() {
        return partnerBuyerId;
    }

    public void setPartnerBuyerId(String partnerBuyerId) {
        this.partnerBuyerId = partnerBuyerId;
    }


    @Override
    public String toString() {

        return MoreObjects.toStringHelper(this)
                .add("bindStatus", bindStatus)
                .add("modifyTime", modifyTime)
                .add("partnerBuyerId", partnerBuyerId)
                .add("supper", super.toString())
                .toString();
    }
}

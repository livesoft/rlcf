package bank.resp;

import app.Const;
import app.models.analyze.Interfacecallrecord;
import bank.BankKit;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class TransferMerchantRspDto extends BaseRspDto {
    /**
     * 外部订单号
     */
    private String outerOrderNo;
    /**
     * 内部支付订单
     */
    private String innerOrderNo;
    /**
     * 订单状态：
     * 0-待支付，1-退款处理中，2-处理中，3-交易成功，4-交易关闭，5-删除，6-已退款，7-交易失败，8-交易完成
     */
    private int orderStatus;

    /**
     * 手续费，精确到小数点后面两位
     */
    private double fee;

    /**
     * 币种，默认CNY
     */
    private String currency;

    /**
     * 转账金额
     */
    private double amount;

    private Interfacecallrecord interfacecallrecord;

    public static TransferMerchantRspDto buildRsp(String rsp) {
        Map<String, String> rspMap = BankKit.buildResp(rsp);

        TransferMerchantRspDto transferMerchantRspDto = new TransferMerchantRspDto();
        transferMerchantRspDto.setBaseInfo(rspMap);
        if (transferMerchantRspDto.isSuccess()) {
            transferMerchantRspDto.setAmount(Doubles.tryParse(rspMap.get(Const.FIELD_AMOUNT)));
            transferMerchantRspDto.setCurrency(rspMap.get("currency"));
            transferMerchantRspDto.setOuterOrderNo(rspMap.get("outerOrderNo"));
            transferMerchantRspDto.setOrderStatus(Ints.tryParse(rspMap.get("orderStatus")));
            transferMerchantRspDto.setCurrency(rspMap.get("currency"));
        }
        return transferMerchantRspDto;
    }

    public String getOuterOrderNo() {
        return outerOrderNo;
    }

    public void setOuterOrderNo(String outerOrderNo) {
        this.outerOrderNo = outerOrderNo;
    }

    public String getInnerOrderNo() {
        return innerOrderNo;
    }

    public void setInnerOrderNo(String innerOrderNo) {
        this.innerOrderNo = innerOrderNo;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Interfacecallrecord getInterfacecallrecord() {
        return interfacecallrecord;
    }

    public void setInterfacecallrecord(Interfacecallrecord interfacecallrecord) {
        this.interfacecallrecord = interfacecallrecord;
    }
}

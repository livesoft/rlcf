package bank.resp;

import bank.BankConstant;
import bank.BankKit;

import java.util.Map;

/**
 * <p>
 * 绑定用户DTO
 * </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public final class BindUserRspDto extends BaseRspDto {

    private String partnerBuyerId;


    public static BindUserRspDto build(String resp) {

        final Map<String, String> resMap = BankKit.buildResp(resp);

        BindUserRspDto userRespDto = new BindUserRspDto();
        userRespDto.setPartnerBuyerId(resMap.get(BankConstant.BP_PARTNER_BUYER_ID));

        userRespDto.setBaseInfo(resMap);

        return userRespDto;
    }

    public String getPartnerBuyerId() {
        return partnerBuyerId;
    }

    public void setPartnerBuyerId(String partnerBuyerId) {
        this.partnerBuyerId = partnerBuyerId;
    }
}

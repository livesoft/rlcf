package app.services;

import app.kit.TypeKit;
import app.models.member.Account;

import java.math.BigDecimal;

import static app.Const.FIELD_AMOUNT;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public final class AccountService {

    public static final AccountService me = new AccountService();

    /**
     * 投资本金字段
     */
    public static final String FIELD_INVESTMENTS = "investments";
    /**
     * 收益字段
     */
    public static final String FIELD_INCOME      = "income";


    /**
     * 计算会员账户金额变化
     *
     * @param decimal_amount 支付金额
     * @param memberId       会员ID
     * @return 账户信息
     */
    public Account calculationAccount(BigDecimal decimal_amount, int memberId) {
        // 更新会员账户信息
        // 总本金 ＝ 之前的本金＋本次购买投资的金额
        final Account account = Account.dao.findbyMember(memberId);
        final BigDecimal db_investments = account.getBigDecimal(FIELD_INVESTMENTS);
        final BigDecimal investments = db_investments == null ? decimal_amount : db_investments.add(decimal_amount);
        account.set(FIELD_INVESTMENTS, investments);
        account.set(FIELD_AMOUNT, account.getBigDecimal(FIELD_AMOUNT).add(decimal_amount));
        return account;
    }

    /**
     * 计算会员支出账户金额变化
     *
     * @param decimal_amount 减去金额
     * @param memberId       会员ID
     * @return 账户信息
     */
    public Account expendAccount(BigDecimal decimal_amount, int memberId) {
        // 更新会员账户信息
        // 总本金 ＝ 之前的本金＋本次购买投资的金额
        final Account account = Account.dao.findbyMember(memberId);
        final BigDecimal db_investments = account.getBigDecimal(FIELD_INVESTMENTS);
        final BigDecimal investments = db_investments == null ? BigDecimal.ZERO : db_investments.subtract(decimal_amount);
        account.set(FIELD_INVESTMENTS, investments);
        account.set(FIELD_AMOUNT, account.getBigDecimal(FIELD_AMOUNT).subtract(decimal_amount));
        return account;
    }

    /**
     * 转让产品 买方 资金变化
     *
     * @param assets    转让资产
     * @param buyAmount 购买金额
     * @param memberId  会员ID
     * @return 账户
     */
    public Account transferAccount(BigDecimal assets,BigDecimal buyAmount, int memberId) {
        // 更新会员账户信息
        // 总本金 ＝ 之前的本金＋本次购买投资的金额
        final Account account = Account.dao.findbyMember(memberId);
        account.set(FIELD_INVESTMENTS, TypeKit.getBigdecimal(account, FIELD_INVESTMENTS).add(assets));
        account.set(FIELD_AMOUNT, account.getBigDecimal(FIELD_AMOUNT).add(buyAmount));
        return account;
    }

    /**
     * 转让产品 卖方 资金变化
     *
     * @param playmoney 打款金额
     * @param assets    转让资产
     * @param memberId  会员ID
     * @return 账户
     */
    public Account transferSaleAccount(BigDecimal playmoney, BigDecimal assets, int memberId) {
        // 更新会员账户信息
        // 总本金 ＝ 之前的本金＋本次购买投资的金额
        final Account account = Account.dao.findbyMember(memberId);
        final BigDecimal db_investments = account.getBigDecimal(FIELD_INVESTMENTS);
        final BigDecimal investments = db_investments == null ? BigDecimal.ZERO : db_investments.subtract(assets);
        account.set(FIELD_INVESTMENTS, investments);
        account.set(FIELD_AMOUNT, account.getBigDecimal(FIELD_AMOUNT).subtract(assets));
        if (playmoney.compareTo(assets) == 1) {
            // 打款金额大于资产
            account.set(FIELD_INCOME, TypeKit.getBigdecimal(account, FIELD_INCOME).add(playmoney.subtract(assets)));
        }
        return account;
    }

    public Account income(BigDecimal profit, BigDecimal principal, int memberId) {

        final Account account = Account.dao.findbyMember(memberId);
        account.set(FIELD_INCOME, TypeKit.getBigdecimal(account, FIELD_INCOME).add(profit));
        // 本金减去当前打款的金额
        account.set(FIELD_INVESTMENTS, account.getBigDecimal(FIELD_INVESTMENTS).subtract(principal));
        // 收益时本金不处理
        // account.set("amount", account.getBigDecimal("amount").add(profit));
        return account;
    }

    public Account back(BigDecimal amount, int memberId) {

        final Account account = Account.dao.findbyMember(memberId);
        // 本金减去当前打款的金额
        account.set(FIELD_INVESTMENTS, account.getBigDecimal(FIELD_INVESTMENTS).subtract(amount));
        // 收益时本金不处理
        // account.set("amount", account.getBigDecimal("amount").add(profit));
        return account;
    }
}

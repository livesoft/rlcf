package app.services.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 *     授权参数
 * </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface AuthParam {

    /**
     * 是否进行授权设置
     *
     * @return 授权
     */
    boolean auth() default false;

    /**
     * 是否进行UUID判断
     * @return 默认都进行UUID判断
     */
    boolean uuid() default true;
}

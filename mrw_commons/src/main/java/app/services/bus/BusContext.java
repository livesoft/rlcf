package app.services.bus;

import app.services.bus.event.AccessEvent;
import app.services.bus.event.UserLoginEvent;
import app.services.bus.event.WithdrawalsEvent;
import app.services.bus.subscibe.AccessSubscriber;
import app.services.bus.subscibe.UserLoginSubscriber;
import app.services.bus.subscibe.WithdrawalsSubscriber;
import com.google.common.eventbus.AsyncEventBus;
import goja.GojaConfig;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.concurrent.Executors;

/**
 * <p>
 * 系统消息总线异步服务
 * </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class BusContext {
    /**
     * 异步事件的管理者
     */
    private static final AsyncEventBus EVENT_BUS =
            new AsyncEventBus(Executors.newFixedThreadPool(GojaConfig.getPropertyToInt("app.bus.pools", 12)));


    public static void init() {
        new AccessSubscriber(EVENT_BUS);
        new UserLoginSubscriber(EVENT_BUS);
        new WithdrawalsSubscriber(EVENT_BUS);
    }




    public static void postLogin(int member_id, String  userName,DateTime now_time, String ip, boolean status) {
        UserLoginEvent event = UserLoginEvent.memberLogin(member_id, userName, now_time, ip, status);
        EVENT_BUS.post(event);
    }


    public static void postAccess(int member_id, int product,DateTime now_time, String ip) {
        EVENT_BUS.post(new AccessEvent(member_id, product, ip, now_time));
    }

    public static void postWithdrawals(BigDecimal amount, String desc) {
        EVENT_BUS.post(new WithdrawalsEvent(amount, desc));
    }
}

package app.services.bus.event;

import org.joda.time.DateTime;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class AccessEvent {

    public final int memberId;

    public final int productId;

    public final String ip;

    public final DateTime time;

    public AccessEvent(int memberId, int productId, String ip, DateTime time) {
        this.memberId = memberId;
        this.productId = productId;
        this.ip = ip;
        this.time = time;
    }

    public int getMemberId() {
        return memberId;
    }

    public int getProductId() {
        return productId;
    }

    public String getIp() {
        return ip;
    }

    public DateTime getTime() {
        return time;
    }
}

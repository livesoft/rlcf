package app.services.bus.event;

import java.math.BigDecimal;

/**
 * <p> 平台提现事件 </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class WithdrawalsEvent {

    /**
     * 提现金额
     */
    public final BigDecimal amount;

    /**
     * 提现说明
     */
    public final String desc;

    public WithdrawalsEvent(BigDecimal amount, String desc) {
        this.amount = amount;
        this.desc = desc;
    }


}

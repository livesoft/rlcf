package app.services.bus.event;

import org.joda.time.DateTime;

/**
 * <p> 用户登录事件</p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class UserLoginEvent {

    /**
     * true=会员，false=管理员
     */
    public final boolean member;

    public final int userId;

    public final String userName;

    public final DateTime loginTime;

    public final String ip;

    public final boolean status;

    private UserLoginEvent(boolean member, int userId, String userName, DateTime loginTime, String ip, boolean status) {
        this.member = member;
        this.userId = userId;
        this.userName = userName;
        this.loginTime = loginTime;
        this.ip = ip;
        this.status = status;
    }


    public static UserLoginEvent memberLogin(int member_id, String member_name, DateTime loginTime, String ip, boolean status) {
        return new UserLoginEvent(true, member_id, member_name, loginTime, ip, status);
    }

    public static UserLoginEvent userLogin(int userId, String userName, DateTime loginTime, String ip, boolean status) {
        return new UserLoginEvent(false, userId, userName, loginTime, ip, status);
    }
}

package app.services.bus.subscibe;

import app.Const;
import app.kit.CommonKit;
import app.models.analyze.ProductAccess;
import app.services.bus.event.AccessEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import goja.StringPool;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class AccessSubscriber {

    public AccessSubscriber(EventBus eventBus) {
        eventBus.register(this);
    }


    @Subscribe
    public void handleAccess(AccessEvent accessEvent) {
        ProductAccess productAccess = new ProductAccess();
        productAccess.set(StringPool.PK_COLUMN, CommonKit.uuid());
        productAccess.set(Const.FIELD_PRODUCT, accessEvent.productId);
        productAccess.set("access_time", accessEvent.time);
        productAccess.set("access_time_year", accessEvent.time.getYear());
        productAccess.set("access_time_month", accessEvent.time.getMonthOfYear());
        productAccess.set("access_time_day", accessEvent.time.getDayOfMonth());
        productAccess.set("ip", accessEvent.ip);
        productAccess.set("member", accessEvent.memberId);
        productAccess.save();
    }
}

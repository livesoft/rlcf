package app.services;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public final class SerialService {

    public static final String SN = "sn";

    /**
     * 产生产品流水号
     */
    public static String productCode() {
        Record record = Db.findFirst(SqlKit.sql("serial.productCode"));
        return record.getStr(SN);
    }
    /**
     * 产生订单流水号
     */
    public static String orderCode() {
        Record record = Db.findFirst(SqlKit.sql("serial.orderCode"));
        return record.getStr(SN);
    }
    /**
     * 产生订单流水号
     */
    public static String orderCode(String productSimpleCode) {
        Record record = Db.findFirst(SqlKit.sql("serial.productSmipleCode"));
        return productSimpleCode + "_" + record.getStr(SN);
    }
    /**
     * 产生会员流水号
     */
    public static String memberCode() {
        Record record = Db.findFirst(SqlKit.sql("serial.memberCode"));
        return record.getStr(SN);
    }
}

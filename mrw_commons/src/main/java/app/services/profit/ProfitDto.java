package app.services.profit;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;

/**
 * <p> 收益 DTO</p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ProfitDto {

    /**
     * 期次
     */
    public final int issue;


    /**
     * 应收本息
     */
    public final BigDecimal interestOnPrincipal;
    /**
     * 应收本金
     */
    public final BigDecimal principal;
    /**
     * 应收利息
     */
    public final BigDecimal interest;
    /**
     * 剩余本金
     */
    public final BigDecimal remainingPrincipal;

    /**
     * @param issue               期次
     * @param interestOnPrincipal 应收本息
     * @param principal           应收本金
     * @param interest            应收利息
     * @param remainingPrincipal  剩余本金
     */
    private ProfitDto(int issue,
                      BigDecimal interestOnPrincipal,
                      BigDecimal principal,
                      BigDecimal interest,
                      BigDecimal remainingPrincipal) {
        this.issue = issue;
        this.interestOnPrincipal = interestOnPrincipal;
        this.principal = principal;
        this.interest = interest;
        this.remainingPrincipal = remainingPrincipal;
    }

    /**
     * @param issue           期次
     * @param principal       应收本金
     * @param interestOfMonth 应收利息
     * @return 收益DTO
     */
    public static ProfitDto createDto(int issue,
                                      BigDecimal principal,
                                      BigDecimal interestOfMonth) {
        return new ProfitDto(issue, new BigDecimal(0), principal, interestOfMonth, new BigDecimal(0));
    }

    /**
     * @param issue           期次
     * @param monthlyIncome   应收本息
     * @param principal       应收本金
     * @param interestOfMonth 应收利息
     * @param restPrincipal   剩余本金
     * @return 收益DTO
     */
    public static ProfitDto createDto(int issue,
                                      BigDecimal monthlyIncome,
                                      BigDecimal principal,
                                      BigDecimal interestOfMonth,
                                      BigDecimal restPrincipal) {
        return new ProfitDto(issue, monthlyIncome, principal, interestOfMonth, restPrincipal);
    }


    public BigDecimal getInterestOnPrincipal() {
        return interestOnPrincipal;
    }

    public BigDecimal getPrincipal() {
        return principal;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public int getIssue() {
        return issue;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("issue", issue)
                .add("interestOnPrincipal", interestOnPrincipal)
                .add("principal", principal)
                .add("interest", interest)
                .add("remainingPrincipal", remainingPrincipal)
                .toString();
    }
}


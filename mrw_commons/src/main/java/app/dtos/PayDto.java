package app.dtos;

import bank.BankConstant;
import bank.resp.OrderInfoRspDto;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class PayDto {

    public final int orderStatus;


    public final BigDecimal totalAmount;

    public final BigDecimal fee;

    public final String outerOrderNo;


    public final String innerOrderNo;


    public final DateTime respBizDate;

    private PayDto(String orderStatus,
                   String totalAmount, String fee,
                   String outerOrderNo, String innerOrderNo,
                   DateTime respBizDate) {
        this.orderStatus = Ints.tryParse(orderStatus);
        this.totalAmount = Strings.isNullOrEmpty(totalAmount) ? BigDecimal.ZERO : NumberUtils.createBigDecimal(totalAmount);
        this.fee = Strings.isNullOrEmpty(fee) ? BigDecimal.ZERO : NumberUtils.createBigDecimal(fee);
        this.outerOrderNo = outerOrderNo;
        this.innerOrderNo = innerOrderNo;
        this.respBizDate = respBizDate;
    }
    private PayDto(int orderStatus,
                   BigDecimal totalAmount, BigDecimal fee,
                   String outerOrderNo, String innerOrderNo,
                   DateTime respBizDate) {
        this.orderStatus = orderStatus;
        this.totalAmount = totalAmount;
        this.fee = fee;
        this.outerOrderNo = outerOrderNo;
        this.innerOrderNo = innerOrderNo;
        this.respBizDate = respBizDate;
    }


    public static PayDto createPayReturn(String orderStatus, String totalAmount, String fee, String outerOrderNo, String innerOrderNo, String respBizDate) {
        DateTime respDate;
        respDate = Strings.isNullOrEmpty(respBizDate) ? DateTime.now() : DateTime.parse(respBizDate, BankConstant.DATE_FORMATTER);
        return new PayDto(orderStatus, totalAmount, fee, outerOrderNo, innerOrderNo, respDate);
    }
    public static PayDto create(OrderInfoRspDto orderInfoRspDto) {
        return new PayDto(orderInfoRspDto.getOrderStatus(), orderInfoRspDto.getAmount(), orderInfoRspDto.getFee(),
                orderInfoRspDto.getOuterOrderNo(), orderInfoRspDto.getInnerOrderNo(), DateTime.parse(orderInfoRspDto.getRespBizDate(), BankConstant.DATE_FORMATTER));
    }


    public static PayDto createPayNotify(String orderStatus, String totalAmount, String fee, String outerOrderNo, String innerOrderNo) {
        return new PayDto(orderStatus, totalAmount, fee, outerOrderNo, innerOrderNo, DateTime.now());
    }


}

package app.dtos;

import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import goja.Func;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class SmsDto {


    public final String phone;

    public final String content;

    public final DateTime sendTime;
    public final Date     sendDate;
    public final DateTime execTime;

    public final String sender;

    public final boolean error;
    public final String  exception;

    public final String extno;

    public final String source;

    public final String response;

    // 响应返回的消息内容

    /**
     * 发送状态
     */
    public final int    status;
    /**
     * 发送编号
     */
    public final String sendid;
    /**
     * 错误号码数
     */
    public final int    invalid;
    /**
     * 发送成功数
     */
    public final int    success;
    /**
     * 黑名单数
     */
    public final int    black;
    /**
     * 消息
     */
    public final String msg;


    private SmsDto(String phone,
                     String content,
                     DateTime sendTime,
                     DateTime execTime,
                     String sender,
                     boolean error,
                     String exception,
                     String extno,
                     String source,
                     String response) {
        this.phone = phone;
        this.content = content;
        this.sendTime = sendTime;
        this.execTime = execTime;
        this.sendDate = sendTime.toDate();
        this.sender = sender;
        this.error = error;
        this.exception = exception;
        this.extno = extno;
        this.source = source;
        this.response = response;

        if (Strings.isNullOrEmpty(response)) {
            this.status = -1;
            this.sendid = StringUtils.EMPTY;
            this.invalid = 0;
            this.success = 0;
            this.black = 0;
            this.msg = StringUtils.EMPTY;
        } else {

            final List<String> responses = Func.COMMA_SPLITTER.splitToList(response);
            if (responses.size() == 6) {
                this.status = Ints.tryParse(responses.get(0));
                this.sendid = responses.get(1);
                this.invalid = Ints.tryParse(responses.get(2));
                this.success = Ints.tryParse(responses.get(3));
                this.black = Ints.tryParse(responses.get(4));
                this.msg = responses.get(5);
            } else {
                this.status = -1;
                this.sendid = StringUtils.EMPTY;
                this.invalid = 0;
                this.success = 0;
                this.black = 0;
                this.msg = StringUtils.EMPTY;
            }
        }

    }

    public Date getSendDate() {
        return sendDate;
    }

    public String getPhone() {
        return phone;
    }

    public String getContent() {
        return content;
    }

    public DateTime getSendTime() {
        return sendTime;
    }

    public DateTime getExecTime() {
        return execTime;
    }

    public String getSender() {
        return sender;
    }

    public boolean isError() {
        return error;
    }

    public String getException() {
        return exception;
    }

    public String getExtno() {
        return extno;
    }

    public String getSource() {
        return source;
    }

    public String getResponse() {
        return response;
    }

    public int getStatus() {
        return status;
    }

    public String getSendid() {
        return sendid;
    }

    public int getInvalid() {
        return invalid;
    }

    public int getSuccess() {
        return success;
    }

    public int getBlack() {
        return black;
    }

    public String getMsg() {
        return msg;
    }

    public static SmsDto create(String phone, String content, DateTime sendTime, DateTime execTime, String response) {
        return new SmsDto(phone, content, sendTime, execTime, StringUtils.EMPTY, false, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, response);
    }

    public static SmsDto createByRegister(String phone, String content, DateTime sendTime, DateTime execTime, String response) {
        return new SmsDto(phone, content, sendTime, execTime, StringUtils.EMPTY, false, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, response);
    }

}

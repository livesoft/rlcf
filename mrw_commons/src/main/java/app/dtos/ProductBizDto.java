package app.dtos;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductBizDto {

    public final boolean cancel;

    public final boolean transfer;

    public final String transferDesc;

    public final String cancelDesc;

    public final String playmoneyDesc;

    private ProductBizDto(boolean cancel, boolean transfer, String transferDesc, String cancelDesc, String playmoneyDesc) {
        this.cancel = cancel;
        this.transfer = transfer;
        this.transferDesc = transferDesc;
        this.cancelDesc = cancelDesc;
        this.playmoneyDesc = playmoneyDesc;
    }

    public static ProductBizDto create(boolean cancel, boolean transfer, String transferDesc, String cancelDesc, String playmoneyDesc) {
        return new ProductBizDto(cancel, transfer, transferDesc, cancelDesc, playmoneyDesc);
    }



    public boolean isCancel() {
        return cancel;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public String getTransferDesc() {
        return transferDesc;
    }

    public String getCancelDesc() {
        return cancelDesc;
    }

    public String getPlaymoneyDesc() {
        return playmoneyDesc;
    }
}

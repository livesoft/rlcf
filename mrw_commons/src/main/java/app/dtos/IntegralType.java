package app.dtos;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public enum IntegralType {
    /**
     * 签到积分
     */
    SIGNIN,
    /**
     * 消费积分
     */
    CASH,
    /**
     * 邀请奖励
     */
    INVITE
}

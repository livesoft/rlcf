package app;

import com.jfinal.kit.PathKit;

import java.io.File;
import java.math.BigDecimal;

/**
 * <p>系统常量 </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface Const {


    //    String CACHE_CATEGORY_DICT = "cache.dict.categorys";
//    String CACHE_BRAND         = "cache.brand.home";
    String CACHE_NOTES      = "cache.notes";
    String CACHE_BASICINFOS = "cache.basicinfos";


    BigDecimal HUNDRED = new BigDecimal(100);


    //导出EXCEL路径
    interface Report {
        String REPORT_DIR_PATH = PathKit.getWebRootPath() + File.separator + "reports" + File.separator;
        String PRODUCT_PATH    = REPORT_DIR_PATH + "product.xlsx";
        String ORDER_PATH      = REPORT_DIR_PATH + "order.xlsx";
        String MEMBER_PATH     = REPORT_DIR_PATH + "member.xlsx";
        String LOGSMS_PATH     = REPORT_DIR_PATH + "log_sms.xlsx";

        /**
         * 日终销售报表
         */
        String DAY_SALE_PATH              = REPORT_DIR_PATH + "day_sale.xlsx";
        /**
         * 每日销售报表
         */
        String DAY_SALE_DETAIL_PATH       = REPORT_DIR_PATH + "day_sale_detail.xlsx";
        /**
         * 产品收益报表
         */
        String PRODUCT_PROFIT_PATH        = REPORT_DIR_PATH + "product_profit.xlsx";
        String PRODUCT_PROFIT_DETAIL_PATH = REPORT_DIR_PATH + "product_profit_detail.xlsx";
        /**
         * 日退单赎回报表
         */
        String DAY_REDEEM_PATH            = REPORT_DIR_PATH + "day_redeem.xlsx";
        /**
         * 每日退单报表
         */
        String DAY_REDEEM_DETAIL_PATH     = REPORT_DIR_PATH + "day_redeem_detail.xlsx";

        /**
         * 募集失败报表
         */
        String RAISEFAIL_PATH = REPORT_DIR_PATH + "raisefail.xlsx";

        /**
         * 购买明细
         */
        String PRODUCT_BUY_PATH = REPORT_DIR_PATH + "product_buy.xlsx";

        /**
         * 日转让报表
         */
        String DAY_TRANSFER_PATH          = REPORT_DIR_PATH + "day_transfer.xlsx";
        /**
         * 日转让明细
         */
        String DAY_TRANSFER_DETAIL_PATH   = REPORT_DIR_PATH + "day_transfer_detail.xlsx";
        /**
         * 挂单费
         */
        String DAY_LOADINGFEE_PATH        = REPORT_DIR_PATH + "day_loadingfee.xlsx";
        /**
         * 挂单费明细
         */
        String DAY_LOADINGFEE_DETAIL_PATH = REPORT_DIR_PATH + "day_loadingfee_detail.xlsx";
        /**
         * 手续费报表
         */
        String DAY_FEE_PATH               = REPORT_DIR_PATH + "day_fee.xlsx";
        /**
         * 手续费明细
         */
        String DAY_FEE_DETAIL_PATH        = REPORT_DIR_PATH + "day_fee_detail.xlsx";

        /**
         * 财务对账单
         */
        String FINANCE_COST_PATH = REPORT_DIR_PATH + "finance_cost.xlsx";

        /**
         * 产品资金汇总报表
         */
        String TOTAL_PRODUCT_PATH          = REPORT_DIR_PATH + "total_product.xlsx";


    }


    String EDIT_ACTION   = "edit";
    String CREATE_ACTION = "create";

    String ACTION_ATTR = "action";

    String PARAM_DATA = "data";

    /**
     * 页码
     */
    String PARAM_PAGE     = "page";
    /**
     * 每页数量
     */
    String PARAM_PAGESIZE = "pageSize";

    /**
     * 消息
     */
    String MESSAGE_ATTR = "message";
    // 常用字段名称

    /**
     * 统计字段常量
     */
    String FIELD_CNT             = "cnt";
    /**
     * 状态字段
     */
    String FIELD_STATUS          = "status";
    /**
     * 积分
     */
    String FIELD_INTEGRAL        = "integral";
    /**
     * 编号
     */
    String FIELD_CODE            = "code";
    /**
     * 创建时间
     */
    String FIELD_CREATE_TIME     = "create_time";
    /**
     * 产品
     */
    String FIELD_PRODUCT         = "product";
    /**
     * 起息日模式
     */
    String FIELD_VALUEDATEOFFSET = "valuedate_offset";
    /**
     * 风险承受能力
     */
    String FIELD_RISK_TOLERANCE  = "risk_tolerance";
    /**
     * 会员
     */
    String FIELD_MEMBER          = "member";
    /**
     * 收益率
     */
    String FIELD_YIELD           = "yield";
    /**
     * 类型
     */
    String FIELD_TYPE            = "type";
    /**
     * 价格
     */
    String FIELD_PRICE           = "price";
    /**
     * 资金
     */
    String FIELD_AMOUNT          = "amount";
    /**
     * 名称
     */
    String FIELD_NAME            = "name";
    /**
     * 收益方式
     */
    String FIELD_COLLECTION_MODE = "collection_mode";
    /**
     * 期限单位
     */
    String FIELD_TIME_LIMIT_UNIT = "time_limit_unit";
    /**
     * 理财期限
     */
    String FIELD_TIME_LIMIT      = "time_limit";
    /**
     * 期限字段
     */
    String FIELD_TIME_TERM       = "term";
    /**
     * 订单ID
     */
    String FIELD_ORDER_ID        = "order_id";

    /**
     * 默认分页大小
     */
    int PAGE = 10;


    /**
     * 时间区间间隔
     */
    String DATE_SPLIT = " - ";

    /**
     * 时间格式
     */
    String DATE_SLASH_FORMMAT = "yyyy/MM/dd";


    /**
     * 积分规则设置用到的积分代码
     */
    interface POINTS_CODE {
        /**
         * 签到积分code
         */
        String SIGN    = "sign";
        /**
         * 消费积分code
         */
        String COST    = "cost";
        /**
         * 转发积分code
         */
        String FORWARD = "frd";
        /**
         * 邀请积分code
         */
        String INVITE  = "invite";

    }

    String YES = "Y";

    String NO = "N";

    /**
     * 消息类型
     */
    interface MSGS_TYPE {
        /**
         * 系统消息
         */
        int SYS  = 0;
        /**
         * 用户间消息
         */
        int USER = 1;
    }

    /**
     * 协议类别代码
     */
    interface AGREEMENT {
        /**
         * 转让协议
         */
        String TRANSFER = "transfer";
        /**
         * 注册协议
         */
        String REGISTER = "register";
        /**
         * 理财产品购买协议
         */
        String BUY      = "buy";
        /**
         * 风险提示
         */
        String RISK     = "risk";

        /**
         * 快捷支付协议
         */
        String FASTPAYMENT = "fastpayment";
    }
}

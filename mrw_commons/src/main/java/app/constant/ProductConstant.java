package app.constant;


/**
 * <p> 产品常量 </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface ProductConstant {





    /* 待发布 -> 上线 -> 售罄 -> 成立 -> 到期结束*/
    /* 待发布 -> 上线 -> 销售结束 -> 成立 -> 到期结束*/
    /* 待发布 -> 上线 -> 募集失败*/


    /*status*/

    /**
     * 待发布 1
     */
    int READY     = 0;
    /**
     * 上线 1
     */
    int ONLINE    = 1;
    /**
     * 售罄
     */
    int EMPTY     = 2;
    /**
     * 募集失败
     */
    int FAIL      = 3;
    /**
     * 结束
     */
    int END       = 4;
    /**
     * 删除 1
     */
    int DEL       = 6;
    /**
     * 销售结束状态 1
     */
    int END_SALES = 7;
    /**
     * 产品成立
     */
    int FOUND     = 8;


}

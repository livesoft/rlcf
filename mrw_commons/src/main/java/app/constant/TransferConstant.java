package app.constant;


/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface TransferConstant {

    /*
    * 产品装让流程
    *  1. 申请转让 -> 后台审核 -> 交易中 -> 交易成功
    *  2. 申请转让 -> 后台申请 -> 交易中 -> 24小时（转让时效）还是没有达成交易 -> 转让失效
    */

    /**
     * 待审核
     */
    int STATUE_PAUSE   = 0;
    /**
     * 交易中
     */
    int STATUE_TRADING = 1;
    /**
     * 交易成功
     */
    int STATUE_SUCCESS = 2;
    /**
     * 转让失效
     */
    int STATUE_FAIL = 3;
    /**
     * 转让撤销
     */
    int STATUE_CANCEL = 4;

    /**
     * 一口价
     */
    String MOE_PRICE = "PRICE";
    /**
     * 拆分
     */
    String MOE_SPLIT = "SPLIT";
}

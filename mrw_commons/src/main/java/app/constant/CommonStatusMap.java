package app.constant;

import app.models.order.TradeMoney;
import app.models.product.Product;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import goja.StringPool;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class CommonStatusMap {

    private static final ImmutableMap<String, String> ORDER_TYPES = ImmutableMap.<String, String>builder()
            .put(OrderConstant.BUY_TYPE, "投资(购买)")
            .put(OrderConstant.INC_TYPE, "收益发放")
            .put(OrderConstant.RPN_TYPE, "退单")
            .put(OrderConstant.TFR_TYPE, "投资（转让）")
            .put(OrderConstant.TFR_PAY_TYPE, "转让打款")
            .put(OrderConstant.CHARGING, "充值")
            .put(OrderConstant.WITHDRAWALS, "提现")
            .put(OrderConstant.PRODUCTFAIL, "募集失败退款")
            .put(OrderConstant.ORDRFAIL, "交易失败退款")
            .put(OrderConstant.TPUSH_TYPR,"转让挂单")
            .build();


    private static final ImmutableMap<Integer, String> ORDER_STAUTS = ImmutableMap.<Integer, String>builder()
            .put(OrderConstant.TOBEPAY, "待支付")
            .put(OrderConstant.REFUNDING, "退款中")
            .put(OrderConstant.HANDING, "处理中")
            .put(OrderConstant.TRADE_SUCCESS, "交易成功")
            .put(OrderConstant.TRADE_CLOSE, "交易关闭")
            .put(OrderConstant.DEL, "已删除")
            .put(OrderConstant.REFUNDED, "已退款")
            .put(OrderConstant.TRADE_FAILURE, "交易失败")
            .put(OrderConstant.TRADE_COMPLETE, "交易完成")
            .put(OrderConstant.NEW, "待处理")
            .build();

    /**
     * rlo_trade_money （交易金额结算) status 对应
     */
    private static final ImmutableMap<Integer,String> TRADEMONEY_STATUS = ImmutableMap.<Integer,String>builder()
            .put(TradeMoney.WAIT_STATUS, "待打款")
            .put(TradeMoney.CONFIRM_STATUS,"财务确认打款")
            .put(TradeMoney.OK_STATUS,"领导审核确认打款")
            .put(TradeMoney.SUCCESS_STATUS,"已打款")
            .put(TradeMoney.FAIL_STATUS,"打款失败")
            .build();

    /**
     * 产品状态
     */
    private static final ImmutableMap<Integer,String> PRODUCT_STATUS = ImmutableMap.<Integer,String>builder()
            .put(ProductConstant.READY,"待发布").put(ProductConstant.ONLINE,"销售中").put(ProductConstant.EMPTY,"售罄")
            .put(ProductConstant.FAIL,"募集失败").put(ProductConstant.END,"已结束").put(ProductConstant.DEL,"已删除")
            .put(ProductConstant.END_SALES,"销售结束").put(ProductConstant.FOUND,"已成立").build();


    /**
     * 金额调账方式  对应
     */
    private static final ImmutableMap<Integer, String> ADJUST_MODE = ImmutableMap.<Integer, String>builder()
            .put(0, "")
            .put(1, "+")
            .put(2, "-")
            .build();

    public static String getChineseOrderType(String order_type) {
        if (Strings.isNullOrEmpty(order_type)) {
            return StringPool.EMPTY;
        }
        return ORDER_TYPES.get(order_type);
    }

    public static String getChineseOrderType(int order_status) {

        return ORDER_STAUTS.get(order_status);
    }

    public static String getChineseTrademoneyStatus(int status){
        return TRADEMONEY_STATUS.get(status);
    }

    /**
     * 调账方式
     * @param type 方式
     * @return 方式展示
     */
    public static String getAdjustMode(int type){
        return ADJUST_MODE.get(type);
    }

    /**
     * 产品状态对应的中文名称
     * @param status 状态值
     * @return 对应的展示中文名称
     */
    public static String getChineseProductStatus(int status){
        return PRODUCT_STATUS.get(status);
    }
}


package app.constant;


/**
 * <p>
 * 会员状态
 * </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface MemberConstant {

    int DEFAULT_STATUS = 0;

    /**
     * 已删除
     */
    int DELETE = 0;

    /**
     * 正常
     */
    int NORMAL = 1;

    /**
     * 禁用
     */
    int DISABLE = 2;

    /**
     * 锁定，可解锁
     */
    int LOCK = 3;

    /* 会员产品状态 */

    /* 募集中 -> 持有中 */
    /* 持有中 -> 打款中 -> 已结束 */
    /* 持有中 -> 转让审核中 -> 转让交易中 -> 已转让 */
    /* 持有中 -> 赎回审核中 -> 已赎回 */

    /**
     * 投资认购期的产品： 会员产品状态   A  产品认购中 ； 可赎回，不可转让
     */
    int PRODUCT_SALE          = 1;
    /**
     * 产品募集成功： 会员产品状态  B 产品认购成功 ： 不可赎回，可转让
     */
    int PRODUCT_SALES_SUCCESS = 2;
    /**
     * 产品募集失败： 会员产品状态  C 产品认购失败 ： 不可赎回，不可转让
     */
    int PRODUCT_SALE_FAIL     = 3;
    /**
     * 产品到期： 会员产品状态： K 已结束 - 不可赎回 不可转让
     */
    int PRODUCT_EXPIRE        = 4;
    /**
     * 宝宝类产品持有中
     */
    int PRODUCT_HOLDING       = 5;
    /**
     * 产品状态已成立，可转让 不可赎回
     */
    int PRODUCT_FOUND         = 8;


    /**
     * 会员产品申请赎回： 会员产品状态 ： D（赎回申请中）
     */
    int REDEEM_AUDIT   = 1;
    /**
     * 会员产品申请赎回成功： 会员产品状态 ： E （ 赎回成功）
     */
    int REDEEM_SUCCESS = 2;
    /**
     * 会员产品申请赎回失败：
     */
    int REDEEM_FAIL    = 3;


    /**
     * 会员产品转让申请： 会员产品状态 ： F （申请转让）
     */
    int TRANSFER_AUDIT         = 1;
    /**
     * 会员产品转让申请成功： 会员产品状态 ： G （转让挂单中）
     */
    int TRANSFER_AUDIT_SUCCESS = 2;
    /**
     * 会员产品转让交易成功： 会员产品状态： H （已转让）
     */
    int TRANSFER_TRADE_SUCCESS = 3;
    /**
     * 会员产品转让交易失败： 会员产品状态 ：I （转让失败）- 不可赎回 可转让
     */
    int TRANSFER_TRADE_FAIL    = 4;

//    ================================

    /**
     * 赎回申请（待审核）
     */
    int REDEM_START = 1;

    /**
     * 赎回确定（待打款）
     */
    int REDEM_OK = 2;

    /**
     * 赎回成功 (已赎回)
     */
    int REDEM_SUCCESS = 3;

    /*打款状态*/

    /**
     * 未打款
     */
    int PLAYMONEY_NO = 0;

    /**
     * 提交打款
     */
    int PLAYMONEY_SUBMIT = 1;

    /**
     * 打款成功
     */
    int PLAYMONEY_OK = 2;


    /**
     * 性别
     */
    interface GENDER {
        int Man   = 1;
        int Women = 0;
        int UNKNOW = 9;
    }


}

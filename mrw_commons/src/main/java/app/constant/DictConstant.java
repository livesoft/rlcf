package app.constant;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface DictConstant {

    /**
     * 理财期限单位 －月
     */
    String MONTH_UNIT = "month";

    /**
     * 理财期限单位-日
     */
    String DAY_UNIT = "day";


    /**
     * 天中文说明
     */
    String DAY_CHINESS   = "天";
    /**
     * 月中文说明
     */
    String MONTH_CHINESS = "月";

    /**
     * 一次性还本付息
     */
    int PROFIT_ONCE =1;

    /**
     * 每月等额本息
     */
    int PROFIT_MONTH = 2;

    /**
     * 本息滚动投资
     */
    int PROFIT_ROLL = 3;

}

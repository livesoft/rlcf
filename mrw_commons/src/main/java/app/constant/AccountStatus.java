package app.constant;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface AccountStatus {
    /**
     * 已删除
     */
    int DELETE = 0;

    /**
     * 正常
     */
    int NORMAL = 1;

    /**
     * 禁用
     */
    int DISABLE = 2;
}

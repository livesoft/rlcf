package app.constant;


import com.google.common.collect.Lists;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public interface OrderConstant {

    /**
     * 投资购买
     */
    String BUY_TYPE     = "BUY";
    /**
     * 投资收益
     */
    String INC_TYPE     = "INC";
    /**
     * 投资赎回
     */
    String RPN_TYPE     = "RPN";
    /**
     * 投资转让
     */
    String TFR_TYPE     = "TFR";
    /**
     * 转让挂单订单
     */
    String TPUSH_TYPR     = "TPR";
    /**
     * 转让打款交易
     */
    String TFR_PAY_TYPE = "TPY";
    /**
     * 充值订单
     */
    String CHARGING     = "CRG";
    /**
     * 提现订单
     */
    String WITHDRAWALS  = "WRS";
    /**
     * 募集打款失败订单
     */
    String PRODUCTFAIL  = "PFP";
    /**
     * 交易失败订单
     */
    String ORDRFAIL  = "OFP";

    /**
     * 待支付
     */
    int TOBEPAY   = 0;
    /**
     * 退款中
     */
    int REFUNDING = 1;
    /**
     * 处理中
     */
    int HANDING   = 2;

    /**
     * 交易成功
     */
    int TRADE_SUCCESS  = 3;
    /**
     * 交易关闭
     */
    int TRADE_CLOSE    = 4;
    /**
     * 已删除
     */
    int DEL            = 5;
    /**
     * 已退款
     */
    int REFUNDED       = 6;
    /**
     * 交易失败
     */
    int TRADE_FAILURE  = 7;
    /**
     * 交易完成
     */
    int TRADE_COMPLETE = 8;
    /**
     * 新建订单
     */
    int NEW            = 9;

//    /**
//     * 电子账户支付
//     */
//    int PAYTYPE_ACCOUNT  = 1;
//    /**
//     * 余额支付
//     */
//    int PAYTYPE_BALANCE  = 2;
//    /**
//     * 银行卡支付
//     */
//    int PAYTYPE_BANKCARD = 3;


    List<String> ORDER_STATUS =
            Lists.newArrayList("待支付", "退款中", "处理中", "交易成功", "交易关闭", "已删除", "已退款", "交易失败", "交易完成", "新建订单");
    List<String> ORDER_TYPES =
            Lists.newArrayList("订单", "充值", "提现", "转账", "外部订单支付", "退款", "其他订单", "积分购买", "缴费", "撤销");

}

package app.models.product;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * 产品品牌
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlp_product_brand")
public class Brand extends Model<Brand> {

    public static final Brand dao = new Brand();

    private static final long serialVersionUID = -2636401409996194371L;

    public List<Brand> findAll() {

        return find(SqlKit.sql("brand.findAll"));
    }

    public List<Brand> findBySort() {
//        return findByCache(Const.CACHE_BRAND, "brand.home", SqlKit.sql("brand.findBySort"));
        return find(SqlKit.sql("brand.findBySort"));
    }

}

package app.models.product;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p>
 * The database rlp_product_yields Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlp_product_yields")
public class ProductYields extends Model<ProductYields> {

    /**
     * The public dao.
     */
    public static final ProductYields dao = new ProductYields();


    private static final long serialVersionUID = -8166179824852003048L;


    public ProductYields findByLastUpdate(int product) {
        return findFirst(SqlKit.sql("product-yields.findByLastUpdate"), product);

    }

    public boolean updateByInitProduct(int product, double profit) {
        final int update = Db.update(SqlKit.sql("product-yields.updateByInitProduct"), product, profit, true);
        return update >= 0;
    }
}
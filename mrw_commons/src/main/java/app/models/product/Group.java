/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.product;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * The table rlp_group mapping model.
 * </p>
 */
@TableBind(tableName = "rlp_group")
public class Group extends Model<Group> {

    /**
     * The public dao.
     */
    public static final Group dao = new Group();

    private static final long serialVersionUID = 8742117341246759103L;


    public List<Group> findWithList() {
        return find(SqlKit.sql("group.findWithList"), true);
    }


    public List<Group> findAll() {
        return find(SqlKit.sql("group.findAll"));
    }

    /**
     * 获取产品中可用的分组列表
     * @return group列表
     */
    public List<Group> findAvalibleAllOfProduct(){
        return find(SqlKit.sql("group.findAvalibleAllOfProduct"),1, Const.YES);
    }

    /**
     * 可用的产品分组 即status=1
     * @return 所有产品分组
     */
    public List<Group> findAvalibleAll() {
        return find(SqlKit.sql("group.findAvalibleAll"));
    }

    /**
     * 查询首页展示的分组
     *
     * @return 首页展示分组
     */
    public List<Group> findByIndexShow() {
        return find(SqlKit.sql("group.findByIndexShow"), Const.YES, 1);
    }


    /**
     * 根据产品id查询所有
     * @param product_id 产品id
     * @return 所有
     */
    public List<Group> findByProduct(Serializable product_id) {
        return find(SqlKit.sql("group_product.findByProduct"), product_id);
    }
}
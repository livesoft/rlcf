package app.models.product;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlp_accept_org", pkName = "product")
public class Accept extends Model<Accept> {
    private static final long serialVersionUID = 5817249934429261733L;


    public static final Accept dao = new Accept();
}

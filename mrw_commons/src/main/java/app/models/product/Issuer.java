/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.product;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlp_issuer mapping model.
 * 发行机构
 * </p>
 */
@TableBind(tableName = "rlp_issuer")
public class Issuer extends Model<Issuer> {

    /**
     * The public dao.
     */
    public static final Issuer dao = new Issuer();


    private static final long serialVersionUID = -284189725405434589L;

    public List<Issuer> findAll() {
        return find(SqlKit.sql("issuer.findAll"));
    }
}
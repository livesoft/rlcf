/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.product;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlp_attr mapping model.
 * </p>
 */
@TableBind(tableName = "rlp_product_attr")
public class ProductAttr extends Model<ProductAttr> {

    /**
     * The public dao.
     */
    public static final ProductAttr dao = new ProductAttr();

    private static final long serialVersionUID = -964664733958685329L;

    public boolean updateStatus(String set_val, String id) {
        int update = Db.update(SqlKit.sql("attr.updateStatus"), set_val, id);
        return update > 0;
    }


    /**
     * 查询所有可用的属性值
     * @return
     */
    public List<ProductAttr> getAvailableAttr() {
        return find(SqlKit.sql("attr.getAvailableAttr"));
    }

    public List<ProductAttr> findAll() {
        return find(SqlKit.sql("attr.findAll"));
    }
}
package app.models.product;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;

/**
 * <p>
 * 产品约束
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlp_purchase_limit", pkName = Const.FIELD_PRODUCT)
public class PurchaseLimit extends Model<PurchaseLimit> {

    /**
     * The public dao.
     */
    public static final PurchaseLimit dao = new PurchaseLimit();


    private static final long serialVersionUID = -5634226478052365978L;
}
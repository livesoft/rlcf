/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.product;

import app.Const;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlp_details mapping model.
 * </p>
 */
@TableBind(tableName = "rlp_details")
public class Details extends Model<Details> {

    /**
     * The public dao.
     */
    public static final Details dao = new Details();

    private static final long serialVersionUID = -794701470865949051L;

    /**
     * 状态设置
     * @param set_val 状态值
     * @param id id
     * @return 更新结果
     */
    public boolean setStatus(boolean set_val, String id) {
        int update = Db.update(SqlKit.sql("details.setStatus"), set_val, id);
        return update > 0;
    }

    /**
     * 根据 产品查询产品详情
     * @param product_id
     */
    public List<Details> findByProduct(int product_id) {
        return find(SqlKit.sql("details.findByProduct"), product_id);
    }

    public List<Details> findByAppItem(int appitem) {
        return find(SqlKit.sql("details.findByAppItem"),appitem);
    }


    /**
     * 查找产品的web端展示内容
     * @param product 产品ID
     * @return 产品现实信息
     */
    public List<Details> findByWebItem(int product) {
        return find(SqlKit.sql("details.findByWebItem"), product, Const.YES);
    }

    /**
     *风险提示内容
     * @param product_id 产品id
     * @return 风险提示文档
     */
    public Details findRiskByProduct(int product_id) {
        return findFirst(SqlKit.sql("details.findRiskByProduct"),product_id);
    }
}
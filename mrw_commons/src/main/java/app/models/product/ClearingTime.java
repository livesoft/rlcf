package app.models.product;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;


/**
 * <p>
 * 产品结算时间
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlp_clearing_time", pkName = Const.FIELD_PRODUCT)
public class ClearingTime extends Model<ClearingTime> {

    public static final ClearingTime dao = new ClearingTime();

    private static final long serialVersionUID = -498349554569924853L;
}

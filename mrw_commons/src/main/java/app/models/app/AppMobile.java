/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p>
 * The table rld_appmobile mapping model.
 * </p>
 */
@TableBind(tableName = "rld_appmobile")
public class AppMobile extends Model<AppMobile> {

    /**
     * The public dao.
     */
    public static final  AppMobile dao              = new AppMobile();

    private static final long      serialVersionUID = 188568625506097804L;


    public AppMobile findByUUID(String uuid) {
        return findFirst(SqlKit.sql("appmobile.findByUUID"), uuid);
    }
}
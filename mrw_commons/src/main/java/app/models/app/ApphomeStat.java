/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rld_apphome_stat mapping model.
 * </p>
 */
@TableBind(tableName = "rld_apphome_stat")
public class ApphomeStat extends Model<ApphomeStat> {

    /**
     * The public dao.
     */
    public static final ApphomeStat dao = new ApphomeStat();

    private static final long serialVersionUID = 9104631245959719168L;


    public List<ApphomeStat> findWithList() {
        return find(SqlKit.sql("apphomestat.findApphomestatWithList"),true);
    }
}
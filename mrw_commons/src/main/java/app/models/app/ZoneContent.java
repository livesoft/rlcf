/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rld_zone_content mapping model.
 * </p>
 */
@TableBind(tableName = "rld_zone_content")
public class ZoneContent extends Model<ZoneContent> {

    /**
     * The public dao.
     */
    public static final ZoneContent dao = new ZoneContent();


    private static final long serialVersionUID = -616502129132572535L;

    public List<ZoneContent> findByGroup(int zone_id) {
        return find(SqlKit.sql("zoneContent.findByGroupAndEnable"), zone_id, true);
    }
}
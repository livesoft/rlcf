/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.FindBy;

import java.util.List;

/**
 * <p>
 * The table rld_product_recommend mapping model.
 * </p>
 */
@TableBind(tableName = "rld_product_recommend")
public class ProductRecommend extends Model<ProductRecommend> {

    /**
     * The public dao.
     */
    public static final ProductRecommend dao = new ProductRecommend();


    private static final long serialVersionUID = -1091825939355502068L;

    public List<ProductRecommend> findAll() {
        return find(SqlKit.sql("appmobile.findProductAll"), true);
    }
}
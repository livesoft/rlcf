/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rld_top_shuffling mapping model.
 * </p>
 */
@TableBind(tableName = "rld_top_shuffling")
public class TopShuffling extends Model<TopShuffling> {

    /**
     * The public dao.
     */
    public static final TopShuffling dao = new TopShuffling();

    private static final long serialVersionUID = 6769064354645012085L;


    public List<TopShuffling> findBySection(int section) {
        return find(SqlKit.sql("topshuffling.findTopShufflingBySection"), true, section);
    }

    /**
     * 状态设置
     * @param set_val 设置值
     * @param id id
     * @return 更新结果
     */
    public boolean updateStatus(String set_val, String id) {
        int update = Db.update(SqlKit.sql("topshuffling.updateStatus"), set_val, id);
        return update > 0;
    }
}
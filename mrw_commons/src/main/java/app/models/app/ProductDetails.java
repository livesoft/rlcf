/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.app;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rld_product_details mapping model.
 * </p>
 */
@TableBind(tableName = "rld_product_details")
public class ProductDetails extends Model<ProductDetails> {

    /**
     * The public dao.
     */
    public static final ProductDetails dao = new ProductDetails();

    private static final long serialVersionUID = -6524086328300372633L;


    public List<ProductDetails> findbyProduct(int product_id) {
        return find(SqlKit.sql("productdetail.findbyproduct"), product_id);
    }
}
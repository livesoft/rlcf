package app.models.order;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import org.joda.time.DateTime;

/**
 * <p>
 * The database rlo_account_amount Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlo_account_amount")
public class AccountAmount extends Model<AccountAmount> {

    /**
     * The public dao.
     */
    public static final AccountAmount dao = new AccountAmount();


    private static final long serialVersionUID = 6237340830658846420L;


    public AccountAmount findByDate(DateTime time) {
        DateTime start = time.millisOfDay().withMinimumValue();
        DateTime end = time.millisOfDay().withMaximumValue();
        return findFirst(SqlKit.sql("tradeamount.findByDate"), start, end);
    }

}

package app.models.order;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The database rlo_trademoney_record Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlo_trademoney_record")
public class TradeMoneyRecord extends Model<TradeMoneyRecord> {

    /**
     * The public dao.
     */
    public static final TradeMoneyRecord dao = new TradeMoneyRecord();


    private static final long serialVersionUID = 4229517584319783560L;


    public List<TradeMoneyRecord> records(int tradeMoneyId){
        return find(SqlKit.sql("trademoney.recordByTrademoney"), tradeMoneyId);
    }
}
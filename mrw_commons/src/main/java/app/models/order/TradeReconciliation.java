package app.models.order;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

import java.util.List;

/**
 * <p>
 * 结算金额调账记录
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlo_trade_reconciliation")
public class TradeReconciliation extends Model<TradeReconciliation>{
    public static final TradeReconciliation dao = new TradeReconciliation();
    private static final long serialVersionUID = -2696149019080219009L;

}

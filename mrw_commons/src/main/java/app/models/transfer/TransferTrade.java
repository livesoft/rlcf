/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.transfer;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * <p>
 * The table rlt_tranfer_trade mapping model.
 * </p>
 */
@TableBind(tableName = "rlt_tranfer_trade")
public class TransferTrade extends Model<TransferTrade> {

    /**
     * The public dao.
     */
    public static final TransferTrade dao = new TransferTrade();


    private static final long serialVersionUID = -2403194793747593985L;


    /**
     * @param tranfer   转让产品
     * @param product   产品
     * @param assignor  转让方
     * @param assignee  受让方/购买方
     * @param amount    交易金额
     * @param tradeDate 交易时间
     * @return 转让交易
     */
    public static TransferTrade create(long tranfer, int product,
                                       int assignor, int assignee,
                                       BigDecimal amount, DateTime tradeDate) {
        TransferTrade transferTrade = new TransferTrade();
        transferTrade.set("tranfer", tranfer);
        transferTrade.set("product", product);
        transferTrade.set("assignor", assignor);
        transferTrade.set("assignee", assignee);
        transferTrade.set("amount", amount);
        transferTrade.set("trade_time", tradeDate);
        transferTrade.set("create_time", tradeDate);
        return transferTrade;

    }
}
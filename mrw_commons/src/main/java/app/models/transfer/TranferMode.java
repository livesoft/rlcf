/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.transfer;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;

/**
 * <p>
 * The table rlt_tranfer_mode mapping model.
 * </p>
 */
@TableBind(tableName = "rlt_tranfer_mode" ,pkName = "tranfer")
public class TranferMode extends Model<TranferMode> {

    /**
     * The public dao.
     */
    public static final TranferMode dao = new TranferMode();


    private static final long serialVersionUID = -8975279578809651945L;
}
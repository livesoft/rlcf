package app.models.transfer;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.Logger;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.math.BigDecimal;

/**
 * <p>
 * The database RLT_TRANSFER_FEES Model.
 * </p>
 *
 * 转让规则
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlt_transfer_rule")
public class TransferRule extends Model<TransferRule> {

    /**
     * The public dao.
     */
    public static final TransferRule dao = new TransferRule();


    public static final String DEFAULT_RULE_ID = "transfer_rule_id";


    private static final long serialVersionUID = -7020483532914285205L;


    public TransferRule findDefault() {
        TransferRule default_rule = findFirst(SqlKit.sql("transfer_product.findByDefaultRule"), DEFAULT_RULE_ID);
        if (default_rule == null) {
            Logger.error("默认转让业务规则不存在，请检查");
        }
        return default_rule;
    }


    /**
     * 计算手续费
     * @param price 价格
     * @param transferRule 手续费规则
     * @return 手续费
     */
    public static BigDecimal transferFee(BigDecimal price, TransferRule transferRule) {
        BigDecimal ratio_percent = transferRule.getBigDecimal("ratio").divide(Const.HUNDRED, 4, BigDecimal.ROUND_HALF_UP);
        return price.multiply(ratio_percent).setScale(2,BigDecimal.ROUND_HALF_UP);
    }


}
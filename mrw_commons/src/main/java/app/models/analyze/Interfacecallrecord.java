/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.analyze;

import bank.BankConstant;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import org.joda.time.DateTime;

/**
 * <p>
 * The table rla_interfacecallrecord mapping model.
 * </p>
 */
@TableBind(tableName = "rla_interfacecallrecord")
public class Interfacecallrecord extends Model<Interfacecallrecord> {

    /**
     * The public dao.
     */
    public static final Interfacecallrecord dao = new Interfacecallrecord();

    private static final long serialVersionUID = 1136443705980424221L;


    public static Interfacecallrecord createRecord(String serviceName, String memberCode, DateTime time,String param) {

        Interfacecallrecord interfacecallrecord = new Interfacecallrecord();
        interfacecallrecord.set("name", BankConstant.BANKSERVICENAME.get(serviceName));
        interfacecallrecord.set("url", serviceName);
        interfacecallrecord.set("call_year", time.getYear());
        interfacecallrecord.set("call_month", time.getMonthOfYear());
        interfacecallrecord.set("call_day", time.getDayOfMonth());
        interfacecallrecord.set("call_time", time);
        interfacecallrecord.set("member_code", memberCode);
        interfacecallrecord.set("params", param);

        return interfacecallrecord;
    }


    public void setResponse(String response) {
        this.set("response", response);
    }

    public void setFail(String exception) {
        this.set("error", 1);
        this.set("exp_txt", exception);
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.analyze;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import org.joda.time.DateTime;

/**
 * <p>
 * The table rla_loginlog mapping model.
 * </p>
 */
@TableBind(tableName = "rla_loginlog")
public class Loginlog extends Model<Loginlog> {

    /**
     * The public dao.
     */
    public static final Loginlog dao = new Loginlog();

    private static final long serialVersionUID = 5666560088908376927L;


    public boolean record(int member_id, String member_name, String ip, int status, int integral) {
        return record(member_id, member_name, ip, status, Const.NO, integral);
    }

    public boolean record(int member_id, String member_name, String ip, int status) {
        return record(member_id, member_name, ip, status, Const.YES, 0);
    }

    public boolean record(int member_id, String member_name, String ip, int status, String sys_flag, int integral) {
        Loginlog loginlog = new Loginlog();
        loginlog.set("username", member_name);
        loginlog.set("user_id", member_id);
        loginlog.set("ip", ip);
        loginlog.set(Const.FIELD_STATUS, status);
        loginlog.set("sys_flag", sys_flag);
        DateTime now = DateTime.now();
        loginlog.set("login_time", now);
        loginlog.set("login_year", now.getYear());
        loginlog.set("login_month", now.getMonthOfYear());
        loginlog.set("login_day", now.getDayOfMonth());
        loginlog.set(Const.FIELD_INTEGRAL, integral);
        return loginlog.save();
    }
}
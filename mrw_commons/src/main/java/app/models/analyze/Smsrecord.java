/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.analyze;

import app.Const;
import app.dtos.SmsDto;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rla_smsrecord mapping model.
 * </p>
 */
@TableBind(tableName = "rla_smsrecord")
public class Smsrecord extends Model<Smsrecord> {

    /**
     * The public dao.
     */
    public static final Smsrecord dao = new Smsrecord();


    private static final long serialVersionUID = 145950002104254573L;

    public List<Smsrecord> findExcelExport() {
        return find(SqlKit.sql("smsrecord.findExcelExport"));
    }


    public boolean record(SmsDto smsDto){
        Smsrecord record = new Smsrecord();
        record.set("sender", smsDto.sender);
        record.set("phone", smsDto.phone);
        record.set("content", smsDto.content);
        record.set(Const.FIELD_STATUS, smsDto.status);
        record.set("call_year", smsDto.sendTime.getYear());
        record.set("call_month", smsDto.sendTime.getMonthOfYear());
        record.set("call_day", smsDto.sendTime.getDayOfMonth());
        record.set("dateline", smsDto.sendTime.toDate());
        record.set("send_date", smsDto.sendDate);
        record.set("sendid", smsDto.sendid);
        record.set("invalid", smsDto.invalid);
        record.set("success", smsDto.success);
        record.set("black", smsDto.black);
        record.set("msg", smsDto.msg);
        record.set("extno", smsDto.extno);
        record.set("response", smsDto.response);
        record.set("error", smsDto.error);
//        record.set("exception", smsDto.exception);
        record.set("source", smsDto.source);
        record.set("retry", 0);
        return record.save();
    }
}
package app.models.analyze;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;

/**
 * <p>
 * The database rla_product_access Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rla_product_access")
public class ProductAccess extends Model<ProductAccess> {

    /**
     * The public dao.
     */
    public static final ProductAccess dao = new ProductAccess();


    private static final long serialVersionUID = 2766109477878131647L;
}
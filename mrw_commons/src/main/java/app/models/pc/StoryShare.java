package app.models.pc;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * 理财分享
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlb_story_share")
public class StoryShare extends Model<StoryShare> {

    /**
     * The public dao.
     */
    public static final StoryShare dao = new StoryShare();

    private static final long serialVersionUID = -2366119217599582455L;
    public List<StoryShare> getAll(){
        return StoryShare.dao.find(SqlKit.sql("share.share_stroy"));
    }
}

package app.models.pc;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * <p/>
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlb_indexpic")
public class IndexPic extends Model<IndexPic> {
    private static final long serialVersionUID = -5434437731016211505L;
    public static final IndexPic dao = new IndexPic();
    public List<IndexPic> getAll(){
        return find(SqlKit.sql("index.indexpics"));
    }
}

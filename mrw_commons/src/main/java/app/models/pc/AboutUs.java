package app.models.pc;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

import java.util.List;

/**
 * <p>
 * PC关于我们
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rld_about_us")
public class AboutUs extends Model<AboutUs> {

    public static final AboutUs dao = new AboutUs();
    private static final long serialVersionUID = 925918823593287726L;

    public List<AboutUs> findAll() {
        return find(SqlKit.sql(""));
    }

    public List<AboutUs> findAllType() {
        return find(SqlKit.sql("aboutus.findAllType"));
    }

    public AboutUs findByType(String type) {
        return findFirst(SqlKit.sql("aboutus.findByType"),type);
    }
}

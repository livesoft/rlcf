package app.models.member;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p>
 *  产品赎回记录MODLE
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlm_redeem_product", pkName = "member_product")
public class RedeemProduct extends Model<RedeemProduct> {

    /**
     * The public dao.
     */
    public static final RedeemProduct dao = new RedeemProduct();


    private static final long serialVersionUID = 3144970430155242448L;


    /**
     * 根据会员产品 id 查询会员产品信息
     * @param memberProductId 会员产品id
     * @param memberId        会员ID
     * @return 赎回纪录
     */
    public RedeemProduct findByMemberProduct(int memberProductId, int memberId) {
        return findFirst(SqlKit.sql("redeemproduct.findByMemberProduct"), memberProductId, memberId);
    }

}
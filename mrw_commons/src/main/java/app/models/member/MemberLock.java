package app.models.member;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import org.joda.time.DateTime;

/**
 * <p>
 * The database rlm_member_lock Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlm_member_lock")
public class MemberLock extends Model<MemberLock> {

    /**
     * The public dao.
     */
    public static final MemberLock dao = new MemberLock();


    private static final long serialVersionUID = 5339952463127479816L;


    public boolean record(int member, String ip){
        MemberLock memberLock = new MemberLock();
        memberLock.set(Const.FIELD_MEMBER, member);
        memberLock.set("ip", ip);
        memberLock.set("lock_time", DateTime.now());
        return memberLock.save();
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import app.Const;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p>
 * The table rlm_identity mapping model.
 * </p>
 */
@TableBind(tableName = "rlm_identity",pkName= Const.FIELD_MEMBER)
public class Identity extends Model<Identity> {

    /**
     * The public dao.
     */
    public static final Identity dao = new Identity();

    private static final long serialVersionUID = 1176491183009880551L;

    /**
     * 审核通过
     * @param member_id 会员id
     */
    public boolean auditOk(String member_id) {
        int update = Db.update(SqlKit.sql("identity.auditOk"), member_id);
        return update > 0;
    }

}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * 会员邀请
 * </p>
 */
@TableBind(tableName = "rlm_invite")
public class Invite extends Model<Invite> {

    /**
     * The public dao.
     */
    public static final Invite dao = new Invite();

    public List<Invite> findByMember(int memberId){
        return find(SqlKit.sql("invite.findByMember"),memberId);
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;

/**
 * <p>
 * The table rlm_security mapping model.
 * </p>
 */
@TableBind(tableName = "rlm_security")
public class Security extends Model<Security> {

    /**
     * The public dao.
     */
    public static final Security dao = new Security();


}
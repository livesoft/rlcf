/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;

/**
 * <p>
 * The table rlm_account mapping model.
 * </p>
 */
@TableBind(tableName = "rld_boards")
public class Boards extends Model<Boards> {

    /**
     * The public dao.
     */
    public static final Boards dao = new Boards();
    private static final long serialVersionUID = 5857876737450679770L;

}
package app.models.member;

import app.Const;
import goja.annotation.TableBind;
import goja.lang.Lang;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

import java.util.List;

/**
 * <p>
 * 产品风险规则表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rls_risk_rule",pkName = "risk_tolerance")
public class RiskRule extends Model<RiskRule>{

    private static final long serialVersionUID = 7276466702087216033L;

    public static final RiskRule dao = new RiskRule();

    public RiskRule findByProductLevel(int risk_level) {
        String riskrule = "riskrule:" + risk_level;
        List<RiskRule> riskRules = findByCache(Const.CACHE_BASICINFOS, riskrule, SqlKit.sql("riskrule.findByProductLevel"), risk_level);
        if (Lang.isEmpty(riskRules)) {
            return null;
        } else {
            return riskRules.get(0);
        }
    }
}

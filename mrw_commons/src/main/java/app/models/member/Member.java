/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import app.Const;
import app.constant.AccountStatus;
import app.constant.MemberConstant;
import app.kit.TypeKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.StringPool;
import goja.annotation.TableBind;
import goja.encry.DigestsKit;
import goja.encry.EncodeKit;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlm_member mapping model.
 * </p>
 */
@TableBind(tableName = "rlm_member")
public class Member extends Model<Member> {

    /**
     * The public dao.
     */
    public static final Member dao = new Member();


    private static final long serialVersionUID = -7772465023770203471L;


    public static final String FIELD_SALT = "salt";
    public static final String FIELD_PASSWORD = "password";

    /**
     * 设置用户密码和盐值.
     *
     * @param plantPwd 明文密码
     */
    public void generatePassword(String plantPwd) {
        byte[] salt = DigestsKit.generateSalt(EncodeKit.SALT_SIZE);
        this.set(FIELD_SALT, EncodeKit.encodeHex(salt));
        byte[] hashPassword = DigestsKit.sha1(plantPwd.getBytes(), salt, EncodeKit.HASH_INTERATIONS);
        this.set(FIELD_PASSWORD, EncodeKit.encodeHex(hashPassword));
    }

    public Member findbyPhone(String phone) {
        return findFirst(SqlKit.sql("member.findByPhone"), phone);
    }

    public Member findByLogin(String name) {
        return findFirst(SqlKit.sql("member.findByLogin"), name, name);
    }

    public Member findByIdWithShowCenter(int id) {
        return findFirst(SqlKit.sql("member.findByIdWithShowCenter"), id, AccountStatus.NORMAL, MemberConstant.NORMAL);
    }



    /**
     * 更新会员的状态
     * @param set_val 需更新的状态值
     * @param id 被更新的会员id
     * @return 更新条数
     */
    public boolean updateStatus(String set_val, String id) {
        int update = Db.update(SqlKit.sql("member.updateStatus"), set_val, id);
        return update > 0;
    }

    /**
     * 更新会员的实名认证状态
     * @param member_id 会员Id
     * @return 是否更新成功
     */
    public boolean auditOk(String member_id) {
        int update = Db.update(SqlKit.sql("member.auditOk"), member_id);
        return update > 0;
    }

    /**
     * 查询所有正常的用户ID和姓名
     * @return 用户列表
     */
    public List<Member> findIdAndNameByStatus(){
        return find(SqlKit.sql("member.findIdAndNameByStatus"),AccountStatus.NORMAL);
    }

    /**
     * 导出的会员列表
     * @return
     */
    public List<Member> findExcelExport() {
        return find(SqlKit.sql("member.findExcelExport"));
    }

    public int findAvailableCount() {
        Member member = findFirst(SqlKit.sql("member.findAvailableCount"));
        return TypeKit.getInt(member, Const.FIELD_CNT);
    }

    public int getId() {
        return TypeKit.getInt(this, StringPool.PK_COLUMN);
    }


    public Member findByUsername(String username) {
        return findFirst(SqlKit.sql("member.findByUsername"), username);
    }
}
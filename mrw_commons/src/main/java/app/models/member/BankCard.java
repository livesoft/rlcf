/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.member;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlm_bank_card mapping model.
 * </p>
 */
@TableBind(tableName = "rlm_bank_card")
public class BankCard extends Model<BankCard> {

    /**
     * The public dao.
     */
    public static final BankCard dao = new BankCard();


    private static final long serialVersionUID = 4803523810716220778L;

    public List<BankCard> findByMember(int memberId) {
        return find(SqlKit.sql("member.bankcardByMember"),memberId);
    }


    /**
     * 指定账户（会员）的银行卡总数
     * @param member_id 账户（会员）id
     * @return 信用卡数量
     */
    public long getTotSizeByAccount(String member_id) {
        BankCard bankCard = findFirst(SqlKit.sql("bankcard.getTotSizeByAccount"), member_id);
        return bankCard.getNumber("cnt").longValue();
    }
}
package app.models.member;

import app.Const;
import app.kit.CommonKit;
import app.kit.TypeKit;
import com.jfinal.plugin.activerecord.Page;
import goja.annotation.TableBind;
import goja.date.DateFormatter;
import goja.lang.Lang;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;
import goja.tuples.Pair;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import static goja.StringPool.EMPTY;

/**
 * <p>  </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlm_profit_details")
public class ProfitDetails extends Model<ProfitDetails>{
    private static final long serialVersionUID = 7822728596164337868L;

    public static final ProfitDetails dao = new ProfitDetails();

    public static final int STATUS_OK   = 1;
    public static final int STATUS_WAIT = 0;



    /**
     * 创建收益详情纪录
     *
     * @param memberId        会员ID
     * @param product         产品ID
     * @param memberProductId 会员产品购买ID
     * @param profit          收益
     * @param now             时间
     * @param principal       本金
     * @return 收益详情
     */
    public ProfitDetails onetimePrifit(int memberId, int product, int memberProductId, BigDecimal profit, DateTime now, BigDecimal principal) {
        ProfitDetails profitDetails = new ProfitDetails();
        profitDetails.set(Const.FIELD_MEMBER,memberId);
        profitDetails.set(Const.FIELD_PRODUCT,product);
        profitDetails.set("member_product",memberProductId);
        profitDetails.set("profit",profit);
        profitDetails.set("principal",principal);
        profitDetails.set("profit_time", now);
        profitDetails.set("profit_time_year", now.getYear());
        profitDetails.set("profit_time_month",now.getMonthOfYear());
        profitDetails.set("profit_time_day",now.getDayOfMonth());
        profitDetails.set(Const.FIELD_STATUS, STATUS_WAIT);
        return profitDetails;
    }

    /**
     *根据会员查询收益记录 分页查询
     *  type 1:表示没有时间，2，三个月，3半年内
     */
    public Page<ProfitDetails> findByMember(String member_id,int page,int pageSize,int type){
        int total = 0;
        final int totalPage;
        final Pair<Integer,Integer> offsets = CommonKit.pageOffset(page, pageSize);
        List<ProfitDetails> productMoneys = null;
        DateTime now = DateTime.now();
        String where = " AND pm.profit_time >= to_date(?,'yyyy-mm-dd,hh24:mi:ss')";
        String sql_tmp = SqlKit.sql("memberproduct.findByMemeberCount");
        String data_sql_tpl = SqlKit.sql("memberproduct.findByMemeber");
        if(type==1){
            ProfitDetails memberCount = findFirst(String.format(sql_tmp, EMPTY), member_id, STATUS_OK);
            total = TypeKit.getInt(memberCount, "cnt");
            productMoneys = find(String.format(data_sql_tpl, EMPTY), member_id, STATUS_OK, offsets.getValue1(), offsets.getValue0());
        }else if(type==2){
            final DateTime month_time = now.plusDays(-90);
            String lastT = month_time.toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD_HH_MM_SS);
            ProfitDetails timeMemberCount = findFirst(String.format(sql_tmp, where), member_id, STATUS_OK,lastT);
            total = TypeKit.getInt(timeMemberCount, "cnt");
            productMoneys = find(String.format(data_sql_tpl, where), member_id, STATUS_OK,lastT, offsets.getValue1(), offsets.getValue0());
        }else if(type==3){
            final DateTime month_time = now.plusDays(-180);
            String lastT = month_time.toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD_HH_MM_SS);
            final ProfitDetails profitDetails = findFirst(String.format(sql_tmp, where), member_id, STATUS_OK,lastT);
            total = TypeKit.getInt(profitDetails, "cnt");
            productMoneys = find(String.format(data_sql_tpl, where), member_id, STATUS_OK,lastT, offsets.getValue1(), offsets.getValue0());
        }
        if(total%pageSize!=0){
            totalPage = total/pageSize+1;
        }else{
            totalPage = total/pageSize;
        }
        return new Page<ProfitDetails>(productMoneys, page, pageSize, totalPage, total);
    }
    /*
    根据会员查询收益总额
     */
    public BigDecimal findAmountByMember(String member_id){
        List<ProfitDetails> profitDetailses = find(SqlKit.sql("memberproduct.findAmountByMember"), member_id, STATUS_OK);
        if (Lang.isEmpty(profitDetailses)) {
            return BigDecimal.ZERO;
        }
        BigDecimal money = profitDetailses.get(0).getBigDecimal("money");
        if (money != null) {
            return money;
        }else{
            return BigDecimal.ZERO;
        }
    }

    /*
    会员号和日期查询收益额
     */
    public BigDecimal findAmountByMembeAndDate(int member_id, DateTime start, DateTime end) {
        String sql = SqlKit.sql("memberproduct.findAmountByMemberAndDate");
        List<ProfitDetails> profitDetailses = find(sql, member_id, start, end, STATUS_OK);
        if (Lang.isEmpty(profitDetailses)) {
            return BigDecimal.ZERO;
        }
        ProfitDetails profitDetails = profitDetailses.get(0);
        BigDecimal money = profitDetails.getBigDecimal("money");
        if (money != null) {
            return money;
        }else{
            return BigDecimal.ZERO;
        }

    }

    public Double getSumProfit() {
        ProfitDetails first= findFirst(SqlKit.sql("profit_detail.getSumProfit"));
        return TypeKit.getDouble(first, "profit_sum");
    }

    public List<ProfitDetails> profitChat() {
        return find(SqlKit.sql("profit_detail.profitChat"));
    }

    public ProfitDetails findByMemberProduct(int memberProductId, int memberId, int productId) {
        return findFirst(SqlKit.sql("profit_detail.findByMemberProduct"), memberProductId, memberId, productId, 0);
    }
}

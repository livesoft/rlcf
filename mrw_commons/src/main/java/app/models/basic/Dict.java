/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import app.Const;
import app.constant.DictConstant;
import app.dtos.ZTreeDto;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import goja.StringPool;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

import static app.Const.FIELD_NAME;
import static goja.StringPool.EMPTY;

/**
 * <p>
 * The table rlb_dict mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_dict")
public class Dict extends Model<Dict> {

    /**
     * The public dao.
     */
    public static final Dict dao = new Dict();


    private static final long serialVersionUID = 6912968613528093336L;


    String DICT_DATATYPE   = "datatype";
    //收益方式
    String COLLECTION_MODE = Const.FIELD_COLLECTION_MODE;
    //风险承受能力
    String RISK_TOLERANCE  = Const.FIELD_RISK_TOLERANCE;
    //风险等级
    String RISK_LEVEL      = "risk_level";
    //PC端关于我们的子菜单
    String ABOUTUS         = "aboutus";

    public List<Dict> findByCategory(String category) {
        return find(SqlKit.sql("dict.findByCategory"), category);
    }

    public Dict findByCategoryAndCode(String category, Object code) {
        return findFirst(SqlKit.sql("dict.findByCategoryAndCode"), category, code);
    }

    /**
     * 数据类型
     *
     * @return 字典数据
     */
    public List<Dict> dataTypes() {
        return findByCategory(DICT_DATATYPE);
    }

    /**
     * 收益方式
     *
     * @return 字典数据
     */
    public List<Dict> collection_mode() {
        return findByCategory(COLLECTION_MODE);
    }

    /**
     * 收益方式
     *
     * @return 字典数据
     */
    public String collection_mode_map(int collection_mode) {
        Dict collectionMode = findByCategoryAndCode(COLLECTION_MODE, collection_mode);
        return collectionMode != null ? collectionMode.getStr(FIELD_NAME) : EMPTY;
    }


    /**
     * PC端 关于我们的子菜单
     *
     * @return 字典数据
     */
    public List<Dict> aboutus() {
        return findByCategory(ABOUTUS);
    }

    /**
     * 风险等级
     *
     * @return 字典数据
     */
    public List<Dict> risk_level() {
        return findByCategory(RISK_LEVEL);
    }

    /**
     * 期限单位
     *
     * @return 字典数据
     */
    public static String getTimeLimit(String code) {
        if (Strings.isNullOrEmpty(code)) {
            return EMPTY;
        }
        if (StringUtils.equals(DictConstant.DAY_UNIT, code)) {
            return DictConstant.DAY_CHINESS;
        } else if (StringUtils.equals(DictConstant.MONTH_UNIT, code)) {
            return DictConstant.MONTH_CHINESS;
        } else {
            return EMPTY;
        }
    }

    /**
     * 风险承受能力
     *
     * @return 风险承受
     */
    public List<Dict> risk_tolerance() {
        return findByCategory(RISK_TOLERANCE);
    }

    /**
     * 风险承受能力
     *
     * @return 风险承受能力Map
     */
    public String getRiskTolerance(int level) {
        Dict risk_tolerance = findByCategoryAndCode(RISK_TOLERANCE, level);
        return risk_tolerance == null ? EMPTY : risk_tolerance.getStr(FIELD_NAME);
    }

    /**
     * 风险等级
     *
     * @param level 风险等级
     * @return 风险等级
     */
    public String getRiskLevel(int level) {
        Dict risk_level = findByCategoryAndCode(RISK_LEVEL, level);
        return risk_level == null ? EMPTY : risk_level.getStr(FIELD_NAME);
    }

    public List<ZTreeDto> findByParent(String parent) {
        List<Dict> dicts = find(SqlKit.sql("dict.findByParent"), parent);
        final List<ZTreeDto> treeDtos = Lists.newArrayList();
        ZTreeDto<Dict> zTreeDto;
        for (Dict dict : dicts) {
            zTreeDto = new ZTreeDto<Dict>();
            zTreeDto.id = dict.getStr(StringPool.PK_COLUMN);
            zTreeDto.name = dict.getStr(FIELD_NAME);
            zTreeDto.isParent = dict.valBoolean("children_flag");
            zTreeDto.data = dict;
            treeDtos.add(zTreeDto);
        }
        return treeDtos;
    }


    public List<Dict> findAll() {
        return find(SqlKit.sql("dict.findAll"));
    }

    /**
     * 获取字典表的Json串
     *
     * @return
     */
    public Map<String, Map<String, Dict>> getDictMaps() {
        Map<String, Map<String, Dict>> dictMaps = Maps.newHashMap();
        Map<String, Dict> parentMaps = Maps.newHashMap();
        try {
            List<Dict> dicts = Dict.dao.findAll();
            Map<String, Dict> childrens;

            for (int i = 0; i < dicts.size(); i++) {
                Dict dict = dicts.get(i);
                final String parent = dict.getStr("parent");
                if (Strings.isNullOrEmpty(parent) || "0".equals(parent)) {
                    childrens = Maps.newHashMap();
                    dictMaps.put(dict.getStr(Const.FIELD_CODE), childrens);
                    for (Dict d : dicts) {
                        if (d.getStr("parent").equals(dict.getStr("id"))) {
                            if (d.getStr("parent").equals(dict.getStr("id"))) {
                                childrens.put(d.getStr(Const.FIELD_CODE), d);
                            }
                        }
                    }
                }
            }

        } finally {
            parentMaps.clear();
        }
        return dictMaps;
    }


}
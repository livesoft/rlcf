/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_risk_category mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_risk_category")
public class RiskCategory extends Model<RiskCategory> {

    /**
     * The public dao.
     */
    public static final RiskCategory dao = new RiskCategory();


    private static final long serialVersionUID = -7959227990294936581L;

    /**
     * 所有的风险分类
     * @return 风险分类
     */
    public List<RiskCategory> findAll() {
        return find(SqlKit.sql("riskcategory.findAll"));
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_notice mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_notice")
public class Notice extends Model<Notice> {

    /**
     * The public dao.
     */
    public static final Notice dao = new Notice();


    private static final long serialVersionUID = 5879502009112772896L;

    public List<Notice> findByEnable() {
        return find(SqlKit.sql("notice.findByStatus"), true);
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

/**
 * <p>
 * 结算时间默认规则
 * </p>
 */
@TableBind(tableName = "rlo_clearing_timerule")
public class ClearingRule extends Model<ClearingRule> {

    /**
     * The public dao.
     */
    public static final ClearingRule dao = new ClearingRule();


    private static final long serialVersionUID = 7479664624531262409L;

    /**
     * 根据收益方式获取对应数据
     * @param product_type 收益方式
     */
    public ClearingRule getByProductType(int product_type){
        return findFirst(SqlKit.sql("clearingRule.getByProductType"), product_type);
    }
}
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_risk_options mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_risk_options")
public class RiskOptions extends Model<RiskOptions> {

    /**
     * The public dao.
     */
    public static final RiskOptions dao = new RiskOptions();


    private static final long serialVersionUID = 6327807227927666585L;

    public List<RiskOptions> findByProblem(int problem) {
        return find(SqlKit.sql("riskoption.findByProblem"), problem);
    }
}
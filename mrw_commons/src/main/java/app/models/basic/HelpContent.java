package app.models.basic;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

import java.util.List;

/**
 * <p>
 * 帮助导航
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlb_help_conent")
public class HelpContent extends Model<HelpContent> {

    public static final HelpContent dao = new HelpContent();

    private static final long serialVersionUID = 7459362219191643025L;

    public HelpContent findDetailById(String id) {
        return findFirst(SqlKit.sql("helpcontent.findDetailById"),id);
    }

    public List<HelpContent> findbyNav(String childnav_id) {
        return find(SqlKit.sql("helpcontent.findbyNav"), childnav_id);
    }
}

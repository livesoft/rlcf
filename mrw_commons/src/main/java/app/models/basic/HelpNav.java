package app.models.basic;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

import java.util.List;

/**
 * <p>
 * 帮助导航
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlb_help_nav")
public class HelpNav extends Model<HelpNav> {
    private static final long serialVersionUID = -3790681400057453635L;

    public static final HelpNav dao = new HelpNav();

    /**
     * 所有父类的帮助标题
     */
    public List<HelpNav> findParent() {
        return find(SqlKit.sql("helpnav.findParent"));
    }

    public List<HelpNav> findAll() {
        return find(SqlKit.sql("helpnav.findAll"));
    }

    public List<HelpNav> findChildsById(String id) {
        return find(SqlKit.sql("helpnav.findChildsById"),id) ;
    }

    public List<HelpNav> findChild() {
        return find(SqlKit.sql("helpnav.findChild"));
    }

    public List<HelpNav> findByParent(int parent_id) {
        return find(SqlKit.sql("helpnav.findByParent"));
    }
}

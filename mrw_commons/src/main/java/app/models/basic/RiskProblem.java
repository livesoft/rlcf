/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_risk_problem mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_risk_problem")
public class RiskProblem extends Model<RiskProblem> {

    /**
     * The public dao.
     */
    public static final RiskProblem dao = new RiskProblem();


    private static final long serialVersionUID = 7434252946479032072L;

    public List<RiskProblem> findByCategory(String category) {
        return find(SqlKit.sql("riskproblem.findByCategory"), category);
    }

    public List<RiskProblem> findAll(){
        return find(SqlKit.sql("riskproblem.findAll"));
    }

}
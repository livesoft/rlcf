/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import app.Const;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import goja.Func;
import goja.StringPool;
import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.storage.FileDto;
import goja.security.shiro.AppUser;
import org.joda.time.DateTime;

import java.util.List;

import static goja.StringPool.COMMA;
import static goja.StringPool.QUESTION_MARK;

/**
 * <p>
 * The table rlb_attachment mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_attachment")
public class Attachment extends Model<Attachment> {

    /**
     * The public dao.
     */
    public static final Attachment dao = new Attachment();


    private static final long serialVersionUID = -7995963617987243326L;

    public static final Function<Attachment, FileDto> ATTACHMENT_FILE_DTO_FUNCTION = new Function<Attachment, FileDto>() {
        @Override
        public FileDto apply(Attachment input) {
            final FileDto fileInfo = FileDto.createFileInfo(input.getStr("url"), input.getStr("folder"), input.getStr("path"));
            fileInfo.setId(input.getNumber(StringPool.PK_COLUMN).intValue());
            fileInfo.setTitle(input.getStr("file_name"));
            return fileInfo;
        }
    };

    public Attachment record(FileDto fileDto, AppUser<Model> appUser, long file_size, String name, String biz_mode) {
        final Attachment attachment = new Attachment();
        attachment.set("width", 0)
                .set("dateline", DateTime.now().toDate())
                .set("file_name", name)
                .set("file_type", fileDto.getExt())
                .set("file_size", file_size)
                .set("url", fileDto.toString())
                .set("downloads", 0)
                .set("image", false)
                .set("user_id", appUser.getId())
                .set("thumb", 0)
                .set("folder", fileDto.getFolder())
                .set("path", fileDto.getStorage())
                .set("remote", false)
                .set(Const.FIELD_STATUS, 1)
                .set("mode_type", biz_mode)
                .save();
        fileDto.setId(attachment.getNumber(StringPool.PK_COLUMN).intValue());
        return attachment;
    }

    public List<FileDto> findByIds(String pks) {
        Preconditions.checkNotNull(pks, "附件主键不能为空");
        List<String> pk_list = Func.COMMA_SPLITTER.splitToList(pks);
        List<String> params = Lists.newArrayListWithCapacity(pk_list.size());
        StringBuilder quote_str = new StringBuilder();
        for (String pk : pk_list) {
            if (quote_str.length() == 0) {
                quote_str.append(QUESTION_MARK);
            } else {
                quote_str.append(COMMA).append(QUESTION_MARK);
            }
            params.add(pk);
        }
        List<Attachment> attachments =  find(SqlKit.sql("attachment.findByIds").replace("$$QUESTION_MARK$$", quote_str), params.toArray());
        return Lists.transform(attachments, ATTACHMENT_FILE_DTO_FUNCTION);
    }

    public List<Attachment> findByPks(String pks) {
        Preconditions.checkNotNull(pks, "附件主键不能为1空");
        List<String> pk_list = Func.COMMA_SPLITTER.splitToList(pks);
        List<String> params = Lists.newArrayListWithCapacity(pk_list.size());
        StringBuilder quote_str = new StringBuilder();
        for (String pk : pk_list) {
            if (quote_str.length() == 0) {
                quote_str.append(QUESTION_MARK);
            } else {
                quote_str.append(COMMA).append(QUESTION_MARK);
            }
            params.add(pk);
        }
        return find(SqlKit.sql("attachment.findByIds").replace("$$QUESTION_MARK$$", quote_str), params.toArray());
    }
}
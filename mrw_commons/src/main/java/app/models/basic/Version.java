/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import goja.annotation.TableBind;
import com.jfinal.plugin.activerecord.Model;

/**
 * <p>
 * The table rlb_version mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_version")
public class Version extends Model<Version> {

    /**
     * The public dao.
     */
    public static final Version dao = new Version();


    private static final long serialVersionUID = -1429053823136664930L;
}
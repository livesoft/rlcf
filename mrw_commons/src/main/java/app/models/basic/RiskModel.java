/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.lang.Lang;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_risk_model mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_risk_model")
public class RiskModel extends Model<RiskModel> {

    /**
     * The public dao.
     */
    public static final RiskModel dao = new RiskModel();


    private static final long serialVersionUID = -2452362412093745905L;

    public List<RiskModel> findByCategory(String category) {
        return find(SqlKit.sql("riskmodel.findByCategory"),category);
    }

    public List<RiskModel> findAll() {
        return find(SqlKit.sql("riskmodel.findAll"));
    }

    /**
     * 根据风险等级代码删除
     * @param s
     * @return
     */
    public boolean deleteByRisk_level(String s) {
        int update = Db.update(SqlKit.sql("riskmodel.deleteByRisk_level"), s);
        return update > 0 ;
    }

    /*
    评估等级
     */
    public String findRisk_level(int score) {
        List<RiskModel> riskModels = find(SqlKit.sql("riskmodel.findByScore"), score, score);
        if (Lang.isEmpty(riskModels)) {
            return "1";
        }
        return riskModels.get(0).getStr("risk_level");
    }
}
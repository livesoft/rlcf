/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.basic;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The table rlb_hotnews mapping model.
 * </p>
 */
@TableBind(tableName = "rlb_hotnews")
public class Hotnews extends Model<Hotnews> {

    /**
     * The public dao.
     */
    public static final Hotnews dao = new Hotnews();


    private static final long serialVersionUID = -46811291968478415L;

    public List<Hotnews> findByEnable() {
        return find(SqlKit.sql("hotnews.findByStatus"), 1);
    }

    public Page<Hotnews> findByPageList(int page,int pageSize) {
        return paginate(page, pageSize, SqlKit.sql("hotnews.select"), SqlKit.sql("hotnews.where"));
    }

    public List<Hotnews> findByHome() {
        return find(SqlKit.sql("hotnews.findByHome"), 1, 5);
    }
}
package app.models.basic;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.lang.Lang;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The database rlb_notes Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rlb_notes")
public class Notes extends Model<Notes> {

    /**
     * The public dao.
     */
    public static final Notes dao = new Notes();


    private static final long serialVersionUID = 8119562707419066960L;

    private static final String NOTE_FIELD = "note";

    public String findByTransfer() {
        return findByCategory(Const.AGREEMENT.TRANSFER);
    }

    public String findByRegister() {
        return findByCategory(Const.AGREEMENT.REGISTER);
    }

    /**
     * 快捷支付协议
     * @return 快捷支付协议
     */
    public String findByFastpayment() {
        return findByCategory(Const.AGREEMENT.FASTPAYMENT);
    }

    /**
     * 风险提示协议
     * @return 风险提示协议
     */
    public String findByRisk() {
        return findByCategory(Const.AGREEMENT.RISK);
    }

    /**
     * 产品购买协议
     *
     * @return 产品购买协议
     */
    public String findByBuy() {
        return findByCategory(Const.AGREEMENT.BUY);
    }

    private String findByCategory(String category) {

        List<Notes> noteses = findByCache(Const.CACHE_NOTES, category, SqlKit.sql("notes.findByCategory"), category);
        if (Lang.isEmpty(noteses)) {
            return null;
        }
        return noteses.get(0).getStr(NOTE_FIELD);
    }
}
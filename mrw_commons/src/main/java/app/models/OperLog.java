package app.models;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import org.joda.time.DateTime;

/**
 * <p>
 * <p/>
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rls_tlog")
public class OperLog extends Model<OperLog> {
    public static final OperLog dao = new OperLog();


    public void log(String content) {
        OperLog operLog = new OperLog();
        operLog.set("module_name", "系统任务");
        operLog.set("content", content);
        operLog.set("record_time", DateTime.now().toDate());
        operLog.save();

    }
}

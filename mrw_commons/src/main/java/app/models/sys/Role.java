package app.models.sys;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The database rls_role Model.
 * 角色表
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rls_role")
public class Role extends Model<Role> {
    /**
     * The public dao.
     */
    public static final Role dao = new Role();


    private static final long serialVersionUID = 3163934163398431558L;

    /**
     * 查询所有角色
     * @return 所有角色
     */
    public List<Role> findIdAndName() {
        return find(SqlKit.sql("role.findIdAndName"));
    }

    public boolean deleteByRole(Object role_id) {
        int update = Db.update(SqlKit.sql("role.deleteByRole"), role_id);
        return update > 0 ;
    }
}

package app.models.sys;

import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;
import goja.rapid.db.Model;

/**
 * <p>
 * The database rls_user Model.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rls_user")
public class User extends Model<User> {

    /**
     * The public dao.
     */
    public static final User dao = new User();


    private static final long serialVersionUID = 3229657292143918088L;

    public User findByLogin(String loginName) {
        return findFirst(SqlKit.sql("user.findByLogin"), loginName);
    }

    public User findByUserName(String username) {
        return findFirst(SqlKit.sql("user.findByLogin"), username);
    }
}
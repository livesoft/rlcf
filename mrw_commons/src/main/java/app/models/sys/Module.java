package app.models.sys;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The database rls_module Model.
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rls_module")
public class Module extends Model<Module> {
    /**
     * The public dao.
     */
    public static final Module dao = new Module();


    private static final long serialVersionUID = 8637676861574892570L;

    public List<Module> findAll() {
        return find(SqlKit.sql("module.findAll"), 1);
    }


    public List<Module> findByRole(String role) {
        return find(SqlKit.sql("module.findByRole"), role, 1);
    }
}

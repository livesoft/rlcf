/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 jfinal app. jfapp Group.
 */

package app.models.sys;

import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

/**
 * <p>
 * The table rls_ismg mapping model.
 * </p>
 */
@TableBind(tableName = "rls_ismg")
public class Ismg extends Model<Ismg> {

    /**
     * The public dao.
     */
    public static final Ismg dao = new Ismg();
    private static final long serialVersionUID = -7006114238729796788L;


    public Ismg findByDefault() {
        return findFirst(SqlKit.sql("ismg.ismgFindByDefault"));
    }
}
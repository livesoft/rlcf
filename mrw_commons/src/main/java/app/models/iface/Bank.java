package app.models.iface;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import goja.annotation.TableBind;
import goja.plugins.sqlinxml.SqlKit;

import java.util.List;

/**
 * <p>
 * The database rli_bank Model.
 * 银行基本信息
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
@TableBind(tableName = "rli_bank")
public class Bank extends Model<Bank> {

    /**
     * The public dao.
     */
    public static final Bank dao = new Bank();


    private static final long serialVersionUID = -6264330769502587640L;


    public List<Bank> findAll() {
        return findByCache("basicinfos", "banks", SqlKit.sql("bank.findAll"), Const.YES);
    }
}
package app.directive;

import goja.StringPool;
import goja.mvc.render.ftl.shiro.permission.PermissionTag;
import org.apache.shiro.subject.Subject;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class HasAnyPermissionsDirective extends PermissionTag {
    @Override
    protected boolean showTagBody(String permissionNames) {
        boolean hasAnyPermission = false;

        Subject subject = getSubject();

        if (subject != null) {
            // Iterate through permissions and check to see if the user has one of the permissions
            for (String permission : permissionNames.split(StringPool.COMMA)) {

                if (subject.isPermitted(permission.trim())) {
                    hasAnyPermission = true;
                    break;
                }

            }
        }

        return hasAnyPermission;
    }
}

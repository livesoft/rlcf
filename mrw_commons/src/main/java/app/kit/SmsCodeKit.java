package app.kit;

import goja.plugins.redis.JedisKit;
import org.apache.commons.lang3.StringUtils;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class SmsCodeKit {

    public static final String REGISTED_PREFIX = "registed:";
    public static final String TRADE_PREFIX    = "trade:";
    public static final String TRANSFER_PREFIX = "transfer:";
    public static final String REDEMPTION_PREFIX = "redemption:";

    static void registed(String phone, String code) {
        // 存入缓存中，设置30分钟为过期时间
        JedisKit.set(REGISTED_PREFIX + phone, code, 30 * 60);
    }

    static void tradepwd(String phone, String code) {
        // 存入缓存中，设置5分钟为过期时间
        JedisKit.set(TRADE_PREFIX + phone, code, 30 * 60);
    }

    static void transfer(String phone, String code) {
        // 存入缓存中，设置5分钟为过期时间
        JedisKit.set(TRANSFER_PREFIX + phone, code, 30 * 60);
    }

    static void redemption(String phone, String code) {
        // 存入缓存中，设置5分钟为过期时间
        JedisKit.set(REDEMPTION_PREFIX + phone, code, 30 * 60);
    }


    /**
     * 验证注册短信码
     * @param phone 电话号码
     * @param code 验证码
     * @return 是否成功
     */
    public static boolean checkRegisted(String phone, String code) {
        final String r_code = JedisKit.get(REGISTED_PREFIX + phone);
        return StringUtils.equals(r_code, code);
    }

    /**
     * 验证赎回产品短信码
     * @param phone 电话号码
     * @param code 验证码
     * @return 是否成功
     */
    public static boolean checkRedemption(String phone, String code) {
        final String r_code = JedisKit.get(REDEMPTION_PREFIX + phone);
        return StringUtils.equals(r_code, code);
    }

    /**
     * 验证交易密码短信码
     * @param phone 电话号码
     * @param code 验证码
     * @return 是否成功
     */
    public static boolean checkTrade(String phone, String code) {
        final String r_code = JedisKit.get(TRADE_PREFIX + phone);
        return StringUtils.equals(r_code, code);
    }
    /**
     * 验证转让短信码
     * @param phone 电话号码
     * @param code 验证码
     * @return 是否成功
     */
    public static boolean checkTransfer(String phone, String code) {
        final String r_code = JedisKit.get(TRANSFER_PREFIX + phone);
        return StringUtils.equals(r_code, code);
    }

    /**
     * 清除注册验证码
     * @param phone 手机号码
     */
    public static void clearRegisted(String phone) {
        JedisKit.del(REGISTED_PREFIX + phone);
    }
    /**
     * 清除交易密码验证码
     * @param phone 手机号码
     */
    public static void clearTrade(String phone) {
        JedisKit.del(TRADE_PREFIX + phone);
    }
    /**
     * 清除转让验证码
     * @param phone 手机号码
     */
    public static void clearTransfer(String phone) {
        JedisKit.del(TRADE_PREFIX + phone);
    }
    /**
     * 清除转让验证码
     * @param phone 手机号码
     */
    public static void clearRedemption(String phone) {
        JedisKit.del(REDEMPTION_PREFIX + phone);
    }
}

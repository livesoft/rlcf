package app.kit;

import app.models.member.Member;
import goja.plugins.redis.JedisKit;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public final class AuthRedisKit {

    public static final  String USER_PREFIX = "rlusers:";
    /**
     * 8个小时
     */
    private static final int    seconds     = 8 * 60 * 60;

    public static void login(int memberId, Member member, String auth_code) {
        JedisKit.set(auth_code, member, seconds);
        JedisKit.set(USER_PREFIX + memberId, auth_code, seconds);
    }

    public static void setMember(Member member, int memberId) {
        String auth_code = JedisKit.get(USER_PREFIX + memberId);
        JedisKit.set(auth_code, member, seconds);
    }


    public static void logout(String authcode, String memberId) {
        JedisKit.del(authcode, USER_PREFIX + memberId);
    }


    public static String getAuthCode(int memberId) {
        return JedisKit.get(USER_PREFIX + memberId);
    }

    public static Member getMember(String authCode) {
        return JedisKit.get(authCode);
    }
}

package app.kit;

import app.Const;
import app.constant.DictConstant;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import goja.GojaConfig;
import goja.StringPool;
import goja.date.DateFormatter;
import goja.libs.Codec;
import goja.tuples.Pair;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;

import java.math.BigDecimal;
import java.util.List;

import static app.Const.DATE_SLASH_FORMMAT;
import static app.Const.DATE_SPLIT;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class CommonKit {


    public static Pair<Integer, Integer> pageOffset(int page) {
        return pageOffset(page, Const.PAGE);
    }

    public static Pair<Integer, Integer> pageOffset(int pageNum, int pageSize) {

        int start = (pageNum - 1) * pageSize;
        int end = start + pageSize;
        return Pair.with(start, end);
    }


    public static boolean isMoible(String mobilePhone){
        return !(Strings.isNullOrEmpty(mobilePhone) ||
                !mobilePhone.matches("^((13)|(14)|(15)|(17)|(18))\\d{9}$"));
    }

    public static BigDecimal getPurchaseAmount(){
        BigDecimal purchaseAmount = NumberUtils.createBigDecimal(GojaConfig.getProperty("member.purchase.amount", "1000000"));
        if(purchaseAmount == null){
            // 默认为100万
            purchaseAmount = new BigDecimal(1000000);
        }
        return purchaseAmount;
    }

    public static double scaleAmount(double value) {
        return scale(4, value);
    }

    public static double scale(double value) {
        return scale(2, value);
    }
    public static double scale(String val) {
        Double value = NumberUtils.createDouble(val);
        if(value == null){
            return 0;
        }
        return scale(2, value);
    }

    public static double scale(int size, double value) {
        BigDecimal bigDecimal = BigDecimal.valueOf(value);
        bigDecimal = bigDecimal.setScale(size, BigDecimal.ROUND_HALF_UP);
        return bigDecimal.doubleValue();
    }


    public static DateTime parseSlashDate(String date){
        return DateTime.parse(date, DateTimeFormat.forPattern(DATE_SLASH_FORMMAT));
    }


    public static Pair<DateTime, DateTime> rangeDatetime(String q_times) {
        if(Strings.isNullOrEmpty(q_times)){
            return null;
        }
        final String[] times = q_times.split(DATE_SPLIT);
        if(times.length != 2){
            return null;
        }
        DateTime start = CommonKit.parseSlashDate(times[0]).millisOfDay().withMinimumValue();
        DateTime end = CommonKit.parseSlashDate(times[1]).millisOfDay().withMaximumValue();
        return Pair.with(start, end);
    }

    public static Pair<String, String> rangeDatestr(String q_times) {
        if(Strings.isNullOrEmpty(q_times)){
            return null;
        }
        final String[] times = q_times.split(DATE_SPLIT);
        if(times.length != 2){
            return null;
        }
        String start = CommonKit.parseSlashDate(times[0]).millisOfDay().withMinimumValue().toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD);
        String end = CommonKit.parseSlashDate(times[1]).millisOfDay().withMaximumValue().toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD);
        return Pair.with(start, end);
    }

    /**
     * 获取去除标签后的内容
     * @param content 需过滤的内容
     * @return 去除标签后的内容
     */
    public static String getSummary(String content){
        if(null == content){
            content = StringPool.EMPTY;
        }
        return Jsoup.parse(content).text();
    }

    public static int termDays(int time_limit, String time_limit_unit, DateTime valuedate) {
        if (Strings.isNullOrEmpty(time_limit_unit)) {
            return time_limit;
        } else if (StringUtils.equals(DictConstant.DAY_UNIT, time_limit_unit)) {
            return time_limit;
        } else if (StringUtils.equals(DictConstant.MONTH_UNIT, time_limit_unit)) {
            DateTime time = valuedate.plusMonths(time_limit);
            return Days.daysBetween(valuedate, time).getDays();
        } else {
            return time_limit;
        }
    }


    public static String uuid(){
        return StringUtils.replace(Codec.UUID(), StringPool.DASH, StringPool.EMPTY);
    }

    /**
     * 供前台查询条件使用 日期区间 中间带 -
     * @param param 格式化的参数值
     * @param formatStr 格式化的类型字符
     * @return 格式化后的list<DateTime>
     */
    public static List<DateTime> string2DateTimeList(String param ,String formatStr){
        DateTimeFormatter format = DateTimeFormat.forPattern(formatStr);
        List<DateTime> date = Lists.newArrayList();
        String[] date_str = param.split(" - ");
        DateTime start = DateTime.parse(date_str[0], format);
        DateTime end = DateTime.parse(date_str[1],format);

        date.add(start.millisOfDay().withMinimumValue());
        date.add(end.millisOfDay().withMaximumValue());

        return date;
    }


    public static String stringDecimal(BigDecimal number){
        return number.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
    }

}

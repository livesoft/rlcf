package app.kit;

import app.Const;
import com.jfinal.plugin.activerecord.Model;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class TypeKit {


    public static int getStatus(Model model){
        if (model == null) {
            return -1;
        }
        Number bigDecimal = model.getNumber(Const.FIELD_STATUS);
        if(bigDecimal == null){
            return -1;
        } else {
            return bigDecimal.intValue();
        }
    }

    public static int getInt(Model model, String field){
        if (model == null) {
            return 0;
        }
        Number bigDecimal = model.getNumber(field);
        if(bigDecimal == null){
            return 0;
        } else {
            return bigDecimal.intValue();
        }
    }

    public static long getLong(Model model, String field) {
        if (model == null) {
            return 0;
        }
        Number field_val = model.getNumber(field);
        if(field_val == null){
            return 0;
        } else {
            return field_val.longValue();
        }
    }

    public static double getDouble(Model model, String field){
        if (model == null) {
            return 0;
        }
        Number field_val = model.getNumber(field);
        if(field_val == null){
            return 0;
        } else {
            return field_val.doubleValue();
        }
    }

    public static float getFloat(Model model, String field) {
        if (model == null) {
            return 0f;
        }
        Number field_val = model.getNumber(field);
        if(field_val == null){
            return 0;
        } else {
            return field_val.floatValue();
        }
    }

    public static BigDecimal getBigdecimal(Model model, String field) {
        if (model == null) {
            return BigDecimal.ZERO;
        }
        BigDecimal field_val = model.getBigDecimal(field);
        if (field_val == null) {
            return BigDecimal.ZERO;
        } else {
            return field_val.setScale(2, BigDecimal.ROUND_HALF_UP);
        }
    }

    public static DateTime getDateTime(Model model, String field){
        Timestamp field_val = model.getTimestamp(field);
        if(field_val == null){
            return null;
        } else {
            return new DateTime(field_val.getTime());
        }
    }
}

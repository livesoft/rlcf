package bank;

import bank.resp.AccountAmountRspDto;
import bank.resp.BindUserRspDto;
import bank.resp.BindUserStatusRspDto;
import bank.resp.OrderInfoRspDto;
import bank.resp.ReconciliationDto;
import bank.resp.TransferMerchantRspDto;
import bank.resp.UnBindUserRspDto;
import com.google.common.base.Optional;
import goja.Logger;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankSerivceTest{

    @Test
    public void testName() throws Exception {

        String yyyyMMddHHmmssss = "RLCF"+DateTime.now().toString("yyyyMMddHHmmssss") +"0001";
        String nyr = StringUtils.substring(yyyyMMddHHmmssss, 4, 20); // 获取年月日字符串
        String countV = StringUtils.substring(yyyyMMddHHmmssss, 20);

        System.out.println("DateTime.now().toString(\"yyyyMMddHHmmssss\") = " + yyyyMMddHHmmssss);
        System.out.println("nyr = " + nyr);
        System.out.println("countV = " + countV);

    }

    @Before
    public void setUp() throws Exception {
        Logger.init();

    }

    @Test
    public void testCheckAccount() throws Exception {
        DateTime now = DateTime.now();
        final Optional<ReconciliationDto> ok = BankService.me.checkAccount(now, now, now.plusDays(-8), now, 1, 200);
        Assert.assertTrue(ok.isPresent());
        System.out.println("ok.get() = " + ok.get());
    }

    @Test
    public void testTransferMerchantAccnt() throws Exception {

        DateTime now = DateTime.now();
        Optional<TransferMerchantRspDto> merchantAccnt = BankService.me.transferMerchantAccnt(now, now, "RLCF00201503200003N", "RLTR" + RandomStringUtils.random(5),
                NumberUtils.createBigDecimal("2.0"), "测试打款");
        System.out.println(merchantAccnt.isPresent());
        System.out.println(merchantAccnt.get());
        System.out.println(merchantAccnt.isPresent() && merchantAccnt.get().isSuccess());

    }

    @Test
    public void testGetBindUserStatus() throws Exception {
        final Optional<BindUserStatusRspDto> bindUserStatus = BankService.me.getBindUserStatus(DateTime.now(), DateTime.now(), "RLCF00201503200003N");
        if (bindUserStatus.isPresent()) {
            System.out.println("bindUserStatus = " + bindUserStatus.get());
        }

    }


    @Test
    public void testGetOrderInfo() throws Exception {

        final Optional<OrderInfoRspDto> orderInfoOpt = BankService.me.getOrderInfo(DateTime.now(), DateTime.now(), "RLCF20150415040446460100");
        if(orderInfoOpt.isPresent()){
            System.out.println("orderInfoOpt.get() = " + orderInfoOpt.get());
        }

    }

    @Test
    public void testUnBindUser() throws Exception {
//        for (int i = 500; i < 700; i++) {
//            final Optional<UnBindUserRspDto> rst = BankSerivce.me().unBindUser(DateTime.now(), DateTime.now(), String.valueOf(i));
//            if (rst.isPresent()) {
//                System.out.println(i + ", "+"unBindUser = " + rst.get());
//            }
//            Thread.sleep(3000);
//        }

        final Optional<UnBindUserRspDto> rst = BankService.me.unBindUser(DateTime.now(), DateTime.now(), "RLCF20150323000001N");
        if (rst.isPresent()) {
            System.out.println("unBindUser = " + rst.get());
        }
    }

    @Test
    public void testBindUser() throws Exception {

        final Optional<BindUserRspDto> bindUserOpt = BankService.me.bindUser(DateTime.now(),
                DateTime.now(), "204", "332527196103017335", "古毅力", "", "6228765001000125405");
        if (bindUserOpt.isPresent()) {
            System.out.println("bindUser = " + bindUserOpt.get());
        }
    }

    @Test
    public void testGetMerchantAccntAmount() throws Exception {
        DateTime now = new DateTime(2015,4,15, 23, 59);
        final Optional<AccountAmountRspDto> merchantAccntAmount = BankService.me.getMerchantAccntAmount(now, now);
        if(merchantAccntAmount.isPresent()){
            System.out.println("merchantAccntAmount.get() = " + merchantAccntAmount.get());
        }

    }
}
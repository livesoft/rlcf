package app.kit;

import goja.date.DateFormatter;
import goja.tuples.Pair;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Test;

public class CommonKitTest {

    @Test
    public void testPageOffset() throws Exception {
       Pair<Integer, Integer> nums=  CommonKit.pageOffset(2);
        System.out.println(nums);
    }

    @Test
    public void testCheckmobilePhone() throws Exception {
        System.out.println("CommonKit.checkmobilePhone(\"17712345678\") = " + CommonKit.isMoible("18655121378"));
    }


    @Test
    public void testUUID() throws Exception {
        for (int i = 0; i < 100; i++) {
            System.out.println("CommonKit.uuid() = " + CommonKit.uuid());

        }

    }


    @Test
    public void testDate() throws Exception {
        DateTime dateTime = new DateTime(2015,4,18,0,0);
        DateTime endDate = dateTime.plusDays(4);
        String time =  endDate.toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD);
        System.out.println("time = " + time);
        System.out.println("endDate = " + Days.daysBetween(endDate, dateTime).getDays());

    }
}
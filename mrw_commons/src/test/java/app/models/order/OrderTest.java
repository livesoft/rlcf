package app.models.order;

import goja.test.ModelTestCase;
import org.joda.time.DateTime;
import org.junit.Test;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class OrderTest  extends ModelTestCase{

    @Test
    public void testSumAmountByMember() throws Exception {
        DateTime.Property property = new DateTime(2015,4,11,0,0).millisOfDay();
        DateTime start = property.withMinimumValue();
        DateTime end = property.withMaximumValue();
        System.out.println("Order.dao.sumAmountByMember(1, DateTime.now().millisOfDay().withMinimumValue()) = "
                + Order.dao.sumAmountByMember(1, start,end));
    }

    @Test
    public void testFindByOrdernoSingle() throws Exception {
        Order order = Order.dao.findByOrdernoSingle("RLCF_20150411104900001");
        System.out.println("order = " + order);
    }
}
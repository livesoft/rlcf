package app.models.order;

import goja.test.ModelTestCase;
import org.junit.Test;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class TradeMoneyTest extends ModelTestCase {

    @Test
    public void testFindByStatus() throws Exception {

        List<TradeMoney> oks = TradeMoney.dao.findByStatus(TradeMoney.OK_STATUS);
        for (TradeMoney ok : oks) {
            System.out.println("ok = " + ok);
        }

    }


    @Test
    public void testAdjustAmount() throws Exception {

        TradeMoney tradeMoney = TradeMoney.dao.findById(1);
        System.out.println("tradeMoney.adjustAmount() = " + tradeMoney.adjustAmount());

    }
}
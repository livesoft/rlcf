package app.models.member;

import app.constant.DictConstant;
import goja.test.ModelTestCase;
import org.junit.Test;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class MemberProductTest extends ModelTestCase {
    @Test
    public void testFindProductProfit() throws Exception {

        List<MemberProduct> byProductProfit = MemberProduct.dao.findByProductProfit( DictConstant.PROFIT_ONCE, 31);
        for (MemberProduct memberProduct : byProductProfit) {
            System.out.println("memberProduct = " + memberProduct);
        }

    }
}
package app.models.product;

import com.jfinal.plugin.activerecord.Page;
import goja.rapid.db.RequestParam;
import goja.test.ModelTestCase;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.List;

public class ProductTest extends ModelTestCase {


    @Test
    public void testFindByPageList() throws Exception {


        final Page<Product> twplist = Product.dao.findByPageList(1, 15,"yield",RequestParam.Direction.DESC, 1, 1, 0);
        System.out.println(twplist);

    }


    @Test
    public void testUpdateStatus() throws Exception {

    }

    @Test
    public void testFindByGroup() throws Exception {

    }

    @Test
    public void testFindByLastSerial() throws Exception {

    }

    @Test
    public void testFindByAllInfo() throws Exception {

    }

    @Test
    public void testFindByWebIntro() throws Exception {

    }

    @Test
    public void testGenerateCode() throws Exception {

    }

    @Test
    public void testFindAllAvailable() throws Exception {

    }

    @Test
    public void testFindNameAndIdAvalible() throws Exception {

    }

    @Test
    public void testUpdateProgress() throws Exception {

    }

    @Test
    public void testFindForPcHome() throws Exception {

    }

    @Test
    public void testFindExcelExport() throws Exception {

    }

    @Test
    public void testFindAllAvailableCount() throws Exception {

    }

    @Test
    public void testUpdateBuy() throws Exception {

    }

    @Test
    public void testGetCollectionMode() throws Exception {

    }

    @Test
    public void testFindByEndTime() throws Exception {


        List<Product> products = Product.dao.findByCheckStatus(new DateTime(2015, 4, 3, 0, 0));
        for (Product product : products) {
            System.out.println("product = " + product);
        }
    }
}
package app.models.transfer;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Test;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class TransferRuleTest {

    @Test
    public void testTransferFee() throws Exception {
        int days = Days.daysBetween(new DateTime(2015, 5, 12, 7, 1), new DateTime(2015, 5, 13, 8, 1)).getDays();
        System.out.println("days = " + days);
    }
}
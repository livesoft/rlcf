package app.services;

import app.Const;
import app.models.member.Points;
import app.models.order.TradeMoney;
import com.google.common.base.Optional;
import goja.test.ModelTestCase;
import org.junit.Test;

import java.util.List;

import static app.models.order.TradeMoney.OK_STATUS;
import static app.models.order.TradeMoney.dao;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class PlaymoneyServiceTest extends ModelTestCase{
    @Test
    public void testOK() throws Exception {
        // 取得购买消费积分规则
        Optional<Points> optional_points = Points.dao.findByCode(Const.POINTS_CODE.COST);
        // 1. 查询所有领导审批的打款的服务,取得所有已经确认后的打款纪录
        List<TradeMoney> tradeMoneys = dao.findByStatus(OK_STATUS);

        for (final TradeMoney tradeMoney : tradeMoneys) {
            PlaymoneyService.me.playmoney(tradeMoney,optional_points);
        }

    }
}
package app.services.profit;

import app.constant.DictConstant;
import app.models.member.MemberProduct;
import com.google.common.base.Optional;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class ProfitServiceTest  {

    @Before
    public void setUp() throws Exception {

        new EhCachePlugin().start();

    }

    @Test
    public void testGetMillionIncome() throws Exception {

        System.out.println("ProfitService.me().getMillionIncome(8.76f) = "
                + ProfitService.me.getMillionIncome(8.76f));

    }

    @Test
    public void testGetYield() throws Exception {

    }

    @Test
    public void testRollProfit() throws Exception {

    }

    @Test
    public void testOnceProfit() throws Exception {
        System.out.println(" onceProfit  = "
                + ProfitService.me.onceProfit(new BigDecimal(200.00), new BigDecimal(5.18), 360, 2));
    }

    @Test
    public void testEqualInterest() throws Exception {

    }

    @Test
    public void testCalculator1() throws Exception {

    }

    @Test
    public void testMe() throws Exception {

    }

    @Test
    public void testPayPerMonth() throws Exception {

    }

    @Test
    public void testAverageCapitalInterest() throws Exception {

        Optional<ProfitDto> profitDtos = ProfitService.me.averageCapitalInterest(1000,12,0.0920/12,1);
        System.out.println("profitDtos = " + profitDtos);
    }

    @Test
    public void testOnceTimeCollection() throws Exception {
        List<MemberProduct> memberProducts = MemberProduct.dao.findByMaturityYield(1, DictConstant.PROFIT_ONCE);
        for (MemberProduct memberProduct : memberProducts) {

            ProfitService.me.onceTimeCollection(memberProduct);
        }

    }
}
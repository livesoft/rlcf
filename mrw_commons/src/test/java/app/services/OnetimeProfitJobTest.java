package app.services;

import app.constant.DictConstant;
import app.models.member.MemberProduct;
import app.services.profit.ProfitService;
import goja.lang.Lang;
import goja.test.ModelTestCase;
import org.junit.Test;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class OnetimeProfitJobTest extends ModelTestCase{


    @Test
    public void testProfit() throws Exception {

        // 1. 查询会员的未结束的理财产品
        List<MemberProduct> memberProducts = MemberProduct.dao.findByMaturityYield(1, DictConstant.PROFIT_ONCE);

        boolean run = !Lang.isEmpty(memberProducts);

        if (run) {
            // 2. 计算收益信息

            for (MemberProduct memberProduct : memberProducts) {
//               年月日为到期日期， 小时为规则设置的小时点
                ProfitService.me.onceTimeCollection(memberProduct);
            }

        }

    }
}
package app.services;

import app.kit.CommonKit;
import goja.test.ModelTestCase;
import org.joda.time.DateTime;
import org.junit.Test;

public class CheckAccountServiceTest extends ModelTestCase{

    @Test
    public void testCheckAccount() throws Exception {
        final DateTime start_time = DateTime.now().plusDays(-7);
        CheckAccountService.me.checkAccount(start_time, DateTime.now());
    }


    @Test
    public void testAccount() throws Exception {

        CheckAccountService.me.checkAmount();
    }
}
package app.services;

import goja.test.ModelTestCase;
import org.junit.Test;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class SerialServiceTest extends ModelTestCase{

    @Test
    public void testProductCode() throws Exception {
        System.out.println("SerialService.productCode() = " + SerialService.productCode());
    }

    @Test
    public void testOrderCode() throws Exception {
        System.out.println("SerialService.orderCode() = " + SerialService.orderCode());
    }

    @Test
    public void testMemberCode() throws Exception {
        System.out.println("SerialService.memberCode() = " + SerialService.memberCode());
    }
}
package app.services;

import goja.test.ModelTestCase;
import org.joda.time.DateTime;
import org.junit.Test;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductServiceTest extends ModelTestCase {

    @Test
    public void testStatusCheck() throws Exception {
        ProductService.me.statusCheck(new DateTime(2015,4,22, 0, 0));
    }

    @Test
    public void testBoardstat() throws Exception {
        ProductService.me.boardstat();
    }


    @Test
    public void testInvokeProfit() throws Exception {
        ProductService.me.invokeProfit(1);

    }
}
<@override name="title">我的产品</@override>
<@override name="content">
<section class="regist_main" p_id="m_my_product">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写手机号
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red">
                2.重置密码
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red">
                3.重置成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <table class="tb_form" border="" cellspacing="" cellpadding="">
                    <tr>
                        <th></th>
                        <td>
                            <p class="reg_ok">
                                密码修改成功！
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="submit">
                            <input type="submit" onclick="window.location.href='${ctx}/login'" value="下一步">
                            <a href="${ctx}/product" class="link_go">去挑选理财产品</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
</@override>

<@extends name="../main/_main.ftl"></@extends>
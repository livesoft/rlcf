<@override name="title">找回密码</@override>
<@override name="content">
<section class="regist_main" p_id="m_my_product">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写手机号
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_3">
                2.重置密码
            </div>
            <div class="grid_1">
            </div>
            <div class="grid_3">
                3.重置成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <form id="registerForm" method="post">
                    <table class="tb_form" border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>手机号：</th>
                            <td>
                                <input id="phone" name="phone" type="text" class="large" placeholder="请输入手机号"/>
                            </td>
                        </tr>
                        <tr>
                            <th>验证码：</th>
                            <td>
                                <input type="text" name="codes" id="codes" placeholder="请输入验证码"/>
                                <img id="code" src="${ctx}/api/captcha" style="vertical-align: middle;"/>
                                <a href="javascript:changeCode()">换一张</a>
                                <span class="msg-box n-right" style="position:static;" for="codes"></span>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="submit"><input id="next" type="submit" value="下一步"></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section></@override>

<@override name="js">
<script type="text/javascript">
    function agreeus() {
        if ($("#agree").attr("checked")) {
            $("#next").attr('disabled', false);
        } else {
            $("#next").attr('disabled', true);
        }
    }
    $('#registerForm').validator({
        //自定义用于当前实例的规则
        rules: {
            mobile: [/^1[3458]\d{9}$/, '请检查手机号格式'],
            hadphone       : function (el, param, field) {
                //do something...
                var url = "${ctx}/forgetpwd/regphone?phone=" + $.trim($("#phone").val());
                return $.get(url, function (data) {
                });
            },
            ckeckedMcode: function (el, param, field) {
                //do something...
                var url = "${ctx}/register/checkedMcode?captcha=" + $.trim($("#codes").val());
                return  $.get(url, function (data) {
                });
            },
        },
        //待验证字段集合
        fields: {
            phone: {rule: 'required;mobile;hadphone;', msg: {required: "手机号不能为空!"}},
            codes: {rule: 'required;ckeckedMcode;', msg: {required: "验证码不能为空!"}}
        },
        valid: function (form) {
            //表单验证通过，提交表单到服务器
            var url = "${ctx}/hadphone?phone=" + $.trim($("#phone").val());
            $.post(url, function (data) {
                if(data.ok==null&&data.error==null){
                    window.location.href = "${ctx}/forgetpwd/reset?phone=" + $.trim($("#phone").val());
                }else{
                    layer.alert("输入的手机号有误！", 2, !1);
                }
            });
        }
    });
    function changeCode() {
        //IE7+/Firefox默认从缓存加载，路径之后加随机参数强制重新下载
        var d = new Date();
        var oImg = document.getElementById('code');
        oImg.src = "${ctx}/api/captcha?t=" + d.toString(38);
    }
</script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
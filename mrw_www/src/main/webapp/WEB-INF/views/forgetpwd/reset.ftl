<@override name="title">找回密码</@override>
<@override name="content">
<section class="regist_main" p_id="m_my_product">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写身份信息
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red">
                2.重置密码
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_3">
                3.重置成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <form id="registerForm">
                    <input type="hidden" value="${phone!}" id="phoneVal">
                    <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>手机号码：</th>
                            <td>
                                <p><span id="phonenum">${phone!}</span></p>
                            </td>
                        </tr>
                        <tr>
                            <th>短信验证码：</th>
                            <td>
                                <input id="mcode" name="codes" type="text" placeholder="请输入验证码" />
                            <#--<span id="tip"></span>-->
                            <#--<a href="javascript:resetMcode()">未收到验证码？</a>-->
                            <#--<br/><span class="msg-box n-right" style="position:static;" for="codes"></span>-->
                                <a id="pull_code" href="javascript:void(0);">点击获取短信验证码？</a>
                                <div class="send_mobile_message_body">
                                    <a class="count_down" href="javascript:void(0);">60秒后重新获取</a>
                                </div>
                                <span class="msg-box n-right" style="position:static;" for="mcode"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>设置登录密码：</th>
                            <td>
                                <input name="password" id="password" type="password" class="large" placeholder="请输入登录密码" />
                                <span>6~16个字符，区分大小写</span>
                                <span class="msg-box n-right" style="position:static;" for="password"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>重复登录密码：</th>
                            <td>
                                <input id="rpassword" name="rpassword" type="password" class="large" placeholder="请输入确认密码" />
                                <span class="msg-box n-right" style="position:static;" for="rpassword"></span>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="submit"><input id="register" type="submit" value="确  认"></td>
                        </tr>
                    </table></form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/app/member/forgetpassword.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
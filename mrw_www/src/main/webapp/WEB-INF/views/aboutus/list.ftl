<@override name="title">关于我们</@override>
<@override name="content">
<section>
    <div class="zx_header_main">
        <div class="container_12">
            <nav class="zx_nav">
                <ul class="ss">
                    <#list menu! as m >
                        <li id="${(m.code)!}"><a href="${ctx}/aboutus/show/${(m.code)!}">${(m.name)!}</a></li>
                    </#list>
                </ul>
                <div class="clear"></div>
            </nav>
        </div>
    </div>
    <div class="container_12">
        <div class="post_list">
            <ul class="ul_list">
    <#list data.list! as n>
                <li>
                    <div class="thumb">
                        <img style="width: 200px; height: 150px;" src="${img_domain!}${(n.pic)!}"/>
                    </div>
                    <div class="info">
                        <div class="title">
                            <span class="fr">${(n.pubdate)!}</span>
                            <span>${(n.title)!}</span>
                        </div>
                        <div class="desc">
                            <#if (n.summary)!?length gt 200>
                                ${(n.summary)?substring(0,190)}
                            <#else >
                                ${(n.summary)!}
                            </#if>
                        </div>
                        <div class="right_link">
                            <a href="${ctx}/aboutus/showNews/${(n.id)!}" class="red"><i class="icon_arr_right"></i>查看详情</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </li>
    </#list>
            </ul>

            <#import '*/macros/pagination.ftl' as pagination>
            <@pagination.pagination page=data! url='${ctx}/aboutus/news'/>
        </div>
        </div>
    </div>
    </div>
</section>
</@override>
<@override name="js">
<script type="text/javascript">
    $(function(){
        // Tabs
        $('#wrapper_tab a').click(function() {
            if ($(this).attr('class') != $('#wrapper_tab').attr('class') ) {
                $('#wrapper_tab').attr('class',$(this).attr('class'));
            }
            return false;
        });
    });
    $("#"+"${cur}").addClass("curent");
</script>
</@override>


<@extends name="../main/_main.ftl"></@extends>
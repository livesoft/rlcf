<@override name="title">理财资讯</@override>
<@override name="content">
<section>
    <div class="zx_header_main">
        <div class="container_12">
            <nav class="zx_nav">
                <ul class="ss">
                    <#list menu! as m >
                        <li id="${(m.code)!}"><a href="${ctx}/aboutus/show/${(m.code)!}">${(m.name)!}</a></li>
                    </#list>
                </ul>
                <div class="clear"></div>
            </nav>
        </div>
    </div>
    <div class="container_12">
        <div class="post_detail">
            <div class="post_header">
                <h1>${(news.title)!}</h1>
                <div class="post_info">
                    <p>${(news.create_time)!}<label>来源：<a href="#"><span>融理财富</span></a></label></p>
                </div>
            </div>
            <div class="post_content">
                ${(news.content)!}
            </div>
            <div class="post_list other">
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
</@override>
<@override name="js">
<script type="text/javascript">
    $(function(){
        // Tabs
        $('#wrapper_tab a').click(function() {
            if ($(this).attr('class') != $('#wrapper_tab').attr('class') ) {
                $('#wrapper_tab').attr('class',$(this).attr('class'));
            }
            return false;
        });

        $("#"+"${cur}").addClass("curent");
    });
</script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
<@override name="title">关于我们</@override>
<@override name="content">
<section>
    <div class="zx_header_main">
        <div class="container_12">
            <nav class="zx_nav">
                <ul class="ss">
                    <#list menu! as m >
                        <li id="${(m.code)!}"><a href="${ctx}/aboutus/show/${(m.code)!}">${(m.name)!}</a></li>
                    </#list>
                </ul>
                <div class="clear"></div>
            </nav>
        </div>
    </div>
    <div class="container_12">
        <div class="post_detail">
            <div class="post_header">
                <h1>${(about.title)!}</h1>
                <div class="post_info">
                    <p>${(about.create_time)!}<label>来源：<a href="#"><span>${(about.source)!}</span></a></label></p>
                </div>
            </div>
            <div class="post_content">
                ${(about.content)!}
            </div>
            <div class="post_list other">
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
</@override>
<@override name="js">
<script type="text/javascript">
    $(function(){
        $("#"+"${cur}").addClass("curent");
    });
</script>
</@override>


<@extends name="../main/_main.ftl"></@extends>
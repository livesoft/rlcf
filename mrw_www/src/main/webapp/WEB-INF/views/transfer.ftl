<@override name="title">产品转让</@override>
<@override name="content">

<section>
    <div class="lczx_banner_main">
        <div class="container_12"></div>
    </div>
    <div class="lczx_product_list">
        <div class="container_12">
            <div class="tab2" id="wrapper_tab">
                <a href="${ctx}/product" class="tab1 tab_link">理财产品</a>
                <a href="#" class="tab2 tab_link">转让市场</a>

                <div class="clear"></div>
                <div class="tab2 tab_body">
                    <div class="zhuanrang_anly">
                        <div class="block vline">
                            <p>
                                交易总额：<span class="red">${(yesterday.sum_price!0)?string(',###.##')}</span>元
                            </p>

                            <p>
                                转让笔数：<span>${yesterday.cunt!'0'}</span>笔
                            </p>
                            <span class="bname">昨日累计</span>
                            <span class="verline"></span>
                        </div>
                        <div class="block history">
                            <p>
                                交易总额：<span class="red">${(all.sum_price!0)?string(',###.##')}</span>元
                            </p>

                            <p>
                                转让笔数：<span>${all.cunt!'0'}</span>笔
                            </p>
                            <span class="bname">历史累计</span>
                            <span class="verline"></span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="zhuanrang_filter_wrap">
                        <div class="zhuanrang_filter">
                            排序：
                            <a href="${ctx}/transfer/yield_${sort_dir!'desc'}#cc"
                               class="btn_orderby <#if sort_field=='yield'>selected ${sort_dir!}</#if>">预期年化收益率</a>
                            <a href="${ctx}/transfer/remaining_days_${sort_dir!'desc'}#cc"
                               class="btn_orderby  <#if sort_field=='remaining_days'>selected ${sort_dir!}</#if>  ">剩余期限</a>
                            <a href="${ctx}/transfer/price_${sort_dir!'desc'}#cc"
                               class="btn_orderby  <#if sort_field=='price'>selected ${sort_dir!}</#if>  ">项目金额</a>

                        </div>
                    </div>
                    <div class="clear"></div>

                    <#if data.list?size &gt;0 >

                        <div id="idx_section_zhuanrang">
                            <div class="zr_area list">
                                <ul>
                                    <#list data.list as p>
                                        <li>
                                            <div class="zr_title">
                                                <h2 title="${(p.name)!}">
                                                    <a href="${ctx}/transfer/info/${p.id!}">
                                                        <#if (p.name)!?length&gt;11>${(p.name)?substring(0,10)}...<#else>${(p.name)!}</#if>
                                                    </a>
                                                </h2>
                                                <p><i class="icon_danbao"></i><#if (p.orgname)??>${p.orgname}</#if>承兑<i class="icon_san"></i>保障收益</p>
                                            </div>
                                            <div class="zr_common" style="width: 125px;">
                                                <p>${(p.amount!0)?string(',###.##')}</p>

                                                <p>资产价值</p>
                                            </div>
                                            <div class="zr_common">
                                                <p class="red">${p.yield!}%</p>

                                                <p>年化利率</p>
                                            </div>
                                            <div class="zr_common">
                                                <p>${p.remaining_days!}天</p>

                                                <p>项目期限</p>
                                            </div>
                                            <div class="zr_common" style="width: 125px;">
                                                <p class="bblue">${(p.price!0)?string(',###.##')}</p>

                                                <p>转让价格</p>
                                            </div>
                                            <div class="zr_opt">
                                                <#if p.status == 2>
                                                    <p>${(p.trade_time)?string('MM-dd HH:mm')} 转让</p>

                                                    <p><a href="${ctx}/transfer/info/${p.id!}" class="btn ed">已转让</a>
                                                    </p>
                                                <#else>
                                                    <p><a href="${ctx}/transfer/info/${p.id!}" class="btn">立即购买</a></p>
                                                </#if>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
                                    </#list>
                                </ul>
                            </div>
                        </div>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=data! url='${ctx}/transfer/${sort_field}_${sort_dir!}'/>
                    <#else>
                        <div class="product_list_none">
                            <p>
                                <i class="icon_none"></i>没有符合条件的数据
                            </p>
                        </div>
                    </#if>

                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
</@override>
<@extends name="./main/_main.ftl"></@extends>
<@override name="title">会员注册</@override>
<@override name="content">
<section class="regist_main">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写注册信息
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_3">
                2.设置密码
            </div>
            <div class="grid_1">
            </div>
            <div class="grid_3">
                3.注册成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <form id="registerForm" method="post" action="${ctx}/register/password">
                    <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>用户名称：</th>
                            <td>
                                <input id="username" name="username" type="text" class="large" placeholder="请输入用户名称" style="ime-mode:disabled"/>
                            </td>
                        </tr>
                        <tr>
                            <th>手机号：</th>
                            <td>
                                <input id="phone" name="phone" type="text" class="large" placeholder="请输入手机号"/>
                            </td>
                        </tr>
                        <tr>
                            <th>推荐人手机号：</th>
                            <td>
                                <input id="rphone" value="<#if phone??>${phone!}</#if>" name="rphone" type="text"
                                       class="large" placeholder="请输入推荐人手机号"/>
                            </td>
                        </tr>
                        <tr>
                            <th>验证码：</th>
                            <td>
                                <input type="text" name="codes" id="codes" placeholder="请输入验证码"/>
                                <img id="code" src="${ctx}/api/captcha" style="vertical-align: middle;"/>
                                <a href="javascript:changeCode()">换一张</a>
                                <span class="msg-box n-right" style="position:static;" for="codes"></span>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <label for="Remember_password" class="rem">
                                    <input type="checkbox" onclick="agreeus()" id="agree">&nbsp;&nbsp;我同意<a
                                        href="javascript:mask_opend();"
                                        class="red">《会员注册协议》</a>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="submit"><input id="next" class="disabled" type="submit" disabled value="下一步">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>注册协议</p>
            <a class="btn-close absolute" href="javascript:mask_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct" style="overflow: auto;overflow-x:hidden;height: 550px;">
            <div class="msg_read">
                <h2 id="title"></h2>

                <div id="content" class="ctt">
                ${note!}
                </div>
            </div>

        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>
</@override>

<@override name="js">
<script src="${ctx}/res/app/register.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
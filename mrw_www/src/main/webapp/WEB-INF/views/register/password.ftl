<@override name="title">注册</@override>
<@override name="css">
<link rel="stylesheet" type="text/css" href="${ctx}/res/js/validator/jquery.validator.css"/>
</@override>
<@override name="content">
<section class="regist_main">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写注册信息
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red">
                2.设置密码
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_3">
                3.注册成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <input type="hidden" id="invite" value="${invite!}"/>
                <input type="hidden" id="username" value="${username!}"/>
                <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                    <tr>
                        <th>手机号码：</th>
                        <td>
                            <input type="hidden" id="phoneVal" value="${phone!}">
                            <p><span id="phonenum">${phone!}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <th>短信验证码：</th>
                        <td>
                            <input id="mcode" name="codes" type="text" placeholder="请输入验证码" />
                            <a id="pull_code" href="javascript:void(0);">获取短信验证码</a>
                            <div class="send_mobile_message_body">
                                <a class="count_down" href="javascript:void(0);">60秒后重新获取</a>
                                <a class="extra" href="javascript:void(0)"><font color="#999">或</font> <a href="${ctx}/register/${phone!}">返回修改手机号</a></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>设置登录密码：</th>
                        <td>
                            <input name="password" id="password" type="password" class="large" placeholder="请输入登录密码" />
                            <span>6~16个字符，区分大小写</span>
                        </td>
                    </tr>
                    <tr>
                        <th>重复登录密码：</th>
                        <td>
                            <input id="rpassword" name="rpassword" type="password" class="large" placeholder="请输入确认密码" />
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="submit"><input id="register" type="submit" value="确  认"></td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/app/member/register.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
<@override name="title">注册</@override>
<@override name="content">

<section class="regist_main">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_3 red">
                1.填写注册信息
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red">
                2.设置密码
            </div>
            <div class="grid_1 red">
            </div>
            <div class="grid_3 red" style="padding: 0px 15px;">
                3.注册成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <table class="tb_form" border="" cellspacing="" cellpadding="">
                    <tr>
                        <th></th>
                        <td>
                            <p class="reg_ok">
                                恭喜你，注册成功！
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="submit">
                            <input type="submit" onclick="javascrip:window.location.href='${ctx}/'" value="下一步">
                            <a href="${ctx}/product" class="link_go">去挑选理财产品</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
</@override>

<@extends name="../main/_main.ftl"></@extends>
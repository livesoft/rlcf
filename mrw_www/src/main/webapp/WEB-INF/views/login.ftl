<@override name="title">登录</@override>
<@override name="css">
<style type="text/css">
    .alert-warning {
        position      : relative;
        height        : 18px;
        padding       : 5px;
        margin-bottom : 5px;
        color         : #cf2d14;
        background    : #ffe9eb;
        border        : 1px solid #feaaa8;
        display       : none;
    }
</style>
</@override>
<@override name="content">
<section class="login_main">
    <div class="container_12">
        <div class="grid_12">
            <article>
                <div class="grid_8 new_customers">
                </div>
                <div class="grid_4 registed_form">
                    <form class="registed" id="loginForm" method="post" action="${ctx}/signin">
                        <h2>登录融理财富</h2>
                        <input type="hidden" name="redirect" value="${redirect!}">
                        <div class="alert-warning">
                            <div class="error-msg"></div>
                        </div>
                        <div class="email">
                            <input id="phone" type="text" placeholder="手机号或用户名" name="phone" class="" value="">
                        </div>

                        <div class="password">
                            <input id="password" type="password" name="password" class="" placeholder="密码" value="">
                        </div>

                        <div class="password">
                            <input name="captcha" id="captcha" style="width: 100px;" type="text" placeholder="请输入验证码"/>
                            <img id="code" src="${ctx}/api/captcha" style="vertical-align: middle;"/>
                            <a id="change_captcha_btn">换一张</a>
                        </div>

                        <div class="remember">

                        <#--<label for="Remember_password" class="rem">-->
                        <#--<input type="checkbox" id="Remember_password">&nbsp;&nbsp;两周内自动登录-->
                        <#--</label>-->
                        </div>
                        <div class="submit">
                            <input type="submit" value="登 录" id="login_btn">
                            <input type="button" class="btn_reg" onclick="window.location.href ='${ctx}/register' "
                                   value="注 册">
                            <a class="forgot" href="${ctx}/forgetpwd">忘记密码?</a>
                        </div>
                    </form>
                </div>
            </article>
        </div>
        <div class="clear"></div>
    </div>
</section></@override>
<@override name="footer">
<#include "*/member/_foot.html">
</@override>
<@override name="js">
<script type="text/javascript" src="${ctx}/res/app/member/login.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="./main/_main.ftl"></@extends>
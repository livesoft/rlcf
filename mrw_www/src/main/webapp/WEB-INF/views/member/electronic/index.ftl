<@override name="title">绑定电子账户</@override>
<@override name="content">
<section class="mypage_wrapper my_product" p_id="m_my_product" p_id="m_my_product">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>绑定电子账户</h3>
                </header>
                <div class="reg_form">
                    <div class="grid_8">
                        <form method="POST" action="${ctx}/member/electronic/bind" id="bind_form" autocomplete="off">
                            <input type="hidden" name="redirect_url" value="${redirect_url!}">
                            <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                                <tr>
                                    <th>姓名：</th>
                                    <td>
                                        <input type="text" class="large" name="realName" placeholder="真实姓名，用于银行认证"
                                               data-rule="required;" value="${realName!}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>身份证号：</th>
                                    <td>
                                        <input type="text" class="large" name="idCard" placeholder="请输入正确身份证号"
                                               data-rule="required;idcard;" value="${idCard!}"
                                               data-rule-idcard="[/^[1-9][0-7]\d{4}((19\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(19\d{2}(0[13578]|1[02])31)|(19\d{2}02(0[1-9]|1\d|2[0-8]))|(19([13579][26]|[2468][048]|0[48])0229))\d{3}(\d|X|x)?$/, '请检查身份证号']"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>银行电子账号：</th>
                                    <td>
                                        <input type="text" class="large" name="electronicNo" placeholder="请输入银行电子账号"
                                               data-rule="required;" value="${electronicNo!}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        <label for="Remember_password" class="rem">
                                            <input type="checkbox" onclick="agreeus()" id="agree">&nbsp;&nbsp;我同意<a href="javascript:mask_opend();"
                                                                                                                    class="red">《快捷支付协议》</a>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td class="submit"><input id="sbmt" class="disabled" disabled type="submit" value="确认"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>绑定电子账户，只需3步<span>手把手教你绑定电子账户</span></h3>
                </header>
                <div class="bdzh_steps">
                    <ul>
                        <li>
                            <p>下载APP</p>

                            <p>1.下载及安装江苏银行APP<a target="_blank" href="http://www.jsbchina.cn/news/dzyh/zxdt/2014812/n64899925.html#2">点击下载</a></p>
                        </li>
                        <li>
                            <p>注册账号</p>

                            <p>2.注册江苏银行账号</p>
                        </li>
                        <li>
                            <p>绑定电子账户</p>

                            <p>3.输入银行电子账号，绑定电子账户</p>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>注册协议</p>
            <a class="btn-close absolute" href="javascript:mask_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct" style="overflow: auto;height: 540px;">
            <div class="msg_read">
                <h2 id="title"></h2>
                <div id="content" class="ctt">
                ${fast_notes!}
                </div>
            </div>

        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>

<section id="side_code" style="border: 1px solid #DDD; width: 110px; height: auto; background: #FFF; z-index: 1000; position: fixed; right: 10px; bottom: 200px;">
    <img style="display: block;" class="poshytip-target" title="扫描二维码下载江苏银行APP" width="110" height="110" src="http://www.jsbchina.cn/files/dianziyinhang/20141230/%E7%9C%81%E5%A4%96%E7%9B%B4%E9%94%80%E9%93%B6%E8%A1%8C%E4%BA%8C%E7%BB%B4%E7%A0%81%E5%89%AF%E6%9C%AC.jpg"/>
    <div style="border-top: 1px solid #efefef; text-align: center; padding:0 3px; background: #f5f5f5;">
        扫描二维码下载江苏银行APP
    </div>
</section>

</@override>

<@override name="js">
<script type="text/javascript">
    <#if error??>
    var error = parseInt('${error}');
    <#else>
    var error = false;
    </#if>
    var message = '${message!''}';

    function mask_opend(){
        $(".masker").show();
    }
    function mask_close(){
        $(".masker").hide();
    }

    function agreeus() {
        if ($("#agree").attr("checked")) {
            $("#sbmt").attr('disabled', false);
            $("#sbmt").removeClass("disabled");
        } else {
            $("#sbmt").attr('disabled', true);
            $("#sbmt").addClass("disabled");
        }
    }
</script>
<script type="text/javascript" src="${ctx}/res/app/member/electronic.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
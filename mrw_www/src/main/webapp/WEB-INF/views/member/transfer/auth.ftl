<@override name="title">我的产品</@override>
<@override name="content">

<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>1</span>
                        <p>确认转让信息</p>
                    </li>
                    <li class="cur">
                        <span>2</span>
                        <p>确认合同</p>
                    </li>
                    <li class="cur">
                        <span>3</span>
                        <p>安全认证</p>
                    </li>
                    <li class="last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p>发布成功</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <div class="reg_form">
                    <div class="grid_8">
                        <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                            <tr>
                                <th>交易密码：</th>
                                <td>
                                    <input type="password" id="password" placeholder="请输入交易密码" />
                                <#--<span>价格只能在10,502~12,523元之间</span>-->
                                </td>
                            </tr>
                            <tr>
                                <th>手机动态码：</th>
                                <td>
                                    <input type="text" placeholder="请输入手机动态码" id="dynamic_code"/>
                                    <a id="sendsms_btn" href="javascript:void(0);">获取手机动态码</a>
                                    <div class="send_mobile_message_body">
                                        <a class="count_down" href="javascript:void(0);">60秒后重新获取</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td class="submit"><input id="sbmt" type="submit" value="确  定"></td>
                            </tr>
                        </table>
                        <form method="post" id="commit_form" action="${ctx}/member/transfer/commit">
                            <input type="hidden" name="mpid" value="${mem_pid!0}">
                            <input type="hidden" name="price" value="${t_price!0}">
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

</@override>

<@override name="js">
<script type="text/javascript">
    $(function(){

        sendRequestMobileMessage($('#sendsms_btn'), '${ctx}/member/transfer/sendsms/', 'POST');

        $("#sbmt").on("click",function(){
            var dynamic_code = $("#dynamic_code").val();
            var password_val = $("#password").val();

            if($.trim(password_val) == ""){
                layer.alert("请输入交易密码", 2, !1);
                return;
            }
            if($.trim(dynamic_code) == ""){
                layer.alert("请输入动态验证码", 2, !1);
                return;
            }

            $.ajax({
                url:"${ctx}/member/transfer/security",
                data:{
                    pwd:password_val, code: dynamic_code
                },
                type:'POST',
                dataType: "JSON"
            }).done(function(msg){
                if(msg.status == "OK"){
                    $('#commit_form').submit();
                }else{
                    layer.alert(msg.message, 5, !1);
                }
            }).fail(function(){
                layer.alert('服务器太忙了', 5, !1);
            });
        });
    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
<@override name="title">我的产品</@override>
<@override name="content">

<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>1</span>
                        <p>确认转让信息</p>
                    </li>
                    <li class="cur">
                        <span>2</span>
                        <p>确认合同</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>安全认证</p>
                    </li>
                    <li class="last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p>发布成功</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block" style="margin-bottom: 10px;">
                <div class="ht_content">
                   ${notes!}
                </div>
            </div>
        </div>

        <div class="pop_but ta_center" style="margin-bottom: 20px;">
            <form action="${ctx}/member/transfer/auth" method="post">
                <input type="hidden" name="mpid" value="${mem_pid!0}">
                <input type="hidden" name="price" value="${t_price!0}">
                <button type="submit" class="btn-pop btn-confirm">同意并继续</button>
            </form>
        </div>

        <div class="clear"></div>
    </div>
</section>


</@override>

<@override name="js">
<script type="text/javascript">
    $(function(){
    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
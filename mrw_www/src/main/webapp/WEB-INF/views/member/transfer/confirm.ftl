<@override name="title">我的产品</@override>
<@override name="content">

<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>1</span>
                        <p>确认转让信息</p>
                    </li>
                    <li>
                        <span>2</span>
                        <p>确认合同</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>安全认证</p>
                    </li>
                    <li class="last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p>发布成功</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>项目信息</h3>
                </header>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>项目名称</th>
                        <th>产品资产</th>
                        <#if (mp.transfer_price)?? && (mp.transfer_price &gt; 0)>
                            <th>投资金额</th>
                        </#if>
                        <th>理财期限</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <span><strong>${(mp.product_name)!}</strong></span>
                        </td>
                        <td>
                            <span id="amount_id">${(mp.amount)!}元</span>
                        </td>
                        <#if (mp.transfer_price)?? && (mp.transfer_price &gt; 0)>
                        <td>
                            <span id="amount_id">${(mp.transfer_price)!}元</span>
                        </td>
                        </#if>
                        <td>
                            <span>${((mp.time_limit)!0)}天</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>项目信息</h3>
                </header>
                <div class="reg_form">
                    <div class="grid_8">
                        <form>

                        </form>
                        <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                            <tr>
                                <th>转让价格：</th>
                                <td>
                                    <input type="text" id="t_price" placeholder="请输入转让价格" class=""/>
                                    <span>价格不能高于${(amount)!}元</span>
                                </td>
                            </tr>
                            <#if aging &gt; 0>
                            <tr>
                                <th>转让时效：</th>
                                <td>
                                    <p>${(aging)!0}小时</p>
                                </td>
                            </tr>
                            </#if>
                            <#if free_times &gt; 0>
                                <tr>
                                    <th>挂单免费时间：</th>
                                    <td>
                                        <p> ${beginTime?string('yyyy-MM-dd')}
                                            ~ ${endTime?string('yyyy-MM-dd')}</p>
                                    </td>
                                </tr>
                            </#if>
                            <#if lodging_fee??>
                                <tr>
                                    <th>挂单费</th>
                                    <td><p>${(lodging_fee)!}元/天</p></td>
                                </tr>
                            </#if>
                            <tr>
                                <th></th>
                                <td>
                                    <dl class="warning_list">
                                        <dt><i class="icon_warning"></i>备注：</dt>
                                        <dd>
                                            · 转让价价格不能高于项目本金+预期收益；
                                        </dd>
                                        <#if aging &gt; 0>
                                        <dd>
                                            · 转让时效为 ${aging!}小时。
                                        </dd>
                                        </#if>
                                        <#if ratio &gt; 0>
                                            <dd>
                                                · 转让手续费为转让价格的${ratio}%。
                                            </dd>
                                        </#if>
                                        <dd>
                                            · 挂单免费时间 为 ${free_times!} 天。
                                        </dd>
                                    </dl>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td class="submit"><input id="sbmt" type="submit" value="下一步"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop"></div>
    <div class="pop-con fixed pop-conadd the_pop" style="top:10%;">
        <div class="pop-title relative">
            <p>确认转让</p>
            <a class="btn-close absolute mask_close" href="javascript:void(0);"  title="关闭"></a>
        </div>
        <div class="pop-con-ct">
            <form action="${ctx}/member/transfer/contract" id="contractform" method="post">
                <input type="hidden" name="mpid" value="${mp.id!}">
            <table class="tb_form" id="info-panel">
            </table>
            </form>
            <div class="pop_but ta_center">
                <a href="javascript:void(0);" class="btn-pop btn-cancel ml10 mask_close" >取消</a>
                <a href="javascript:void(0);" id="confirm1" class="btn-pop btn-confirm">确定</a>
            </div>
        </div>
    </div>
</section>


<script id="confirm_dialog-template" type="text/x-handlebars-template">
    <tr>
        <th>资金价值：</th>
        <td>
            <input type="hidden" name="amount" value="{{amount}}">
            <p><span id="ajax_amount">{{amount}}</span>元<span class="tip"></span></p>
        </td>
    </tr>
    <tr>
        <th>转让价格：</th>
        <td>
            <input type="hidden" name="price" value="{{price}}">
            <p><span class="red" id="ajax_price">{{price}}元</span></p>
        </td>
    </tr>
    {{#if free_times_show}}
        <tr>
            <th>免费天数：</th>
            <td>
                <p><span id="free_times">{{free_times}}</span>天</p>
            </td>
        </tr>
    {{/if}}

    <input type="hidden" id="loss" value="{{loss}}">
    {{#if loss_show}}
    <tr>
        <th>预计亏本金额：</th>
        <td>
            <p><span class="red">{{loss}}</span>元 = (资金价值 - (转让价格 + 手续费))</p>
        </td>
    </tr>
    {{/if}}
    {{#if lodging_fee_show}}
    <tr>
        <th>挂单费：</th>
        <td>
            <input type="hidden" name="lodging_fee" value="{{lodging_fee}}">
            <p><span id="lodging_fee">{{lodging_fee}}</span>元/天</p>
        </td>
    </tr>
    {{/if}}
    {{#if fee_show}}
    <tr>
        <th>转让手续费：</th>
        <td>
            <input type="hidden" name="fee" value="{{fee}}">
            <p><span id="ajax_fee">{{fee}}</span>元<span class="tip">转让价格*${ratio}%</span></p>
        </td>
    </tr>
    {{/if}}
    {{#if aging_show}}
    <tr>
        <th>转让时效：</th>
        <td>
            <p><span id="ajax_aging">{{aging}}</span>小时<span class="tip">超过时效未成交将被系统强制下架</span></p>
        </td>
    </tr>
    {{/if}}
</script>

</@override>

<@override name="js">
<script type="text/javascript">
    var mpId = '${(mp.id)!0}';
</script>
<script type="text/javascript" src="${ctx}/res/app/member/transfer/confirm.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
<@override name="title">我的产品</@override>
<@override name="content">

<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>1</span>
                        <p>确认转让信息</p>
                    </li>
                    <li class="cur">
                        <span>2</span>
                        <p>确认合同</p>
                    </li>
                    <li class="cur">
                        <span>3</span>
                        <p>安全认证</p>
                    </li>
                    <li class="cur last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p>发布成功</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <#if error??>

                    <div class="zrsq_error">
                        <p class="htitle">转让发布失败！</p>

                        <p>· 该产品已转让；</p>

                        <p class="link">您现在可以 <a href="${ctx}/member/product">管理理财项目</a> 或 <a
                                href="${ctx}/product">浏览更多投资项目</a></p>
                    </div>
                <#else>

                    <div class="zrsq_ok">
                        <p class="htitle">转让产品已发布<span>订单编号：<i class="red">${(order.trade_no)!}</i></span></p>

                        <p>· 转让成交后，融理财富将以短信通知您；</p>

                        <p class="link">您现在可以 <a href="${ctx}/member/product">管理理财项目</a> 或 <a
                                href="${ctx}/product">浏览更多投资项目</a></p>
                    </div>
                </#if>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

</@override>

<@override name="js">
</@override>
<@extends name="../../main/_main.ftl"></@extends>
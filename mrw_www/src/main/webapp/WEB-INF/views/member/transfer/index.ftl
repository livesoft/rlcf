<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_product" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的理财</h3>
                </header>
                <div class="tab2 wdlc_tab" id="wrapper_tab_2">

                    <a href="${ctx}/member/product" class="tab1 tab_link">持有中</a>
                    <a href="${ctx}/member/product/over" class="tab3 tab_link">已结束</a>
                    <div class="clear"></div>
                    <div class="tab2 tab_body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>项目名称</th>
                                <th>订单号</th>
                                <th>项目本金</th>
                                <th>收益率</th>
                                <th>购买时间</th>
                                <th>持有天数</th>
                                <th>转让状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if data.list?? && (data.list?size >0 )>
                                <#list data.list! as d>
                                <tr>
                                    <td>
                                        <span>${d.product_name!}</span>
                                    </td>
                                    <td>
                                        <span>${d.trade_no!0}</span>
                                    </td>
                                    <td>
                                        <span>${d.amount!0}</span>
                                    </td>
                                    <td class="red">
                                        <span>${d.yield!0}%</span>
                                    </td>
                                    <td>
                                        <span>${d.buy_time_year!0}年${d.buy_time_month!0}月${d.buy_time_day!0}日</span>
                                    </td>
                                    <td>
                                        <span>${d.have_day!0}</span>
                                    </td>
                                    <td>
                                        <#if d.transfer_status??>
                                            <#if d.transfer_status == 1><span>已转让</span>
                                            <#elseif d.transfer_status == 2><span>挂单中</span>
                                            <#elseif d.transfer_status == 3><span>已转让</span>
                                            <#elseif d.transfer_status == 4><span>转让失败</span>
                                            </#if>
                                        </#if>
                                    </td>
                                    <td>
                                        <a href="${ctx}/member/product/detail/${d.id!}" class="common_button">详情</a>
                                    </td>

                                </tr>
                                </#list>
                            <#else>

                            <tr>
                                <td colspan="8">
                                    <span>暂无记录</span>
                                </td>
                            </tr>
                            </#if>

                            </tbody>
                        </table>
                        <#import '*/../../macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=data! url='${ctx}/member/product/transferable'/>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop"></div>
    <div class="pop-con absolute pop-conadd the_pop">
        <div class="pop-title relative">
            <p>确认赎回</p>
            <a class="btn-close absolute" href="javascript:void(0);" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct">
            <p class="ta_center">
                确认赎回该产品吗？
            </p>
            <div class="pop_but ta_center">
                <a href="javascript:void(0);" class="btn-pop btn-cancel ml10">取消</a>
                <a href="javascript:void(0);" class="btn-pop btn-confirm">确定</a>
            </div>
        </div>
    </div>
</section>
</@override>
<@override name="footer">
    <#include "*/../_foot.html">
</@override>
<@extends name="../../main/_main.ftl"></@extends>
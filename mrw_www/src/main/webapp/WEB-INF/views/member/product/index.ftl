<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_product" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的理财</h3>
                </header>
                <div class="wdlc_tab tab1" id="wrapper_tab_2">
                    <a href="${ctx}/member/product" class="tab1 tab_link">持有中</a>
                    <a href="${ctx}/member/product/over" class="tab3 tab_link">已结束</a>
                    <div class="clear"></div>
                    <div class="tab1 tab_body">
                        <form action="${ctx}/member/product" method="get">
                        <table class="tb_form higher" border="" cellspacing="" cellpadding="" style="margin: 0;">
                            <tbody>
                            <tr>
                                <th style="width: 70px;"><label for="orderno">订单号：</label> </th>
                                <td style="width: 135px;">
                                    <input id="orderno" name="orderno" maxlength="24" type="text" value="${orderno!}" style="width: 180px;">
                                </td>
                                <th style="width: 60px;">状态：</th>
                                <td style="width: 100px;">
                                    <select id="status_chosen" name="ps">
                                        <option value="">全部</option>
                                        <option value="1" <#if ps?? && ps==1>selected</#if> >退单申请中</option>
                                        <option value="2" <#if ps?? && ps==2>selected</#if> >已退单</option>
                                        <#--<option value="3" <#if ps?? && ps==3>selected</#if> >可转让</option>-->
                                        <option value="4" <#if ps?? && ps==4>selected</#if> >转让中</option>
                                        <option value="5" <#if ps?? && ps==5>selected</#if> >已转让</option>
                                    </select>
                                </td>

                                <td style="width: 120px;">
                                    <button type="submit" class="common_button">查询</button>
                                </td>
                            </tr>
                            </tbody>
                        </table></form>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>项目名称</th>
                                <th>订单号</th>
                                <th>投资金额</th>
                                <th>收益率</th>
                                <th>认购状态</th>
                                <th>交易状态</th>
                                <th>到期时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if data.list?? && (data.list?size >0 )>
                                <#list data.list! as d>
                                <tr>
                                    <td>
                                        <span class="poshytip-target" title="${d.product_name}"><#if d.product_name?? && d.product_name?length &gt; 10> ${d.product_name?substring(0,10)}... <#else>${d.product_name!}</#if> </span>
                                    </td>
                                    <td>
                                        <span class="poshytip-target" title="${d.trade_no!}">${d.trade_no?substring(5,10)}...</span>
                                    </td>
                                    <td>
                                        <span><#if d.transfer_price?? && d.transfer_price&gt;0>${d.transfer_price!0}元<#else>${d.amount!0}元</#if></span>
                                    </td>
                                    <td class="red">
                                        <span>${d.yield!0}%</span>
                                    </td>
                                    <td>
                                        <span><#if d.product_status==1 || d.product_status==2>认购中<#elseif d.product_status==3>认购失败<#elseif d.product_status==4>已结束<#elseif d.product_status==8>认购成功</#if> </span>
                                    </td>
                                    <td>
                                        <span><#if d.trade_status==3>交易成功<#else>交易失败</#if> </span>
                                    </td>
                                    <td>
                                        <span>${(d.due_date)!?string('yyyy-MM-dd')}</span>
                                    </td>
                                    <td>
                                        <a href="${ctx}/member/product/detail/${d.id!}" class="common_button">详情</a>
                                    </td>
                                </tr>
                                </#list>
                            <#else>
                            <tr>
                                <td colspan="8">
                                    <span>暂无记录</span>
                                </td>
                            </tr>
                            </#if>

                            </tbody>
                        </table>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=data! url='${ctx}/member/product'/>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

</@override>
<@override name="js">
    <script type="text/javascript">
        $('.poshytip-target').poshytip({
            className: 'tip-yellowsimple',
            showTimeout: 1,
            alignTo: 'target',
            alignX: 'center',
            alignY: 'bottom',
            offsetY: 5,
            allowTipHover: false
        });
    </script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
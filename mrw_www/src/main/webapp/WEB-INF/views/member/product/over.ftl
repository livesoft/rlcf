<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_product" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的理财</h3>
                </header>
                <div class="tab3 wdlc_tab" id="wrapper_tab_2">

                    <a href="${ctx}/member/product" class="tab1 tab_link">持有中</a>
                    <a href="${ctx}/member/product/over" class="tab3 tab_link">已结束</a>
                    <div class="clear"></div>
                    <div class="tab3 tab_body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>项目名称</th>
                                <th>订单号</th>
                                <th>项目本金</th>
                                <th>收益率</th>
                                <th>购买时间</th>
                                <th>交易状态</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if data.list?? && (data.list?size >0 )>
                                <#list data.list! as d>
                                <tr>
                                    <td>
                                        <span>${d.product_name!}</span>
                                    </td>
                                    <td>
                                        <span>${d.trade_no!0}</span>
                                    </td>
                                    <td>
                                        <span>${d.amount!0}</span>
                                    </td>
                                    <td class="red">
                                        <span>${d.yield!0}</span>
                                    </td>
                                    <td>
                                        <span>${(d.buy_time)!?string('yyyy-MM-dd')}</span>
                                    </td>
                                    <td>
                                        <span><#if d.trade_status==3>交易成功<#else>交易失败</#if> </span>
                                    </td>
                                    <td>
                                        <a href="${ctx}/member/product/detail/${d.id!}" class="common_button">详情</a>
                                    </td>
                                </tr>
                                </#list>
                            <#else>
                            <tr>
                                <td colspan="7">
                                    <span>暂无记录</span>
                                </td>
                            </tr>
                            </#if>

                            </tbody>
                        </table>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=data! url='${ctx}/member/product/over'/>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop"></div>
    <div class="pop-con absolute pop-conadd the_pop">
        <div class="pop-title relative">
            <p>确认赎回</p>
            <a class="btn-close absolute" href="javascript:void(0);" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct">
            <p class="ta_center">
                确认赎回该产品吗？
            </p>
            <div class="pop_but ta_center">
                <a href="javascript:void(0);" class="btn-pop btn-cancel ml10">取消</a>
                <a href="javascript:void(0);" class="btn-pop btn-confirm">确定</a>
            </div>
        </div>
    </div>
</section>
</@override>

<@extends name="../../main/_main.ftl"></@extends>
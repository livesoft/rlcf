<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_product" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="grid_9 mypage_main">
                <div class="mypage_common_block">
                    <header class="mypage_common_header">
                        <a href="${ctx}/member/product" class="right_link">返回</a>

                        <h3>订单信息</h3>
                    </header>
                    <div class="kzr_ddxx">
                        <ul>
                            <li style="width: 100%;"><label>订单编号：</label><span>${(product.trade_no)!}</span></li>
                        </ul>
                        <div class="clear"></div>
                        <ul>
                            <li style="width: 130px;"><label>订单类别：</label><span>${orderType!}</span></li>
                            <li style="width: 200px;">
                                <label>购买日期：</label><span>${(product.create_time)!?string('yyyy-MM-dd HH:mm')}</span>
                            </li>
                            <li style="width: 160px;"><label>订单金额：</label><span class="red">${(product.trade_amount)!}</span>元</li>
                            <li style="width: 140px;"><label>订单状态：</label><#if (product.trade_status == 3)>交易成功<#else> <span class="red">交易失败</span> </#if></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <header class="mypage_common_header">
                        <h3>产品信息</h3>
                    </header>
                    <div class="kzr_cpxx">
                        <ul>
                            <li><label>产品名称：</label><span title="${(product.productname)!}">
                                <#if (product.productname)!?length&gt;11>${(product.productname)?substring(0,10)}...<#else>${(product.productname)!}</#if></span></li>
                            <li><label>产品品牌：</label><span>${(product.brandname)!}</span></li>
                            <li><label>产品资产：</label><span>${(product.amount)!}元</span></li>
                            <li><label>收益方式：</label><span>${(collection_mode)!}</span></li>
                            <li><label>募集资金：</label><span>${(product.price)!'0'}元</span></li>
                            <li><label>投资金额：</label><span>${(product.trade_amount)!'0'}元</span></li>
                            <#--<li><label>可投金额：</label><span>${(product.remaining_amount)!'0'}元</span></li>-->
                            <li><label>预期年化收益率：</label><span class="red">${(product.yield)!}%</span></li>
                            <li><label>风险程度：</label><span><i
                                    class="icon_heart_small ht${(product.risk_tolerance)!'1'}"></i></span></li>
                            <li>
                                <label>理财期限：</label><span>${(product.time_limit)!}${(product.time_limit_unit == 'day')!?string('天','月')}</span>
                            </li>
                            <#--<li><label>剩余投资天数：</label><span>${(days)!'0'}天</span></li>-->
                            <li><label>认购状态：</label><span>${productStatus!}</span></li>
                            <li><label>购买日期：</label><span>${(product.buy_time)!?string('yyyy-MM-dd')}</span></li>
                            <li><label>起息日期：</label><span>${(product.valuedate)!?string('yyyy-MM-dd')}</span></li>
                            <li><label>结息日期：</label><span>${(product.due_date)!?string('yyyy-MM-dd')}</span></li>
                            <li><label>预期收益：</label><span class="red">${(prifit)!'0.00'}</span>元</li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <#if transferproduct??>
                        <header class="mypage_common_header">
                            <h3>转让信息</h3>
                        </header>
                        <div class="kzr_cpxx">
                            <ul>
                                <li><label>挂单时间：</label><span>${put_time?string('yyyy-MM-dd HH:mm')}</span></li>
                                <li><label>挂单价格：</label><span class="red">${(transferproduct.price)!}</span>元</li>
                                <li><label>挂单天数：</label><span> ${(put_days)!}天 </span></li>
                                <li><label>手续费：</label><span class="red">${(transferproduct.handing_charge)!'0'}</span>元</li>
                                <li><label>挂单费：</label><span class="red"><#if transferproduct.lodging_free??>${transferproduct.lodging_free}元<#else>${transfer_rule.lodging_fee!}元/天</#if></span></li>
                               <#if transferproduct.trade_time??> <li><label>交易时间：</label><span>${(transferproduct.trade_time)?string('yyyy-MM-dd HH:mm')}</span></li></#if>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </#if>


                    <header class="mypage_common_header">
                    </header>
                    <div class="kzr_cpxx">

                        <ul>
                            <#if bizbtn.transferDesc?? && bizbtn.transferDesc?length &gt; 0>
                            <ul>
                                <li style="width: 100%;"><label>转让状态说明：</label><span>${bizbtn.transferDesc}</span></li>
                            </#if>
                            <#if bizbtn.cancelDesc?? && bizbtn.cancelDesc?length &gt; 0>
                                <li style="width: 100%;"><label>退单状态说明：</label><span>${bizbtn.cancelDesc}</span></li>
                            </#if>
                            <#if bizbtn.playmoneyDesc?? && bizbtn.playmoneyDesc?length &gt; 0>
                                <li style="width: 100%;"><label>打款状态：</label><span>${bizbtn.playmoneyDesc}</span></li>
                            </#if>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <div class="pop_but ta_center">
                        <#if bizbtn.cancel>
                            <a href="javascript:void(0);" id="back_btn" data-pk="${product.id!}"
                               class="btn-pop btn-confirm ml10">退单</a>
                        <#else>
                            <a href="javascript:void(0);" class="btn-pop btn-cancel ml10" title="${bizbtn.cancelDesc!}">退单</a>
                        </#if>
                        <#if bizbtn.transfer>
                            <a href="javascript:void(0);"
                               class="btn-pop btn-confirm" id="transfer_btn">转让</a>
                        <#elseif product.transfer_status == 2>
                        <#--2表示挂单中-->
                            <a href="javascript:void(0);" data-pk="${(product.id)!}"
                               class="btn-pop btn-confirm" id="can_transfer_btn">撤销转让</a>
                        <#else>
                            <a href="javascript:void(0);" class="btn-pop btn-cancel ml10"
                               title="${bizbtn.transferDesc!}">转让</a>
                        </#if>

                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="masker" style="display: none;" id="cancel_transfer_dialog">
    <div class="pop_mask fixed the_pop"></div>
    <div class="pop-con absolute pop-conadd the_pop">
        <div class="pop-title relative">
            <p>撤销转让确定</p>
            <a class="btn-close absolute" href="javascript:void(0);" id="cancel_transfer_mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct">
            <input type="hidden" id="member_product_id">

            <p class="ta_center">
                您确定撤掉该产品的转让么？
            </p>
            <p class="ta_center"><span class="red" style="font-size: 12px;">温馨提示：撤销转让产品时若已经有用户同时购买您的产品，平台是不允许你撤销转让。</span> </p>
            <input type="hidden" id="product_id_transfer">

            <div class="pop_but ta_center">

                <a href="javascript:void(0);" class="btn-pop btn-cancel ml10" id="cancel_transfer_cancel_btn">取消</a>
                <a href="javascript:void(0);" class="btn-pop btn-confirm" id="cancel_transfer_confirm_btn">确定</a>
            </div>
        </div>
    </div>
</section>

    <#if bizbtn.cancel>
    <section class="masker" style="display: none;" id="cancel_dialog">
        <div class="pop_mask fixed the_pop"></div>
        <div class="pop-con absolute pop-conadd the_pop">
            <div class="pop-title relative">
                <p>确认退单</p>
                <a class="btn-close absolute" href="javascript:void(0);" id="mask_close" title="关闭"></a>
            </div>
            <div class="pop-con-ct">
                <input type="hidden" id="member_product_id">

                <p class="ta_center">
                    确认退单么？
                </p>
                <p class="ta_center"><span class="red" style="font-size: 12px;">注：退款24小时内到账，如遇周末或者法定节假日顺延，请注意查收!</span> </p>

                <div class="pop_but ta_center">

                    <a href="javascript:void(0);" class="btn-pop btn-cancel ml10" id="cancel_btn">取消</a>
                    <a href="javascript:void(0);" class="btn-pop btn-confirm" id="confirm_btn">确定</a>
                </div>
            </div>
        </div>
    </section>
    </#if>
</@override>

<@override name="js">
<script type="text/javascript">
    var mpid = '${product.id!}';
</script>
<script type="text/javascript" src="${ctx}/res/app/member/productdetail.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
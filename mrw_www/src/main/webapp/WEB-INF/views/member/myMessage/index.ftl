<@override name="title">我的消息</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_message" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的消息</h3>
                </header>
                <div class="list_filter bold">
                    <div class="fl_right">
                        <p>仅显示最近三个月的消息：共${count!}条消息，其中<span class="red">${unread!}</span>条未读</p>
                    </div>
                    <div>
                        <label for="checkall"><input onclick="checkall(${message?size!})" type="checkbox" id="checkall"/>&nbsp;全选</label>
                        <a href=" <#if message??&&message?size gt 0>javascript:delmessage()</#if>" class="btn_gray">删除</a>
                        <a href="<#if message??&&message?size gt 0>javascript:sub()</#if>" class="btn_gray">标记为已读</a>
                    </div>
                </div>
                <table class="table table-bordered tb_msg" id="tb_msg">
                    <thead>
                    <tr>
                        <th width="140">发自</th>
                        <th>内容</th>
                        <th>时间</th>
                    </tr>
                    </thead>
                    <tbody>
                        <#if message.list??&&message.list?size gt 0>
                            <#list message.list as m>
                            <tr id="tr${m.id!}" class="<#if m.status??&&m.status==0>unread<#else></#if>">
                                <td>
                                    <input name="megs" id="check${m_index}" type="checkbox" value="${m.id!}"/>
                                    <span class="msg_from"><#if m.type==1>用户消息<#else>系统消息</#if></span>
                                </td>
                                <td align="left">
                                    <a href="javascript:lookup(${m.id!})"><span class="msg_title">${m.title!}</span>
                                    <#--<span class="msg_desc">${m.content!}</span>-->
                                    </a>
                                </td>
                                <td>
                                    <span class="msg_time">${m.send_time!}</span>
                                </td>
                            </tr>
                            </#list>
                        <#else>
                        <tr>
                            <td colspan="3">
                                <span>暂无消息</span>
                            </td>
                        </tr>
                        </#if>
                    </tbody>
                    <tfoot>
                    <td colspan="3">
                        <p class="page_info">${(page-1)*10+1}-${page*10}条，共${count!}条</p>
                    </td>
                    </tfoot>
                </table>
                <div class="pagination right">
                    <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=message! url='${ctx}/member/myMessage' pos="right"/>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
<section class="masker" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>阅读消息</p>
            <a class="btn-close absolute" href="javascript:mask_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct">
            <div class="msg_read">
                <h2 id="title"></h2>
                <div id="content" class="ctt">
                </div>
            </div>
        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>

</@override>

<@override name="js">
<script type="text/javascript">
    var ctx = "${ctx}";
</script>
<script type="text/javascript">

    function checkall(msize){
        if( $("#checkall").attr('checked')){
            for (var i = 0; i <= msize; i++) {
                $("#check"+i).attr('checked', true);
            }
        }else{
            for (var i = 0; i <= msize; i++) {
                $("#check"+i).attr('checked', false);
            }
        }

    }

    function sub(){
        var spCodesTemp = "";
        $('input:checkbox[name=megs]:checked').each(function (i) {
            if (0 == i) {
                spCodesTemp = $(this).val();
            } else {
                spCodesTemp += ("," + $(this).val());
            }
        });
        var formData = {mid:spCodesTemp};
        $.ajax({
            url: ctx + "/member/myMessage/read",
            data:formData,
            type: 'POST',
            success:function(data){
                if(data.status=='OK'){
                    window.location.reload();
                }else{
                    layer.alert(data.message, 2, !1);
                }
            }
        });
    }

    function delmessage(){
        if($("#checkall").attr('checked')){

        }
        $.layer({
            shade: [0],
            area: ['auto','auto'],
            dialog: {
                msg: '您是否删除选中的消息？',
                btns: 2,
                type: 4,
                btn: ['删除','不删除'],
                yes: function(){
                    var spCodesTemp = "";
                    $('input:checkbox[name=megs]:checked').each(function (i) {
                        if (0 == i) {
                            spCodesTemp = $(this).val();
                        } else {
                            spCodesTemp += ("," + $(this).val());
                        }
                    });
                    var formData = {mid:spCodesTemp};
                    $.ajax({
                        url: ctx + "/member/myMessage/delete",
                        data:formData,
                        type: 'POST',
                        success:function(data){
                            if(data.status=='OK'){
                                window.location.reload();
                            }else{
                                layer.alert(data.message, 2, !1);
                            }
                        }
                    });
                }, no: function(){

                }
            }
        });
    }

    function toogleDialog(target, show) {
        var mask = $(target).show();
        if (!show) {
            mask.hide();
            return;
        }
        var dialog = mask.find('.pop-con').show();
        var scrollTo = $(document).scrollTop();
        var winHeight = $(window).height();
        var winWidth = $(window).width();

        dialog.css({
            margin: 0,
            top: scrollTo + (winHeight - dialog.height()) / 2,
            left: (winWidth - dialog.width()) / 2
        });
    }

    function lookup(mid) {
        $.ajax({
            url: ctx + "/member/myMessage/lookup?mid=" + mid,
            type: 'GET',
            success: function (data) {
                $("#title").html(data.title);
                $("#content").html('<p>'+data.content+'</p>');
               toogleDialog('.masker', true);
            }
        });
    }

    function mask_close(){
        toogleDialog('.masker', false);
        parent.location.reload();
    }
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
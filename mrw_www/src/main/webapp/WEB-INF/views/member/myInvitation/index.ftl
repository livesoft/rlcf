<@override name="title">我的邀请</@override>
<@override name="content">
<meta name="description" content="来融理财富，购买新手专享产品享10%超高年化收益~ ${domain!}register?phone=${phone!}">
<section class="mypage_wrapper my_product" l_id="my_invite" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的邀请</h3>
                </header>
                <div class="zhsz_detail mb50">
                    <ul>
                        <li><i class="icon_yaoqing"></i><label>已邀请好有：</label><span class="red">${invites?size!}</span> 人</li>
                        <li><i class="icon_jifen"></i><label>累计结算收入：</label><span class="red">
                            <#assign score = 0 />
                            <#if invites??&&invites?size gt 0>
                                <#list invites as invite>
                                    <#assign score = invite.integral+score />
                                </#list>
                             ${score!}
                            <#else>
                        0
                            </#if>
                        </span> 积分</li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <header class="mypage_common_header">
                    <h3>邀请链接</h3>
                </header>
                <div class="yq_link">
                    <div class="tip">
                        复制下方邀请链接发送给好友，好友通过链接注册并交易，您即可获得奖励。 <a href="javascript:void(0);" title="邀请好友，好友成功注册后，每一位好友注册成功，即赠送${(points.integral)!'0'}积分。" id="checkRule">查看详细规则</a>
                    </div>
                    <div class="link">
                        来融理财富，购买新手专享产品享10%超高年化收益~<br />
                       ${domain!}register?phone=${phone!}
                    </div>
                    <div class="share_area">


                        <div class="bshare-custom"><a title="分享到QQ空间" class="bshare-qzone"></a
                                ><a title="分享到新浪微博" class="bshare-sinaminiblog"></a>
                            <a title="分享到人人网" class="bshare-renren"></a>
                            <a title="分享到腾讯微博" class="bshare-qqmb"></a>
                            <a title="分享到网易微博" class="bshare-neteasemb"></a>
                            <a title="更多平台" class="bshare-more bshare-more-icon more-style-addthis"></a>

                            <span class="BSHARE_COUNT bshare-share-count">0</span>
                            <#--<span class="common_button" >   <a href="#" class="common_button">复制链接</a></span>-->
                        </div>

                        <#--<p>-->
                            <#--分享到：<a href="#"><i class="icon_share"></i>QQ空间</a>-->
                            <#--<a href="#"><i class="icon_share xlwb"></i>新浪微博</a>-->
                            <#--<a href="#"><i class="icon_share rrw"></i>人人网</a>-->
                            <#--<a href="#"><i class="icon_share txwb"></i>腾讯微博</a>-->
                            <#--<a href="#"><i class="icon_share wywb"></i>网易微博</a>-->
                            <#--<a href="#" class="common_button">复制链接</a>-->
                        <#--</p>-->
                    </div>

                    <div class="clear"></div>

                </div>
                <header class="mypage_common_header">
                    <h3>邀请结果</h3>
                </header>
                <table class="table table-bordered tb_yqjg">
                    <thead>
                    <tr>
                        <th>邀请人</th>
                        <th>邀请时间</th>
                        <th>邀请结果</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#if invites??&&invites?size gt 0>
                        <#list invites as invite>
                        <tr>
                            <td>
                                <span>${invite.phone!}</span>
                            </td>
                            <td>
                                <span>${invite.reg_time!}</span>
                            </td>
                            <td class="green">
                                <span>注册成功</span>
                            </td>
                        </tr>
                        </#list>
                    <#else>
                    <tr>
                        <td colspan="3">
                            <span>暂无邀请信息</span>
                        </td>
                    </tr>
                    </#if>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript">
    var ctx = "${ctx}";
</script>
<script type="text/javascript" src="${ctx}/res/app/member/questionnaire.js"></script>
<script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=2&amp;lang=zh"></script>
<script type="text/javascript" charset="utf-8" src="http://static.bshare.cn/b/bshareC0.js"></script>
<script type="text/javascript">

    $('#checkRule').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'bottom',
        offsetY: 5,
        allowTipHover: false
    });

</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>&nbsp;</span>

                        <p>&nbsp;</p>
                    </li>
                    <li class="cur">
                        <span>1</span>
                        <p>安全认证</p>
                    </li>
                    <li class="cur">
                        <span>2</span>

                        <p>赎回成功</p>
                    </li>
                    <li class="cur last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p></p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <div class="zrsq_ok">
                    <p class="htitle">退单成功<span>订单编号：<i class="red">${order.trade_no!}</i></span></p>

                    <p class="link">您现在可以 <a href="${ctx}/member/product">管理我的理财项目</a> 或 <a href="${ctx}/product">浏览更多投资项目</a></p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@extends name="../../main/_main.ftl"></@extends>
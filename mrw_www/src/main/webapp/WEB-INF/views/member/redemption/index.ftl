<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block sqzr_steps">
                <ul>
                    <li class="cur">
                        <span>&nbsp;</span>

                        <p>&nbsp;</p>
                    </li>
                    <li class="cur">
                        <span>1</span>
                        <p>安全认证</p>
                    </li>
                    <li >
                        <span>2</span>

                        <p>赎回成功</p>
                    </li>
                    <li class="last-child">
                        <span><i class="icon_step_over"></i></span>
                        <p></p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <div class="reg_form">
                    <div class="grid_8">
                        <input type="hidden" id="memberproductid" value="${memberProduct.id!}"/>
                        <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                            <tr>
                                <th>交易密码：</th>
                                <td>
                                    <input type="password" id="trade_pwd" placeholder="请输入交易密码" />
                                </td>
                            </tr>
                            <tr>
                                <th>手机动态码：</th>
                                <td>
                                    <input type="text" id="sms_code" placeholder="请点击获取短信码" />
                                    <a id="sendsms_btn" href="javascript:void(0);">获取手机动态码</a>
                                    <div class="send_mobile_message_body">
                                        <a class="count_down" href="javascript:void(0);">60秒后重新获取</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td class="submit"><input id="ok_btn" type="submit" value="确  定"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript">

    $(function(){

        sendRequestMobileMessage($('#sendsms_btn'), ctx + '/member/redemption/send', 'POST');

        $('#ok_btn').click(function(e){

            var pwd = $('#trade_pwd').val();
            var code = $('#sms_code').val();
            var memberproductid = $('#memberproductid').val();

            $.ajax({
                url: ctx + '/member/redemption/verification',
                type: 'POST',
                cache:false,
                data: {'pwd': pwd, code: code}
            }).done(function(rst){
                if(rst.status == 'OK'){
                    window.location.href = ctx + '/member/redemption/success/'+memberproductid
                } else {
                    layer.alert(rst.message, 5, !1);
                }
            });
            return false;
        })
    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
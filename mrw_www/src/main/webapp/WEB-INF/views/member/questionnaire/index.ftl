<@override name="title">风险评估</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="account_settings" p_id="m_my_product">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block fxpg_item">
                <#if result??>
                    <#list result as problem>
                        <span id="no${problem_index+1!}">第${problem_index+1!}题</span>
                    </#list>
                </#if>
            </div>
            <div class="mypage_common_block fxpg_list" id="box_pan">
                <ul id="qa_box">
                    <#if result??>
                        <#list result as problem>
                            <li>
                                <h2>${problem_index+1!}. ${problem.title!}</h2>

                                <div class="items">
                                    <#list problem.options as option>
                                        <label for="${problem_index+1!}b_${option_index+1!}_a">
                                            <input name="option" type="checkbox" value="${option.id!}"
                                                   id="${problem_index+1!}b_${option_index+1!}_a"
                                                   data-problem-index="${problem_index+1}"
                                                   data-option-size="${problem.options?size!}"
                                                    >${option.content!}
                                        </label>
                                    </#list>
                                </div>
                            </li>
                        </#list>
                    </#if>
                </ul>
                <div class="icon_result" id="rstbox" style="display: none;"></div>
            </div>
            <div class="sbmt_area">
                <a href="javascript:void(0);" data-size="${result?size!}" data-redirect-serv="${redirect_url!}" id="submit_btn" class="common_button">${retry???string('重新评测','提交')}</a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/app/member/questionnaire.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
<@override name="title">交易记录</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_account_record" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>交易记录</h3>
                </header>
                <div class="tab1" id="wrapper_tab_2" class="wdlc_tab">
                    <a href="${ctx}/member/myAccount/${timetype}-0" class="<#if type==0>tab1</#if> tab_link">所有记录</a>

                    <a href="${ctx}/member/myAccount/${timetype}-1" class="<#if type==1>tab1</#if> tab_link">资产增加记录</a>
                    <a href="${ctx}/member/myAccount/${timetype}-2" class="<#if type==2>tab1</#if> tab_link">资产扣减记录</a>
                    <div class="clear"></div>
                    <div class="tab1 tab_body">
                        <div class="list_filter">
                            查询时间：
                            <select id="timetype" onchange="findByTime()">
                                <option <#if timetype==1>selected="selected" </#if> value="1">最近三个月</option>
                                <option <#if timetype==2>selected="selected" </#if> value="2">最近半年</option>
                            </select>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>时间</th>
                                <th>名称</th>
                                <th>资产（收入/支出）</th>
                                <#--<th>状态</th>-->
                            </tr>
                            </thead>
                            <tbody>
                                <#if accountAmounts.list??&&accountAmounts.list?size gt 0>
                                    <#list accountAmounts.list as aa>
                                    <tr>
                                        <td>
                                            <span>${aa.record_time!}</span>
                                        </td>
                                        <td>
                                            <span><strong>${aa.name!}</strong></span>
                                        </td>

                                        <#if aa.ea!=0>
                                            <td class="green">
                                                <span>-${aa.ea?string!}</span>
                                            </td>
                                        <#else>
                                            <td class="red">
                                                <span>+${aa.ex?string!}</span>
                                            </td>
                                        </#if>

                                        <#--<td>-->
                                            <#--<span>${aa.remark!}</span>-->
                                        <#--</td>-->
                                    </tr>
                                    </#list>
                                <#else>
                                <tr>
                                    <td colspan="3">
                                        <span>暂无记录</span>
                                    </td>
                                </tr>
                                </#if>

                            </tbody>
                        </table>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=accountAmounts! url='${ctx}/member/myAccount/${timetype!}-${type!}' pos="right"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript">
    function findByTime(){
        window.location.href="${ctx}/member/myAccount/"+$("#timetype").val()+"-${type}";
    }
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
<@override name="title">我的定制</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_boards" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的定制</h3>
                </header>
                <div class="tab1 wdlc_tab" id="">
                    <div class="tab1 tab_body">
                        <div class="list_filter">
                            <a href="${ctx}/member/boards/sub_page" class="btn_gray">我要定制</a>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>姓名</th>
                                <th>联系电话</th>
                                <th>时间</th>
                                <th>定制内容</th>
                            </tr>
                            </thead>
                            <tbody>
                                <#if data.list??&&data.list?size gt 0>
                                    <#list data.list as aa>
                                    <tr>
                                        <td>
                                            <span>${aa.name!}</span>
                                        </td>
                                        <td>
                                            <span>${aa.phone!}</span>
                                        </td>
                                        <td>
                                            <span>${aa.create_time!?string('yyyy-MM-dd HH:mm')}</span>
                                        </td>
                                        <td>
                                            <span class="poshytip-target" title="${aa.content!}"><#if aa.content?? && aa.content?length&gt;20>${aa.content?substring(0,19)}...<#else>${aa.content!}</#if></span>
                                        </td>
                                    </tr>
                                    </#list>
                                <#else>
                                <tr>
                                    <td colspan="4">
                                        <span>暂无记录</span>
                                    </td>
                                </tr>
                                </#if>
                            </tbody>
                        </table>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=integralRecord! url='${ctx}/member/boards' pos="right"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript">
    $('.poshytip-target').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        alignY: 'bottom',
        offsetY: 5,
        allowTipHover: false
    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
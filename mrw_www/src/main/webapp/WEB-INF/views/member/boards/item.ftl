<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_boards" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_8 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>私人定制</h3>
                </header>
                <form name="member" method="post" id="boardsForm" action="${ctx}/member/boards/save">
                    <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>联系人：</th>
                            <td>
                                <input id="name" name="boards.name" type="text" value="${(boards.name)!}" class="large required"
                                       placeholder="请输入姓名" data-rule="required;"/>
                            </td>
                        </tr>
                        <tr>
                            <th>手机号码：</th>
                            <td>
                                <input id="phone" name="boards.phone" type="text" value="${(boards.phone)!}" class="large"
                                       placeholder="请输入手机号码" data-rule="required;mobile;"/>
                            </td>
                        </tr>
                        <tr>
                            <th>定制内容：</th>
                            <td>
                                <textarea cols="50" name="boards.content" data-rule="required;">${(boards.content)!}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="submit"><input id="register" type="submit" value="确  认"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

</@override>

<@override name="js">
<script type="text/javascript">
    $(document).ready(function () {

        $('#boardsForm').validator({
            rules:{
                mobile         : [/^0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/, '请检查手机号格式']
            }
            stopOnError: false,
            timely     : true
        });

    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
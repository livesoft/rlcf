<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper" p_id="m_my_product">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <a href="${ctx}/member/center" class="right_link">&lt; 返回</a>

                    <h3>收益明细</h3>
                </header>
                <div class="list_filter">
                    <div class="fr">收益：<span class="red bold">${money?string(',###.00')!}</span>元</div>
                    查询时间：
                    <select id="time" name="type" onchange="findByTime()">
                        <option <#if type==2>selected="selected"</#if> value="2">最近三个月</option>
                        <option <#if type==3>selected="selected"</#if> value="3">最近半年</option>
                    </select>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>收益时间</th>
                        <th>产品名称</th>
                        <th>本金</th>
                        <th>收益率</th>
                        <th>收益额</th>
                    </tr>
                    </thead>
                    <tbody>
                        <#if productmoneys.list??&&productmoneys.list?size gt 0>
                            <#list productmoneys.list as pm>
                            <tr>
                                <td>
                                    <span>${pm.time!}</span>
                                </td>
                                <td>
                                    <span><strong>${pm.pname!}</strong></span>
                                </td>
                                <td>
                                    <span> ${pm.principal!}</span>
                                </td>
                                <td>
                                    <span> ${pm.yield!}%</span>
                                </td>
                                <td>
                                    <span class="red">${pm.profit!}</span>
                                </td>
                            </tr>
                            </#list>
                        <#else>
                        <tr>
                            <td colspan="5">
                                <span>暂无收益</span>
                            </td>
                        </tr>
                        </#if>
                    </tbody>
                </table>
                <#import '*/macros/pagination.ftl' as pagination>
                <@pagination.pagination page=productmoneys! url='${ctx}/member/moreProductMoney/${type}'/>
                <#---->
                <#--<div class="pagination right">-->
                    <#--<ul>-->
                        <#--<li class="prev"><span>上一页</span></li>-->
                        <#--<li class="curent"><a href="#">1</a></li>-->
                        <#--<li><a href="#">2</a></li>-->
                        <#--<li><a href="#">3</a></li>-->
                        <#--<li><a href="#">4</a></li>-->
                        <#--<li><a href="#">5</a></li>-->
                        <#--<li><span>...</span></li>-->
                        <#--<li><a href="#">100</a></li>-->
                        <#--<li class="next"><a href="#">下一页</a></li>-->
                    <#--</ul>-->
                <#--</div>-->
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript">
    function findByTime(){
        var page = '${page!}';
        var type = $("#time").val();
        window.location.href = '${ctx}/member/moreProductMoney/'+type
    }
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
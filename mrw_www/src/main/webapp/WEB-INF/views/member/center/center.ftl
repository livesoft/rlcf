<@override name="title">我的产品</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="my_center" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="wdzc_userinfo">
                <div class="avatar"><img src="${ctx}/res/css/img/tx.png" alt="Avatar"></div>
                <div class="autor" style="margin-bottom: 10px;"><strong id="phone">
                ${show_center.phone?substring(0,3)}
                    ****${show_center.phone?substring(7)}
                </strong><span>, 你好</span></div>
                <p style="margin-bottom: 15px;"><i class="icon_tzfg"></i>投资风格：<span>
                    <#if show_center.risk_tolerance?? && show_center.risk_tolerance!=0>
                        <#if dicts??&&dicts?size gt 0>
                            <#list dicts as model >
                                <#if model.code?string==show_center.risk_tolerance?string>${model.name!}</#if>
                            </#list>
                        </#if>
                    <#else>
                        <a href="${ctx}/member/questionnaire">评估</a>
                    </#if>
                </span></p>

                <p><i class="icon_tzfg"></i>剩余积分：<span class="red">
                ${show_center.integral!'0'}
                </span></p>

                <div class="clear"></div>
            </div>
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>我的资产
                    <#--<span>资产及收益不包括基金产品</span>-->
                    </h3>
                </header>
                <div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>累计投资金额</th>
                                <th>本金</th>
                                <th>累计收益</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="large red">
                                ${show_center.amount!}
                                <#--<span id="totalmoney">2<small>.00</small></span>-->
                                </td>
                                <td class="large">
                                ${show_center.investments!}
                                <#--<span id="money">2<small>.00</small></span>-->
                                </td>
                                <td class="large">
                                ${show_center.income!}
                                <#--<span id="othermoney">0<small>.00</small></span>-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <header class="mypage_common_header">
                    <h3>平均收益额</h3>
                </header>
                <div id="chart" style="height: 350px;">
                ${show_center.avg_yield!}
                </div>
                <header class="mypage_common_header">
                    <#if productmoneys??&&productmoneys?size gt 0>
                        <a href="${ctx}/member/moreProductMoney" class="right_link">更多收益详情</a>
                    <#else>
                    </#if>
                    <h3>收益明细</h3>
                </header>
                <div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>收益时间</th>
                                <th>产品名称</th>
                                <th>本金</th>
                                <th>收益率</th>
                                <th>收益额</th>
                            </tr>
                        </thead>
                        <tbody id="myproduct">
                            <#if productmoneys??&&productmoneys?size gt 0>
                                <#list productmoneys as pm>
                                    <tr>
                                        <td>
                                            <span>${pm.time!}</span>
                                        </td>
                                        <td>
                                            <span>${pm.pname}</span>
                                        </td>
                                        <td>
                                            <span> ${pm.principal!}</span>
                                        <#--<span>200<small>.00</small></span>-->
                                        </td>
                                        <td>
                                            <span> ${pm.yield!}%</span>
                                        </td>
                                        <td class="red">
                                            <span>${pm.profit!}</span>
                                        </td>
                                    </tr>
                                </#list>
                            <#else>
                                <tr>
                                    <td colspan="5">
                                        <span>暂无收益</span>
                                    </td>
                                </tr>
                            </#if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/js/chart/highcharts.js"></script>
<script type="text/javascript">

    $('#chart').highcharts({
        title: false,
        xAxis: {
            categories: ['第一周', '第二周', '第三周', '第四周', '第五周']
        },
        yAxis: {
            title: false,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            margin: 30,
            verticalAlign: 'top',
            borderWidth: 0
        },
        credits: {
            enabled:false
        },
        series: [{
            name: '平均收益额',
            data: [${weekMoney[0]}, ${weekMoney[1]}, ${weekMoney[2]}, ${weekMoney[3]}, ${weekMoney[4]}]
        }]
    });

</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
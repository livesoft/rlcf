<@override name="title">修改个人资料</@override>
<@override name="css">
<link rel="stylesheet" type="text/css" href="${ctx}/res/js/validator/jquery.validator.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/res/css/laydate/need/laydate.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/res/css/laydate/skins/dahong/laydate.css"/>
</@override>
<@override name="content">
<section class="regist_main" p_id="m_my_product">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_5 red">
                1.填写信息
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_5">
                2.修改成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <form name="member" method="post" id="member" action="${ctx}/account/update">
                    <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>性别：</th>
                            <td>
                                <input id="waman" <#if member.gender??&&member.gender==0>checked</#if> name="gender" type="radio" value="0"/>&nbsp;女&nbsp;&nbsp;&nbsp;&nbsp;
                                <input id="man" <#if member.gender??&&member.gender==1>checked</#if> name="gender" type="radio" value="1"/>&nbsp;男
                            </td>
                        </tr>
                        <tr>
                            <th>生日：</th>
                            <td>
                                <input readonly class="large" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" name="birthday" id="birthday" type="text" value="2015-05-05"/>
                                <span></span>
                            </td>
                        </tr>
                        <tr>
                            <th>邮箱：</th>
                            <td>
                                <input id="email" name="email" type="text" value="${member.email!}" class="large"
                                       placeholder="请输入邮箱"/>
                            </td>
                        </tr>
                        <tr>
                            <th>QQ：</th>
                            <td>
                                <input id="qq" name="qq" type="text" value="${member.qq!}" class="large"
                                       placeholder="请输入QQ"/>
                            </td>
                        </tr>
                        <tr>
                            <th>微信号：</th>
                            <td>
                                <input id="wxchat" name="wxchat" value="${member.wxchat!}" type="text"
                                       class="large" placeholder="请输入微信号"/>
                            </td>
                        </tr>
                        <tr>
                            <th>常驻地址：</th>
                            <td>
                                <input id="address" name="address" type="text" class="large" value="${member.address!}"
                                       placeholder="请输入常驻地址"/>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="submit"><input id="register" type="submit" value="确  认"></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/js/laydate.js"></script>
<script type="text/javascript">
    $('#member').validator({
        //自定义用于当前实例的规则
        rules: {
            email: [/^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/, '请检查邮箱格式'],
            qq: [/^[1-9]\d{4,12}$/, '请检查QQ格式']
        },
        //待验证字段集合
        fields: {
            '#email': {rule: 'email'},
            '#qq': {rule: 'qq'},
            '#wxchat': {rule: 'wx;length[~10]'},
            '#address': {rule: 'wx;length[~100]'}
        },
        valid: function (form) {
            //表单验证通过，提交表单到服务器
            var url = "${ctx}/member/account/update";
            $.ajax({
                url: url,
                data:{gender:$('input:radio[name=gender]:checked').val(),email:$("#email").val(),address:$("#address").val(),qq:$("#qq").val(),wxchat:$("#wxchat").val(),birthday:$("#birthday").val()},
                type:"POST",
                success:function(data){
                    $.layer({
                        shade: [0],
                        area: ['auto','auto'],
                        dialog: {
                            msg: '修改成功！',
                            btns: 1,
                            type: 1,
                            btn: ['确定'],
                            yes: function(){
                                window.location.href="${ctx}/member/account/accountSettings";
                            }
                        }
                    });
                }
            });
        }
    });
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
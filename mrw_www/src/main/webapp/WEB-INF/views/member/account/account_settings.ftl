<@override name="title">个人信息</@override>
<@override name="content">
<section class="mypage_wrapper my_product" l_id="account_settings" p_id="m_my_product">
    <div class="container_12 mypage_inner">
        <div class="grid_3" id="slider_bar">
            <aside>
                <nav class="right_menu">
                    <#include "../_right.html">
                </nav>
            </aside>
        </div>
        <div class="grid_9 mypage_main">
            <div class="mypage_common_block">
                <header class="mypage_common_header">
                    <h3>基本信息</h3>
                </header>
                <div class="ui_wrapper">
                    <div class="wdzc_userinfo zhsz">
                        <div class="avatar"><img src="${ctx}/res/css/img/tx.png" alt="Avatar"></div>
                        <div class="autor mb10"><strong>${show_center.phone?substring(0,3)}****${show_center.phone?substring(7)}</strong><span>, 你好</span></div>
                        <p>投资风格：<#if show_center.risk_tolerance?? && show_center.risk_tolerance !=0>
                            <#if dicts??&&dicts?size gt 0>
                                <#list dicts as model >
                                    <#if model.code?string==show_center.risk_tolerance?string>${model.name!}</#if>
                                </#list>
                            </#if>
                            <a href="${ctx}/member/questionnaire?retry=true" class="link">重新评估</a>
                        <#else>
                            <a href="${ctx}/member/questionnaire">评估</a>
                            </#if></p>

                        <#--<p>会员等级：V2</p>-->

                        <div class="clear"></div>
                    </div>
                    <div class="zhsz_detail">
                        <ul>
                            <li><label>性别：</label><span>
                                <#if show_center.gender??&&show_center.gender==1>
                                    男
                                <#elseif show_center.gender??&&show_center.gender==0>
                                    女
                                <#else>
                                    未知
                                </#if>
                            </span></li>
                            <li><label>生日：</label><span>${show_center.birthday!}</span></li>
                            <li><label>邮箱：</label><span>${show_center.email!}</span></li>
                            <li><label>QQ号：</label><span>${show_center.qq!}</span></li>
                            <li><label>微信号：</label><span>${show_center.wxchat!}</span></li>
                            <li><label>常驻地址：</label><span>${show_center.address!}</span></li>
                        </ul>
                        <div class="opt_aside">
                            <a href="${ctx}/member/account/toUpdateInfo">修改</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="ui_wrapper">
                    <div class="mail_phone">
                        <ul>
                            <li>
                                <p><strong>电子账户</strong></p>

                                <p><#if show_center.electronic_account??&&show_center.electronic_account!=""><i
                                        class="icon_ok"></i>已绑定<#else><i
                                        class="icon_warning_2"></i>未绑定<a href="${ctx}/member/electronic"
                                                                         class="link">绑定</a></#if></p>
                                <p class="gray9">
                                    <small>完成银行电子账户认证，您才可以进行投资；如有疑问，请查看<a href="${ctx}/help">《绑卡须知》</a></small>
                                </p>
                            </li>
                            <#--<li>-->
                                <#--<p><strong>手机</strong></p>-->

                                <#--<p><i class="icon_ok"></i>已绑定<a href="#" class="link">修改</a></p>-->

                                <#--<p class="gray9">-->
                                    <#--<small>您可享有手机相关的服务</small>-->
                                <#--</p>-->
                            <#--</li>-->
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <header class="mypage_common_header">
                    <h3>密码管理</h3>
                </header>
                <div class="pwd_manage">
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                <label><strong>登录密码：</strong>登录融理时需要输入的密码</label>
                                <span class="state"><i class="icon_ok"></i>已设置</span>
                                <a href="${ctx}/member/account/tochangePassword">重置登录密码</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><strong>交易密码：</strong>在账户资金变动时需要输入的密码
                                </label>
                            <span class="state"><i
                                    class="<#if show_center.trade_password_flag=='N'>icon_warning_2<#else>icon_ok</#if>"></i>
                                <#if show_center.trade_password_flag=='N'>未设置<#else>已设置</#if></span>
                                <a href="${ctx}/member/account/toAccountPassword"><#if show_center.trade_password_flag=='N'>
                                    设置交易密码<#else>重置交易密码</#if></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>

<@extends name="../../main/_main.ftl"></@extends>
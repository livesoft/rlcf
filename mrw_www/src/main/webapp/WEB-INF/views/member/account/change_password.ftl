<@override name="title">我的产品</@override>
<@override name="content">
<section class="regist_main">
    <div class="container_12">
        <div class="reg_steps">
            <div class="grid_5 red">
                1.填写密码
            </div>
            <div class="grid_1 half">
            </div>
            <div class="grid_5">
                2.设置成功
            </div>
            <div class="clear"></div>
        </div>
        <div class="reg_form">
            <div class="grid_8">
                <div class="reg_tip">
                    <i class="icon_tip"></i>
                    密码为6-16个字符，区分大小写。建议使用字母加数字或符号的组合
                </div>
                <table class="tb_form higher" border="" cellspacing="" cellpadding="">
                    <tr>
                        <th>当前密码：</th>
                        <td>
                            <input id="old_password" type="password" class="large" placeholder="请输入当前密码" />
                            <a href="${ctx}/forgetpwd">忘记密码？</a>
                        </td>
                    </tr>
                    <tr>
                        <th>新密码：</th>
                        <td>
                            <input id="password" type="password" class="large" placeholder="请输入新密码" />
                        </td>
                    </tr>
                    <tr>
                        <th>确认新密码：</th>
                        <td>
                            <input id="rpassword" type="password" class="large" placeholder="请确认新密码" />
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="submit"><input id="change" type="submit" value="确认"></td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
</@override>

<@override name="js">
<script type="text/javascript" src="${ctx}/res/app/member/password.js?_=${.now?date}">
</script>
</@override>
<@extends name="../../main/_main.ftl"></@extends>
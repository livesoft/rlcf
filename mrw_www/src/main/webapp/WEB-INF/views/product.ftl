<@override name="title">理财产品</@override>
<@override name="content">
<style type="text/css">
    .clearfix:after {
        content : " ";
        display : block;
        clear   : both;
        height  : 0;
    }

    .clearfix {
        zoom : 1;
    }
</style>
<section>
    <div class="lczx_banner_main">
        <div class="container_12"></div>
    </div>
    <div class="lczx_product_list">
        <div class="container_12" id="cc">
            <div class="tab1" id="wrapper_tab">
                <a href="#" class="tab1 tab_link">理财产品</a>
                <a href="${ctx}/transfer" class="tab2 tab_link">转让市场</a>

                <div class="clear"></div>
                <div class="tab1 tab_body" style="position: static;">
                    <div class=" product_filter">
                        <dl>
                            <dt>筛选：</dt>
                            <dd class="filter_list" data-name="type">
                                <a href="${ctx}/product/0-${timeType}-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if brand==0>class="select" </#if>>全部</a>
                                <#if brands??&&brands?size gt 0>
                                    <#list brands as b>
                                        <a href="${ctx}/product/${b.id!}-${timeType}-${statusType}-${sort_field}_${sort_dir!}#cc"
                                           <#if brand==b.id>class="select" </#if>>${b.name!}</a>
                                    </#list>
                                </#if>
                            </dd>
                        </dl>
                        <dl>
                            <dt></dt>
                            <dd class="filter_list" data-name="deadline">
                                <a href="${ctx}/product/${brand}-0-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==0>class="select"</#if> >全部</a>
                                <a href="${ctx}/product/${brand}-1-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==1>class="select"</#if> >0-10天</a>
                                <a href="${ctx}/product/${brand}-2-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==2>class="select"</#if> >10-30天</a>
                                <a href="${ctx}/product/${brand}-3-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==3>class="select"</#if> >1-3个月</a>
                                <a href="${ctx}/product/${brand}-4-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==4>class="select"</#if> >3-6个月</a>
                                <a href="${ctx}/product/${brand}-5-${statusType}-${sort_field}_${sort_dir!}#cc"
                                   <#if timeType==5>class="select"</#if> >6-12个月</a>
                            </dd>
                        </dl>
                        <dl>
                            <dt></dt>
                            <dd class="filter_list" data-name="status">
                                <a href="${ctx}/product/${brand}-${timeType}-0-${sort_field}_${sort_dir!}#cc"
                                   <#if statusType==0>class="select"</#if>>全部</a>
                                <a href="${ctx}/product/${brand}-${timeType}-1-${sort_field}_${sort_dir!}#cc"
                                   <#if statusType==1>class="select"</#if>>上线</a>
                                <a href="${ctx}/product/${brand}-${timeType}-2-${sort_field}_${sort_dir!}#cc"
                                   <#if statusType==2>class="select"</#if>>售罄</a>
                                <a href="${ctx}/product/${brand}-${timeType}-4-${sort_field}_${sort_dir!}#cc"
                                   <#if statusType==4>class="select"</#if>>到期</a>
                            </dd>
                        </dl>
                        <dl>
                            <dt>排序：</dt>
                            <dd>
                                <a href="${ctx}/product/${brand}-${timeType}-${statusType}-yield_${sort_dir!'desc'}#cc"
                                   class="btn_orderby <#if sort_field=='yield'>selected ${sort_dir!}</#if>">预期年化收益率</a>
                                <a href="${ctx}/product/${brand}-${timeType}-${statusType}-price_${sort_dir!'desc'}#cc"
                                   class="btn_orderby
                                 <#if sort_field=='price'>selected ${sort_dir!}</#if>  ">项目金额</a>
                                <a href="${ctx}/product/${brand}-${timeType}-${statusType}-due_date_${sort_dir!'desc'}#cc"
                                   class="btn_orderby
                                  <#if sort_field=='due_date'>selected ${sort_dir!}</#if>  ">到期时间</a>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                    <#if data ?? && data.list?size &gt;0 >
                        <div id="idx_section_zhuanrang">
                            <div class="zr_area lclist">
                                <ul>
                                    <#list data.list as p>
                                        <li>
                                            <div class="zr_title long">
                                                <h2 title="${(p.name)!}"><a
                                                        href="${ctx}/product/item/${p.id}"> <#if (p.name)!?length&gt;16>${(p.name)?substring(0,16)}...<#else>${(p.name)!}</#if> </a></h2>
                                                <#if p.orgname??>
                                                    <p><i class="icon_danbao"></i>${p.orgname}承兑<i class="icon_san"></i>保障收益
                                                    </p>
                                                <#else>
                                                    <p><i class="icon_danbao"></i><i class="icon_san"></i> 保障收益</p>
                                                </#if>
                                            </div>
                                            <div class="zr_common">
                                                <p class="red">${p.yield!}%</p>

                                                <p>年化利率</p>
                                            </div>
                                            <div class="zr_common">
                                                <p>${p.time_limit} <#if p.time_limit_unit=='month'>月<#else>天</#if></p>

                                                <p>理财期限</p>
                                            </div>
                                        <div class="zr_common" style="width: 125px;">
                                            <#if p.status == 8 || p.status == 2 || p.status == 7 || p.status == 4>
                                                    <p class="bblue">${(p.raised_mount)!?string(',###.00')}</p>

                                                    <p>募集资金</p>
                                            <#else>
                                                    <p class="bblue">${(p.remaining_amount)!?string(',###.00')}</p>

                                                    <p>剩余可投金额</p>
                                            </#if>
                                        </div>

                                            <div class="zr_common">
                                                <p class="mjjd">
                                                    <span class="vga"></span>
                                                    <span class="mjnum">
                                                        <@progress status=p.status progress=p.progress raised_mount=p.raised_mount/>%</span>
                                                </p>


                                                <p>募集进度</p>
                                            </div>
                                            <div class="zr_opt">
                                                <p>30天内<span class="red">${p.buys!'0'}</span>人购买</p>

                                                <#if p.status == 2 || p.status == 7>
                                                    <p><a href="${ctx}/product/item/${p.id}" class="btn ed">售罄</a></p>
                                                    <span class="edimg"></span>
                                                <#elseif p.status == 8>
                                                    <p><a href="${ctx}/product/item/${p.id}" class="btn ed">已成立</a></p>
                                                    <span class="edimg"></span>
                                                <#elseif p.status == 4>
                                                    <p><a href="${ctx}/product/item/${p.id}" class="btn ed">已结束</a></p>
                                                    <span class="edimg"></span>
                                                <#else>
                                                    <#if p.begin_date??>
                                                        <#if p.begin_date?date?iso_local == .now?date?iso_local >
                                                            <#if  nowhour < p.sale_time>
                                                                <p><a class="product_count_down btn ed"
                                                                      href="javascript:void(0);"
                                                                      data-product-id="${p.id}"
                                                                      data-begin="${p.begin_date?date}"
                                                                      data-begin-time="${p.sale_time}">${(p.sale_time)!}
                                                                    :00开售</a></p>
                                                            <#else>
                                                                <p><a href="${ctx}/product/item/${p.id}" class="btn">马上赚钱</a>
                                                                </p>
                                                            </#if>
                                                        <#elseif p.begin_date?date &gt; .now?date>
                                                            <p><a class="product_count_down btn ed"
                                                                  href="javascript:void(0);"
                                                                  data-product-id="${p.id}"
                                                                  data-begin="${p.begin_date?date}"
                                                                  data-begin-time="${p.sale_time}">
                                                            ${p.begin_date?string('MM-dd')} ${(p.sale_time)!}:00开售</a>
                                                            </p>
                                                        <#else >
                                                            <p><a href="${ctx}/product/item/${p.id}"
                                                                  class="btn">马上赚钱</a></p>
                                                        </#if>
                                                    <#else>
                                                        <p><a href="${ctx}/product/item/${p.id}" class="btn">马上赚钱</a>
                                                        </p>
                                                    </#if>
                                                </#if>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
                                    </#list>

                                </ul>
                            </div>
                        </div>
                        <#import '*/macros/pagination.ftl' as pagination>
                        <@pagination.pagination page=data! url='${ctx}/product/${brand}-${timeType}-${statusType}-${sort_field}_${sort_dir!}'/>
                    <#else>
                        <div class="product_list_none">
                            <p>
                                <i class="icon_none"></i>没有符合条件的数据
                            </p>
                        </div>
                    </#if>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
</@override>
<@override name="js">
<script type="text/javascript" src="${ctx}/res/js/raphael-min.js"></script>
<script type="text/javascript">
    var nowDateTime = '${.now?string('yyyy-MM-dd hh:MM:ss')}';
</script>
<script type="text/javascript" src="${ctx}/res/app/product/list.js?_=${.now?string('yyyyMMdd')}"></script>

</@override>

<@extends name="./main/_main.ftl"></@extends>
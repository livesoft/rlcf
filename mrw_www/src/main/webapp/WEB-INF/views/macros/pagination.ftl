<#macro pagination page url="" pos="">
    <#assign pageNumber=(page.pageNumber)!1 pageSize=(page.pageSize)!12 totalPage=(page.totalPage)!0 totalRow=(page.totalRow)!0>

<div class="clear"></div>
<div id="pagination" class="pagination ${pos}">
    <div>
        <input type="hidden" name="pageNumber" class="page-no" value="${pageNumber}"/>
        <input type="hidden" name="pageSize" class="page-size" value="${pageSize}"/>
    </div>
    <#if totalPage &gt; 0>
        <ul>
            <#assign pagingSize = 5>

            <!-- 上一页 -->
            <#if pageNumber == 1>
                <li class="prev"><span>←</span></li>
            <#else>
                <li class="prev"><a href="${url}?page=${pageNumber-1}">←</a></li>
            </#if>

            <!-- 不能全部显示 -->
            <#if (totalPage > pagingSize)>
                <#assign startPage = pageNumber - (pagingSize / 2) ? floor>
                <#if (startPage < 1)>
                    <#assign startPage = 1>
                </#if>

                <#assign endPage = startPage + pagingSize - 1>

                <#if (endPage > totalPage)>
                    <#assign endPage = totalPage startPage = totalPage - pagingSize + 1>
                </#if>
            <#else>
                <#assign startPage = 1 endPage = totalPage>
            </#if>

            <!-- ... -->
            <#if (totalPage > pagingSize && startPage != 1)>
                <li><span>...</span></li>
            </#if>

            <#list startPage..endPage as i>
                <#if pageNumber == i>
                    <!-- 当前页 -->
                    <li class="curent"><a href="javascript:void(0);">${i}</a></li>
                <#else>
                    <li><a href="${url}?page=${i}">${i}</a></li>
                </#if>

            </#list>

            <!-- ... -->
            <#if (totalPage > pagingSize && endPage != totalPage)>
                <li><span>...</span></li>
            </#if>

            <!-- 下一页 -->
            <#if pageNumber == totalPage>
                <li class="next"><span>→</span></li>
            <#else>
                <li class="next"><a href="${url}?page=${pageNumber+1}">→</a></li>
            </#if>
        </ul>
    </#if>
</div>
</#macro>
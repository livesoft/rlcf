<@override name="title">帮助中心</@override>
<@override name="content">

<style type="text/css">

    .help_body {
        border-top: 1px solid #eee;
        padding: 15px 0;
    }

    .help_body .help_sidebar {
        border-right: 1px solid #eee;
        font-size: 1.1em;
    }

    .help_sidebar .side_nav {
        margin-bottom: 0;
        padding: 0;
        list-style: none;
    }

    .help_sidebar .side_nav li {
        position: relative;
        display: block;
    }

    .help_sidebar .side_nav li a {
        padding: .4em 13px;
        color: #999;
        -webkit-transition: .15s;
        transition: .15s;
        display: block;
    }

    .help_sidebar .side_nav li a:hover,
    .help_sidebar .side_nav li a.active{
        background: #f5f5f5;
        color: #333;
    }

    .help_sidebar .side_nav .nav-header {
        border-left: 3px solid #3bb4f2;
        padding: 0 10px;
        margin-top: .3em;
        margin-bottom: .3em;
        font-weight: 700;
        font-size: 100%;
        color: #555;
        text-transform: uppercase;
    }

    .help_main .question {
        margin-bottom: 40px;

    }

    .help_main .question h1 {
        font-weight: normal;
        margin-bottom: 15px;
        border-left: 3px solid #de453f;
        line-height: 18px;
        text-indent: 5px;
        color: #333;
        margin-top: 5px;
        margin-left: 2px;
        font-size: 16px;
    }

    .help_main .question dl {
        color: #666;
        margin-left: 10px;
        margin-bottom: 5px;
        padding-left: 0;
        font-size: 14px;
        cursor: pointer;
    }

    .help_main .question dl dt {
        padding: 5px 10px 5px 0;
        white-space: normal;
        word-break: break-all;
        margin-bottom: 2px;
        font-weight: bold;
    }

    .help_main .question dl dd {
        color: #777;
        padding: 5px 10px 5px 0;
        margin-left: 22px;
        white-space: normal;
        word-break: break-all;
        overflow: hidden;
        cursor: default;
    }

</style>


<div class="help_body container_12">
    <div class="grid_3">
        <section class="help_sidebar">
                <#list parent! as p>
                    <ul data-show="false" class="side_nav">
                        <li class="nav-header" style="cursor: pointer;">${p.title!}</li>
                        <#list child! as c>
                            <#if c.parent?? && c.parent= p.id>
                                <li style="display: none;"><a class="help_item" data-id="${c.id!}" href="javascript:void(0);">${c.title!}</a></li>
                            </#if>
                        </#list>
                    </ul>
                </#list>
        </section>
    </div>
    <div class="grid_9 help_main">

        <article class="question">
            <h1></h1>
            <div id="question_body">
                <#list contents! as c>
                    <dl>
                        <dt>${c_index + 1}、${c.title!}</dt>
                        <dd>${c.conent!}</dd>
                    </dl>
                </#list>
            </div>
        </article>

    </div>
    <div class="clear"></div>
</div>


</@override>
<@override name="js">

<script type="text/javascript">

    $('.nav-header').bind('click', function () {
        var ul = $(this).parent();
        var toggle = ul.attr('data-show');

        if (toggle == 'show') {
            $(this).siblings().hide();
            ul.attr('data-show', 'hide')
        } else {
            $(this).siblings().show();
            ul.attr('data-show', 'show')
        }
    });



    $('.help_item').bind('click', function () {
        $.ajax({
            url: '${ctx}/help/getJsonByChild?id=' + $(this).attr('data-id'),
            success: function (data) {
                var root = data || [];
                var html = [];
                for (var i = 0; i < root.length; i++) {
                    var item = root[i];
                    html.push('<dl>');
                    html.push(' <dt>'+ (i + 1) +'、'+ item.title +'</dt>');
                    html.push(' <dd>'+ item.conent +'</dd>');
                    html.push('</dl>');
                }
                $('#question_body').html(html.join(''));
            }
        });
    });

    var firstHelps = $('.help_sidebar ul').eq(0).attr('data-show', 'show').find('.nav-header');
    var firstItem = firstHelps.siblings().show().filter(":first").find('a');
    if (firstItem.length > 0) {
        firstItem.click();
    }


</script>

</@override>
<@extends name="../main/_main.ftl"></@extends>
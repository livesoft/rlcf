<@override name="title">交易提示</@override>
<@override name="content">

<section class="mypage_wrapper">
    <div class="container_12 mypage_inner blank">
        <div class="grid_12 mypage_main">
            <div class="mypage_common_block">
                <div class="<#if status != 1>zrsq_error <#else>zrsq_ok</#if>">
                    <p class="htitle"> <#if status == 1>
                        订单支付成功！
                    <#elseif status == 2>
                        订单已提交，无法重复提交！
                    <#elseif status == 0>
                        交易失败！
                    <#elseif status == 4>
                        产品已成交或者已过期，无法购买
                    </#if>
                        <#if order??>
                        <strong class="red">${(order.product_name)!}</strong></p>
                    <p class="hititle">订单号：<i class="red">${order.trade_no!}</i></span></p>
                    <table class="table table-bordered" style="margin:30px 0px 35px -150px;">
                        <tr>
                            <th colspan="3">订单详情</th>
                        </tr>
                        <tr>
                            <td><label>理财期限：</label><span>${(order.time_limit)!} ${(order.time_limit_unit == 'day')!?string('天','月')}</span></td>
                            <td><label>起息日期：</label><span>${(order.valuedate)!?string('yyyy-MM-dd')}</span></td>
                            <td><label>预期年化收益率：</label><span class="red">${(order.yield)!}%</span></td>
                        </tr>
                        <tr>
                            <td><label>风险程度：</label><span><i class="icon_heart ht${order.risk_tolerance!'1'}"></i></span></td>
                            <td><label>到息日期：</label><span>${(order.due_date)!?string('yyyy-MM-dd')}</span></td>
                            <td><label>订单金额：</label><span class="red">${order.trade_amount!}</span>元</td>
                        </tr>
                    </table></#if>
                    <p class="link"><a href="${ctx}/member/center" class="common_button">查看帐户</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="${ctx}/product">继续购买理财产品</a></p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
</@override>
<@extends name="*/main/_main.ftl"></@extends>
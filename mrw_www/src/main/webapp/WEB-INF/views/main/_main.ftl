<#compress>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>融理财富平台<@block name="title"></@block></title>
    <SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" SRC="https://seal.wosign.com/tws.js"></SCRIPT>
    <link rel="Shortcut Icon" href="${ctx}/res/img/rl_icon.png"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/css/grid.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/css/style.css?_=${.now?string('yyyyMMdd')}"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/js/validator/jquery.validator.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/css/other.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/res/css/tip-yellowsimple.css"/>
    <@block name="css"></@block>
    <script src="${ctx}/res/js/html5.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery-1.8.3.min.js" type="text/javascript" charset="utf-8"></script>

</head>
<body>

<div class="ie-upgrade">
    您的IE浏览器版本太老了，请升级这个古董吧！
    <a href="javascript:void(0);" class="close">X</a>
</div>

<div id="top">
    <div class="container_12">
        <div class="grid_6">
            <div class="auth_wrap">
                <ul id="uname">
                    <#if member??>
                        <li><a href="${ctx}/member/center">${member.phone?substring(0,3)}
                            ****${member.phone?substring(7)} 欢迎来到融理财富</a></li>
                        <li><a href="${ctx}/member/signout">[退出]</a></li>
                    <#else>
                        <li><a href="${ctx}/login"><i class="icon-login"></i> 登录</a></li>
                        <li><a href="${ctx}/register"><i class="icon-reg"></i> 注册</a></li>
                    </#if>

                </ul>
            </div>
        </div>
        <div class="grid_6">
            <nav>
                <ul>
                    <li><a href="#"><i class="icon-mobile"></i>下载手机版</a></li>
                    <li class="vline"><a href="http://wpa.qq.com/msgrd?v=3&uin=2074262328&site=qq&menu=yes"
                                         target="_blank"><i class="icon-kefu"></i>融理客服1</a></li>
                    <li class="vline"><a href="http://wpa.qq.com/msgrd?v=3&uin=1952692858&site=qq&menu=yes"
                                         target="_blank"><i class="icon-kefu"></i>融理客服2</a></li>
                    <li><a href="${ctx}/help"><i class="icon-help"></i>帮助中心</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<header>
    <div class="container_12">
        <div class="header-wrap">
            <div class="grid_5">
                <h1 id="site_logo"><a href="${ctx}/" title=""><img src="${ctx}/res/css/img/logo_pc.png" alt="Logo"></a>
                </h1>
            </div>
            <div class="grid_7">
                <nav class="primary">
                    <ul>
                        <li id="m_index" <#if curent??&&curent=1>class="curent"</#if>><a href="${ctx}/">首页</a></li>
                        <li id="m_product" <#if curent??&&curent=2>class="curent"</#if>><a
                                href="${ctx}/product">理财产品</a></li>
                        <li id="m_transfer" <#if curent??&&curent=4>class="curent"</#if>><a href="${ctx}/transfer">转让专区</a></li>
                        <li id="m_aoutus" <#if curent??&&curent=3>class="curent"</#if>><a href="${ctx}/aboutus">关于我们</a>
                        </li>
                        <li id="m_activity"><a href="${ctx}/coming">活动专区</a></li>
                        <li id="m_my_product"><a href="${ctx}/member/center">我的资产</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
    <@block name="content"></@block>
    <footer>
        <div class="footer_navigation gray">
            <div class="container_12">
                <div class="grid_2">
                    <h3>关于我们<span></span></h3>
                    <nav class="f_menu">
                        <ul>
                            <li><a target="_blank" href="${ctx}/aboutus/show/1">公司简介</a></li>
                            <li><a target="_blank" href="${ctx}/aboutus/show/2">团队管理</a></li>
                            <li><a target="_blank" href="${ctx}/aboutus/show/4">招贤纳士</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="grid_2">
                    <h3>帮助中心<span></span></h3>
                    <nav class="f_menu">
                        <ul>
                            <li><a target="_blank" href="${ctx}/help">会员账户</a></li>
                            <li><a target="_blank" href="${ctx}/help">理财产品</a></li>
                            <li><a target="_blank" href="${ctx}/help">绑卡须知</a></li>
                            <li><a target="_blank" href="${ctx}/help">投资</a></li>

                        </ul>
                    </nav>
                </div>
                <div class="grid_2">
                    <h3>友情链接<span></span></h3>
                    <nav class="f_menu">
                        <ul>
                            <li><a target="_blank" href="http://www.jsbchina.cn/">江苏银行</a></li>
                            <li><a target="_blank" href="http://www.10086.cn/">中国移动</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="grid_2">
                    <div class="qrcode">
                        <a href="#" class="qrlink">&nbsp;</a>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="svc_tel">
                        <p>客服电话</p>

                        <p>400-8070-333</p>

                        <p>9:00-17:00 (工作日)</p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="footer_info gray">
            <div class="container_12">
                <p class="copyright">版权所有&nbsp;&copy;&nbsp;南京银领金融信息服务有限公司 江苏银领金融信息服务有限公司&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ICP
                    证 苏ICP备14041846号</p>
            </div>
        </div>
    </footer>

    <div id="backup" class="window-back" style="right: 90px;">
        <span class="icon-backup">backUp</span>
    </div>

    <script type="text/javascript">
        var ctx = '${ctx}';
    </script>
    <script src="${ctx}/res/js/jquery.easing.1.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.anythingslider.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.anythingslider.fx.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.carouFredSel-5.2.2-packed.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/handlebars.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/layer/layer.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/validator/jquery.validator.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/validator/local/zh_CN.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.poshytip.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/jquery.form.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/common.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctx}/res/js/md5.js" type="text/javascript" charset="utf-8"></script>
    <@block name="js"></@block>
</body>
</html>
</#compress>
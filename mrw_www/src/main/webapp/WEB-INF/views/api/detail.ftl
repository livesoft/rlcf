<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" name="viewport"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
    <meta content="telephone=no" name="format-detection"/>
    <meta content="email=no" name="format-detection"/>
    <title>更多详情 </title>
    <link type="text/css" rel="stylesheet" href="${ctx}/static/css/base.css"/>
    <link type="text/css" rel="stylesheet" href="${ctx}/static/css/app.css"/>
</head>
<body class="ff_yahei moreDesc bg_grey text_blacksmall fs_smaller">

<#list details as dt>
    <div class="module">
        <h1 class="fs_small text_grey">${dt.name!}</h1>
        <#if dt.data_type == 'images'>
            <p class="bg_white lineBottom imgFrame">
                <#list dt.attribute_val2?split(',') as img>
                    <span><img src="${image_domain}${img!}"/></span>
                    <br>
                </#list>
            </p>
        <#elseif dt.data_type == 'image'>
            <p class="bg_white lineBottom imgFrame">
                <span><img src="${image_domain}${dt.attribute_val!}" /></span>
            </p>
        <#else>
            <p class="bg_white lineBottom">
            ${dt.attribute_val!}
            </p>
        </#if>
    </div>
</#list>

<script type="text/javascript" src="${ctx}/static/js/zepto.min.js"></script>
<script type="text/javascript" src="${ctx}/static/js/touch.min.js"></script>
<script type="text/javascript" src="${ctx}/static/js/popup.js"></script>
<script type="text/javascript" src="${ctx}/static/js/drag.js"></script>
<script type="text/javascript" src="${ctx}/static/js/scaleImg.js"></script>
</body>
</html>

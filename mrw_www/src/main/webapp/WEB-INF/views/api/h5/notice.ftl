<#compress >
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript">
        var uin = "";
        var key = "";
        var pass_ticket = "";

        String.prototype.html = function (encode) {
            var replace = ["&#39;", "'", "&quot;", '"', "&nbsp;", " ", "&gt;", ">", "&lt;", "<", "&amp;", "&", "&yen;", "¥"];
            //console.log(replace);
            if (encode) {
                replace.reverse();
            }
            for (var i = 0, str = this; i < replace.length; i += 2) {
                str = str.replace(new RegExp(replace[i], 'g'), replace[i + 1]);
            }

            return str;
        };
        pass_ticket = encodeURIComponent(pass_ticket.html(false).html(false).replace(/\s/g, "+"));
    </script>

    <title></title>

    <link rel="stylesheet" type="text/css" href="${ctx}/static/css/msg.css">

</head>
<body id="activity-detail" class="zh_CN " ontouchstart="">

<div id="js_article" class="rich_media">

    <div id="js_top_ad_area" class="top_banner">

    </div>


    <div class="rich_media_inner">
        <div id="page-content">
            <div id="img-content" class="rich_media_area_primary">
                <h2 class="rich_media_title" id="activity-name">
                ${(notice.title)!}
                </h2>

                <div class="rich_media_meta_list">
                    <em id="post-date" class="rich_media_meta rich_media_meta_text">${(notice.pubdate?string('yyyy-MM-dd'))!}</em>

                    <a class="rich_media_meta rich_media_meta_link rich_media_meta_nickname" href="javascript:void(0);"
                       id="post-user"> 融理财富 </a>
                    <span class="rich_media_meta rich_media_meta_text rich_media_meta_nickname">融理财富</span>

                </div>


                <div class="rich_media_content">
                ${(notice.content)!}
                </div>

                <link rel="stylesheet" type="text/css" href="${ctx}/static/css/article.css" property="">


                <div class="rich_media_tool">
                </div>
            </div>

            <div class="rich_media_area_extra">
                <div class="mpda_bottom_container" id="js_bottom_ad_area"></div>
            </div>

        </div>

    </div>
</div>
</body>
</html>
</#compress>

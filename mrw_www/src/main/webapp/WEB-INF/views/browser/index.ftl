<@override name="title">浏览器升级</@override>

<@override name="content">

<section class="browser-container">
    <div class="main-content">
        <div style="text-align: center;">
            <div class="ie-title">
                <img src="${ctx}/res/img/browser/old_ie.png">
                <h3>您的IE浏览器太老啦</h3>
            </div>
        </div>
        <div class="ie-content" style="text-align: center;">
            <span>为了更安全、快捷地体验后台融理网站，我们强烈建议您升级到最新版本的</span>
            <span>
                <a href="http://www.microsoft.com/zh-cn/download/internet-explorer-10-details.aspx" target="_blank">Internet Explore </a>或使用其他浏览器。
            </span>
            <div class="ie-target" style="margin-top: 10px;">
                <a href="javascript:void(0);" id="browser_goon_visit">继续访问&gt;</a>
            </div>
        </div>
        <ul class="clearfix">
            <li>
                <a href="http://www.microsoft.com/zh-cn/download/internet-explorer-9-details.aspx" target="_blank">
                    <img src="${ctx}/res/img/browser/i.png" alt="下载IE9" title="下载IE9">
                    <p>IE9</p>
                </a>
            </li>
            <li>
                <a href="http://support.apple.com/kb/DL1531?viewlocale=zh_CN&amp;locale=zh_CN" target="_blank">
                    <img src="${ctx}/res/img/browser/s.png" alt="下载Safari" title="下载Safari">
                    <p>Safari</p>
                </a>
            </li>
            <li>
                <a href="http://www.firefox.com.cn/download/" target="_blank">
                    <img src="${ctx}/res/img/browser/f.png" alt="下载Firefox" title="下载Firefox">
                    <p>Firefox</p>
                </a>
            </li>
            <li>
                <a href="http://www.google.cn/intl/zh-CN/chrome/browser/" target="_blank">
                    <img src="${ctx}/res/img/browser/c.png" alt="下载Chrome" title="下载Chrome">
                    <p>Chrome</p>
                </a>
            </li>
            <li>
                <a href="http://chrome.360.cn/" target="_blank">
                    <img src="${ctx}/res/img/browser/360.jpg" alt="急速360Chrome" title="下载急速360">
                    <p>360</p>
                </a>
            </li>

        </ul>
    </div>
</section>

</@override>

<@extends name="../main/_main.ftl"></@extends>
<@override name="title">首页</@override>
<@override name="content">
<section>
    <div class="idx_slider" id="slider_body">
        <ul id="slider">
            <#if bars??&&bars?size gt 0>
                <#list bars as bar>
                    <li class="bar_cls">
                        <div class="slid_content">
                            <a class="buy_now" href="${ctx}/login"></a>
                        </div>
                        <a href=<#if bar.href_url??>"${bar.href_url}"</#if>>
                            <img src="${img}${bar.pic!}" alt="${bar.title!}" title="${bar.title!}">
                        </a>
                    </li>
                </#list>
            </#if>
        </ul>
    </div>
    <div id="idx_section2">
        <div class="container_12">
            <div>
                <div class="p_wrap blue idx">
                    <p>国内首家规范类金融产品专属理财服务平台</p>

                    <p>帮助中国13亿用户实现金融理财梦！</p>
                </div>
            </div>
            <div class="charts_title">
                <ul>
                    <#list brands as b>
                        <li>
                            <div data-id="${b.id}" class="likechart <#if b_index == 1>pink <#elseif b_index==2>blue<#elseif b_index == 3>yellow<#elseif b_index == 4>purple</#if>">
                            ${b.name!}<span>${(b.avg_yield)!0}%</span>
                            </div>
                            <p>交易额：<span>${(b.amount!0)?string(',###')}</span>元</p>

                            <p>投资人数：<span>${b.persons!0}</span>人</p>
                        </li>
                    </#list>
                </ul>
                <div class="clear"></div>
                <div class="block_more">
                    <a href="${ctx}/product">更多...</a>
                </div>
            </div>
        </div>
    </div>

    <div id="idx_section_chart">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p>国内首家规范类金融产品专属理财服务平台</p>

                    <p>帮助中国13亿用户实现金融理财梦！</p>
                </div>
            </div>
            <div id="chart_area" class="chart_area">

            </div>
        </div>
    </div>

    <div id="idx_section_analy">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p>国内首家规范类金融产品专属理财服务平台</p>

                    <p>帮助中国13亿用户实现金融理财梦！</p>
                </div>
            </div>
            <div class="analy_area">
                <ul>
                    <li>
                        <h2>${(amount!0)?string(',###.00')}</h2>

                        <p>总投资额（元）</p>
                    </li>
                    <li>
                        <h2>${stat_member!'0'}</h2>

                        <p>总注册人数（人）</p>
                    </li>
                    <li>
                        <h2>${(porfit!0)?string(',###.00')}</h2>

                        <p>累计发放收益（元）</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>


    <div id="idx_section_tzzq">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p>国内首家规范类金融产品专属理财服务平台</p>

                    <p>帮助中国13亿用户实现金融理财梦！</p>
                </div>
            </div>
            <div class="product_list">
                <a id="img_prev" class="arows" href="#"></a>
                <a id="img_next" class="arows" href="#"></a>

                <div>
                    <ul class="small_img clearfix" id="thumblist">

                        <#if products??&&products?size gt 0>
                            <#list products as product>
                                <li class="grid_4">
                                    <div class="grid_4">
                                        <div class="product_card <#if product.status &gt;= 2>none</#if>">
                                            <p title="${(product.name)!}"><a
                                                    href="${ctx}/product/item/${product.id!}"> <#if (product.name)!?length&gt;11>${(product.name)?substring(0,10)}
                                                ...<#else>${(product.name)!}</#if></a></p>

                                            <p>预期年化收益率</p>

                                            <p>${product.yield!}<span>%</span></p>

                                            <p>30天内 <span class="red">${product.count!'0'}</span> 人购买</p>

                                            <p>理财期限 <span class="red">${product.time_limit!}</span> ${(product.time_limit_unit=='month')?string('月','天')}
                                            </p>
                                            <#if product.status == 8 || product.status == 2 || product.status == 7 || product.status == 4>
                                                <p>募集资金 <span class="red">${(product.raised_mount!0)?string(',###.00')}</span> 元</p>
                                            <#else>

                                                <p>剩余可投金额 <span class="red">${(product.remaining_amount!0)?string(',###.00')}</span> 元</p>
                                            </#if>

                                            <#if (product.enddays >= 0)><p>剩余投资 <span
                                                    class="red">${product.enddays +1}</span> 天 </p></#if>
                                            <#if (product.org_name)??>
                                                <p class="danbao"><a href="javascript:void(0);"><i class="icon_danbao"></i>${(product.org_name)!}承兑</a></p>
                                            </#if>


                                            <#if product.status == 1>
                                                <div class="optbar">
                                                    <#if (product.begindays >= 0)>
                                                        <#if (product.sale >= 0)>
                                                           <a href="${ctx}/product/item/${product.id!}">马上赚钱</a>
                                                        <#else>
                                                            <a href="javascript:void(0);">${(product.sale_time)!}:00开售</a>
                                                        </#if>
                                                    <#elseif product.begindays &gt; 0 && product.enddays &gt; 0>
                                                        <a href="${ctx}/product/item/${product.id!}">马上赚钱</a>
                                                    <#else>
                                                        <a href="javascript:void(0);">${product.begin_date?string('MM-dd')} ${(product.sale_time)!}:00开售</a>
                                                    </#if>
                                                </div>
                                            <#else>
                                                <div class="optbar"><a href="javascript:void(0);">马上赚钱</a></div>
                                            </#if>
                                            <#if product.status &gt; 1>
                                                <div class="yishouqin"></div>
                                            </#if>
                                        </div>
                                    </div>
                                </li>
                            </#list>
                        </#if>
                    </ul>
                </div>
                <div class="block_more">
                    <a href="${ctx}/product">更多...</a>
                </div>
            </div>
        </div>
    </div>


    <div id="idx_section_zhuanrang">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p>转让专区</p>

                    <p>保障收益</p>
                </div>
            </div>
            <div class="zr_area">
                <ul>

                    <#list transfers! as t>
                        <li>
                            <div class="zr_title home">
                                <h2 title="${(t.product_name)!}" > <a href="${ctx}/transfer/info/${t.id!}"><#if (t.product_name)!?length&gt;6>${(t.product_name)?substring(0,6)}...<#else>${(t.product_name)!}</#if></a> </h2>
                                    <p><i class="icon_danbao"></i> <#if (t.orgname)??>${t.orgname}承兑 </#if></p>
                            </div>
                            <div class="zr_common amount">
                                <p>${(t.amount!0)?string(',###.00')}</p>

                                <p>转让资产</p>
                            </div>
                            <div class="zr_common yield">
                                <p class="red">${t.yield!0}%</p>

                                <p>年化利率</p>
                            </div>
                            <div class="zr_common timelimit">
                                <p>${t.remaining_days!0}天</p>

                                <p>项目期限</p>
                            </div>
                            <div class="zr_common amount">
                                <p class="bblue">${(t.price!0)?string(',###.00')}</p>

                                <p>转让价格</p>
                            </div>
                            <div class="zr_opt">
                                <#if t.status == 2>
                                    <p>${t.trade_time?string('MM.dd HH:mm')} 转让</p>

                                    <p><a href="${ctx}/transfer/info/${t.id!}" class="btn ed">已转让</a></p>
                                <#else>
                                    <p><a href="${ctx}/transfer/info/${t.id!}" class="btn">立即购买</a></p>
                                </#if>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </#list>
                    <div class="clear"></div>
                </ul>
                <div class="block_more">
                    <a href="${ctx}/transfer">更多...</a>
                </div>
            </div>
        </div>
    </div>


    <div id="idx_section6">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p id="lczx_title">理财分享</p>

                    <p id="lczx_ftitle">实时了解行业最新资讯，更利于做您投资规划哦！</p>
                </div>
            </div>
            <div class="product_list">
                <div>
                    <ul class="small_img clearfix lc_list" id="lczx_list">
                        <#if shares??&&shares?size gt 0>
                            <#list shares as share>
                                <li class="grid_6">
                                    <div class="item" style="min-height: 130px;">
                                    <h2>${share.title!}</h2>
                                    <p><#if share.content?length gt 60>${share.content?substring(0,60)!}
                                        ...<#else>${share.content!}</#if></p>
                                    <p><strong>${share.name!}</strong></p>
                                    <div class="uphone">
                                        <p><img src="${img}${share.pic!}"></p>
                                    </div>
                                </div>
                                </li>
                            </#list>
                        </#if>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="idx_section_lczx">
        <div class="container_12">
            <i class="icon_arr"></i>

            <div>
                <div class="p_wrap blue idx">
                    <p>理财资讯</p>

                    <p>实时了解行业最新资讯，更利于做您投资规划哦！</p>
                </div>
            </div>
            <div class="timeline_area">

                <div class="lf">
                    <#if hotnews[0]?? >
                        <div class="zxblock">
                            <h2><span class="time">${(hotnews[0].pubdate)!?string('yyyy-MM-dd')}</span>${(hotnews[0].title)}</h2>

                            <p><#if (hotnews[0].summary)!?length gt 60>${(hotnews[0].summary)?substring(0,60)!}
                                ...<#else>${(hotnews[0].summary)!}</#if></p>
                            <i></i>
                        </div>
                    </#if>
                    <#if hotnews[2]?? >
                        <div class="zxblock">
                            <h2><span class="time">${(hotnews[2].pubdate)!?string('yyyy-MM-dd')}</span>${(hotnews[2].title)}</h2>

                            <p><#if (hotnews[2].summary)!?length gt 60>${(hotnews[2].summary)?substring(0,60)!}
                                ...<#else>${(hotnews[2].summary)!}</#if></p>
                            <i></i>
                        </div>
                    </#if>
                </div>
                <div class="rt">

                    <#if hotnews[1]?? >
                        <div class="zxblock">
                            <h2><span class="time">${(hotnews[1].pubdate)!?string('yyyy-MM-dd')}</span>${(hotnews[1].title)}</h2>

                            <p><#if (hotnews[1].summary)!?length gt 60>${(hotnews[1].summary)?substring(0,60)!}
                                ...<#else>${(hotnews[1].summary)!}</#if></p>
                            <i></i>
                        </div>
                    </#if>

                    <#if hotnews[3]?? >
                        <div class="zxblock">
                            <h2><span class="time">${(hotnews[3].pubdate)!?string('yyyy-MM-dd')}</span>${(hotnews[3].title)}</h2>

                            <p><#if (hotnews[3].summary)!?length gt 60>${(hotnews[3].summary)?substring(0,60)!}
                                ...<#else>${(hotnews[3].summary)!}</#if></p>
                            <i></i>
                        </div>
                    </#if>
                </div>

                <div class="clear"></div>
            </div>
            <div class="block_more">
                <a href="${ctx}/aboutus/news">更多...</a>
            </div>
        </div>
    </div>
</section>


</@override>
<@override name="js">
<script type="text/javascript" src="${ctx}/res/js/chart/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        $('#slider').anythingSlider({
            enableArrows  : true,
            buildStartStop: false,
            autoPlay      : true,
            delay         : 4000,
            pauseOnHover  : false,
            startPanel    : 0
        }).anythingSliderFx({
            '.slid_content h2'            : ['caption-Left', '100'],
            '.slid_content p'             : ['caption-Left', '610px', '600']
            <#if !member??>
                , '.slid_content .buy_now': ['caption-Left', '1220px', '1000']
            </#if>
        });

        $('#thumblist').carouFredSel({
            prev: '#img_prev',
            next: '#img_next',
            auto: false
        });

        $('#thumblist_zrtj').carouFredSel({
            prev: '#img_prev_zrtj',
            next: '#img_next_zrtj',
            auto: false
        });


        $('#lczx_list').carouFredSel({
            prev: '#img_prev_2',
            next: '#img_next_2'
        });


        $('#chart_area').highcharts({
            chart   : {
                backgroundColor: '#f3f3f3'
            },
            title   : false,
            subtitle: false,
            xAxis   : [{
                categories: [<#list statDays as da> '${da?string('MM-dd')}' <#if da_has_next>,</#if></#list>]
            }],
            yAxis   : [{
                labels: {
                    format: '{value} 元',
                    style : {
                        color: '#89A54E'
                    }
                },
                title : {
                    text : '日投资总额',
                    style: {
                        color: '#89A54E'
                    }
                }
            }],
            tooltip : {
                shared: true
            },
            legend  : false,
            credits : false,
            series  : [{
                name   : '日投资总额',
                color  : '#C8EAEB',
                type   : 'area',
                data   : [<#list statAmounts as am> ${am} <#if am_has_next>,</#if></#list>],
                tooltip: {
                    valueSuffix: ' 元'
                }
            }]
        });

        $('.likechart').css('cursor', 'pointer').click(function () {
            var id = $(this).attr('data-id');
            window.location.href = ctx + '/product/' + id;
        });

    });
</script>


</@override>
<@extends name="../main/_main.ftl"></@extends>

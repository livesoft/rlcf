<@override name="title">产品转让</@override>
<@override name="content">
<section>
    <div class="lczx_banner_main">
        <div class="container_12"></div>
    </div>
    <div class="breadcrumbs">
        <div class="container_12">
            <div class="grid_12">
                <a href="#">首页</a><span></span><a href="${ctx}/transfer">转让市场</a><span></span><span
                    class="current">转让详情</span>
            </div>
        </div>
    </div>

    <div class="cpxq_section1">
        <div class="container_12">
            <div>
                <div class="p_wrap" style="margin-bottom: 45px;">
                    <p class="blue">${product.name!}</p>
                </div>
            </div>
            <div>
                <div class="tb_xszx">
                    <table border="" cellspacing="" cellpadding="">
                        <tr>
                            <td style="border-right: 1px dashed #ccc;border-bottom: 1px dashed #ccc;width: 48%;">
                                <p>
                                    <span style="width:30%">预期年化收益率</span>
                                    <span style="width:30%">理财期限</span>
                                    <span>风险评级<a href="javascript:void(0);" class="poshytip-target"
                                                 title="一心：极低风险 <br>二心: 较低风险<br>三心: 中等风险<br>四心: 较高风险<br>五心: 高风险">[?]</a>
                                    </span>
                                </p>

                                <p style="font-size: 30px;line-height: 30px;">
                                    <span style="width:28%;color: #cc252c;">${product.yield!}%</span>
                                    <span style="width:30%">${product.time_limit!} ${time_limit_unit!}</span>
                                    <span><i class="icon_heart ht${product.risk_tolerance!'1'}"></i></span>
                                </p>
                            </td>
                            <td style="border-bottom: 1px dashed #ccc;">
                                <p>
                                    <span style="width:49%;"><i class="icon_yigou"></i>已购人数 ${(product.buys)!'0'}
                                        人 </span>
                                    <span style="width:49%;"><i class="icon_fenxian"></i>收益方式 ${coll_mode!}
                                        <a href="javascript:void(0);" class="poshytip-target"
                                           title="到期还本付息：投资到期后归还本金和利息；<br>等额本息：按月归还等同数额的本金和利息。">[?]</a></span>
                                </p>

                                <div class="tmpdiv">
                                    <div>
                                        <i class="icon_huikuan"></i>起投金额 ${product.min_invest_amount!}元 ${product.status}
                                    </div>
                                    <div>
                                        <i class="icon_jindu"></i>投资进度
                                        <div class="prog_bar">
                                            <p class="mb5"><span style="width:<@progress status=product.status progress=product.progress raised_mount=product.raised_mount/>%;"></span>
                                            </p>
                                        </div>
                                        <span><@progress status=product.status progress=product.progress raised_mount=product.raised_mount/>%</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="row2">

                                <p>
                                    <input type="hidden" value="" id="order_id">
                                    <#if p.status == 1 || p.status==3>
                                        <a href="#" class="btn_ljtz" id="btn_investment">立即购买</a>
                                    <#elseif p.status == 2>
                                        <a href="#" class="btn_ljtz" style="background-color:#aeaeae; ">已转让</a>
                                    <#elseif p.status == 4>
                                        <a href="#" class="btn_ljtz" style="background-color:#aeaeae; ">已撤销</a>
                                    </#if>
                                </p>

                                <p>
                                    <label for="lic_check">
                                        <input type="checkbox" name="agree" id="lic_check" checked/>
                                        我同意<a href="javascript:mask_details_opend();">《产品说明书》</a>
                                    </label>
                                    <label for="notes_check">
                                        <input type="checkbox" name="agree" id="notes_check" checked/>
                                        我同意<a href="javascript:mask_notes_opend();">《产品风险提示书》</a>
                                    </label>
                                    <label for="buy_check">
                                        <input type="checkbox" name="agree" id="buy_check" checked/>
                                        我同意<a href="javascript:mask_opend();">《产品转让协议》</a>
                                    </label>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="zrxq_section1">
        <div class="container_12">
            <div>
                <div class="p_wrap" style="margin-bottom: 45px;">
                    <p>转让信息</p>
                </div>
            </div>
            <div>
                <div class="sec_cpxx">
                    <div class="line1">
                        <label>转让人<span>${p.assignor_phone?substring(0,3)}
                            ****${p.assignor_phone?substring(7)}</span></label>
                        <label>转让资金：<span class="red">${p.amount!}</span></label>
                        <label>预期收益：<span class="red">${prifit!}</span></label>
                        <label>资产价值：<span class="red">${p.amount! + prifit!}</span></label>
                    </div>
                    <div class="line2 top20">
                        <div class="block">
                            <p>转让价格</p>

                            <p>${p.price!}</p>
                        </div>
                        <#if days?? && days &gt; 0 && p.status != 2>
                            <div class="block">
                                <p>剩余期限</p>

                                <p>${days!}天</p>
                            </div>
                        </#if>
                        <div class="block">
                            <p>预期年化利率</p>

                            <p>${p.yield!}%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cpxq_section2">
        <div class="container_12">
            <div>
                <div class="p_wrap" style="margin-bottom: 45px;">
                    <p>产品详情</p>

                    <p><#if product.accept_org??>${product.accept_org}承兑&nbsp;&nbsp;</#if>
                        单笔限额${product.max_invest_amount?string!}元&nbsp;&nbsp;</p>
                </div>
            </div>
            <div>
                <div class="tb_cpxq higher">
                    <table border="" cellspacing="" cellpadding="">
                        <tr>
                            <th>产品名称</th>
                            <td>${product.name!}</td>
                            <th>理财期限</th>
                            <td>${product.time_limit?string!}${time_limit_unit!}</td>
                        </tr>
                        <tr>
                            <th>年化利率</th>
                            <td>${product.yield?string!}%</td>
                            <th>发布日期</th>
                            <td>${(product.publish_time!)?string('yyyy-MM-dd')}</td>
                        </tr>
                        <tr>
                            <th>起息日期</th>
                            <td><#if product.valuedate??>${product.valuedate?string('yyyy-MM-dd')}</#if></td>
                            <th>结息日期</th>
                            <td>${product.due_date!?string('yyyy-MM-dd')}</td>
                        </tr>
                        <tr>
                            <th>到帐日</th>
                            <td>T+${product.valuedate_offset!}<a href="javascript:void(0);" class="poshytip-target"
                                                                 title="T+${product.valuedate_offset!}：产品到期后，投资本金和收益到账的日期">[?]</a>
                            </td>
                            <th>募集资金</th>
                            <td>${product.price!}元</td>
                        </tr>
                        <tr>
                            <th>认购开始时间</th>
                            <td>${product.begin_date!?string('yyyy-MM-dd')}</td>
                            <th>认购结束时间</th>
                            <td>${product.end_date!?string('yyyy-MM-dd')}</td>
                        </tr>
                        <tr>
                            <th>发行机构</th>
                            <td colspan="5" style="padding-left: 10px;">${product.issuer_name!}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <#if intros?? && intros?size &gt;0 >
        <div class="cpxq_section3">
            <div class="container_12">
                <div>
                    <div class="p_wrap" style="margin-bottom: 45px;">
                        <p>项目描述</p>
                    </div>
                </div>
                <div>
                    <div class="sec_xmms">
                        <#list intros as intro>
                            <div class="line">
                            ${intro.attribute_val!}
                            </div>
                        </#list>
                    </div>
                </div>
            </div>
        </div>
    </#if>

</section>


<section class="masker mask_details" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_details_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>产品说明书</p>
            <a class="btn-close absolute" href="javascript:mask_details_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct" style="overflow: auto; overflow-x: hidden;height: 520px;">
            <div class="msg_read">
                <h2 id="title"></h2>

                <div id="content" class="ctt">
                ${details!}
                </div>
            </div>

        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_details_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>


<section class="masker mask_notes" style="display: none;">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_notes_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>产品风险提示书</p>
            <a class="btn-close absolute" href="javascript:mask_notes_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct" style="overflow: auto; overflow-x:hidden;height: 520px;">
            <div class="msg_read">
                <h2 id="title"></h2>

                <div id="content" class="ctt">
                ${risk_txt!}
                </div>
            </div>

        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_notes_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>

<section class="masker" style="display: none;" id="lisc_ma">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop" onclick="mask_close()"></div>
    <div class="pop-con absolute top10 pop-conadd the_pop">
        <div class="pop-title relative">
            <p>产品转让协议</p>
            <a class="btn-close absolute" href="javascript:mask_close();" id="mask_close" title="关闭"></a>
        </div>
        <div class="pop-con-ct" style="overflow: auto;height: 540px;">
            <div class="msg_read">
                <h2 id="title"></h2>

                <div id="content" class="ctt">
                ${transfer_notes!}
                </div>
            </div>

        </div>
        <div class="pop_but ta_center" style="margin: 0;padding-bottom: 15px;">
            <a href="javascript:mask_close();" class="btn-pop btn-confirm">确定</a>
        </div>
    </div>
</section>
<section class="masker" style="display: none;" id="confirm_order_masker">
    <!-- 弹出框-->
    <div class="pop_mask fixed the_pop"></div>
    <div class="pop-con top10 fixed pop-conadd the_pop" id="confirm_panel">

    </div>
</section>

<script id="confirm_order-template" type="text/x-handlebars-template">
    <div class="pop-title relative">
        <p>核对订单信息</p>
        <a class="btn-close absolute" href="javascript:void(0);" id="mask_close" title="关闭"></a>
    </div>
    <div class="pop-con-ct">
        <div class="cpxq_info">
            <h2>{{product.name}}
                <small>订单号：{{order.trade_no}}</small>
            </h2>
            <div class="detail">
                <div class="fl">
                    <p><label>理财期限：</label><span>{{product.time_limit}} 天</span></p>

                    <p><label>风险程度：</label><span><i class="icon_heart ht{{product.risk_tolerance}}"></i> </span></p>

                    <p><label>起息日期：</label><span>{{begin_interest}}</span></p>

                    <p><label>到息日期：</label><span>{{end_interest}}</span></p>
                </div>
                <div class="fr">
                    <p><span>预期年化收益率</span></p>

                    <p><span class="red"><strong>{{product.yield}}%</strong></span></p>
                </div>
                <div class="clear"></div>
            </div>

            <div style="display:none;">

                <form id="submit_order_form" method="post" action="${jump_url!}" target="_blank">
                    <input name="sign" type="hidden" value="{{pay.sign}}"/>
                    <input name="signType" type="hidden" value="{{pay.signType}}"/>
                    <input name="service" type="hidden" value="{{pay.service}}">
                    <input name="publicKeyCode" type="hidden" value="{{pay.publicKeyCode}}">
                    <input name="createDate" type="hidden" value="{{pay.createDate}}">
                    <input name="createTime" type="hidden" value="{{pay.createTime}}">
                    <input name="bizDate" type="hidden" value="{{pay.bizDate}}">
                    <input name="subject" type="hidden" value="{{pay.subject}}">
                    <input name="body" type="hidden" value="{{pay.body}}">
                    <input name="partnerId" type="hidden" value="{{pay.partnerId}}">
                    <input name="charset" type="hidden" value="{{pay.charset}}">
                    <input name="partnerBuyerId" type="hidden" value="{{pay.partnerBuyerId}}">
                    <input name="outerOrderNo" type="hidden" value="{{pay.outerOrderNo}}">
                    <input name="amount" type="hidden" value="{{pay.amount}}">
                    <input name="currency" type="hidden" value="{{pay.currency}}">
                    <input name="jumpSec" type="hidden" value="{{pay.jumpSec}}">
                    <input name="returnUrl" type="hidden" value="{{pay.returnUrl}}">
                    <input name="notifyUrl" type="hidden" value="{{pay.notifyUrl}}">
                    <input name="channelNo" type="hidden" value="{{pay.channelNo}}">
                    <input name="version" type="hidden" value="{{pay.version}}">
                    <input name="field3" type="hidden" value="{{pay.field3}}">
                    <input name="field2" type="hidden" value="{{pay.field2}}">
                    <input name="field1" type="hidden" value="{{pay.field1}}">
                    <input name="msgID" type="hidden" value="{{pay.msgID}}">
                    <input name="svrCode" type="hidden" value="{{pay.svrCode}}">
                </form>
            </div>

        </div>
        <div class="pop_but ta_center mt10">
            <p class="dd_confirm">您的订单金额：<span class="red">{{order.trade_amount}}</span>元</p>
            <a href="javascript:void(0);" class="btn-pop btn-cancel ml10" id="close_order">返回修改</a>
            <a href="javascript:void(0);" target="_blank" id="submit_order" class="btn-pop btn-confirm">马上支付</a>
        </div>
    </div>
</script>

</@override>
<@override name="js">
<script type="text/javascript">
    var project_json = '${transferproductjson!}';
    var transferProductId = '${p.id!}';

    $('.poshytip-target').poshytip({
        className    : 'tip-yellowsimple',
        showTimeout  : 1,
        alignTo      : 'target',
        alignX       : 'center',
        alignY       : 'bottom',
        offsetY      : 5,
        allowTipHover: false
    });


    function toogleDialog(target, show) {
        var mask = $(target).show();
        if (!show) {
            mask.hide();
            return;
        }
        var dialog = mask.find('.pop-con').show();
        var scrollTo = $(document).scrollTop();
        var winHeight = $(window).height();
        var winWidth = $(window).width();

        dialog.css({
            margin: 0,
            top   : scrollTo + (winHeight - dialog.height()) / 2,
            left  : (winWidth - dialog.width()) / 2
        });
    }

    function mask_details_opend() {
        toogleDialog(".mask_details", true);
    }
    function mask_details_close() {
        toogleDialog(".mask_details", false);
    }

    function mask_notes_opend() {
        toogleDialog(".mask_notes", true);
    }
    function mask_notes_close() {
        toogleDialog(".mask_notes", false);
    }


    function mask_opend() {
        toogleDialog("#lisc_ma", true);
    }
    function mask_close() {
        toogleDialog("#lisc_ma", false);
    }


</script>
<script type="text/javascript" src="${ctx}/res/app/product/transfer.js?_=${.now?string('yyyyMMdd')}"></script>
</@override>
<@extends name="../main/_main.ftl"></@extends>
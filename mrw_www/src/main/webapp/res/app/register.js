function toogleDialog(target, show) {
    var mask = $(target).show();
    if (!show) {
        mask.hide();
        return;
    }
    var dialog = mask.find('.pop-con').show();
    var scrollTo = $(document).scrollTop();
    var winHeight = $(window).height();
    var winWidth = $(window).width();

    dialog.css({
        margin: 0,
        top   : scrollTo + (winHeight - dialog.height()) / 2,
        left  : (winWidth - dialog.width()) / 2
    });
}
function mask_opend() {
    toogleDialog(".masker", true);
}
function mask_close() {
    toogleDialog(".masker", false);
}
function agreeus() {
    if ($("#agree").attr("checked")) {
        $("#next").attr('disabled', false);
        $("#next").removeClass("disabled");
    } else {
        $("#next").attr('disabled', true);
        $("#next").addClass("disabled");
    }
}
$('#registerForm').validator({
    //自定义用于当前实例的规则
    rules : {
        mobile         : [/^0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/, '请检查手机号格式'],
        hadphone       : function (el, param, field) {
            //do something...
            var url = ctx +"/register/regphone?phone=" + $.trim($("#phone").val());
            return $.get(url, function (data) {
            });
        }, name_exist       : function (el, param, field) {
            //do something...
            var url = ctx +"/register/checkname?name=" + $.trim($("#username").val());
            return $.get(url, function (data) {
            });
        },
        isphone        : function (el, param, field) {
            //do something...
            var url = ctx +"/register/hadphone?phone=" + $.trim($("#rphone").val());
            return $.get(url, function (data) {

            });
        },
        comparephone   : function (el, param, field) {
            //do something...
            if ($.trim($("#phone").val()) != $.trim($("#rphone").val())) {
                return {"ok": null}
            } else {
                return {"error": "推荐人手机号不能为自己手机号!"}
            }
        }, ckeckedMcode: function (el, param, field) {
            //do something...
            var url = ctx +"/register/checkedMcode?captcha=" + $.trim($("#codes").val());
            return $.get(url, function (data) {
            });
        }
    },
    //待验证字段集合
    fields: {
        username: {rule: 'required;length[6~16];name_exist;', msg: {required: '用户名称不能为空'}},
        phone : {rule: 'required;mobile;hadphone;', msg: {required: "手机号不能为空!"}},
        rphone: {rule: 'mobile;comparephone;isphone;'},
        codes : {rule: 'required;ckeckedMcode;', msg: {required: "验证码不能为空!"}}
    },
    valid : function (form) {
        //表单验证通过，提交表单到服务器
        form.submit();
    }
});

function changeCode() {
    //IE7+/Firefox默认从缓存加载，路径之后加随机参数强制重新下载
    var d = new Date();
    var oImg = document.getElementById('code');
    oImg.src = ctx +"/api/captcha?t=" + d.toString(38);
}
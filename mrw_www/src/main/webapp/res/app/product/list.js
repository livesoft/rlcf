$(document).ready(function () {
    //var nowMillisecond = new Date(nowDateTime).getTime();
    //var product_count_down_els = $('a.product_count_down');
    //
    //function initProductCountDown () {
    //    product_count_down_els.each(function () {
    //        var el = $(this), time = "";
    //        time += el.attr('data-begin') + " ";
    //        time += el.attr('data-begin-time') + ':00:00';
    //        calculate(time, nowMillisecond, el);
    //    });
    //}
    //
    //function calculate (time, nowMillisecond, el) {
    //    var productMillisecond = new Date(time).getTime(),
    //        diff = 0, tip, productId;
    //
    //    if ((diff = productMillisecond - nowMillisecond) <= 0) {
    //        productId = el.attr('data-product-id');
    //        el.text('马上赚钱').attr('href', '${ctx}/product/item/' + productId);
    //        return;
    //    }
    //    var minute = parseInt(diff / (1000 * 60));
    //
    //    if (minute < 60) {
    //        tip = minute + "分钟后开售";
    //    } else {
    //        var hour = parseInt(minute / 60);
    //        minute = parseInt(minute % 60);
    //        tip = hour + "小时" + (minute != 0 ? minute + "分钟" : "") + "后开售";
    //    }
    //    el.text(tip);
    //}
    //
    //initProductCountDown();
    //
    //setInterval(function () {
    //    nowMillisecond += 5000;
    //    initProductCountDown();
    //}, 5000);
    var svgCircle = function (t, obj) {
        return t = $.extend({
                parent: null,
                w     : 75,
                R     : 30,
                sW    : 20,
                color : ["#000", "#000"],
                perent: [100, 100],
                speed : 10,
                delay : 1e3
            },
            t),
            obj.each(function () {
                var e = t.parent;
                if (!e) return !1;
                {
                    var r = t.w,
                        a = Raphael(e, r, r),
                        n = t.R,
                        o = {
                            stroke: "#f98a91"
                        };
                    document.location.hash
                }
                a.customAttributes.arc = function (e, a, n) {
                    {
                        var o, c = 360 / a * e,
                            s    = (90 - c) * Math.PI / 180,
                            i    = r / 2 + n * Math.cos(s),
                            h    = r / 2 - n * Math.sin(s);
                        t.color
                    }
                    return o = a == e ? [["M", r / 2, r / 2 - n], ["A", n, n, 0, 1, 1, r / 2 - .01, r / 2 - n]] : [["M", r / 2, r / 2 - n], ["A", n, n, 0, +(c > 180), 1, i, h]],
                    {
                        path: o
                    }
                };
                var c = (a.path().attr({
                    stroke        : "#f0f0f0",
                    "stroke-width": t.sW
                }).attr({
                    arc: [100, 100, n]
                }), a.path().attr({
                    stroke        : "#f36767",
                    "stroke-width": t.sW
                }).attr(o).attr({
                    arc: [.01, t.speed, n]
                }));
                t.perent[1] > 0 ? setTimeout(function () {
                        c.animate({
                                stroke: t.color[1],
                                arc   : [t.perent[1], 100, n]
                            },
                            900, ">")
                    },
                    t.delay) : c.hide()
            })
    };

    var animateDiv = $('.mjjd');

    function animateEle() {
        var playRange = {top: 10, bottom: 100};
        animateDiv.each(function () {
            $(this).data('bPlay', true);
            var nPercent = $(this).parent().find('.mjnum').text().replace(/\%/, '');
            svgCircle({
                parent: $(this)[0],
                w     : 40,
                R     : 18,
                sW    : 4,
                color : ["#f98a91", "#f98a91", "#f98a91"],
                perent: [100, nPercent],
                speed : 150,
                delay : 400
            }, $(this));
        });
    }

    animateEle();
});
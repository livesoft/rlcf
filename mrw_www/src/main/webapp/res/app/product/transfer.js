$(function () {

    var $confirmPanel = $('#confirm_panel'),
        $orderId = $('#order_id'),
        $licCheck = $('#lic_check'),
        $btnInvestment = $('#btn_investment'),
        $confirmOrderMasker = $('#confirm_order_masker');

    var confirm_order_hbs = Handlebars.compile($('#confirm_order-template').html());


    $('#lic_check,#notes_check,#buy_check').change(function () {
        if ($('input#lic_check:checkbox').is(':checked')
            && $('input#notes_check:checkbox').is(':checked')
            && $('input#buy_check:checkbox').is(':checked')) {
            $btnInvestment.removeClass('btn-cancel');
            $btnInvestment.css('background-color', '#cc252c');
        } else {
            $btnInvestment.addClass('btn-cancel');
            $btnInvestment.css('background-color', '#aeaeae');
        }
    });


    $btnInvestment.click(function (e) {
        e.preventDefault();

        if($(this).hasClass('btn-cancel')){
            return;
        }

        if (!$('input#lic_check:checkbox').is(':checked')) {
            layer.alert('同意《产品说明书》后才能进行投资理财', 5, !1);
            return;
        }
        if (!$('input#notes_check:checkbox').is(':checked')) {
            layer.alert('同意《产品风险揭示书》才能进行投资理财', 5, !1);
            return;
        }
        if (!$('input#buy_check:checkbox').is(':checked')) {
            layer.alert('同意《产品转让协议》才能进行投资理财', 5, !1);
            return;
        }


        var amount_input = $('#amount_input').val();
        var order_id = $orderId.val();
        var productId = $.parseJSON(project_json)['id'];
        $.ajax({
            url : ctx + '/transfer/investment/' + productId,
            type: 'POST',
            cache: false,
            data: {amount: amount_input, order: order_id},
            beforeSend: function(){
                layer.load();
            }
        }).done(function (rst) {
            layer.closeAll();
            if (rst.status == 'OK') {
                $orderId.val(rst.data.order['id']);
                $confirmPanel.html(confirm_order_hbs(rst.data));
                $confirmOrderMasker.show();
            } else if (rst.status == 'NOLOGIN') {
                layer.alert('请先进行登录', -1, function () {
                    window.location = ctx + '/login?redirect=/transfer/info/' + productId;
                });

            } else {
                if(rst.data == 'ELE'){
                    layer.alert(rst.message, -1, function () {
                        window.location = ctx + '/member/electronic?redirect_url=/transfer/item/'+productId ;
                    });
                } else {
                    layer.alert(rst.message, 5, !1);
                }
            }
        });
        return false;

    });

    $confirmOrderMasker.on('click', '#close_order', function (e) {
        e.preventDefault();

        $confirmPanel.html('');
        $confirmOrderMasker.hide();
        return false;
    });

    $confirmOrderMasker.on('click', '#mask_close', function (e) {
        e.preventDefault();

        $confirmPanel.html('');
        $confirmOrderMasker.hide();
        return false;
    });

    $confirmOrderMasker.on('click', '#submit_order', function (e) {
        e.preventDefault();
        var order_id = $orderId.val();
        $.ajax({
            type: 'POST',
            url: ctx + '/pay/order/'+ order_id,
            cache: false
        });
        var $orderForm = $('#submit_order_form');
        $orderForm.submit();
        $confirmPanel.html('');
        $confirmOrderMasker.hide();
        return false;
    });




});
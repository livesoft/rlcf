$(document).ready(function () {

    sendRequestMobileMessage($('#pull_code'), ctx + '/register/sendsms?phone=' + $('#phoneVal').val(), 'POST');
    $('#register').click(function (e) {

        var pwd = $.trim($('#password').val());
        if(!pwd){
            layer.alert('请输入您的登录密码', 2, !1);
            return;
        }

        if(pwd.length < 6 || pwd.length > 16){

            layer.alert('密码长度只能在6到16位', 2, !1);
            return;
        }

        var rpassword = $.trim($("#rpassword").val());
        if(!rpassword){
            layer.alert('请输入重复登录密码', 2, !1);
            return;
        }

        if(pwd != rpassword){
            layer.alert('登录密码和重复登录密码不一致', 2, !1);
            return;
        }

        $("#register").attr('disabled', true);
        var formData = {
            phone: $.trim($('#phoneVal').val()),
            invite_mobile: $.trim($('#invite').val()),
            codes: $.trim($("#mcode").val()),
            username: $.trim($("#username").val()),
            plant_pwd: pwd,
            rpassword: rpassword
        };
        $.ajax({
            url: ctx + '/register/save',
            data: formData,
            type: 'POST',
            cache: false,
            success: function (data) {
                if (data.status == 'OK') {
                    window.location.href = ctx + '/register/complete';
                } else {
                    layer.alert(data.message, 2, !1);
                    $("#register").attr('disabled', false);
                }
            }
        });
    });
});

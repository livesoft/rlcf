$(document).ready(function () {

    sendRequestMobileMessage($('#pull_code'), ctx + '/forgetpwd/sendsms?phone=' + $('#phoneVal').val(), 'POST');
    $('#registerForm').validator({
        //自定义用于当前实例的规则
        rules: {
            comparepassword:function(el,param,field){
                if ($.trim($("#password").val()) != $.trim($("#rpassword").val())) {
                    return {"error": "两次输入的密码不一致!"}
                } else {
                    return {"ok": null}
                }
            }
        },
        //待验证字段集合
        fields: {
            codes: {rule: 'required;', msg: {required: "验证码不能为空!"}},
            password:{rule: 'required;length[6~16];', msg: {required: "新密码不能为空!"}},
            rpassword:{rule:'required;length[6~16];comparepassword;',msg: {required: "确认密码不能为空!"}}
        },
        valid: function (form) {
            //表单验证通过，提交表单到服务器
            var formData = {phone: $.trim($('#phoneVal').val()),code: $.trim($("#mcode").val()), password: $.trim($("#password").val()), rpassword: $.trim($("#rpassword").val())};
            $("#register").attr('disabled', true);
            $.ajax({
                url: ctx + '/forgetpwd/change',
                data: formData,
                type: 'POST',
                cache: false,
                success: function (data) {
                    if (data.status == 'OK') {

                        window.location.href = ctx + '/forgetpwd/ok';
                    } else {
                        layer.alert(data.message, 2, !1);
                        $("#register").attr('disabled', false);
                    }
                }
            });
        }
    });
});

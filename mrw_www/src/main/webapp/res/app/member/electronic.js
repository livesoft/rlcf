$(document).ready(function () {

    $('.poshytip-target').poshytip({
        className: 'tip-yellowsimple',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'left',
        alignY: 'center',
        offsetY: 0,
        allowTipHover: false
    });

    $('#bind_form').validator({
        stopOnError: false,
        timely     : true
    });

    if(error){
        switch (error){
            case 1:
                layer.alert('请输入真实姓名',5, !1);
                break;
            case 2:
                layer.alert('请输入正确的身份证号',5, !1);
                break;
            case 3:
                layer.alert('请输入银行电子账号',5, !1);
                break;
            case 4:
                if (message.length > 0) {
                    layer.alert(message, 5, !1);
                } else {
                    layer.alert('绑定电子账号出错，请稍后重试', 5, !1);
                }
                break;
        }
    }
});
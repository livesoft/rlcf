$(function () {

    var confirm_transfer_content = Handlebars.compile($('#confirm_dialog-template').html());
    var exp = /^(([1-9]\d{0,9})|0)(\.\d{1,4})?$/;
    $("#sbmt").click(function () {
        //检查输入项目 只能是数字 2 不能大于项目本金
        var $tPrice = $("#t_price");

        var price_val = $tPrice.val();

        if ($.trim($tPrice.val()) == "") {
            layer.alert("请输入转让价格", 2, !1);
            return;
        }

        if (!exp.test($tPrice.val())) {
            layer.alert("请输入正确的转让价格，小数点后最多2位", 2, !1);
            return;
        }

        $.ajax({
            url     : ctx + "/member/transfer/dialog",
            data    : {
                mem_pid: mpId,
                t_price: price_val
            },
            type    : 'POST',
            dataType: "JSON"
        }).done(function (msg) {
            if (msg.status == 'OK') {

                var vd = msg.data;
                vd['lodging_fee_show'] = vd['lodging_fee'] > 0;
                vd['fee_show'] = vd['fee'] > 0;
                vd['loss_show'] = vd['loss'] > 0;
                vd['aging_show'] = vd['aging'] > 0;
                vd['free_times_show'] = vd['free_times'] > 0;
                var content_html = confirm_transfer_content(vd);
                $('#info-panel').html(content_html);
                $(".masker").show();
            } else {

                layer.alert(msg.message, 5, !1);
            }
        });
    });

    $("#confirm1").on("click", function () {
        //转让产品，转让价格
        var val = $('#loss').val();
        if (parseFloat(val) > 0) {

            layer.confirm('预计转让亏本金额为' + val + '元, 您确定要继续转让么？', function () {
                $('#contractform').submit();
            });
        } else {

            $('#contractform').submit();
        }

    });

    $(".mask_close").click(function () {
        $(".masker").hide();
    });
});
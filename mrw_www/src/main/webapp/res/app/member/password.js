$(document).ready(function () {
    $('#change').click(function(){
        var old_password = $('#old_password').val();
        var password = $('#password').val();
        var rpassword = $('#rpassword').val();
        if($.trim(old_password).length<=0){
            layer.alert("当前密码不能为空", 2, !1);
            return;
        }else if($.trim(old_password).length<6){
            layer.alert("当前密码长度6~16位", 2, !1);
            return;
        }else if($.trim(password)<=0){
            layer.alert("新密码不能为空", 2, !1);
            return;
        }else if($.trim(password).length<6||$.trim(password).length>16){
            layer.alert("新密码长度6~16位", 2, !1);
            return;
        }else if($.trim(rpassword)<=0){
            layer.alert("确认密码不能为空", 2, !1);
            return;
        }else if($.trim(rpassword).length<6||$.trim(rpassword).length>16){
            layer.alert("确认密码长度6~16位", 2, !1);
            return;
        }else if($.trim(rpassword)!=$.trim(password)){
            layer.alert("两次输入的密码不一样", 2, !1);
            return;
        }
        var formData= {old_password:old_password,password:password,rpassword:rpassword};
        $("#change").attr('disabled', true);
        $.ajax({
            url  : ctx +"/member/account/checkedPassword",
            data : formData,
            type : 'POST',
            cache: false,
            success:function(data){
                formData= {old_password:old_password,password:password,rpassword:rpassword};
                if(data.status=='OK'){
                    $.ajax({
                        url  : ctx +"/member/account/changePassword",
                        data : formData,
                        type : 'POST',
                        cache: false,
                        success:function(data){
                            if(data.status=='OK'){
                                window.location.href=ctx+"/member/account/ok"
                            }else{
                                layer.alert(data.message, 2, !1);
                                $("#change").attr('disabled', false);
                            }
                        }
                    })
                }else{
                    layer.alert(data.message, 2, !1);
                    $("#change").attr('disabled', false);
                }
            }
        });
    });
});

function resetMcode(){
    var url = ctx +"/api/sms/register/" +$.cookie('phone');
    $.get(url, function (data) {
        if (data.status == "OK") {
            $("#tip").html("验证码重发成功！")
        } else {
            $("#tip").html(data.message);
        }

    });
}
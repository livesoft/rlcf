$(document).ready(function () {
    $('#change').click(function (e) {
        var oldpassword = $.trim($("#old_password").val());
        var password = $.trim($("#password").val());
        var rpassword = $.trim($("#rpassword").val());
        $("#change").attr('disabled', true);
        var formData;
        if(flag){
            formData = {oldpassword: oldpassword, password: password, rpassword: rpassword};
            $.ajax({
                url: ctx + '/member/account/changeAccountPassword',
                data: formData,
                type: 'POST',
                cache: false
            }).done(function (rst) {
                if (rst.status == 'OK') {
                    window.location.href=ctx+"/member/account/ok";
                } else {
                    layer.alert(rst.message, 2, !1);
                    $("#change").attr('disabled', false);
                }
            })

        }else{
            var redirect = $.trim($('#redirect').val());
            redirect = redirect.length <= 0 ? ctx + '/member/account/ok' : ctx + redirect;
            formData = {password: password, rpassword: rpassword};
            $.ajax({
                url: ctx + '/member/account/settingAccountPassword',
                data: formData,
                type: 'POST',
                cache: false
            }).done(function (rst) {
                if (rst.status == 'OK') {
                    window.location.href = redirect;
                } else {
                    layer.alert(rst.message, 2, !1);
                    $("#change").attr('disabled', false);
                }
            })
        }

    });


});

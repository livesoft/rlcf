$(document).ready(function () {

    function toogleDialog(target, show) {
        var mask = $(target).show();
        if (!show) {
            mask.hide();
            return;
        }
        var dialog = mask.find('.pop-con').show();
        var scrollTo = $(document).scrollTop();
        var winHeight = $(window).height();
        var winWidth = $(window).width();

        dialog.css({
            margin: 0,
            top: scrollTo + (winHeight - dialog.height()) / 2,
            left: (winWidth - dialog.width()) / 2
        });
    }

    $('.poshytip-target').poshytip({
        className    : 'tip-yellowsimple',
        showTimeout  : 1,
        alignTo      : 'target',
        alignX       : 'center',
        alignY       : 'bottom',
        offsetY      : 5,
        allowTipHover: false
    });

    var $member_product_id  = $('#member_product_id'),
        $productTransferId  = $('#product_id_transfer');

    $('a#can_transfer_btn').click(function (e) {
        $productTransferId.val($(this).data('pk'));
        toogleDialog("#cancel_transfer_dialog", true);
    });

    $('a#cancel_transfer_confirm_btn').click(function () {

        var productId = $productTransferId.val();
        if (productId) {

            $.ajax({
                url  : ctx +'/member/transfer/revocation/' + productId,
                type : 'POST',
                cache: false
            }).done(function (rst) {
                if (rst.status == 'OK') {
                    $productTransferId.val('');
                    toogleDialog("#cancel_transfer_dialog", false);
                    layer.alert('温馨提示，您的产品转让已撤销！', -1, function () {
                        window.location.reload();
                    });
                } else {
                    toogleDialog("#cancel_transfer_dialog", false);
                    layer.alert(rst.message, -1, !1);
                }
            })
        }
    });


    $('a#cancel_transfer_mask_close,a#cancel_transfer_cancel_btn').click(function () {
        $productTransferId.val('');
        toogleDialog("#cancel_transfer_dialog", false);
    });


    $('a#back_btn').click(function (e) {
        var id = $(this).attr('data-pk');
        $member_product_id.val(id);
        toogleDialog("#cancel_dialog", true);
    });

    $('#cancel_btn,#mask_close').click(function () {
        $member_product_id.val('');
        toogleDialog("#cancel_dialog", false);
    });
    $('#confirm_btn').click(function () {
        toogleDialog("#cancel_dialog", false);
        // 先进行判断是否设置了交易密码
        $.ajax({
            url  : ctx + '/member/account/tradepwd',
            cache: false
        }).done(function (rst) {
            if (rst.status == 'OK') {
                window.location.href = ctx + '/member/redemption/' + mpid;
            } else {
                layer.alert('十分抱歉，您还没有设置交易密码，无法申请退单!', -1, function () {
                    window.location.href = ctx + '/member/account/toAccountPassword?redirect=/member/product/detail/'+mpid;
                });
            }
        });
        return false;
    });

    $('#transfer_btn').click(function () {
        $.ajax({
            url  : ctx + '/member/account/tradepwd',
            cache: false
        }).done(function (rst) {
            if (rst.status == 'OK') {
                window.location.href = ctx + '/member/transfer/confirm/' + mpid;
            } else {
                layer.alert('十分抱歉，您还没有设置交易密码，无法进行转让!', -1, function () {
                    window.location.href = ctx + '/member/account/toAccountPassword?redirect=/member/product/detail/'+mpid
                });
            }
        });
        return false;
    })
});
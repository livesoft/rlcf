$(document).ready(function () {
    var $loginForm = $('#loginForm'),
        $errorMsg = $('.error-msg', $loginForm),
        $password = $('#password'),
        $phone = $('#phone');
    $phone.focus();


    $('#login_btn').click(function (e) {
        e.preventDefault();
        var phone = $phone.val();
        var password = $password.val();
        var $captcha = $('#captcha');
        var captcha = $captcha.val();

        if ($.trim(phone).length <= 0) {
            $errorMsg.html('请输入手机号码');
            $errorMsg.closest('.alert-warning').show();
            $phone.focus();
            return;
        }

        if ($.trim(password).length <= 0) {
            $errorMsg.html('请输入登录密码');
            $errorMsg.closest('.alert-warning').show();
            $password.focus();
            return;
        }

        if ($.trim(captcha).length <= 0) {
            $errorMsg.html('请输入验证码');
            $errorMsg.closest('.alert-warning').show();
            $captcha.focus();
            return;
        }



        var formData = {phone: phone, password: password, captcha: captcha};

        $.ajax({
            url  : ctx + '/login/checkLogin',
            data : formData,
            type : 'POST',
            cache: false
        }).done(function (rst) {
            if (rst.status == 'OK') {
                $loginForm.submit();
            } else {
                layer.alert(rst.message, 2, !1);
                $('#code').prop('src', ctx + '/api/captcha?t=' + new Date().toString(38));
            }
        })

    });


    $('#change_captcha_btn').click(function (e) {
        e.preventDefault();
        $('#code').prop('src', ctx + '/api/captcha?t=' + new Date().toString(38));
    });

});
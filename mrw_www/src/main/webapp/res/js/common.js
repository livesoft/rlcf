
$.ajaxSetup({
    error: function(jqXHR, textStatus, errorThrown){
        if (jqXHR.status == 500) {
            window.location.href = ctx + '/500.html'
        }
    }
});

$(document).ready(function () {

    $('#browser_goon_visit').bind('click', function () {
        $.cookie('browser_goon_visit', 1);
    });
    var locationHref = window.location.href;
    if (locationHref.indexOf('/browser') == -1 && $.browser.msie && $.browser.version < 8 && $.cookie('browser_goon_visit') != 1) {
        $.cookie('browser_goon_visit', 1);
        window.location.href = "/browser";
    }

    initBackUp();

});


// 全局返回顶部
function initBackUp () {
    var win = $(window);
    var backup = $('#backup');
    var right = (win.width() - 1008) / 2 - backup.width() - 20;
    backup.css('right', right);

    win.scroll(function(){
        if (win.scrollTop() > 300){
            backup.fadeIn(200);
        } else {
            backup.fadeOut(500);
        }
    });
    backup.bind('click', function () {
        $('body,html').animate({scrollTop:0}, 200);
        return false;
    });
}


// 发送手机短信，60秒倒计时
function sendRequestMobileMessage (target, url, type) {
    var send_status = true, countDownInterval, count = 0;

    target.bind('click', function () {
        var el = $(this).hide();
        var send_code = el.next();
        var send_time = send_code.find('a.count_down');
        send_code.css('display', 'inline-block');

        if (!send_status) {
            return;
        }
        count = 60;
        send_time.html(count + '秒后重新获取');

        $.ajax({
            url: url,
            type: (type || 'get').toUpperCase(),
            success: function (data) {
                countDownInterval = setInterval(countDown, 1000);
            }
        });

        function countDown () {
            send_time.html(count + '秒后重新获取');
            if (count == 0) {
                el.show();
                send_code.hide();
                clearInterval(countDownInterval);
                send_status = true;
            } else {
                send_status = false;
            }
            count--;
        }
    })

}

var Popup=function(param){
	var css=param.css,
		html=param.html,
		maskCls=param.maskCls,
		popupBoxCls=param.popupBoxCls,
		closeID=param.closeID,
		callBack=param.callBack||function(){}
		that={};

	var body=$("body");

	if(!$(".mask")[0]){
	  body.append(css+html);
	}

	that.show=function(){
		var $mask=$("."+maskCls),//遮罩层对象
			$popBox=$("."+popupBoxCls),//弹出层对象
			$close=$("#"+closeID);//关闭遮罩层按钮
			
		var $win=$(window),
			$doc=$(document),
			$mask=$mask,
			$popBox=$popBox,
			$close=$close;

		var countPos=function(){
			var maxHt=Math.max($doc.height(),$win.height());
			
			$mask.css({"height":maxHt+"px"});
			$mask.css({"display":"block"});
			
			wh=$win.width()-$popBox.width();
			ht=maxHt-$popBox.height();
			
			//$popBox.css({"left":parseInt(wh/2)+"px","top":parseInt(ht/2)+"px"});
		}
		
		//countPos();
		
		$win.resize(function(){
			 countPos();
		});
		$close.click(function(){
			that.hide();	
		});
		callBack();
	}
	that.hide=function(param){
		var $mask=$(".mask");
		
		if(param){
			$mask=$(param.maskCls);
		};
		
		$mask.remove();
	}
	return that;
}
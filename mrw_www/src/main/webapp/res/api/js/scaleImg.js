var openPop=function(params){
		var hanlder=params.dragFunc,
			img=params.img;
		
		var paramsObj={
				html:"<div class='mask'>"+
						"<div class='popupBox'>"+
							"<div id='popImgframe' class='popImgframe'>"+
								
							"</div>"+
							"<div id='close' class='btnFrame'>关闭</div>"+
						"</div>"+
					"</div>",
				css:"<style type='text/css'>"+
						".mask{width:100%;height:100%;position:fixed;left:0;top:0;background-color:rgba(0,0,0,0.5);z-index:99999;}"+
						".popupBox{width:100%;overflow:hidden;height:300px;position:absolute;0px;top:0px;text-align:center;background-color:rgba(255,255,255);}"+
						".popImgframe{height:280px;overflow:hidden;}"+
						".btnFrame{height:44px;line-height:44px;background:#333;margin-top:5%;position:absolute;bottom:0;left:0;width:100%;color:#fff;}"+
						".popupBox img{position:relative;left:0;top:0;}"+
					"</style>",
				maskCls:"mask",
				popupBoxCls:"popupBox",
				closeID:"close",
				callBack:function(){
					var popupBox=$(".popupBox"),
						imgFrame=document.getElementById("popImgframe"),
						touchstartHandler=hanlder;
					
					var ht=$(window).height(),
						$imgFrame=$(imgFrame);
					
					$(imgFrame).append(img);
					popupBox.css({"height":ht+"px"});
					$(imgFrame).css({"height":(ht-54)+"px"});
					
					$("img",imgFrame)[0].addEventListener("touchstart",touchstartHandler);
				}
		}
		var popup=new Popup(paramsObj)
		popup.show();
	}
	
	var tapHanlder=function(event){
		var openPopFunc=event.data.openPopFunc,
			dragFunc=event.data.dragFunc;
			
		var img=$(this).children().eq(0).clone();
		
		openPopFunc({dragFunc:dragFunc,img:img});
	}
	
	var init=function(params){
		var $imgFrame=params.$imgFrame,
			openPop=params.openPop,
			touchstartHandler=params.touchstartHandler,
			tapHanlder=params.tapHanlder;
		
		//var imgFrame=$(".module span");
		$imgFrame.bind("tap",{openPopFunc:openPop,dragFunc:touchstartHandler},tapHanlder);
	}
	
	var setup=function($imgFrame){//需传入img图片的父元素，且其必须非jquery对象
		init({
			$imgFrame:$imgFrame,
			openPop:openPop,
			touchstartHandler:touchstartHandler,
			tapHanlder:tapHanlder
		});
	}
	setup($(".module span"));
var touchstartHandler=function(event){
	var elementToDrag=this;
	
	
	var startX = event.targetTouches[0].clientX,
		startY = event.targetTouches[0].clientY;

	//alert(elementToDrag);
	var origX = elementToDrag.offsetLeft,
		origY = elementToDrag.offsetTop;
		
	var deltaX = startX - origX,
		deltaY = startY - origY;
		
	var deltaWidth=parseFloat(window.getComputedStyle(this).width).toFixed(2)-parseFloat(window.getComputedStyle(this.parentNode).width).toFixed(2),
		delatHeight=parseFloat(window.getComputedStyle(this).height).toFixed(2)-parseFloat(window.getComputedStyle(this.parentNode).height).toFixed(2);
	
	var minX=-deltaWidth,
		maxX=0,
		minY=-delatHeight,
		maxY=0;
	
	var moveHandler=function(event){
		//将鼠标光标移动的距离赋给拖动的元素
		var tempLeft=event.targetTouches[0].clientX - deltaX,
			tempTop=event.targetTouches[0].clientY - deltaY;
				
		//alert("11p:"+tempTop);
		
		if(tempLeft<=minX){
			tempLeft=minX;
		}else if(tempLeft>=maxX){
			tempLeft=maxX;
		}
		
		console.log("minX:"+minX+";maxX:"+maxX);
		
		if(tempTop<=minY){
			tempTop=minY;
		}else if(tempTop>=maxY){
			tempTop=maxY;
		}
		
		//alert("1tempLeft:"+tempLeft+";tempTop:"+tempTop);
		elementToDrag.style.left = tempLeft+ "px";
		elementToDrag.style.top = tempTop+ "px";

		//阻止冒泡
		if (e.stopPropagation){
			e.stopPropagation(); //DOM Level 2	
		}else{
			e.cancelBubble = true; //IE	
		} 
	},
	upHandler=function(e) {
		
		//移除对鼠标左键弹起和鼠标光标移动事件侦听的侦听器
		if (document.removeEventListener) { //DOM event model
			document.removeEventListener("touchend", upHandler, true);
			document.removeEventListener("touchmove", moveHandler, true);
		}
		
		//阻止冒泡
		if(e.stopPropagation){
			e.stopPropagation();	
		}else{
			e.cancelBubble = true;	
		}
	}
	
	//阻止冒泡
	if (event.stopPropagation){
		event.stopPropagation(); //DOM Level2	
	}else{
		event.cancelBubble = true; //IE	
	}
	
	//取消默认行为
	if (event.preventDefault){
		event.preventDefault(); //DOM Level2	
	}else{
		event.returnValue = false; //IE	
	} 
	
	//注册相关事件
	if (document.addEventListener) { //DOM Level2 event model
		document.addEventListener("touchmove", moveHandler, true);
		document.addEventListener("touchend", upHandler, true);
	}	
}
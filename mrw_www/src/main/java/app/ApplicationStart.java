package app;

import app.directive.HasAnyPermissionsDirective;
import app.directive.ProductProgressDirective;
import app.kit.SmsKit;
import app.services.bus.BusContext;
import com.jfinal.render.FreeMarkerRender;
import freemarker.template.Configuration;
import goja.mvc.AppLoadEvent;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ApplicationStart implements AppLoadEvent {

    @Override
    public void load() {
        BusContext.init();
        SmsKit.init();

        Configuration configuration = FreeMarkerRender.getConfiguration();
        configuration.setSharedVariable("progress", new ProductProgressDirective());
    }
}

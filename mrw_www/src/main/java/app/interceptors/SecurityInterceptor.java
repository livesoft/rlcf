package app.interceptors;


import app.Const;
import app.models.basic.Msgs;
import app.models.member.Member;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import goja.StringPool;
import goja.annotation.AppInterceptor;
import goja.security.goja.SecurityKit;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
@AppInterceptor
public class SecurityInterceptor implements Interceptor {

    private static final List<String> security_urls = Lists.newArrayList(
            "/member/center",
            "/member/moreProductMoney",
            "/member/account/accountSettings",
            "/member/account/changePassword",
            "/member/account/changeComplete",
            "/member/account/changeAccountPassword",
            "/member/account/tochangePassword",
            "/member/account/checkedPassword",
            "/member/account/toUpdateInfo",
            "/member/account/changeAccountPassword",
            "/member/account/toAccountPassword",
            "/member/account/settingAccountPassword",
            "/member/questionnaire",
            "/member/questionnaire/answer",
            "/member/questionnaire/answerresult",
            "/member/myInvitation",
            "/member/myInvitation/index",
            "/member/myMessage",
            "/member/myMessage/index",
            "/member/myMessage/lookup",
            "/member/myMessage/read",
            "/member/myMessage/delete",
            "/member/myAccount",
            "/member/myAccount/index",
            "/member/electronic",
            "/member/redemption",
            "/member/redemption/send",
            "/member/redemption/verification",
            "/member/redemption/success",
            "/member/product",
            "/member/product/detail",
            "/member/product/over",
            "/member/myIntegral",
            "/member/myIntegral/index",
            "/member/transfer",
            "/member/transfer/confirm",
            "/member/transfer/auth",
            "/member/transfer/commit",
            "/member/electronic"
    );

    private static final List<String> no_security_urls = Lists.newArrayList(
            "/login", "/register", "/nextregister"
    );

    @Override
    public void intercept(ActionInvocation ai) {

        Controller controller = ai.getController();
        String actionKey = ai.getActionKey();
        final HttpServletRequest request = controller.getRequest();
        final boolean islogin = SecurityKit.isLogin(request);
        if (security_urls.contains(actionKey)) {
            if (!islogin) {
                String method = controller.getRequest().getMethod().toUpperCase();

                if ("GET".equals(method)) {
                    String para = controller.getPara();
                    if (!Strings.isNullOrEmpty(para)) {
                        actionKey = actionKey + StringPool.SLASH + para;
                    }
                    controller.redirect("/login?redirect="+actionKey);
                } else {
                    controller.redirect("/login");
                }
                return;
            }
        }
        if (no_security_urls.contains(actionKey)) {
            if(islogin){
                controller.redirect(StringPool.SLASH);
                return;
            }
        }
        if (islogin) {
            Member member = SecurityKit.getLoginUser(request);
            controller.setAttr(Const.FIELD_MEMBER, member);
            controller.setAttr("messages", Msgs.dao.findCountByUnreade(member.getId()));
        }
        ai.invoke();
    }
}

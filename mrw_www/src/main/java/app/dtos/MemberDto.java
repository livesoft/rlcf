package app.dtos;

import app.Const;
import app.constant.MemberConstant;
import app.models.member.Member;
import app.services.SerialService;
import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import goja.StringPool;
import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class MemberDto implements Serializable {

    private static final long serialVersionUID = 677289248136318635L;
    public final String phone;

    public final String plant_pwd;

    public final String invite_mobile;

    public final String captcha;

    @JSONCreator
    public MemberDto(@JSONField(name = "phone") String phone,
                     @JSONField(name = "plant_pwd") String plant_pwd,
                     @JSONField(name = "invite_mobile") String invite_mobile,
                     @JSONField(name = "captcha") String captcha) {
        this.phone = phone;
        this.plant_pwd = plant_pwd;
        this.invite_mobile = invite_mobile;
        this.captcha = captcha;
    }


    public Member toModel() {
        final Member member = new Member();
        member.set("phone", this.phone);
        member.set("nick_name", this.phone);
        member.set("gender", 0);
        member.set("avatar", StringPool.EMPTY);
        member.set("invitemobile", this.invite_mobile);
        final DateTime currentDate = DateTime.now();
        member.set(Const.FIELD_CREATE_TIME, currentDate);
        member.set("regiest_time", currentDate);

        member.set(Const.FIELD_CODE, SerialService.memberCode());
        member.set(Const.FIELD_STATUS, MemberConstant.NORMAL);

        member.generatePassword(plant_pwd);

        return member;
    }




}

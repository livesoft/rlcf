package app.dtos;

import app.models.app.ProductDetails;
import app.models.product.Product;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductDto {

    private final Product product;

    private final List<ProductDetails> details;

    private Double amount;

    private int buyers;

    public ProductDto(Product product, List<ProductDetails> details) {
        this.product = product;
        this.details = details;
    }

    public Product getProduct() {
        return product;
    }

    public List<ProductDetails> getDetails() {
        return details;
    }

    public static ProductDto build(Product product, List<ProductDetails> details) {
        return new ProductDto(product, details);
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getBuyers() {
        return buyers;
    }

    public void setBuyers(int buyers) {
        this.buyers = buyers;
    }
}

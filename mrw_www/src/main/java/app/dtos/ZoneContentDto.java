package app.dtos;

import app.Const;
import app.models.app.ZoneContent;

import java.io.Serializable;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class ZoneContentDto implements Serializable {

    private static final long serialVersionUID = -925597802296877335L;

    private final int product;

    private final int    image_height;
    private final int    image_width;
    private final int    row_index;
    private final String image_url;

    public ZoneContentDto(ZoneContent content) {
        this.product = content.getNumber(Const.FIELD_PRODUCT).intValue();
        this.image_height = content.getNumber("image_height").intValue();
        this.image_width = content.getNumber("image_width").intValue();
        this.row_index = content.getNumber("row_index").intValue();
        this.image_url = content.getStr("image_url");
    }

    public int getProduct() {
        return product;
    }

    public int getImage_height() {
        return image_height;
    }

    public int getImage_width() {
        return image_width;
    }

    public int getRow_index() {
        return row_index;
    }

    public String getImage_url() {
        return image_url;
    }
}

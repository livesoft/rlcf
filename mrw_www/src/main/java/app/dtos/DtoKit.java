package app.dtos;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Throwables;
import com.jfinal.core.Controller;
import goja.mvc.AjaxMessage;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletInputStream;
import java.io.IOException;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class DtoKit {

    public static <T> T getDto(Controller controller, Class<T> target) {
        final ServletInputStream inputStream;
        try {
            inputStream = controller.getRequest().getInputStream();
            String json_txt = IOUtils.toString(inputStream);
            return JSON.parseObject(json_txt, target);
        } catch (IOException e) {
            controller.renderJson(AjaxMessage.failure("参数不正确"));
            throw Throwables.propagate(e);
        }
    }
}

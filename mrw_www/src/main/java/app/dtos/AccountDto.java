package app.dtos;

import app.models.member.AccountAmount;

import java.util.Collection;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public final class AccountDto {

    public final int month;

    public final Collection<AccountAmount> amounts;

    public AccountDto(int month, Collection<AccountAmount> amounts) {
        this.month = month;
        this.amounts = amounts;
    }


    public int getMonth() {
        return month;
    }

    public Collection<AccountAmount> getAmounts() {
        return amounts;
    }
}

package app.controllers;

import app.models.basic.HelpContent;
import app.models.basic.HelpNav;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * The url home Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class HelpController extends Controller {

    public void index() {
        List<HelpNav> parent = HelpNav.dao.findParent();

        List<HelpNav> child = HelpNav.dao.findChild();
        setAttr("parent",parent);
        setAttr("child",child);

        List<HelpContent> contents = Lists.newArrayList();

        if(null != parent && parent.size() >0){
            HelpNav parentFirst = parent.get(0);
            if(null != parentFirst){
                List<HelpNav> childList = HelpNav.dao.findChildsById(parentFirst.getStr("id"));
                if(null != childList && childList.size() > 0){
                    contents = HelpContent.dao.findbyNav(childList.get(0).getStr("id"));
                }
            }
        }
        setAttr("contents",contents);
        render("index.ftl");
    }

    /**
     * 根据传递的子标题进行查询
     */
    public void getJsonByChild(){
        String childnav_id = getPara("id","0");
        List<HelpContent> contents = HelpContent.dao.findbyNav(childnav_id);
        renderJson(contents);
    }
}
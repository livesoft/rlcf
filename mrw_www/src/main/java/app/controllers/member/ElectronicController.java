package app.controllers.member;

import app.Const;
import app.models.basic.Notes;
import app.models.member.Account;
import app.models.member.Member;
import bank.BankService;
import bank.resp.BindUserRspDto;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import goja.StringPool;
import goja.Validator;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;
import org.joda.time.DateTime;

import java.sql.SQLException;

/**
 * <p>
 * The url member/electronic Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ElectronicController extends Controller {

    /**
     * The index route.
     * the url /member/electronic
     * the view in index.ftl
     * 绑定电子账号界面
     */
    @Before(GET.class)
    public void index() {
        final String redirect_url = getPara("redirect_url");
        final String error = getPara("error");
        setAttr("error", error);
        setAttr("redirect_url", redirect_url);
        setAttr("fast_notes", Notes.dao.findByFastpayment());

    }


    @Before(POST.class)
    public void bind() {

        String electronicNo = getPara("electronicNo");
        String idCard = getPara("idCard");
        String realName = getPara("realName");

        setAttr("electronicNo",electronicNo);
        setAttr("idCard",idCard);
        setAttr("realName",realName);

        if (Strings.isNullOrEmpty(realName)) {
            setAttr("error", 1);
            render("index.ftl");
            return;
        }


        if (Strings.isNullOrEmpty(idCard)) {
            setAttr("error", 2);
            render("index.ftl");
            return;
        }

        if (Strings.isNullOrEmpty(electronicNo)) {
            setAttr("error", 3);
            render("index.ftl");
            return;
        }

        if (!Validator.isIdentityCard(idCard)) {
            setAttr("error", 2);
            render("index.ftl");
            return;
        }

        final String redirect_url = getPara("redirect_url");
        final Member member = SecurityKit.getLoginUser(getRequest());
        final Optional<BindUserRspDto> bindUserOpt = BankService.me
                .bindUser(DateTime.now(), DateTime.now(),
                        member.getStr(Const.FIELD_CODE), idCard, realName, StringPool.EMPTY, electronicNo);
        if (bindUserOpt.isPresent()) {
            BindUserRspDto bindUserRspDto = bindUserOpt.get();
            if (bindUserRspDto.isSuccess()) {
                member.set("electronic_account", electronicNo);
                member.set("real_name", realName);
                member.set("cart_no", idCard);

                final Account account = Account.dao.findbyMember(member.getId());
                account.set("electronic_account", electronicNo);
                final boolean ok = Db.tx(new IAtom() {
                    @Override
                    public boolean run() throws SQLException {
                        return member.update() && account.update();
                    }
                });
                if (ok) {
                    if(Strings.isNullOrEmpty(redirect_url)){
                        redirect("/member/account/accountSettings");
                    } else {
                        redirect(redirect_url);
                    }
                    return;
                }
            } else {
                setAttr("message", bindUserRspDto.getRespMsg());
            }
        }
        setAttr("error", 4);
        render("index.ftl");


    }
}
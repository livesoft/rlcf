package app.controllers.member;

import app.models.member.Invite;
import app.models.member.Member;
import app.models.member.Points;
import goja.GojaConfig;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;

import java.util.List;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class MyInvitationController extends Controller {
    /*
    跳转我的邀请页面
     */
    public void index(){
        Member member = SecurityKit.getLoginUser(getRequest());
        List<Invite> invites = Invite.dao.findByMember(member.getId());

        Points invite = Points.dao.invite();

        setAttr("points", invite);
        setAttr("domain", GojaConfig.domain());
        setAttr("phone",member.getStr("phone"));
        setAttr("invites",invites);
    }
}

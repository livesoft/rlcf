package app.controllers.member;

import app.Const;
import app.models.member.AccountAmount;
import app.models.member.Member;
import com.jfinal.plugin.activerecord.Page;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;

/**
 * <p>
 * The url home Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class MyAccountController extends Controller {


    public void index() {
        int page = getParaToInt("page", 1);
        int pageSize = getParaToInt("pageSize", 10);
        int timetype = getParaToInt(0, 1);
        int type = getParaToInt(1, 0);
        final  Member member = SecurityKit.getLoginUser(getRequest());
        final Page<AccountAmount> accountAmounts = AccountAmount.dao.findByMemerbTimeType(member.getId(),timetype,page,pageSize,type);
        setAttr("accountAmounts",accountAmounts);
        setAttr("page",page);
        setAttr("timetype",timetype);
        setAttr(Const.FIELD_TYPE,type);
    }
}
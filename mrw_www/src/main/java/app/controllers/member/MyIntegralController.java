package app.controllers.member;

import app.Const;
import app.models.member.IntegralRecord;
import app.models.member.Member;
import com.jfinal.plugin.activerecord.Page;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;

/**
 * <p>
 * The url home Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class MyIntegralController extends Controller {

    public void index() {
        int page = getParaToInt("page", 1);
        int pageSize = getParaToInt("pageSize", 10);
        int timetype = getParaToInt(0, 1);
        int type = getParaToInt(1, 0);
        Member member = SecurityKit.getLoginUser(getRequest());
        Page<IntegralRecord> integralRecords = IntegralRecord.dao.findByMemerbTimeType(member.getId(),timetype,page,pageSize,type);
        setAttr("integralRecord",integralRecords);
        setAttr("page",page);
        setAttr("timetype",timetype);
        setAttr(Const.FIELD_TYPE,type);
    }
}
package app.controllers.member;

import app.Const;
import app.models.member.Boards;
import app.models.member.Member;
import com.jfinal.plugin.activerecord.Page;
import goja.StringPool;
import goja.mvc.Controller;
import goja.plugins.sqlinxml.SqlKit;
import goja.security.goja.SecurityKit;
import org.joda.time.DateTime;

import java.util.UUID;

/**
 * <p>
 * 私人定制
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class BoardsController extends Controller {

    public void index() {
        Member member = SecurityKit.getLoginUser(getRequest());
        int page = getParaToInt("page", 1);
        int pageSize = getParaToInt("pageSize", Const.PAGE);

        Page<Boards> paginate = Boards.dao.paginate(page, pageSize, SqlKit.sql("boards.select"), SqlKit.sql("boards.where"),member.getNumber("id").intValue());
        setAttr("data", paginate);
    }

    public void sub_page(){
        render("item.ftl");
    }
    public void save() {
        Member member = SecurityKit.getLoginUser(getRequest());
        Boards boards = getModel(Boards.class, "boards");
        boards.set("id", UUID.randomUUID().toString().replace("-",""));
        boards.set("member_id",member.getNumber(StringPool.PK_COLUMN).intValue());
        boards.set("create_time", DateTime.now());
        boards.save();
        redirect("/member/boards");
    }
}

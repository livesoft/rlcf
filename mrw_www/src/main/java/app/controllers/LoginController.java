package app.controllers;

import app.Const;
import app.constant.MemberConstant;
import app.models.member.Member;
import app.services.bus.BusContext;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.ext.interceptor.POST;
import goja.mvc.Controller;
import goja.mvc.kit.Servlets;
import goja.mvc.render.CaptchaRender;
import goja.security.goja.SecurityKit;
import org.joda.time.DateTime;

/**
 * <p>
 * The url login Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class LoginController extends Controller {

    /**
     * The index route.
     * the url /login
     * the view in index.ftl
     */
    public void index() {
        setAttr("redirect", getPara("redirect"));
        render("../login.ftl");
    }

    /**
     * 检查是否正确
     */
    public void checkLogin() {
        String phone = getPara("phone");
        if (Strings.isNullOrEmpty(phone)) {
            renderAjaxFailure("请输入用户名或者手机号码");
            return;
        }
        String password = getPara(Member.FIELD_PASSWORD);
        if (Strings.isNullOrEmpty(password)) {
            renderAjaxFailure("请输入登录密码");
        }
        String captcha = getPara("captcha");
        final boolean validate = CaptchaRender.validate(this, captcha);
        if (!validate) {
            renderAjaxFailure("请输入正确的验证码");
            return;
        }
        Member member = Member.dao.findByLogin(phone);
        if (member == null) {
            renderAjaxFailure(phone + "用户不存在");
        } else {

//            final int member_id = member.getNumber(StringPool.PK_COLUMN).intValue();
            boolean login_status;

            if (member.getNumber(Const.FIELD_STATUS).intValue() == MemberConstant.LOCK) {
                renderAjaxFailure("账户已锁定，无法登录");
                return;
            }
            login_status = SecurityKit.checkPassword(member.getStr(Member.FIELD_SALT),
                    member.getStr(Member.FIELD_PASSWORD), password);
            if (login_status) {
                renderAjaxSuccess();
            } else {
//                Integer size = JedisKit.get("failurelogin:" + member_id);
//                if (size == null) {
//                    size = 1;
//                    JedisKit.set("failurelogin:" + member_id, size);
//                } else {
//                    if (size >= 5) {
//                        MemberLock.dao.record(member_id, Servlets.getIp(getRequest()));
//                        member.set(Const.FIELD_STATUS, MemberConstant.LOCK);
//                        member.update();
//                        renderAjaxFailure("输入错误密码次数过多，账户已锁定");
//                        JedisKit.del("failurelogin:" + member_id);
//                    } else {
//                        size = size + 1;
//                        JedisKit.set("failurelogin:" + member_id, size);
//                    }
//                }
                renderAjaxFailure("用户名或者密码错误!");
            }

        }
    }


    @ActionKey("/signin")
    @Before(POST.class)
    public void signin() {
        String phone = getPara("phone");
        String password = getPara(Member.FIELD_PASSWORD);
        Member member = Member.dao.findByLogin(phone);
        if (member.getNumber(Const.FIELD_STATUS).intValue() == MemberConstant.LOCK) {
            redirect("/login?error=2");
            return;
        }
        final boolean login = SecurityKit.login(member, password, false, getRequest(), getResponse());
        if (login) {
            // 积分奖励
            BusContext.postLogin(member.getId(), member.getStr("phone"), DateTime.now(), Servlets.getIp(getRequest()), true);
            String redirect_url = MoreObjects.firstNonNull(getPara("redirect"), "/");
            redirect(redirect_url);
        } else {
            redirect("/login?error=1");
        }
    }

}
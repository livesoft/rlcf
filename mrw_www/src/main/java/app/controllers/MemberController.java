package app.controllers;

import app.Const;
import app.models.basic.Dict;
import app.models.member.Member;
import app.models.member.ProfitDetails;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.plugin.activerecord.Page;
import goja.StringPool;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by user on 2015/3/19.
 */
public class MemberController extends Controller {

    public void login() {
        render("login.ftl");
    }



    @Before(GET.class)
    public void signout() {
        if (SecurityKit.isLogin(getRequest())) {
            SecurityKit.logout(getRequest(), getResponse());
        }
        redirect("/");
    }


    public void center() {
        Member member = SecurityKit.getLoginUser(getRequest());
        int memberId = member.getId();
        Member show_center = Member.dao.findByIdWithShowCenter(memberId);
        DateTime now = DateTime.now();
        Page<ProfitDetails> productMoneys = ProfitDetails.dao.findByMember(memberId + "", 1, 4, 1);

        float[] weekMoney = new float[5];
        DateTime start_date = now.plusDays(-7).millisOfDay().withMinimumValue();
        DateTime end_time = now.millisOfDay().withMaximumValue();
        BigDecimal serverBigdecimal = new BigDecimal(7);
        weekMoney[0]=ProfitDetails.dao.findAmountByMembeAndDate(memberId,start_date,end_time).divide(serverBigdecimal,2).floatValue();
        start_date = now.plusDays(-14).millisOfDay().withMinimumValue();
        end_time = now.plusDays(-8).millisOfDay().withMaximumValue();
        weekMoney[1]=ProfitDetails.dao.findAmountByMembeAndDate(memberId,start_date,end_time).divide(serverBigdecimal,2).floatValue();
        start_date = now.plusDays(-21).millisOfDay().withMinimumValue();
        end_time = now.plusDays(-15).millisOfDay().withMaximumValue();
        weekMoney[2]=ProfitDetails.dao.findAmountByMembeAndDate(memberId,start_date,end_time).divide(serverBigdecimal,2).floatValue();
        start_date = now.plusDays(-28).millisOfDay().withMinimumValue();
        end_time = now.plusDays(-22).millisOfDay().withMaximumValue();
        weekMoney[3]=ProfitDetails.dao.findAmountByMembeAndDate(memberId,start_date,end_time).divide(serverBigdecimal,2).floatValue();
        start_date = now.plusDays(-35).millisOfDay().withMinimumValue();
        end_time = now.plusDays(-29).millisOfDay().withMaximumValue();
        weekMoney[4]=ProfitDetails.dao.findAmountByMembeAndDate(memberId,start_date,end_time).divide(serverBigdecimal,2).floatValue();
        setAttr("weekMoney",weekMoney);
        setAttr("productmoneys", productMoneys.getList());
        List<Dict> dicts = Dict.dao.risk_tolerance();
        setAttr("dicts", dicts);
        setAttr("show_center", show_center);
        setAttr("curent", 5);
        render("center/center.ftl");
    }

    public void moreProductMoney() {
        Member member = SecurityKit.getLoginUser(getRequest());
        int type = getParaToInt(0, 2);
        int page = getParaToInt("page", 1);
        int pageSize = getParaToInt("pageSize", 10);
        float money = ProfitDetails.dao.findAmountByMember(member.getNumber(StringPool.PK_COLUMN).intValue() + "").floatValue();
        Page<ProfitDetails> productMoneys = ProfitDetails.dao.findByMember(member.getNumber(StringPool.PK_COLUMN).intValue() + "", page, pageSize,type);
        setAttr("page",page);
        setAttr(Const.FIELD_TYPE,type);
        setAttr("money",money);
        setAttr("productmoneys", productMoneys);
        render("center/more.ftl");
    }

}

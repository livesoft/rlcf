package app.controllers;

import goja.mvc.Controller;

/**
 * <p>
 * The url home Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ComingController extends Controller {

    /**
     * The index route.
     * the url /home
     * the view in index.ftl
     敬请期待
     */
    public void index() {
    }
}
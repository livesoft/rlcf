package app.controllers;

import app.Const;
import app.models.basic.Dict;
import app.models.basic.Hotnews;
import app.models.pc.AboutUs;
import com.jfinal.plugin.activerecord.Page;
import goja.GojaConfig;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * 关于我们
 * </p>
 *
 * @author liuhui
 * @version 1.0
 * @since JDK 1.6
 */
public class AboutusController extends Controller{

    public void index(){

        List<Dict> aboutus = Dict.dao.aboutus();
        setAttr("menu",aboutus);
        //默认展示公司简介
        AboutUs about = AboutUs.dao.findByType("1");
        //选中状态
        setAttr("curent",3);
        setAttr("cur",1);
        setAttr("about",about);
    }


    public void show(){
        //区分理财咨询
        String type = getPara(0, "0");
        List<Dict> aboutus = Dict.dao.aboutus();
        AboutUs about = AboutUs.dao.findByType(type);
        setAttr(Const.FIELD_TYPE,type);
        setAttr("menu",aboutus);
        setAttr("about",about);
        //选中状态
        setAttr("curent",3);
        setAttr("cur",type);
        //理财咨询
        if(type.equals("3")){
            redirect("/aboutus/news");
        }else{
            render("index.ftl");
        }
    }

    /**
     * 理财咨询列表
     */
    public void news(){
        List<Dict> aboutus = Dict.dao.aboutus();

        int page = getParaToInt("page", 1);
        int pageSize = getParaToInt("pageSize", 12);

        setAttr("curent",3);
        setAttr("cur",3);

        Page<Hotnews> pageList = Hotnews.dao.findByPageList(page, pageSize);
        setAttr("img_domain",GojaConfig.getProperty("img.domain"));

        setAttr("data",pageList);
        setAttr("menu", aboutus);
        render("list.ftl");
    }

    /**
     * 理财咨询详细
     */
    public void showNews(){
        String id = getPara(0, "0");
        Hotnews news = Hotnews.dao.findById(id);
        setAttr("img_domain",GojaConfig.getProperty("img.domain"));

        setAttr("curent",3);
        setAttr("cur",3);

        List<Dict> aboutus = Dict.dao.aboutus();
        setAttr("menu",aboutus);
        setAttr("news",news);
        render("news.ftl");

    }


}

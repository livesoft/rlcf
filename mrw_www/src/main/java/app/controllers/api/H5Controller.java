package app.controllers.api;

import app.Const;
import app.models.basic.Hotnews;
import app.models.basic.Msgs;
import app.models.basic.Notice;
import goja.mvc.Controller;
import org.joda.time.DateTime;

/**
 * <p>
 * The url h5 Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class H5Controller extends Controller {

    /**
     * The index route.
     * the url /h5
     */
    public void index() {}


    public void notice() {
        final int id = getParaToInt();

        Notice notice = Notice.dao.findById(id);
        setAttr("notice", notice);
    }

    public void message(){
        Msgs msgs = Msgs.dao.findById(getPara());
        setAttr("msg",msgs);
        if(!msgs.valBoolean(Const.FIELD_STATUS)){
            msgs.set("read_time", DateTime.now());
            msgs.update();
        }
    }


    public void news() {
        Hotnews hotnews = Hotnews.dao.findById(getPara());
        setAttr("news", hotnews);
    }
}
package app.controllers.api.product;

import app.models.product.Group;
import app.models.product.Product;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * The url product/group Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class GroupController extends Controller {

    /**
     * The index route.
     * the url /product/group
     */
    public void index() {
        List<Group> groups = Group.dao.findWithList();
        renderAjaxSuccess(groups);
    }

    /**
     * 产品接口，获取某个分组的产品
     */
    @Before(GET.class)
    public void products() {
        int group = getParaToInt(0, 0);
        if (group > 0) {
            // 页码
            int page = getParaToInt(1, 1);
            // 排序方式 1表示默认按照产品发布时间排序；2表示按照期限排序；3表示按照金额排序
            int order_type = getParaToInt(2, 1);
            if (order_type < 1 || order_type > 3) {
                renderAjaxError("未知的排序方式");
            }
            final List<Product> products = Product.dao.findByGroup(group, page, order_type);
            renderAjaxSuccess(products);
        } else {
            renderAjaxFailure("未知分组信息");
        }
    }
}
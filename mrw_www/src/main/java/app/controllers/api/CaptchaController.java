package app.controllers.api;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;

/**
 * <p>
 * The url captcha Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class CaptchaController extends Controller {

    /**
     * The index route.
     * the url /captcha
     */
    @Before(GET.class)
    public void index() {

        renderCaptcha(4);

    }
}
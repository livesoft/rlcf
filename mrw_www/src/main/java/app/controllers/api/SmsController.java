package app.controllers.api;

import app.kit.SmsKit;
import app.kit.VerifyKit;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * The url sms Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class SmsController extends Controller {

    /**
     * The index route.
     * the url /sms
     * the view in index.ftl
     */
    public void index() {
        renderNull();
    }

    /**
     * 注册短信验证码发送
     */
    @Before(GET.class)
    public void register() {
        String phone = getPara();
        if (Strings.isNullOrEmpty(phone)) {
            renderAjaxError("请输入手机号码");
        } else {

            String mobile = StringUtils.replace(phone, "-", StringUtils.EMPTY);
            if (VerifyKit.checkMobile(mobile)) {
                SmsKit.registered(phone);
                renderAjaxSuccess();
            } else {
                renderAjaxError("请输入正确的手机号码");
            }
        }
    }
}
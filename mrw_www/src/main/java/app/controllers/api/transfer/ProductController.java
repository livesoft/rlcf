package app.controllers.api.transfer;

import app.Const;
import app.dtos.ProductDto;
import app.models.app.ProductDetails;
import app.models.product.Product;
import app.models.transfer.TransferProduct;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.lang.Lang;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * The url transfer/product Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductController extends Controller {

    /**
     * 转让市场产品接口
     * the url /transfer/product
     */
    @Before(GET.class)
    public void index() {
        int page = getParaToInt("page");
        List<TransferProduct> transferProducts = TransferProduct.dao.findByMarket(page);
        renderAjaxSuccess(transferProducts);
    }

    /**
     * 转让产品详情
     */
    @Before(GET.class)
    public void show() {
        int id = getParaToInt();
        TransferProduct transferProduct = TransferProduct.dao.findById(id);
        int product_id = transferProduct.getNumber(Const.FIELD_PRODUCT).intValue();
        final Product product = Product.dao.findByAllInfo(product_id);
        if (Lang.isEmpty(product)) {
            renderAjaxFailure("产品数据不存在，无法查找");
            return;
        }
        List<ProductDetails> details = ProductDetails.dao.findbyProduct(product_id);

        final ProductDto dto = ProductDto.build(product, details);
        dto.setAmount(transferProduct.getDouble(Const.FIELD_PRICE));
        renderAjaxSuccess(dto);
    }



}
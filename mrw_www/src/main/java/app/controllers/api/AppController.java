package app.controllers.api;

import app.Const;
import app.models.app.AppMobile;
import app.models.app.ApphomeStat;
import app.models.app.ProductRecommend;
import app.models.app.TopShuffling;
import app.models.app.ZoneContent;
import app.models.basic.Dict;
import app.models.basic.Hotnews;
import app.models.basic.Notice;
import app.models.product.Group;
import app.services.auth.AuthInterceptor;
import com.google.common.base.Strings;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.plugin.ehcache.CacheKit;
import goja.StringPool;
import goja.lang.Lang;
import goja.mvc.AjaxMessage;
import goja.mvc.Controller;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * The url app Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class AppController extends Controller {

    /**
     * 手机首页区域展示
     */
    @Before(GET.class)
    public void index() {

        List<Group> groups = CacheKit.get("app", "group.list");
        if(Lang.isEmpty(groups)){
            groups = Group.dao.findByIndexShow();
            for (Group group : groups) {
                List<ZoneContent> zoneContents = ZoneContent.dao.findByGroup(group.getNumber(StringPool.PK_COLUMN).intValue());
                group.put("contents", zoneContents);

            }
            CacheKit.put("app", "group.list", groups);
        }


        renderAjaxSuccess(groups);
    }

    /**
     * 手机默认绑定接口
     */
    @Before({POST.class})
    public void bind() {
        String uuid = getAttrForStr(AuthInterceptor.ATTR_UUID);
        AppMobile mobile = AppMobile.dao.findByUUID(uuid);
        if (mobile == null) {
            mobile = new AppMobile();
            mobile.set("uuid", uuid);

            Date now = DateTime.now().toDate();
            mobile.set("first_date", now);
            mobile.set("first_time", now);
            mobile.set(Const.FIELD_CREATE_TIME, now);
            if(mobile.save()){
                renderAjaxSuccess();
                return;
            } else {
                renderAjaxFailure("绑定失败，下次重启重新注册!");
            }
        }
        renderAjaxFailure("绑定失败，稍后重试!");
    }

    /**
     * 手机TOP展示
     */
    @Before(GET.class)
    public void top() {
        int section = getParaToInt(0, 0);
        if (section > 0) {
            List<TopShuffling> shufflings = TopShuffling.dao.findBySection(section);
            if (Lang.isEmpty(shufflings)) {
                renderJson(AjaxMessage.nodata());
            } else {
                renderAjaxSuccess(shufflings);
            }
        } else {
            renderAjaxError("非法栏目标记");
        }
    }


    /**
     * 产品推荐接口
     */
    @Before(GET.class)
    public void recommand() {
        List<ProductRecommend> productRecommends = ProductRecommend.dao.findAll();
        renderAjaxSuccess(productRecommends);
    }

    /**
     * 手机首页统计信息展示
     */
    @Before(GET.class)
    public void homestat() {
        List<ApphomeStat> apphomeStats = ApphomeStat.dao.findWithList();
        if (Lang.isEmpty(apphomeStats)) {
            renderJson(AjaxMessage.nodata());
        } else {
            renderAjaxSuccess(apphomeStats);
        }
    }


    /**
     * 系统字典
     */
    @Before(GET.class)
    public void dict() {
        String category = getPara();

        if (Strings.isNullOrEmpty(category)) {
            renderAjaxSuccess();
            return;
        }
        List<Dict> dicts = Dict.dao.findByCategory(category);

        renderAjaxSuccess(dicts);

    }


    /**
     * 公告新闻接口
     */
    public void notice() {
        List<Notice> notices = Notice.dao.findByEnable();
        renderAjaxSuccess(notices);
    }


    public void news() {
        List<Hotnews> hotnewses = Hotnews.dao.findByEnable();

        renderAjaxSuccess(hotnewses);
    }


}
package app.controllers.api.member;

import app.constant.MemberConstant;
import app.models.member.MemberProduct;
import app.services.auth.AuthInterceptor;
import app.services.auth.AuthParam;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.GET;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * The url member/product Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class ProductController extends Controller {

    /**
     * The index route.
     * the url /member/product
     */
    @Before({GET.class, AuthInterceptor.class})
    @AuthParam(auth = true)
    public void index() {

        int memerId = getAttrForInt(AuthInterceptor.ATTR_MEMBER_PK);
        /**
         * 类型
         */
        int type = getParaToInt(0,1);
        // 页码
        int page = getParaToInt(1, 1);

        List<MemberProduct> products;
        switch (type) {
            case MemberConstant.PRODUCT_HOLDING:
//            case MemberConstant.PRODUCT_END:
                products = MemberProduct.dao.findByTypeWithPage(memerId, type, page);
                renderAjaxSuccess(products);
                break;
            default:
                renderAjaxError("未知的类型");
        }

    }
}
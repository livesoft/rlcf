package app.controllers.api;

import app.models.iface.Bank;
import goja.mvc.Controller;

import java.util.List;

/**
 * <p>
 * The url bank Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class BankController extends Controller {

    /**
     * The index route.
     * the url /bank
     * the view in index.ftl
     */
    public void index() {
        List<Bank> banks = Bank.dao.findAll();
        renderAjaxSuccess(banks);
    }
}
package app.controllers;

import app.Const;
import app.kit.TypeKit;
import app.models.basic.Hotnews;
import app.models.member.Member;
import app.models.order.Order;
import app.models.order.TradeMoney;
import app.models.pc.IndexPic;
import app.models.pc.StoryShare;
import app.models.product.Brand;
import app.models.product.Product;
import app.models.transfer.TransferProduct;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.jfinal.core.ActionKey;
import goja.GojaConfig;
import goja.date.DateFormatter;
import goja.lang.Lang;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;
import org.joda.time.DateTime;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * The url home Controller.
 * </p>
 *
 * @author sagyf yang
 * @version 1.0
 * @since JDK 1.6
 */
public class HomeController extends Controller {

    /**
     * The index route.
     * the url /home
     * the view in index.ftl
     * 首页显示：bar轮播图片3张，理财分享4条，理财产品9条
     */
    @ActionKey("/")
    public void index() {
        final HttpServletRequest request = getRequest();
        final boolean isLogin = SecurityKit.isLogin(request);
        if(isLogin){
            Member member = SecurityKit.getLoginUser(request);
            final int risk_tolerance = TypeKit.getInt(member, Const.FIELD_RISK_TOLERANCE);
            setAttr(Const.FIELD_RISK_TOLERANCE, risk_tolerance);
        } else {
            setAttr(Const.FIELD_RISK_TOLERANCE, 0);
        }

        final List<IndexPic> bars = IndexPic.dao.getAll();
        final List<StoryShare> shares = StoryShare.dao.getAll();
        final List<Product> products = Product.dao.findForPcHome();
        final List<Hotnews> hotnewses = Hotnews.dao.findByHome();
        final List<Brand> brands = Brand.dao.findBySort();

        setAttr("transfers", TransferProduct.dao.homeDisplay());

        DateTime now = DateTime.now();
        DateTime start = now.plusDays(-15).millisOfDay().withMinimumValue();
        final Optional<ImmutableListMultimap<String, Order>> orders = Order.dao.statByHome(start, now);
        List<Date> statDay = Lists.newArrayListWithCapacity(15);
        List<BigDecimal> statAmount = Lists.newArrayListWithCapacity(15);
        if (orders.isPresent()) {
            ImmutableListMultimap<String, Order> orderstat = orders.get();
            for (int i = 0; i <= 15; i++) {
                DateTime time = start.plusDays(i);
                statDay.add(time.toDate());
                ImmutableList<Order> ccs = orderstat.get(time.toString(DateFormatter.DATE_TIME_PATTERN_YYYY_MM_DD));
                if(Lang.isEmpty(ccs)){
                    statAmount.add(BigDecimal.ZERO);
                }else {
                    statAmount.add(TypeKit.getBigdecimal(ccs.get(0), Const.FIELD_AMOUNT));
                }
            }
        } else {
            for (int i = 0; i <= 15; i++) {
                DateTime time = start.plusDays(i);
                statDay.add(time.toDate());
                statAmount.add(BigDecimal.ZERO);
            }
        }

        setAttr("statDays", statDay);
        setAttr("statAmounts", statAmount);
        setAttr("brands", brands);
        setAttr("hotnews", hotnewses);
        setAttr(Const.FIELD_AMOUNT, Order.dao.sumByOrderType());
        setAttr("stat_member", Member.dao.findAvailableCount());
        setAttr("porfit", TradeMoney.dao.sumProfit());
        setAttr("img", GojaConfig.getProperty("img.domain"));
        setAttr("products", products);
        setAttr("bars", bars);
        setAttr("shares", shares);
        setAttr("curent", 1);

        render("index.ftl");
    }
}

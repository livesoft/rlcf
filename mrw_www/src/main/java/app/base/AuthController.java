package app.base;

import app.models.member.Member;
import app.services.auth.AuthInterceptor;
import goja.StringPool;
import goja.mvc.Controller;
import goja.security.goja.SecurityKit;

/**
 * <p> </p>
 *
 * @author sogYF
 * @version 1.0
 * @since JDK 1.6
 */
public class AuthController extends Controller {


    protected int getMemberId() {
        if(isPc()){
            return SecurityKit.getLoginUser(getRequest()).getNumber(StringPool.PK_COLUMN).intValue();
        }

        return getAttrForInt(AuthInterceptor.ATTR_MEMBER_PK);
    }

    protected Member getMember() {

        if(isPc()){
            return SecurityKit.getLoginUser(getRequest());
        } else {
            return getAttr(AuthInterceptor.ATTR_MEMBER);
        }
    }


    protected boolean isLogin(){
        if(isPc()){
            return SecurityKit.isLogin(getRequest());
        } else {
            return !( getMember() == null);
        }
    }



    protected boolean isPc(){
        return getAttr("pc");
    }

    protected String getAuthCode(){
        return getAttrForStr(AuthInterceptor.ATTR_AUTH);
    }
}

#/bin/sh
auth_code=2WcUYC/M5YYebErIk77Bgldr5jQF3R4/J33eA/Oooql1zWBX+OXmzA==

# http://121.41.92.107:8080/


http http://127.0.0.1:8080/api/captcha

# 首页信息展示接口
http http://127.0.0.1:8080/api/app

http POST http://127.0.0.1:8080/api/app/bind \
    uuid==1234566

http http://127.0.0.1:8080/api/app/dict/risk_level

http http://127.0.0.1:8080/api/app/homestat

http http://127.0.0.1:8080/api/app/news

http http://127.0.0.1:8080/api/app/notice

http http://127.0.0.1:8080/api/app/recommand


http http://127.0.0.1:8080/api/app/risk

# 银行信息
http http://127.0.0.1:8080/api/bank


# 短信验证码
http http://127.0.0.1:8080/www/api/sms/register/18956020981

# 注册接口
http POST http://127.0.0.1:8080/www/api/member/register \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    plant_pwd==123456 captcha==7463 phone==18956020981 invite_mobile==18655121378


# 登录请求
http POST http://127.0.0.1:8080/www/api/member/signin \
    'Cookie:_CAPTCHA_MD5_CODE_=CF66AE0FCBD861870D2FABC18263E31F' \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    phone==18661209663 password==123123 captcha==CV9J

# 退出接口测试
http POST http://127.0.0.1:8080/www/api/member/signout \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w==


# 绑定电子账户
http POST http://127.0.0.1:8080/www/api/member/account/electronicbind \
    rongli_authentication:E2HdGHxpgJVKATXjZajUwQFfxQkKI5Z8u/M964UDlz7YwdvHusD28w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
        realname==濮真 id_card==632128196602254904 electronic==6228765001000125397

# 开通电子账户
http POST http://127.0.0.1:8080/api/member/account/open \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    username==杨友峰 id_card==340827198411230010 phone==18655121378 email==poplar1123@gmail.com


# 绑定银行卡
http POST http://127.0.0.1:8080/api/member/account/bindcard \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    card_no==5641100123451234 name==杨友峰 bank==银行 id_card==340827198411230010 phone==18655121378

# 我的银行卡
http http://127.0.0.1:8080/api/member/account/cards \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5

# 我的消息
http GET http://127.0.0.1:8080/api/member/message \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w==

# 修改密码
http POST http://127.0.0.1:8080/api/member/password \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    rongli_authentication:ZoBv9nUAEYQqjKfB1FOBo21CCQ4pYbJGJEXOTJG40Ag9JH9I5JF1EA== \
    original_pwd==123456 password==123123

# 产品分类展示接口
http GET http://127.0.0.1:8080/api/product/group

http GET http://127.0.0.1:8080/api/product/group/products/1

http GET http://127.0.0.1:8080/api/product/14

# 购买产品
http POST http://127.0.0.1:8080/api/product/purchase \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    product==14

# 确定购买
http POST http://127.0.0.1:8080/api/product/confirm/3 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    product==14 amount==12002.15 bankcard==1

# 我的理财产品
http http://127.0.0.1:8080/api/member/product/1-1 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5


# 转让市场
http http://127.0.0.1:8080/api/transfer/product \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    page=1



http POST http://127.0.0.1:8080/api/transfer/1 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5


http POST http://127.0.0.1:8080/api/transfer/confirm/1 \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5 \
    price==12.4

# 会员信息
http http://127.0.0.1:8080/api/member \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w==

# 风险承受能力评估问卷获取接口
http http://127.0.0.1:8080/api/app/risk/risk_tolerance \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w==

http POST http://127.0.0.1:8080/api/app/risk \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    rongli_authentication:2WcUYC/M5YYxah64iunSTQ0e8uDORR1JApRvjIYlB7n6FzNxPTCzDA== \
    category==risk_tolerance data==2,3

http POST http://127.0.0.1:8080/api/member/account/password \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    password==1232320

http POST http://127.0.0.1:8080/api/member/account/checkpassword \
    rongli_uuid:1efb55db0b545766ed940db8c32a65b37cc06ae5  \
    rongli_authentication:1NyD96KQ18gq2gSN3eeoCRONeIhZGHijB93pxWS6gEiGM6et+FoL9w== \
    password==1232320


http POST http://127.0.0.1:8080/www/pay/tip \
    orderStatus==2 totalAmount==22.0 currency==CNY fee==0 \
    outerOrderNo==RLCL_20150330121800001 innerOrderNo==20150323104900000067065 partnerBuyerId==RLCF20150323000001N \
    orderType==4 respBizDate==20150323 respCode==000000 respMsg==交易成功 partnerId==8dbc14c4b1314ca492391e0d94fff0e4 field1== field2== field3==


